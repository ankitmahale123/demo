<?php
	session_start();
	require_once("../config/conn.php"); 
	
	if($_REQUEST['status']=='events')
	{
	?>
<script>$('.btn-admin').eq(1).trigger('click');$('#submenuFill').css('visibility','visible');</script>
<?php
	}
	else if($_REQUEST['status']=='registration')
	{
	?>
<script>$('#submenuFill').css('visibility','visible');</script>

<div align="center" style="width:100%; font-size:25px; color:#727272;font-family: lator; margin-top:180px;">Registration content goes here.</div>
<?php
	}
	else if($_REQUEST['status']=='promotion')
	{
	?>
	<script>
		$('#submenuFill').css('visibility','visible');
		$('div[default="true"]').trigger('click');$( '#editor1' ).ckeditor();
		$(".custom-select").each(function(){
                $(this).wrap("<span class='select-wrapper'></span>");
                $(this).after("<span class='holder'></span>");
            });
            $(".custom-select").change(function(){
                var selectedOption = $(this).find(":selected").text();
                $(this).next(".holder").text(selectedOption);
            }).trigger('change');
			
	/* for csv */
	var uploadButton;
	$(function () {
		'use strict';
		// Change this to the location of your server-side upload handler:
		var url = '../fileupload/server/php/';
		$('#csv_file').each(function () {
			$(this).on('click', function () {
				uploadButton = $(this).parent().parent().attr('id');
				//alert(uploadButton);
			});
		$(this).fileupload({
			url: url,
			dataType: 'json',
			maxFileSize: 5000000, // 5 MB
			// Enable image resizing, except for Android and Opera,
			// which actually support image resizing, but fail to
			// send Blob objects via XHR requests:
			disableImageResize: /Android(?!.*Chrome)|Opera/
				.test(window.navigator.userAgent),
			previewCrop: true
		}).on('fileuploadadd', function (e, data) {
			$(".popup-loader").fadeIn();
		}).on('fileuploadprogressall', function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			//$('#progress .progress-bar').css('width',progress + '%');
		}).on('fileuploaddone', function (e, data) {
			$.each(data.result.files, function (index, file) {
			$(".popup-loader").fadeOut();
			$('#csv_done').css('display','inline-block');
			$('#csv_hidden').val(file.url);
			});
		}).on('fileuploadfail', function (e, data) {
			$.each(data.files, function (index) {
				var error = $('<span class="text-danger"/>').text('File upload failed.');
				console.log(error);
			});
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
	});
    </script>
	<style>
        input
        {
            padding: 8px;
            border: 1px solid #ccc;
            border-radius: 3px;
            margin-bottom: 10px;
            font-family: lator;
            color: #2C3E50;
            font-size: 13px;
        }
        tr td
        {
            font-family:lator;
            color:#727272;
            font-size:15px;
        }
        .select-wrapper
        {
            border:1px solid #ccc;
        }
    </style>
  	<table style="width:885px; height:auto; margin-top:10px;">
    	<tr style="height:40px;">
        	<td style="width:100px; padding-left:20px;">To</td>
            <td style="width:785px;"><input type="hidden" id="csv_hidden" value="" /><input type="button" style="-webkit-appearance: none;-moz-appearance: none;-ms-appearance: none;    -o-appearance: none; appearance: none;width:200px; padding:0px; height:35px; color:#fff; font-size:16px; background-color:#ed258f; border:1px solid #ed258f;font-family: lator; position:absolute; cursor:pointer;" value="Upload file" /><input type="file" id="csv_file" style="width:184px; border:none; opacity:0; cursor:pointer;"/><span id="csv_done" style="margin-left:10px; display:none">File Uploaded</span></td>
        </tr>
        <tr>
        	<td></td>
            <td style="font-size:14px;">Please upload only .csv file for bulk email list. <span style="cursor:pointer; text-decoration:underline;" onclick="window.open('../assets/csv/sample.csv','_self');">Click here to see sample file.</span> Kindly upload it in the same format.</td>
        </tr>
        <tr style="height:65px;">
        	<td style="width:100px; padding-left:20px;">Subject</td>
            <td style="width:785px;"><input type="text" style="width:500px;" id="email-subject" /></td>
        </tr>
        <tr>
        	<td colspan="2"><textarea class="ckeditor" id="editor1" style="height:400px; width:80%;margin-top:20px;resize:none;"></textarea></td>
        </tr>
		<tr style="width:885px;">
            <td style="width:885px; padding-top:20px;" align="center" colspan="2"><input type="button" value="SEND" onclick="sendEmailEventPromotion()" style="width:100px;border:none; color:#fff; background:#727272; margin-left:100px; cursor:pointer;" /><input type="button" value="CANCEL" style="width:100px;border:none; color:#fff; background:#727272; margin-left:50px; cursor:pointer;" onclick="window.open('promoterDashboard.php','_self');"/></td>
        </tr>
    </table>
<?php
	}
	else if($_REQUEST['status']=='communication')
	{
	?>
	<style>
    .email-form-td
    {
        display:table-cell;
        padding:10px;
        text-align: right;
        vertical-align:middle;color:#727272;font-family: lator;width:200px; font-size:16px;
    }
    input
    {
        padding: 8px;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-bottom: 10px;
        font-family: lator;
        color: #2C3E50;
        font-size: 13px;
    }
    tr td
    {
        font-family:lator;
        color:#727272;
        font-size:15px;
    }
    .select-wrapper
    {
        border:1px solid #ccc;
    }
    </style>
    <script>
		//$('#submenuFill').css('display','none');
		$('div[default="true"]').trigger('click');$( '#editor1' ).ckeditor();
		$(".custom-select").each(function(){
            $(this).wrap("<span class='select-wrapper'></span>");
            $(this).after("<span class='holder'></span>");
        });
        $(".custom-select").change(function(){
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        }).trigger('change');
	</script>
<div align="center" style="width:100%; font-size:25px; color:#727272;font-family: lator; margin:10px 0px;">
  <div id="news-updates" align="center" class="comm-divs" style="text-align:center; display:none;">
    <div>Updates</div>
    <div>
      <textarea style="height:400px; width:80%;margin-top:20px;resize:none; padding:10px;" id="txt-update"></textarea>
    </div>
    <div class="btn event" style="border-radius:3px; margin:10px 75px; font-size:15px; border:1px solid #ed258f;"  onclick="addNewsUpadtes()">Add</div>
  </div>
  <div id="email-att" class="comm-divs" style="display:block;">
  	<table style="width:885px; height:auto;">
    	<tr style="height:40px;">
        	<td style="width:100px; padding-left:20px;">To</td>
            <td style="width:785px;">
            	<select class="custom-select">
                    <option>&nbsp;All attendees</option>
                    <!--<option>&nbsp;Attendees by ticket type</option>
                    <option>&nbsp;Tickets purchased after certain date</option>-->
                  </select>
                  <span id="eventName" style="padding-left:15px; padding-top:6px; position:absolute;"></span>
            </td>
        </tr>
        <tr style="height:65px;">
        	<td style="width:100px; padding-left:20px;">Subject</td>
            <td style="width:785px;"><input type="text" style="width:500px;" id="email-subject" /></td>
        </tr>
        <tr>
        	<td colspan="2"><textarea class="ckeditor" id="editor1" style="height:400px; width:80%;margin-top:20px;resize:none;"></textarea></td>
        </tr>
<!--        <tr>
            <td style="width:640px; padding-top:10px" colspan="2"><span style="padding-left:20px; padding-right:20px;">Send Test Message To</span><input type="text" id="test-email-id" style="width:500px;" /><input type="button" value="SEND" onclick="sendEmail('test')" style="width:100px; margin-left:15px; border:none; color:#fff; background:#727272; cursor:pointer;" /></td>
        </tr>
-->        
		<input type="hidden" attrEventId="<?php echo $_REQUEST['eventId']; ?>" id="hiddenEventId" />
		<tr style="width:885px;">
            <td style="width:885px; padding-top:20px;" align="center" colspan="2"><input type="button" value="SEND" onclick="sendEmailEventAttendeeUser()" style="width:100px;border:none; color:#fff; background:#727272; margin-left:100px; cursor:pointer;" /><input type="button" value="CANCEL" style="width:100px;border:none; color:#fff; background:#727272; margin-left:50px; cursor:pointer;" onclick="window.open('promoterDashboard.php','_self');" /></td>
        </tr>
    </table>
  </div>
</div>
<?php
	}
	else if($_REQUEST['status']=='attendee')
	{
	?>
<script>$('#submenuFill').css('visibility','visible');</script>
<div align="center" style="width:100%; font-size:25px; color:#727272;font-family: lator; margin-top:180px;">Attendee content goes here.</div>
<?php
	}
	else if($_REQUEST['status']=='dashboard')
	{
	?>
	<script>
	$('#submenuFill').css('visibility','visible');
	$('.dashboard_sub').css('display','none');
    /*function open_attendees(eventNameAttendee,eventId)
	{
		fetchMenuPromoter(this,'communication','email-at',eventNameAttendee,eventId);
	}*/
    </script>
    <style>
	.dash_table {
    border-collapse: collapse;
    margin-bottom: 3em;
    width: 100%;
    background: #fff;
	}
	.dash_td, .dash_th {
		padding: 0.75em 1.5em;
		text-align: left;
	}
	.dash_td.err {
		background-color: #e992b9;
		color: #fff;
		font-size: 0.75em;
		text-align: center;
		line-height: 1;
	}
	.dash_th {
		background-color:#676767;
		font-weight: bold;
		color: #fff;
		white-space: nowrap;
	}
	tbody .dash_th {
		background-color: #676767;
	}
	tbody tr:nth-child(2n-1) {
		background-color: #f5f5f5;
		transition: all .125s ease-in-out;
	}
	tbody tr:hover {
		background-color: rgba(131,176,172,.3);
	}
	</style>
    <div align="center" style="width:95%; font-size:15px; color:#727272;font-family: lator; margin-left:auto; margin-right:auto;">
    	<?php
		$date=date('Y-m-d');
		$queryEvent=mysqli_query($mysqli,"select event.id as eventId,DATE_FORMAT(event.ticketFrom,'%M %d, %Y %H:%i %p') as ticketFrom,DATE_FORMAT(event.ticketTo,'%M %d, %Y %H:%i %p') as ticketTo,event.name as eventName,organization.name as organizationName from event inner join organization on organization.id=event.organizationId where event.status='approve' AND event.userId='".$_SESSION['promoterId']."' AND event.date>='".$date."'");
		if(mysqli_num_rows($queryEvent)>0){
		?>
    	<table align="center" class="dash_table">
					<thead>
						<tr>
							<th class="dash_th">Event Name</th>
							<th class="dash_th">Status</th>
							<th class="dash_th">Start Sale</th>
							<th class="dash_th">End Sale</th>
                            <th class="dash_th"></th>
						</tr>
					</thead>
					<tbody>
                    	<?php while($result=mysqli_fetch_assoc($queryEvent)){ ?>
						<tr>
                            <td class="dash_td"><?php echo $result['eventName']; ?></td>
                            <td class="dash_td">On Sale</td>
                            <td class="dash_td"><?php echo $result['ticketFrom']; ?></td>
                            <td class="dash_td"><?php echo $result['ticketTo']; ?></td>
                            <!--<td class="dash_td" onclick="view_sales('<?php /*?><?php echo $result['id']; ?><?php */?>')" style="cursor:pointer;">View Sales</td>-->
                            <!--<td class="dash_td" onclick="open_attendees('<?php /*?><?php echo $result['eventName']; ?><?php */?>')" style="cursor:pointer;">View Attendees</td>-->
                            <td class="dash_td" onclick="view_attendee('<?php echo $result['eventId']; ?>')" style="cursor:pointer;">View Attendees</td>
                        </tr>
                        <?php } ?>
					</tbody>
				</table>
                <?php } else { ?>
                <div style="margin-top:150px;font-size:25px; color:#727272;font-family: lator;">No data found.</div>
                <?php } ?>
    </div>
<?php
	}
	else
	{
?>
<script>$('#extra_top_menu_list').siblings('div').removeClass('menuActive').addClass('menuDefault');</script>
<div align="center" style="width:600px; height:300px; margin-left:auto; margin-right:auto; margin-top:30px;">
  <table style="width:500px; height:300px;" align="center" cellpadding="0" cellspacing="0">
    <tr align="center" style="margin-top:20px;">
      <td style="width:250px;color:#727272;font-family: lator;"><input type="password" class="input_setting old_pass" placeholder="Old Password" /></td>
    </tr>
    <tr align="center">
      <td style="width:250px;color:#727272;font-family: lator;"><input type="password" class="input_setting new_pass" placeholder="New Password" /></td>
    </tr>
    <tr align="center">
      <td style="width:250px;color:#727272;font-family: lator;"><input type="password" class="input_setting confirm_pass" placeholder="Confirm Password" /></td>
    </tr>
    <tr align="center">
      <td><input type="button" class="join" value="Update" style="width:130px; cursor:pointer;" onclick="updatePasswordPromoter('<?php echo $_SESSION['promoterEmail']; ?>')" />
        <input type="button" class="join" value="Cancel" style="width:130px; margin-left:20px;" onclick="window.open('promoterDashboard.php','_self')"/></td>
    </tr>
  </table>
  <div class="error" align="center" style="display:none"></div>
</div>
<?php } ?>