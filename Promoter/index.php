<?php
	session_start();
	require_once("../config/conn.php");
	if(isset($_SESSION["promoterId"]))
	{
		header("Location:promoterDashboard.php");
	}
	
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UFundoo | Promoter</title>
<link rel="shortcut icon" href="../assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="../assets/css/ufundoo.css" type="text/css" />
<script src="../assets/js/jquery-1.9.1.min.js"></script>
<script src="../assets/js/jquery-ui.js"></script>
<script src="../assets/js/ufundoo.js"></script>
</head>
<body>
<!-- loader -->
<div class="loading" style="display:none">
	<?php 
    	include('../loader.php');
    ?>
</div>
<!-- end here -->

<!--SIGN IN -->
<div class="landing-box-admin">
    <div style="width:375px;margin:0 auto;">
          <p style="font-size:24px;width:100%; text-align:center;margin:5px 0 15px 0;color:#494949;">Login Ufundoo As a Promoter</p>
          <input type="text" placeholder="Email" class="txtbox txtLoginmail" style="font-size:16px; color:#000; line-height:20px; outline:none; border:1px solid #9a9a9a; border-radius:35px; width:330px; height:50px; padding-left:15px; padding-right:15px; margin-top:20px;"/>
          <input type="password" placeholder="Password" class="txtbox txtLoginpassword" style="font-size:16px; color:#000; line-height:20px; outline:none; border:1px solid #9a9a9a; border-radius:35px; height:50px; padding-left:15px; padding-right:15px; width:330px; margin-top:20px;"/>
          <div class="searchBtn" onclick="signInPromoter()" style="padding:10px; margin-top:20px; width:355px; height:34px;">Login</div>
        </div>
    <div class="errorLogin" style="margin-top:20px; text-align:center" align="center"></div>
</div>
<!-- END OF SIGNIN-->

<script>
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	signInPromoter();
}});
</script>
</body>
</html>