<?php
	session_start();
	require_once("../config/conn.php");
	if(!isset($_SESSION["promoterId"]))
	{
		header("Location:index.php");
	}	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UFundoo | Promoter</title>
<link rel="shortcut icon" href="../assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="../assets/css/ufundoo.css" type="text/css" />
<link rel="stylesheet" href="../assets/css/jPages.css" type="text/css" />
<link rel="stylesheet" href="../assets/css/cal.css" type="text/css" />
<script src="../assets/js/jquery-1.9.1.min.js"></script>
<script src="../assets/js/jquery-ui.js"></script>
<script src="../assets/js/ufundoo.js"></script>
<script src="../assets/js/jPages.js"></script>
<link rel="stylesheet" href="../assets/css/uploadEvent.css" type="text/css" />
<script src="../assets/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<script src="../assets/js/ufundoo.js"></script>
<script src="../fileupload/js/vendor/jquery.ui.widget.js"></script> 
<!-- The Load Image plugin is included for the preview images and image resizing functionality --> 
<script src="../fileupload/js/load-image.all.min.js"></script> 
<!-- The Canvas to Blob plugin is included for image resizing functionality --> 
<script src="../fileupload/js/canvas-to-blob.min.js"></script> 
<!-- The Iframe Transport is required for browsers without support for XHR file uploads --> 
<script src="../fileupload/js/jquery.iframe-transport.js"></script> 
<!-- The basic File Upload plugin --> 
<script src="../fileupload/js/jquery.fileupload.js"></script> 
<!-- The File Upload processing plugin --> 
<script src="../fileupload/js/jquery.fileupload-process.js"></script> 
<!-- The File Upload image preview & resize plugin --> 
<script src="../fileupload/js/jquery.fileupload-image.js"></script> 
<!-- The File Upload validation plugin --> 
<script src="../fileupload/js/jquery.fileupload-validate.js"></script> 
<!-- jQuery easing plugin -->
<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
<script src="ckeditor/ckeditor.js"></script> 
<script src="ckeditor/adapters/jquery.js"></script> 
<!-- maxlength.js -->
<script src="../assets/js/maxlength.js"></script> 

<style>
.ui-datepicker-trigger{margin-top:4px; height:27px; position:absolute; cursor:pointer}
.ui-datepicker-div{padding-left:7px;}
.ui-timepicker-div{padding-left:7px; height:140px;}
.ui-slider .ui-slider-handle{height:0.8em; width:0.8em;top:-.4em}
input[type=button],textarea
{
	outline:none;
	-webkit-appearance: none; /*Safari/Chrome*/
    -moz-appearance: none; /*Firefox*/
    -ms-appearance: none; /*IE*/
    -o-appearance: none; /*Opera*/
    appearance: none;
}
.select-wrapper{
		float: left;
		display: inline-block;
		background:url(../assets/img/arrow.png) no-repeat right 8px center;
		cursor: pointer;
		
	}
	.select-wrapper, .select-wrapper select{
		width: 125px;
		//height: 40px;
		height:30px;
		//line-height: 26px;
		line-height: 30px;
		font-size:15px;
	}
	
	.select-wrapper .holder{
		display: block;
		//margin: 0 35px 0 5px;
		white-space: nowrap;            
		overflow: hidden;
		cursor: pointer;
		position: relative;
		//padding-top:7px;
		z-index: -1;
		color:#444;
		width:125px;
	}
	.select-wrapper select{
		margin: 0;
		position: absolute;
		z-index: 2;            
		cursor: pointer;
		outline: none;
		opacity: 0;
		/* CSS hacks for older browsers */
		_noFocusLine: expression(this.hideFocus=true); 
		-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
		filter: alpha(opacity=0);
		-khtml-opacity: 0;
		-moz-opacity: 0;
		width:125px;
	}
	.custom-select
	{
		background-color:#fff;
	}
	.headerBtn {
    color: #2e302d;
}
</style>
</head>
<body>
<!-- loader -->
<div class="loading" style="display:none">
	<?php 
    	include('../loader.php');
    ?>
</div>
<!-- end here -->

<div class="wrapper">
    <!-- header -->
	<div class="header">
        <?php include('../include/header-promoter.php'); ?>
    </div>
    <!-- end here -->
    
    <!-- container -->
  	<div class="content" align="center">
    	
        <div style="display:table;height:60px; width:950px; margin-top:0px;" id="submenuFill" align="center">
        	<!-- top menu -->
            <div style="display:table-cell; vertical-align:middle; width:69px;height:60px; height:auto;">
            </div>
            <div style="vertical-align:middle; width:885px;height:60px; height:auto; display:table-cell;" class="events_sub">
            	<div style="display:table-row">
                	<div style="display:table-cell; vertical-align:middle; width:220px; height:60px; font-size:15px; font-weight:bold">
                    	<div class="btn-admin" align="center" onclick="fetchEventPromoter(this,'approve','live')">CURRENT</div>
                    </div>
                    <div style="display:table-cell; vertical-align:middle; width:220px; height:60px;font-size:15px; font-weight:bold">
                    	<div class="btn-admin" align="center" onclick="fetchEventPromoter(this,'pending','pending')">PENDING</div>
                    </div>
                    <div style="display:table-cell; vertical-align:middle; width:220px; height:60px;font-size:15px; font-weight:bold">
                    	<div class="btn-admin" align="center" onclick="fetchEventPromoter(this,'disapprove','disapprove')">DISAPPROVED</div>
                    </div>
                    <div style="display:table-cell; vertical-align:middle; width:220px; height:60px;font-size:15px; font-weight:bold">
                    	<div class="btn-admin" align="center" onclick="fetchEventPromoter(this,'approve','past')">PAST</div>
                    </div>
                </div>
            </div>
            <div style="display:table-cell; vertical-align:middle; width:69px;height:60px; height:auto;">
            </div>
            <!-- end here -->
        </div>
    
    	<!-- main content -->
        <div style="display:table; min-height:400px; height:auto; width:950px; text-align:left">
            <!-- left side list -->
            <div style="display:table-cell; vertical-align:top; width:69px;min-height:400px; height:auto;">
            </div>
            <!-- end here -->
            
            <!-- middle side list -->
            <div style="display:table-cell;width:885px;min-height:400px; height:auto;" id="dataFill">
            </div>
            <!-- end here -->
            
            <!-- right side list -->
            <div style="display:table-cell; vertical-align:top; width:69px;min-height:400px; height:auto;">
            </div>
            <!-- end here -->
        </div>
        <!-- end here -->
           
    </div>
    <!-- end here -->
    
    <!-- header -->
<?php /*?>	<div class="footer">
        <?php include('../include/footer.php'); ?>
    </div>
<?php */?>    <!-- end here -->
</div>
<script>
$(document).ready(function(e) {
   $('.btn-admin').eq(0).trigger('click');
	if($(window).width()<1200)
	{
		$('.logout').css({'right':'-8px'});
	}
});
</script>
</body>
</html>