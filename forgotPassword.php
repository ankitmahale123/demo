<!DOCTYPE html>
<html>
<head>
<title>Ufundoo</title>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
</head>
<body>

<div style="margin-top:15px; height:70px;" align="center">
	<img src="assets/img/logo.png" height="67" style="cursor:pointer;" onClick="window.open('index.php','_self');"/>
</div>

<!-- container of form -->
<div style="width:100%;">
	  <div class="reset_container" align="center" style="margin-top:25px;">
      <div><input type="password" id="txtcpassword" placeholder="Password" class="txtbox confirm_password" style="width:400px; height:35px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" /></div>
      <div><input type="password" id="txtcpasswordconfirm" placeholder="Confirm Password" class="txtbox confirm_password"  style="width:400px; height:35px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none"/></div>
      <div class="join resetbBtn" onclick="reset_password_link('<?php echo $_REQUEST['id']; ?>','<?php echo $_REQUEST['role']; ?>')" id="resetBtn">Reset</div>
      </div>
      <div class="errorSignUp" align="center" style="margin-top:20px; font-size:15px; color:#464646"></div>
      <div id="after_reset" style="text-align: center; margin-left:auto; margin-right:auto; display:none; margin-top:20px;">
      	<div class="join resetbBtn" onclick="window.open('index.php','_self')" style="display: inline-block;width: 150px;float: left;">Home page</div>
        <div class="join resetbBtn" onclick="window.open('index.php?reset=yes&resetRole=<?php echo $_REQUEST['role']; ?>','_self')" style="display: inline-block;float: left; width: 150px;margin-left: 50px;">Login</div>
      </div>
</div>
<!-- end here -->

<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/ufundoo.js"></script>
<script>
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	reset_password_link('<?php echo $_REQUEST['id']; ?>','<?php echo $_REQUEST['role']; ?>');
}});
</script>
</body>
</html>