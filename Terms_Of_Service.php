<?php
	session_start();
	require_once("config/conn.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UFundoo</title>
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/ufundoo.js"></script>
<style>
.headerBtn {
	color: #2e302d;
}
.description {
	font-size:16px;
	padding: 0 15px;
}
h3 {
	font-size:18px;
}
a{font-weight:500;}
</style>
<script>
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	if($("#signup-container").css('display')=="block")
	{
		signUp();
	}
	else if($("#login-container").css('display')=="block")
	{
		signIn();
	}
	else
	{
		feedback();
	}
}});
</script>
</head>

<body>
<!-- loader -->
<div class="loading" style="display:none">
  <?php 
    	include('loader.php');
    ?>
</div>
<!-- end here --> 

<!-- forgot password -->
<div id="forgot_password">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class" style="width:400px;"> <img src="assets/img/btn_close.png" id="logo-close" onclick="fp_close()" height="20" width="20" style="top:10px; right:10px;" />
      <div style="text-align:center; margin-top:35px; font-size:20px; font-weight:bold">Please provide email address</div>
      <div style="text-align:center; margin-top:20px; font-family: calibri;">
        <input type="text" style="" placeholder="Email" class="fp_text" />
      </div>
      <div style="margin-top:15px; width:250px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="forgot_password()">Ok</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="fp_close()">Cancel</span> </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- signup prompt -->
<div id="signup-container">
  <div class="signup-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeSignup()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Join UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:25px; height:395px;">
        <div style="height:55px;width:100%;">
          <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; float:left; outline:none" placeholder="First Name" class="txtFirstName" />
          <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; margin-left:20px; float:left; outline:none" placeholder="Last Name" class="txtLastName" />
        </div>
        <input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="username" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Password (Atleast 4 characters)" class="password" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Confirm Password" class="confirm_password" />
        <div style="margin-bottom:10px; width:370px; text-align:left"> <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15"/><span style="margin-top:-3px;">I agree to Ufundoo's term of use and privacy policy.</span> </div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUp()" />
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
        <div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Already member ?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;">
          <input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Sign In"  onclick="openLogin()"/>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- login prompt -->
<div id="login-container">
  <div class="login-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeLogin()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Login UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:15px; height:405px;">
        <input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmail" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpassword" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:25px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:8px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signIn()"/>
        <div style="margin-bottom:10px; width:370px; height:15px;"> <span style="">OR</span> </div>
        <a href="googleLogin1.php?status=user" style="text-decoration:none;color:white">
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" />
        </a> <span onclick="FBLogin('user');">
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" />
        </span> </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
        <div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Not a member yet?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;">
          <input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join Now"  onclick="openSignup()" />
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- signup prompt -->
<div id="promoter-container">
  <div class="promoter-container-wrapper">
    <div class="admin-delete-class-promoter"><img src="assets/img/btn_close.png" id="logo-close" onclick="closePromoter()" height="20" width="20" /> 
      
      <!-- login -->
      <div style="float:left; width:50%; height:520px; border-right:2px solid #ccc; margin-top:25px; text-align:center;">
        <div style="text-align:center;font-size:35px; font-weight:bold">Already a Promoter?</div>
        <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Login</div>
        <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmailPost" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpasswordPost" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:75px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signInPost()"/>
        <!--<div style="margin-bottom:10px;height:15px;">
               <span style="">OR</span>
            </div>
            <a href="googleLogin1.php?status=promoter" style="text-decoration:none;color:white"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
            <span onclick="FBLogin('promoter');"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>--> 
      </div>
      <!-- end here --> 
      
      <!-- sign up -->
      <div style="float:left; width:50%; height:520px; margin-top:25px; text-align:center">
        <div style="text-align:center;font-size:35px; font-weight:bold">Become a Promoter?</div>
        <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Sign Up</div>
        <div>
          <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Firstname" class="firstname" />
          <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none; margin-left:4%;" placeholder="Lastname" class="lastname" />
        </div>
        <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Email" class="username_post" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Password (Atleast 4 characters)" class="password_post" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:15px; outline:none" placeholder="Confirm Password" class="confirm_password_post" />
        <div style="margin-bottom:10px; width:75%; margin-left:auto; margin-right:auto"> <img src="assets/img/unchecked.png" name="fainMail" onclick="check_single(this)" status="0" class="fainMail_post" style="cursor: pointer; float: left; margin-top:3px;" height="15" id="fainMail"/>Send me FainMail on everything events and entertainment. </div>
        <div style="margin-bottom:10px; width:70%; margin-left:64px; text-align:left"> <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree_post" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15" id="agree"/>I agree to Ufundoo's term of use and privacy policy. </div>
        <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUpPost()" />
      </div>
      <!-- end here --> 
      
    </div>
  </div>
</div>
<!-- end here --> 

<!-- header -->
<div class="header">
  <?php include('include/header.php'); ?>
</div>
<!-- end here --> 

<!-- wrapper -->
<div class="wrapper"> 
  
  <!-- container -->
  <div class="content"> 
    
    <!-- main content -->
    <div style="display:table; min-height:100px; height:auto; width:1024px; margin-top:30px; margin-left:auto; margin-right:auto; margin-bottom:30px;">
      <div style="display:table-row">
        <div style="display:table-cell; width:550px; min-height:30px; height:auto; padding-left:25px;">
          <h3>Introduction</h3>
          <div class="description">
            <ul>
              <li>The terms 'Ufundoo' and 'ufundoo.com' are used in this document (or Agreement) to refer to the Web site itself and to Ufundoo, Inc., the company which owns and operates the Ufundoo website.</li>
              <li> ufundoo.com, or Ufundoo, is an online event discovery, self-service ticketing and deals platform, which connects its members and customers through buzzworthy content. This document covers the terms and conditions under which these services are provided and the privacy policy of the site.</li>
              <li>Your registration as a member of Ufundoo or the use of any of the features and services on Ufundoo either as a registered member or as an unregistered visitor constitutes acceptance of these terms and conditions and the privacy policy defined herein. These terms and conditions apply automatically to any new services or features introduced on Ufundoo.</li>
              <li> If you do not agree with any of the terms and conditions or with the privacy policy of Ufundoo, you are NOT granted rights to use Ufundoo services and must not register as a member of Ufundoo or use any of its services.</li>
              <li>Ufundoo reserves the right to update the terms, conditions and notices of this agreement without notice to you. It is your responsibility to periodically review the most current version of this Agreement as your continued use of Ufundoo signifies your acceptance of any changed terms.</li>
              <li>This Agreement constitutes the entire agreement between you and Ufundoo and governs your use of the Ufundoo Service, superseding any prior agreements between you and Ufundoo.</li>
              <li>Ufundoo is entitled to assign this agreement, and all rights and obligations hereunder, to a successor to all or any of its assets, whether by sale, merger or otherwise. These terms shall be binding upon and shall inure to the benefit of the parties hereto and their respective representatives, heirs, administrators, successors and permitted assigns except as otherwise provided herein.</li>
            </ul>
          </div>
          <h3>Terms of Service</h3>
          <div class="description">
            <p>Ufundoo is a service provider as defined in the Digital Millennium Copyright Act of 1998. Ufundoo is an interactive computer service as defined by 47 U.S.C. § 230. We do not control the truth or accuracy of content posted or the safety, or legality of the items advertised or sold. You may find other users' information or advertisements posted to be offensive, harmful, inaccurate or deceptive. The views expressed on the website are not those of ufundoo.com, and any errors or omissions in them belong to the respective contributors / copyright holders. Through the use of this site, you agree to hold harmless Ufundoo and its sponsors, owners, shareholders or employees against any and all claims related to the opinions expressed by others.</p>
            <p>The remainder of this document covers the specific terms and conditions under which each major service category of Ufundoo is provided for your use. It is your responsibility to make sure you read and understand this document before using any Ufundoo service. Please do not contribute to Ufundoo in any manner unless you have carefully read and agree with all of these terms. </p>
          </div>
          <h3>Accessing and Transmitting Ufundoo Content</h3>
          <div  class="description">
            <p>All the content available for public access on Ufundoo is protected by copyright. Your use of this material must adhere to the following terms, unless you are the original contributor of the content to Ufundoo, in which case your use is governed by the terms governing Ufundoo's relationship with its contributors as covered below. </p>
            <ul>
              <li>You may download and print a single copy of any content item for your personal, non-commercial use. Content items include, but are not restricted to, events, deals, vendor listings, articles, columns, photographs, community posts and reader comments on articles.</li>
              <li>You may not modify any content you download or print from Ufundoo. You must retain all copyright notices and logos on any content item you download or print.</li>
              <li>You may not make any content item originating from Ufundoo available for public access by any means whatsoever without obtaining prior written permission from Ufundoo.</li>
              <li>You may forward Internet URLs (Uniform Resource Locators) to third parties for private, non-commercial use, along with brief extracts that do not violate Fair Use guidelines.</li>
              <li>You may post, or otherwise make publicly available, excerpts from Ufundoo content items, in any publicly accessible forum, so long as they follow standard Fair Use guidelines and Ufundoo is cited alongside the excerpt as the original source with a link back to Ufundoo.</li>
            </ul>
          </div>
          <h3>Contributing Content to Ufundoo</h3>
          <div class="description">
            <p>In addition to being an event discovery & ticketing platform, a deals marketplace and content publisher, Ufundoo also provides a variety of forums for its members to express themselves in the form of articles, columns, opinions, comments, reviews, art and photographs. Ufundoo provides value to its contributors by making available to them its vast global audience reach.</p>
            <p>The terms in this section apply to all the content contributed to Ufundoo in any of its various services and forums. </p>
            <ul>
              <li>You truthfully assert that the content being contributed is your creation and that you own the copyright to the content. Unless you have deliberately assigned the copyright to someone else, you automatically own the copyright to all of your original creations.</li>
              <li>You truthfully assert you have not infringed on anybody else's copyright in the creation of this article.</li>
              <li>We are not responsible or liable for loss if visitors to the site violate your copyright and distribute your copyrighted content without your permission.</li>
              <li>You grant us exclusive unlimited Internet distribution rights and electronic distribution rights, and non-exclusive, unlimited print publication and distribution rights either through Ufundoo's own or through other media. This includes Web publishing, syndication on other web sites, syndication with print publishers, publishing as part of a collection of articles, publishing as a book, email, ftp, telnet and other Internet-based information transfer protocols.</li>
              <li>You may, however, publish the content you contribute to Ufundoo, on your personal, non-commercial Web site if you possess one.</li>
              <li>In case the contributions are digital images of artwork in traditional media such as canvas, that is available for sale, you may also publish digital images of your artwork on commercial Web sites that sell art online, only for the purpose of sale.</li>
              <li>Once granted and your content is published, the distribution rights cannot be revoked. In other words, once your contribution is published on Ufundoo it automatically becomes a permanent part of the publicly-accessible Ufundoo archives. You may, however, subject to our approval, update or revise your contributions on Ufundoo.</li>
              <li>You will not receive any monetary compensation for your submissions. The only in-kind compensation we promise is exposure on the World Wide Web and a home for your expression on the web.</li>
              <li>You grant us the right to organize, categorize, collate your contributed content and present it on the website in any manner we see fit. We may remove or edit your contribution at our discretion without having to secure your permission. We will however make reasonable efforts to inform you of the changes.</li>
              <li>We generally do not object to our contributors posting their Ufundoo contributions on other, freely accessible non-commercial Internet locations, so long as Ufundoo is clearly cited as the original publisher, and a link to www.ufundoo.com is provided. You, however, agree to secure our permission first before such re-publication on other Internet sites happens.</li>
              <li>You agree to state ufundoo.com as the previous publisher prominently in case you print your article in an offline magazine.</li>
            </ul>
            <p>While you are on the site and engaged in any form of communication on any of the forums, you agree NOT to:</p>
            <ul>
              <li>Upload or post or otherwise make available any Content that is unlawful, harmful, threatening, abusive, harassing, defamatory, vulgar, obscene, libelous, promotes violence, is invasive of another's privacy, hateful, or racially, ethnically or otherwise objectionable; impersonate any person or entity, including, but not limited to, a Ufundoo official, forum leader, guide or host, or falsely state or otherwise misrepresent your affiliation with a person or entity;</li>
              <li>Forge headers or otherwise manipulate identifiers in order to disguise the origin of any Content transmitted;</li>
              <li>Upload or post otherwise make available any Content that you do not have a right to make available, under any law or under contractual or fiduciary relationships (such as inside, proprietary or confidential information, learned or disclosed as part of employment relationships or under nondisclosure agreements);</li>
              <li>Upload or post or otherwise make available any Content that infringes any patent, trademark, trade secret, copyright or other proprietary rights of any party. You may, however, post excerpts of copyrighted material so long as they adhere to Fair Use guidelines.</li>
              <li>Upload or post or otherwise make available any unsolicited or unauthorized advertising, promotional materials, "junk mail," "spam," "chain letters," "pyramid schemes," or any other form of business solicitation. Repeated postings of a promotional nature is considered abuse of the forums. Businesses or individuals wishing to advertise on the Ufundoo website should <a href="https://www.ufundoo.com/callUs.php" target="_blank">contact us</a> at this address.</li>
              <li>Upload or post or otherwise make available any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;</li>
              <li>Disrupt the normal flow of dialogue by repeatedly posting the same or similar material, or otherwise acting in a manner that negatively affects other users' ability to engage in exchanges;</li>
              <li>Violate any applicable local, state, national or international law, including, but not limited to, regulations promulgated by the U.S. Securities and Exchange Commission, any rules of any national or other securities exchange, including, without limitation, the New York Stock Exchange, the American Stock Exchange or the NASDAQ, and any regulations having the force of law;</li>
              <li>"Stalk" or otherwise harass another user; Collect or store personal data about other users either manually or by means of automated software tools; Promote illegal activities; Inhibit other users from using and enjoying the unedited forums.</li>
              <li>Create events or deals that contain untruthful representations, deceptive practices, inducements to commit any illegal activity.</li>
            </ul>
          </div>
          <h3>Using Ufundoo Services</h3>
          <div class="description">
            <p>This Section covers the terms under which various Ufundoo services, including, but not limited to, link forwarding services, event information, mercant information, deals or coupon information and any information available on and/or through Ufundoo, are made available to you.</p>
            <p>You agree that:
            <ul>
              <li>Ufundoo may electronically block or otherwise deny access to the Ufundoo Web site, to any user deemed to be in violation of any of the terms in this document.</li>
              <li>Ufundoo is not obliged to ensure that the Web site is available to all users at all times. While we make all attempts to deny access only to those individuals who violate Ufundoo regulations, we are occasionally forced to use access denial methods that cause a disruption in access for other users.</li>
              <li>Ufundoo is not responsible for a loss of access to Ufundoo services due to failure of networks connected to the Internet, or any other temporary hardware or software failure.</li>
              <li>Ufundoo reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Service (or any part thereof) with or without notice.</li>
              <li>A possibility exists that the Site could include inaccuracies or errors. Additionally, a possibility exists that unauthorized additions, deletions and alterations could be made by third parties to the Site. Although Ufundoo attempts to ensure the integrity and the accuracy of the Site, it makes no guarantees whatsoever as to the correctness or accuracy of the Site. In the event that such an inaccuracy arises, please inform Ufundoo by emailing staff(at)ufundoo.com so that it can be corrected.</li>
              <li>This Site may contain links to other Web sites operated by third parties ("Linked Sites"). You acknowledge that, when you click on a link to visit a Linked Site, a frame may appear that contains the Ufundoo logo, advertisements and/or other content selected by Ufundoo. You acknowledge that Ufundoo and its sponsors neither endorse nor are affiliated with the Linked Site and are not responsible for any content that appears on the Linked Site. You also acknowledge that the owner of the Linked Site neither endorses nor is affiliated with Ufundoo and its sponsors.</li>
              <li>If you use ufundoo.com's free Email-a-Friend and Guestlist services, you are submitting your email address for registration and subscription to our site's services and agree to receive solicited emails from us relating to special promotions and relevant information relating to our content.</li>
            </ul>
            </p>
          </div>
          <h3>Event Manager and Event Ticketing</h3>
          <div class="description">
            <p>The following are the terms and conditions under which ufundoo.com is providing the ticketing service and under which this website is to be used.
            <ul>
              <li>Introduction and Definitions : ufundoo.com Event Manager is the on-line service operated by ufundoo.com, Inc., a New Jersey corporation with its principal place of business in New Jersey (the "Company"), on the World Wide Web of the Internet, consisting of ticketing services, ticketing resources, and related content provided by the Company, and by third parties. "Subscriber" means every person who establishes a connection for access to and use of ufundoo.com.</li>
              <li>Agreement with Terms and Conditions : This Agreement sets forth the terms and conditions that apply to use of ufundoo.com by Subscriber. By using ufundoo.com, Subscriber agrees to comply with all of the terms and conditions hereof.</li>
              <li>Changes in Terms and Conditions : The Company has the right to change or discontinue any aspect or feature of ufundoo.com, including, but not limited to, content, hours of availability, and equipment needed for access or use, at any time. The Company has the right to change or modify the terms and conditions applicable to Subscriber's use of ufundoo.com, or any part thereof, or to impose new conditions, including, but not limited to, adding fees and charges for use at any time. Such changes, modifications, additions or deletions shall be effective immediately upon notice thereof, which may be given by means including, but not limited to, posting on ufundoo.com, or by electronic or conventional mail, or by any other means. Any use of ufundoo.com by Subscriber subsequent to such notice shall be deemed to constitute acceptance by Subscriber of such changes, modifications or additions.</li>
              <li>Ticketing : ufundoo.com sells tickets on behalf of promoters, teams, bands and venues which means ufundoo.com does not set the ticket prices or determine seating locations. All ticket sales are final and no exchanges or refunds are allowed after a purchase has been made. Where tickets are mailed to the Subscriber, neither the client nor ufundoo.com will replace lost, stolen, damaged or destroyed tickets. When you receive your tickets, please keep them in a safe place. Please note that direct sunlight or heat can sometimes damage tickets. Where "Ticket Delivery" feature is available and chosen, please allow enough time for the delivery of your ticket order. If you have selected delivery by mail, please allow at least fourteen business days after your order is placed to receive your tickets. For express delivery allow at least 2 business days. These are approximate times, and ufundoo.com is not responsible for deliveries that take longer. Will call orders can be processed up to the day of the event. Where "Will Call' feature is available and chosen, the Subscriber must show proof of identification and a printed copy of the electronic receipt to receive the actual ticket. Occasionally, events are cancelled, delayed or postponed by the promoter, team, performer or venue for a variety of reasons. If the event is cancelled, delayed or postponed for any reason please contact the organizer for information on receiving a refund. If the event was moved or rescheduled, the venue or promoter may set refund limitations. ufundoo.com assumes no responsibility for such refunds and processes refunds only as instructed by the event organizer. ufundoo.com will send a confirmation of the order for tickets via email to the subscriber upon successful processing of the order. It is the responsibility of the Subscriber to ensure that the email address provided is correct and accurate.</li>
            </ul>
            </p>
          </div>
          <h3>Deals Marketplace and Merchant Services</h3>
          <div class="description">
            <p>Ufundoo provides self-service Deals as a service to enable its merchant-users to connect with customers. Ufundoo provides the platform but does not directly participate in, nor is it involved in any Merchant business beyond providing the platform through which merchants provide their products and services. Please keep in mind that Deals are posted not by Ufundoo, but by independent merchants. Users of this service are responsible for all aspects of the transactions in which they choose to participate. Ufundoo requires a user to register before posting a Deal but does not screen or control users who may sell or buy on Deals, nor does Ufundoo review or authenticate all deals, items, or services offered for sale. Ufundoo has no control over the quality, safety or legality of the items advertised, the truth or accuracy of the deals, the ability of sellers to sell items or the ability of buyers to buy items. Therefore, users of this service should be aware that:
            <ul>
              <li>Sellers and buyers are completely responsible for working out the sale and exchange of goods or services. They must resolve any disputes that may arise in their dealings.</li>
              <li>Sellers and buyers are responsible for researching and complying with any applicable laws, regulations or restrictions on items, services, or manner of sale or exchange that may pertain to transactions in which they participate.</li>
              <li>Sellers and buyers are responsible for all applicable taxes and for all costs incurred by participating in Ufundoo Deals.</li>
              <li>Ufundoo will not be liable for any damages of any kind incurred as a result of the information contained in the Ufundoo Deals or the use of Ufundoo Deals</li>
              <li>Users may not use or manipulate this service for any fraudulent activity or purpose. Items or services offered for sale must comply with applicable laws</li>
              <li>Ufundoo may take action to delete the deals and/or Ufundoo user accounts that we believe are misusing this service. Prohibited spam or otherwise impermissible marketing activities in Ufundoo Deals may include, but are not limited to, the following:
                <ul>
                  <li>Creating a deal that is unrelated to the category in which the deal is placed.</li>
                  <li>Creating identical deals in the same category and location.</li>
                  <li>Creating a deal with inappropriate links, titles, descriptions, or unauthorized use of content to which the person posting does not have rights.</li>
                  <li>Creating a deal through unauthorized robots (crawling software).</li>
                  <li>Soliciting a Ufundoo Deals merchant to create a deal on another deals site.</li>
                  <li>Creating deals for services that compete with any of Ufundoo's services.</li>
                  <li>Because Ufundoo is a platform, in the event that users have disputes, Ufundoo and its employees, officers, agents and directors are released from claims, demands and damages (actual and consequential) of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way connected with such disputes. We do not control the information provided by other users that is made available through our system. Users may find other user's information to be offensive, harmful, inaccurate, or deceptive. Please use caution and common sense for your own safety. Please note that there are also risks of dealing with underage persons or people acting under false pretense. Additionally, there may also be risks dealing with international trade and foreign nationals.</li>
                </ul>
              </li>
            </ul>
            </p>
          </div>
          <h3>Member Registration</h3>
          <div class="description">
            <p>Ufundoo requires registration for those who participate in a variety of its services. When you register as a Ufundoo member, you accept the following terms and conditions :
            <ul>
              <li>You agree to provide true, accurate, current and complete information as required in the registration form.</li>
              <li>Sellers and buyers are responsible for researching and complying with any applicable laws, regulations or restrictions on items, services, or manner of sale or exchange that may pertain to transactions in which they participate.</li>
              <li>You agree maintain and promptly update the Registration Data as necessary to keep it true, accurate, current and complete.</li>
              <li>If, after investigation, Ufundoo has reasonable grounds to suspect that any user's information is untrue, inaccurate, not current or incomplete, Ufundoo may suspend or terminate that user's account and prohibit any and all current or future use of the Ufundoo Sites (or any portion thereof) by that user other than as expressly provided herein</li>
              <li>Each user is wholly responsible for maintaining the confidentiality of account login and password information and for all activities occurring there under. Ufundoo cannot and will not be liable for any loss or damage arising from a user's failure to comply with this Section 4, including any loss or damage arising from any user's failure to: (1) immediately notify Ufundoo of any unauthorized use of his or her password or account or any other breach of security; and (2) ensure that he or she exits from his or her account at the end of each session.</li>
              <li>You agree that Ufundoo reserves the right to publish any and all information provided as part of Ufundoo registration, except email address and password to authenticate the user.</li>
            </ul>
            </p>
          </div>
          <h3>Disclaimer of Warranties and Indemnity</h3>
          <div class="description">
            <p>YOUR USE OF Ufundoo  IS AT YOUR OWN RISK. THIS WEBSITE IS PROVIDED BY Ufundoo , INC. ON AN "AS IS" AND "AS AVAILABLE" BASIS. TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, WEBSITE DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.</p>
            <p>YOU ACKNOWLEDGE THAT ANY WARRANTY THAT IS PROVIDED IN CONNECTION WITH ANY OF THE PRODUCTS OR SERVICES MADE AVAILABLE ON OR THROUGH Ufundoo  IS PROVIDED SOLELY BY THE OWNER, ADVERTISER OR MANUFACTURER OF THAT PRODUCT AND/OR SERVICE AND NOT BY Ufundoo , INC.</p>
            <p>Ufundoo , INC. MAKES <b>NO REPRESENTATIONS OR WARRANTIES</b> OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THE WEBSITE OR THE INFORMATION, CONTENT, MATERIALS, OR PRODUCTS ON THE WEBSITE, INCLUDING THAT</p>
            .
            <ul>
              <li>Ufundoo AND ITS SERVICES WILL MEET YOUR REQUIREMENTS,</li>
              <li>Ufundoo WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR FREE,</li>
              <li>THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THOUGH Ufundoo  WILL MEET YOUR EXPECTATIONS, BE RELIABLE OR ACCURATE, AND</li>
              <li>ERRORS IN THE SOFTWARE WILL BE CORRECTED.</li>
            </ul>
            <p>YOUR SOLE REMEDY FOR DISSATISFACTION WITH Ufundoo , SITE-RELATED SERVICES AND/OR CONTENT OR INFORMATION CONTAINED WITHIN THE SITE IS TO STOP USING THE SITE AND/OR ITS SERVICES.</p>
            <p>ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF Ufundoo  IS DONE AT YOUR OWN DISCRETION AND RISK AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS.</p>
            <p>You agree to indemnify, defend and hold Ufundoo , Inc. harmless from all claims, damages and expenses (including attorney's fees) made by any third party arising out of your content, your use of Ufundoo , your connection to Ufundoo , your violation of this Agreement, our Terms of Use or our Privacy Policy, and the development, operation, maintenance, use and contents of your website.</p>
          </div>
          <h3>Limitation of Liability</h3>
          <div class="description">
            <p>IN NO EVENT SHALL Ufundoo  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER, INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA, OR OTHER INTANGIBLE LOSSES RESULTING FROM [A] THE USE OR THE INABILITY TO USE OUR SERVICES OR ACCESS CONTENT, [B] THE COST OF PROCUREMENT OF SUBSTITUTE GOODS AND SERVICES RESULTING FROM TRANSACTIONS ENTERED INTO THROUGH OR FROM Ufundoo , [C] THE UNAUTHORIZED ACCESS TO OR ALTERATIONS OF YOUR TRANSMISSIONS OR DATA, [D] STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON THE SERVICE, OR [E] ANY OTHER MATTER RELATING TO THE SERVICE, EVEN IF Ufundoo , INC. HAS BEEN ADVISED OF THE POSSIBILITY OF DAMAGES.</p>
          </div>
          <h3>Void Where Prohibited</h3>
          <div class="description">
            <p>Although Ufundoo  is accessible worldwide, not all products or services discussed or referenced on Ufundoo  are available to all persons or in all geographic locations or jurisdictions. Ufundoo  reserves the right to limit the provision of any product or service to any person, geographic area or jurisdiction it so desires, in its sole discretion and to limit the quantities of any such product or service that it provides. Any offer for any product or service made on Ufundoo  is void where prohibited.</p>
            <p>In addition, Ufundoo  Inc. makes no representation that materials on Ufundoo  are appropriate or available for use in locations outside the United States; therefore, accessing those materials from territories where they are illegal is prohibited. Those who choose to access Ufundoo  from locations outside the United States do so on their own initiative and are responsible for compliance with local laws.</p>
          </div>
          <h3>Financial Transactions and Taxes</h3>
          <div class="description">
            <p>If you make any financial transactions on Ufundoo , including buying tickets, buying deals or paying for advertising, you may be asked for credit card or other payment information. You agree that all information you provide is accurate, complete and current and that you will pay all charges owed, including any applicable taxes. You agree that you are solely responsible for any personal income reporting and tax payments required of you by applicable government authorities.</p>
          </div>
          <h3>Termination</h3>
          <div class="description">
            <p>Ufundoo  reserves the right to discontinue in whole or in part any portion of Ufundoo  services or programs with or without notice.</p>
          </div>
          <h3>Additional Notices</h3>
          <div class="description">
            <ul>
              <li>All pages within this Internet site are the property of Ufundoo , Inc. Permission (which may be revoked at any time) is granted to download the material in this Site without alterations and for private and non-commercial use only so long as the following copyright notice is included: © 2013-2014, ufundoo.com</li>
              <li>'Ufundoo ', 'ufundoo.com', 'Ufundoo  Ticketing', 'Ufundoo  Deals', 'Ufundoo  E-Ticketing', 'TicketPilot', 'DealPilot', and other logos associated with these services are all trademarks and service marks of Ufundoo , Inc. All other trademarks, service marks and logos used in this Site the trademarks, service marks or logos of their respective owners.</li>
              <li>Any offer for any product or service made in the Site is void where prohibited.</li>
              <li>Third party trademarks, service marks, logos, trade names and copyrights found on Ufundoo  belong to their respective owners.</li>
              <li>All rights to the Ufundoo  Material not expressly granted herein are reserved.</li>
              <li>Third Party Merchants and Advertisers: You acknowledge that Ufundoo  Inc. does not control the performance of merchants or advertisers that appear on Ufundoo . Therefore, if you choose to purchase a product or service, you release us from any and all claims you may have regarding the third party's failure to deliver or perform.</li>
            </ul>
          </div>
          <h3>Independent Investigation</h3>
          <div class="description">
            <p>YOU ACKNOWLEDGE THAT YOU HAVE READ THIS AGREEMENT AND AGREE TO ALL ITS TERMS AND CONDITIONS. YOU UNDERSTAND THAT WE MAY AT ANY TIME (DIRECTLY OR INDIRECTLY) SOLICIT MEMBER REFERRALS ON TERMS THAT MAY DIFFER FROM THOSE CONTAINED IN THIS USER AGREEMENT. YOU HAVE INDEPENDENTLY EVALUATED THE DESIRABILITY OF PARTICIPATING IN Ufundoo  OR ITS PROGRAMS AND ARE NOT RELYING ON ANY REPRESENTATION, GUARANTEE OR STATEMENTS OTHER THAN AS SET FORTH IN THIS AGREEMENT. </p>
          </div>
          <h3>Privacy Policy / Anti-Spam Policy</h3>
          <div class="description">
            <p>ufundoo.com does NOT Spam. We make every effort technically possible to prevent Spamming and enforce the strictest consequences on anyone who is found to abuse our website to Spam.</p>
            <p>If you have received an email from us and did not request to receive the email, please <a href="https://www.ufundoo.com/callUs.php" target="_blank">contact us</a> immediately so we can investigate the matter for you and resolve the situation at once. Due to the nature of our website and services, you may receive an email from us for the following reasons: <br/>
              a) If you purchase any product or services from our website. <br/>
              b) If you are a registered member and subscribed to our mailing list. <br/>
              c) If you email us and we respond back to your inquiry.<br/>
              d) If one of your friends or family members or acquaintances sends you a link from our website. </p>
            <p>We do not record your email address into our database for any of these reasons, except only in the case when you voluntarily register with us. Just as you can voluntarily register with us and subscribe to our newsletter(s), you can also unsubscribe yourself by logging into your account and going to http://www.ufundoo.com/account/account.cfm. If you have any problems or questions, please <a href="https://www.ufundoo.com/callUs.php" target="_blank">contact us</a> and we will resolve your problem immediately.</p>
            <p>Privacy issues are a high priority for us at ufundoo and we follow standard industry practices to maintain your personal information. Please read the following to learn more about our privacy policy.</p>
            <p><b>ufundoo does not spam or sell/lease its user information to third parties.</b></p>
          </div>
          <h3>Policy Coverage</h3>
          <div class="description">
            <p>This Privacy Policy covers ufundoo's treatment of personally identifiable information that ufundoo collects when you are on the ufundoo site, and when you use ufundoo's services. This policy also covers ufundoo's treatment of any personally identifiable information that ufundoo's business partners share with ufundoo. This policy does not apply to the practices of companies that ufundoo does not own or control, or to people that ufundoo does not employ or manage.</p>
          </div>
          <h3>Personal Information Collected by ufundoo</h3>
          <div class="description">
            <p><b>ONLINE REGISTRATION:</b> In order to become a ufundoo member, you must first create a member account on ufundoo by providing information about yourself including but not limited to name, email address, country, ZIP/Postal Code, birth date, a brief summary of yourself and a password. ufundoo checks for the validity of the email addresses.</p>
            <p><b>OPTIONAL PROFILE INFORMATION:</b> Once your provide the basic information to create an account, you will have the opportunity to provide additional information in the Profile section describing your skills, interests, professional and academic background and favorites. Provision of this additional information beyond what is required at registration is entirely optional; it allows you to better identify yourself and create superior networking opportunities with other through shared contexts. but enables you to better identify yourself and find new opportunities on ufundoo. Your Profile information can only be viewed by other ufundoo visitors or registered members or members of your personal network only depending on the option you choose.</p>
            <p><b>FORMING YOUR OWN PERSONAL NETWORK:</b> You can invite others to connect with you directly in ufundoo by entering their names and email addresses. This information will be used by ufundoo to send your invitation along with a message that you write. The names and email addresses of people that you invite will be used only to send your invitation and reminders. You will also have the option to upload your contact information either manually or via a computer upload directly from your Hotmail, Yahoo or Outlook section into your own private address book on ufundoo. All information entered there will only be viewable by you and will not be searchable by others in the ufundoo network without your permission. All information that you enter or upload about your contacts will be covered by the same terms of this privacy policy as cover your own personally identifiable information.</p>
            <p><b>RECOGNIZING YOU AS A ufundoo MEMBER:</b> For ufundoo to recognize you as a registered member, you must login with your login email and password. At the time of login, you will have the option to instruct the site to remember you so when you come back the next time to use ufundoo, you would not have to login with your email and password. As is common in the industry, ufundoo uses the technology of cookies and site log files to monitor user registration, to recognize registered members when they revisit the site and to track site usage. Our ability to identify you as a registered member allows us to customize the ufundoo experience and to protect your personal information based on your account settings.</p>
            <p><b>INTERNET COMMUNICATION PROTOCOL INFORMATION:</b> The communication standards of the Internet industry transmit the URL of the site from which you visit ufundoo, the IP address of your computer, the IP address of your ISP, various attributes of your computer such as operating system, resolution and browser software, and the date and time of your various requests for pages on ufundoo. ufundoo file logging system automatically logs all of this information in its log files for its analysis.</p>
            <p><b>NEWSLETTER SUBSCRIPTIONS:</b> Visitors to ufundoo may also sign up with their name and email address for a variety of newsletters that ufundoo produces with varying frequency (e.g., News headlines) without having to register. ufundoo confirms the subscription by sending an email to the email address specified before sending the newsletter.</p>
          </div>
          <h3>Use of Information by ufundoo</h3>
          <div class="description">
            <p>ufundoo is an online website that offers a variety of services designed to connect people from around the world. The information, whether personal information of any form of self-expression is used to help you describe yourself to other users. ufundoo also collects additional information in order to operate and improve our service. ufundoo is the sole owner of the information collected on the ufundoo site.</p>
            <p><b>PROFILE SHARING CUSTOMIZATION:</b> Whether you register on ufundoo, what parts of your profile you fill, who you add to your network and what visibility levels you want to set for your profile are entirely up to you in the form of settings you can make on your profile. You also have the ability to change these at any point in time. ufundoo will show the information to enable personal networking only based on your preference.
            <p><b>VISIBILITY OF NETWORK LINKAGES:</b> Where applicable, the path of members that connect you with another member of your network will be displayed to that member. This network path will not be shown to those who are not members of your personal network.</p>
            <p><b>VISIBILITY OF PUBLIC CREATIVE CONTRIBUTIONS:</b> In addition to being a social and professional networking site, ufundoo also provides a variety of forums for its members to express themselves in the form of articles, columns, opinions, comments, reviews, art and photographs. Since this expression is linked to the member's identity and the expression is in the public domain, the compilation of a member's creative contributions to ufundoo will be visible to all visitors of ufundoo whether or not they are registered members of ufundoo.</p>
            <p><b>VISIBILITY OF PRIVATE MESSAGES WITHIN NETWORK:</b> You will be able to create and share messages within your personal network. The visibility of these private notes will be restricted to your network unless you or one of the members in your network choose to forward it to others within or outside ufundoo.</p>
            <p><b>SHARING OF INFORMATION WITH THIRD PARTIES:</b> ufundoo takes your privacy very seriously. ufundoo will never sell, rent, or otherwise provide your personally identifiable information to any third parties unless it is to carry out your instructions and to provide specific services. As an example, we send your information to a credit card processing company to process a payment you have made for the use of a ufundoo service. These third parties are bound by strict confidentiality agreements which limit their use of such information. In addition, ufundoo may share your information to send direct mailers or complimentary coupons/offers to a mailing agency to process postal mailing. Again, such an agency would be bound by strict confidentiality agreements that limit their use of such information.</p>
            <p><b>EMAIL COMMUNICATIONS:</b> ufundoo generally communicates with you as a registered member of ufundoo through the primary email that you have specified at the time of registration. This is also used as the login text string in conjunction with the password to authenticate you on the ufundoo site. Your email and password are never shared with anybody within or outside ufundoo. ufundoo uses this email address to send you three types of communications:</p>
            <ul>
              <li>Sending you emails related to the proper functioning of your account (e.g. when you receive a response to an event or deals inquiry you have posted on ufundoo or when you receive an invitation to join the personal network of another person)</li>
              <li>Sending you newsletters that you have subscribed to</li>
              <li>Sending you occasional announcements about changes in ufundoo's policies or promotions introduction of new services</li>
              <li>Please note that these email communications may contain commercial messages of advertisers; however, ufundoo does not provide your personal information or emails to any of the advertisers; we just include the advertising banners they provide us in the emails that we send. As always you can opt out of the newsletters or promotional announcements.</li>
            </ul>
            <p><b>PARTNERING WITH OTHERS:</b> ufundoo may partner with other online services to offer a combined service. Whenever we offer such combined services, ufundoo will make it clear who the partner is and it will be entirely up to you to use or not to use the service. Where any personal information is passed along to the partner you will be informed prior to that sharing.</p>
            <p><b>PROVISION OF AGGREGATE DEMOGRAPHIC INFORMATION TO THIRD PARTIES:</b> ufundoo reserves the right to provide aggregate demographic information of its member base to advertisers and others so as to attract pertinent advertisers to advertise on ufundoo. We do not shared personal information of individual users with advertisers.</p>
            <p><b>COMMUNICATIONS TO OTHERS FROM YOU THROUGH ufundoo: </b>You will be able to send emails to others -- members and non-members -- through the ufundoo system. For example, you may send a comment on an article written by another member. Or you may send an email invitation to a non-member to join your personal network. In these instances, these emails will list your name and email address in the header of the message. The recipients of these emails initiated by you will get to know your name and email address. ufundoo is not responsible for how these recipients use this information. Your contact information will only be shared with another member only if both of you indicated you would like to establish contact with each other.</p>
            <p><b>COMMUNICATIONS FROM OTHERS TO YOU THROUGH ufundoo:</b> Only registered members of ufundoo are allowed to communicate with you through ufundoo through a form on ufundoo. Unless and until you respond to this email, they will not get to know your email address. You may, however, choose an account setting that prevents any other member from contacting you or inviting you to join their personal network.</p>
            <p><b>DISCLOSURES REQUIRED BY LAW:</b> It is possible that we may need to disclose personal information when required by law. We will disclose such information wherein we have a good-faith belief that it is necessary to comply with a court order, ongoing judicial proceeding, or other legal process served on our company or to exercise our legal rights or defend against legal claims.</p>
          </div>
          <h3>Accessing and Changing Your Account Information</h3>
          <div class="description">
            <p>You can review the personal information you provided us and make any desired changes to the information, or to the settings for your ufundoo account, at any time by logging in to your account on the ufundoo website and editing the information on your Profile page. You can also close your account through the ufundoo website by sending your request to staff(at)ufundoo.com with your login email address. You request will be processed within 7 business days.</p>
          </div>
          <h3> Abuse Policy on ufundoo.com</h3>
          <div class="description">
            <p>ufundoo.com is committed to ensuring and enforcing a safe and friendly online community space for all our members. To this effect, we have come up with a Policy on Abuse that we would like all our members to be aware of. These rules govern the use of the ufundoo.com website and its product/service offerings and we would like all our members to cooperate in complying with these rules so that everyone can have a pleasant experience.</p>
          </div>
          <h3>Conduct and use of language:</h3>
          <div class="description">
            <ul>
              <li>Member contributions must be civil and tasteful and must not be malicious or designed to offend. Use of swear-words or profanity is strictly prohibited.</li>
              <li>Do not refer to the personality of other participants or attack an individual's character or personally insult another member. Do not use the website to harass, threaten or verbally abuse or intimidate anyone, or behave in any manner that would cause annoyance or distress to other members. </li>
              <li>Members of ufundoo.com may not insult or defame the site, including any of its employees, moderators or freelancers. You may not post the contents (in part or whole) of any email that you may receive from any ufundoo.com employee(s) and/or moderator.</li>
              <li>Do not post any material that is obscene, defamatory, profane, libelous, threatening, harassing, abusive, hateful or embarrassing towards other person(s) or an entity. We encourage a healthy exchange of opinions and disagreements. By all means challenge an opinion - but please do it respectfully. Name-calling, insults and use of foul or abusive language will not be tolerated. This includes the use of offensive member Display Names and URLs (your personal website address on ufundoo.com).</li>
              <li>Please be cautious when you divulge the details of your identity or personal information in your post(s), including photos of yourself or your family and friends. Anything posted online is accessible to the entire internet community and ufundoo.com is not liable in cases where such information is misused. Please be aware of the general dangers of online information sharing like identity theft, phishing and other fraudulent activities.</li>
              <li>Members of ufundoo.com are not permitted to collect or distribute personal data concerning other members at any time.
                Do not post messages more than once. This is also known as "flooding." ufundoo.com reserves the right to delete such posts and may terminate the accounts of such members.</li>
              <li>A member of ufundoo.com must not impersonate another member.</li>
            </ul>
          </div>
          <h3>Advertising and Spamming:</h3>
          <div class="description">
            <ul>
              <li>Commercial messages or promotional messages from businesses or organizations are not permitted on ufundoo. ufundoo.com provides a platform for its members to express themselves in the form of articles, columns, opinions, comments, reviews, art and photos but the website must not be used as a medium for marketing or selling to our members (unless posted in ufundoo Deals).</li>
              <li>No advertising or promotion is allowed except where it is for an event, publication or similar item that has direct relevance to the subject of discussion and is non-commercial in nature. Please bear in mind that the space on ufundoo is for creative contributions by members and not for personal advertising or promotion(s). Information about locating and sharing knowledge and expertise is welcomed, but must have relevance to the topic on which an opinion is expressed or a discussion is initiated.</li>
              <li>Do not post chain letters, promote pyramid schemes or recruit members for any "network marketing" or "multilevel marketing" businesses.</li>
              <li>Do not recruit members to join another website forum or chat group, whether personal or otherwise. If you have a suggestion for a new forum, please <a href="https://www.ufundoo.com/callUs.php" target="_blank">contact us</a> or send your queries to us by <a href="https://www.ufundoo.com/callUs.php" target="_blank">clicking here</a>.</li>
              <li>ufundoo.com encourages healthy discussion on the topics that members contribute to in forums and where relevant, members are allowed to post links to their content. However, overt promotion of, or inappropriate links to other known commercial websites on any forum are not permitted and will be removed. If you are unsure whether a link or a posting is allowed, please send your queries to us by <a href="https://www.ufundoo.com/callUs.php" target="_blank">clicking here</a>. The definition of an "appropriate link" is one that is relevant to both the posting and the forum topic. A "commercial" site is considered as one which promotes a product or service for a price or for other consideration that suggests it is a for-profit business.</li>
            </ul>
          </div>
          <h3>Copyrighted Material:</h3>
          <div class="description">
            <p>Uploading copies of copyrighted material as a whole or in part on ufundoo.com without explicit permission from the copyright owner shall be deemed to infringe on the copyright and this applies to materials such as text, images, music, movies, games, and other software in digital and analog format. ufundoo.com reserves the right to immediately delete such material without prior permission/consultation from the member. If you suspect that an image submitted to ufundoo.com is copyright protected and has been used fraudulently, please <a href="https://www.ufundoo.com/callUs.php" target="_blank">contact us</a> immediately by <a href="https://www.ufundoo.com/callUs.php" target="_blank">clicking here</a>.</p>
          </div>
          <h3>Safety Guidelines:</h3>
          <div class="description">
            <p>ufundoo.com is always reaching out to a wider audience by showcasing the best of our member contributions. However, in doing so, we will not allow anything to be uploaded on the site that we think could be potentially endangering or is inflammatory or embarrassing to our members. It also helps members to be aware of certain things in the interest of their safety.</p>
            <ul>
              <li>All members are responsible for the safety of their personal information. We recommend that members do not disclose their names, addresses, email addresses, telephone numbers or other personal information online.</li>
              <li>All members need to be aware that when they upload their photos to the site, the photos are not only visible to the entire internet community but can also be downloaded, since any image that can be displayed on a computer screen can be saved by the person viewing it. It is technically not possible to prevent this and hence ufundoo.com cannot be held liable in such cases. If you are worried about someone else viewing or saving your images then please do not post them.</li>
              <li>Only meet someone in person that you have met online once you have verified their identity and have been assured of your personal safety in any such meeting. ufundoo.com cannot be held liable for any such private meetings amongst its users.</li>
            </ul>
            <p>Legalities:</p>
            <ul>
              <li>All members are reminded that they may be held legally accountable for what they say or do online. In particular, members may be held liable for any defamatory comments, threats and/or untrue statements or other illegal and fraudulent claims made by them.</li>
              <li>ufundoo.com does not endorse the opinions expressed by its members.</li>
              <li>If any member fails to observe the above rules of conduct at any time, ufundoo.com reserves the right to terminate the membership of the member and/or delete all their contributions (either temporarily or permanently) from the site, depending on the nature and severity of the breach and without prior warning or consent of the member.</li>
            </ul>
            <p> Registration as a ufundoo.com member at any time - past or present and use of the ufundoo.com website and all its product and service offerings will be taken as acceptance of the above rules of conduct. ufundoo.com's decision on all matters is final and binding on its members. Legal jurisdiction for all disputes is in the state of New York, USA. </p>
            <p>ufundoo.com endeavours to foster a vibrant online community of South Asians worldwide by providing a platform with global reach to express themselves and connect with other people, thus forming mutually beneficial relationships. We are here to serve our members 24x7 and use all feedback to refine and improve the online experience for our members. </p>
            <p>You can reach TEAM ufundoo anytime by <a href="https://www.ufundoo.com/callUs.php" target="_blank">clicking here</a> and we will respond to you in a timely manner. We hope you have an enjoyable and rewarding experience on ufundoo. Welcome Aboard! </p>
          </div>
        </div>
      </div>
    </div>
    <!-- end here --> 
  </div>
  <!-- end here --> 
  
  <!-- footer -->
  <div class="footer">
    <?php include('include/footer.php'); ?>
  </div>
  <!-- end here --> 
</div>
<!-- end here -->
</body>
</html>