<?php
	session_start();
	require_once("config/conn.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UFundoo</title>
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/ufundoo.js"></script>
<style>
.headerBtn {
    color: #2e302d;
}
td{font-size:16px !important;}
.inputText{font-size:16px;color:#000;line-height:20px;outline:none;border:1px solid #9a9a9a;border-radius:25px;padding: 0;width:170px;height: 50px;padding: 0 15px;}
.inputText1{ border: 1px solid hsl(0, 0%, 60%);
    border-radius: 25px;
    color: hsl(0, 0%, 0%);
    font-size: 16px;
    height: 50px;
    line-height: 20px;
    outline: medium none;
    padding: 0 15px;
    width: 360px;}
</style>
<script>
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	if($("#signup-container").css('display')=="block")
	{
		signUp();
	}
	else if($("#login-container").css('display')=="block")
	{
		signIn();
	}
	else
	{
		career();
	}
}});
</script>
</head>

<body>
<!-- loader -->
<div class="loading" style="display:none">
	<?php 
    	include('loader.php');
    ?>
</div>
<!-- end here -->

<!-- forgot password -->
<div id="forgot_password">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class" style="width:400px;"> <img src="assets/img/btn_close.png" id="logo-close" onclick="fp_close()" height="20" width="20" style="top:10px; right:10px;" />
      <div style="text-align:center; margin-top:35px; font-size:20px; font-weight:bold">Please provide email address</div>
      <div style="text-align:center; margin-top:20px; font-family: calibri;"><input type="text" style="" placeholder="Email" class="fp_text" /></div>
      <div style="margin-top:15px; width:250px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="forgot_password()">Ok</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="fp_close()">Cancel</span> </div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- signup prompt -->
<div id="signup-container">
  <div class="signup-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeSignup()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Join UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:25px; height:395px;">
      	<div style="height:55px;width:100%;">
        	<input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; float:left; outline:none" placeholder="First Name" class="txtFirstName" />
            <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; margin-left:20px; float:left; outline:none" placeholder="Last Name" class="txtLastName" />
        </div>
      	<input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="username" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Password (Atleast 4 characters)" class="password" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Confirm Password" class="confirm_password" />
        <div style="margin-bottom:10px; width:370px; text-align:left">
            <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15"/><span style="margin-top:-3px;">I agree to Ufundoo's term of use and privacy policy.</span>
        </div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUp()" />
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
      	<div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Already member ?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;"><input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Sign In"  onclick="openLogin()"/></div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- login prompt -->
<div id="login-container">
  <div class="login-container-wrapper">
  	<div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeLogin()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Login UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:15px; height:405px;">
      	<input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmail" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpassword" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:25px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:8px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signIn()"/>
        <div style="margin-bottom:10px; width:370px; height:15px;">
           <span style="">OR</span>
        </div>
        <a href="googleLogin1.php?status=user" style="text-decoration:none;color:white"><input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
        <span onclick="FBLogin('user');"><input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
      	<div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Not a member yet?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;"><input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join Now"  onclick="openSignup()" /></div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- signup prompt -->
<div id="promoter-container">
  <div class="promoter-container-wrapper">
  	<div class="admin-delete-class-promoter"><img src="assets/img/btn_close.png" id="logo-close" onclick="closePromoter()" height="20" width="20" />
    
    	<!-- login -->
    	<div style="float:left; width:50%; height:520px; border-right:2px solid #ccc; margin-top:25px; text-align:center;">
        	<div style="text-align:center;font-size:35px; font-weight:bold">Already a Promoter?</div>
            <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Login</div>
            <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmailPost" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpasswordPost" />
            <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:75px;" onclick="fp_open()">Forgot Password?</div>
            <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signInPost()"/>
            <!--<div style="margin-bottom:10px;height:15px;">
               <span style="">OR</span>
            </div>
            <a href="googleLogin1.php?status=promoter" style="text-decoration:none;color:white"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
            <span onclick="FBLogin('promoter');"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>-->
        </div>
        <!-- end here -->
        
        <!-- sign up -->
        <div style="float:left; width:50%; height:520px; margin-top:25px; text-align:center">
        	<div style="text-align:center;font-size:35px; font-weight:bold">Become a Promoter?</div>
            <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Sign Up</div>
            <div>
            	<input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Firstname" class="firstname" />
                <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none; margin-left:4%;" placeholder="Lastname" class="lastname" />
            </div>
            <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Email" class="username_post" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Password (Atleast 4 characters)" class="password_post" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:15px; outline:none" placeholder="Confirm Password" class="confirm_password_post" />
            <div style="margin-bottom:10px; width:75%; margin-left:auto; margin-right:auto">
                <img src="assets/img/unchecked.png" name="fainMail" onclick="check_single(this)" status="0" class="fainMail_post" style="cursor: pointer; float: left; margin-top:3px;" height="15" id="fainMail"/>Send me FainMail on everything events and entertainment.
            </div>
            <div style="margin-bottom:10px; width:70%; margin-left:64px; text-align:left">
                <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree_post" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15" id="agree"/>I agree to Ufundoo's term of use and privacy policy.
            </div>
            <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUpPost()" />
        </div>
        <!-- end here -->
        
    </div>
  </div>
</div>
<!-- end here -->

	<!-- header -->
	<div class="header">
        <?php include('include/header.php'); ?>
    </div>
    <!-- end here -->
    
<!-- wrapper -->
<div class="wrapper">
    <!-- container -->
  	<div class="content">
    
    	<!-- main content -->
        <div style="display:table; min-height:100px; height:auto; width:1024px; margin-top:30px; margin-left:auto; margin-right:auto; margin-bottom:30px;">
        			<div style="display:table; margin-left:auto; margin-right:auto">
                    	<div style="font-size:30px; margin-bottom:15px; text-align:center">
                        	Jobs
                        </div>
                    	<table align="center">
                        	<tr>
                            	<td style="color:#000;">Full Name</td>
                            </tr>
                        	<tr>
                                <td style="padding-top:5px;"><input type="text" class="inputText" placeholder="First" id="cont_fname" style="margin-right: 10px;" /><input type="text" class="inputText" placeholder="Last" id="cont_lname" style="margin-left: 10px;"/></td>
                            </tr>
                            <tr>
                            	<td style="color:#000; padding-top:15px">Email Address</td>
                            </tr>
                            <tr>
                                <td style="padding-top:5px;"><input type="text" class="inputText1" placeholder="Email" id="cont_email"/></td>
                            </tr>
                            <tr>
                            	<td style="color:#000;padding-top:15px">Phone Number</td>
                            </tr>
                            <tr>
                                <td style="padding-top:5px;"><input type="text" class="inputText1" placeholder="Phone Number" id="cont_ph"/></td>
                            </tr>
                            <tr>
                            	<td style="color:#000;padding-top:15px">Which area are you interested in working on?</td>
                            </tr>
                            <tr>
                                <td style="color:#000;padding-top:5px"><input type="radio" name="area" value="technology" checked="checked"  style="margin-right: 10px;" />Technology</td>
                            </tr>
                            <tr>
                            	<td style="color:#000;padding-top:5px"><input type="radio" name="area" value="media"  style="margin-right: 10px;"/>Media</td>
                            </tr>
                            <tr>
                            	<td style="color:#000;padding-top:5px"><input type="radio" name="area" value="marketing"  style="margin-right: 10px;"/>Marketing</td>
                            </tr>
                            <tr>
                            	<td style="color:#000;padding-top:5px"><input type="radio" name="area" value="other" id="other"  style="margin-right: 10px;"/>Other</td>
                            </tr>
                            <tr id="other_text" style="display:none;">
                            	<td style="padding-top:5px">
                                	<input type="text" id="other_text_area" style="font-size:16px; color:#000; line-height:20px; outline:none; border:1px solid #9a9a9a; border-radius:25px; padding:25px; width:340px; height:8px; margin-left:17px;"/>
                                </td>
                            </tr>
                            <tr>
                            	<td style="color:#000;padding-top:15px">Why are you interested in joining ufundoo?</td>
                            </tr>
                            <tr>
                                <td style="padding-top:5px;"><textarea style="font-size:16px; color:#000;outline:none; border:1px solid #9a9a9a; border-radius:25px; padding:25px; width:360px; height:170px; resize:none; padding-top:10px; line-height:25px; font-family:helvetica" id="cont_why"></textarea></td>
                            </tr>
                            <tr>
                                <td align="center" style="padding-top:25px;"><div class="searchBtn" style="width:170px;font-size: 18px;padding: 8px;" onclick="career()">Submit</div></td>
                            </tr>
                            <tr>
                            	<td><div class="error" style="text-align: center;margin-top: 10px;"></div></td>
                            </tr>
                        </table>
                    </div>
        </div>
        <!-- end here -->
     </div>   
    <!-- end here -->
    
    <!-- footer -->
	<div class="footer">
        <?php include('include/footer.php'); ?>
    </div>
    <!-- end here -->
</div>
<!-- end here -->

</body>
</html>