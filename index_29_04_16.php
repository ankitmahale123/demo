<?php
	session_start();
	require_once("config/conn.php"); 
	$date=date('Y-m-d');
	$queryEvent=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,event.coverImage,organization.name as organizationName,location.name as locationName,ticketType.id as ticketTypeId, SUM(ticketStatus.availableQty) as totalNumber from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId inner join ticketStatus on ticketStatus.eventId=event.id where event.status='approve' and event.date>='".$date."' GROUP BY event.id ORDER BY event.date DESC LIMIT 4");
	
	$queryNewsNo=mysqli_query($mysqli,"select * from newsUpdates ORDER BY id DESC");
	$noOfNews=mysqli_num_rows($queryNewsNo);
	$queryNews=mysqli_query($mysqli,"select * from newsUpdates ORDER BY id DESC LIMIT 2");
	$querySliderImage=mysqli_query($mysqli,"select image from featuredImage where status='1'");
	$noOfSlides=mysqli_num_rows($querySliderImage);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Ufundoo | Home</title>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="https://ufundoo.com" />
<meta name="twitter:title" content="Small Island Developing States Photo Submission" />
<meta name="twitter:description" content="View the album on Flickr." />
<meta name="twitter:image" content="https://ufundoo.com/img.png" />
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/main.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<link rel="stylesheet" href="assets/css/datepicker.css" type="text/css" />
<script>
window.fbAsyncInit = function() {
		FB.init({
		appId      : '705873652868650', // replace your app id here
		channelUrl : '', 
		status     : true, 
		cookie     : true, 
		xfbml      : true  
		});
	};
	(function(d){
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	}(document));
	
	function FBLogin(type){
		FB.login(function(response){
			if(response.authResponse){
				window.location.href = "actions.php?status="+type;
			}
		}, {scope: 'email,user_likes'});
	}
</script>
</head>

<body>
<!-- loader -->
<div class="loading" style="display:none">
	<?php 
    	include('loader.php');
    ?>
</div>
<!-- end here -->

<!-- subscribe already -->
<div id="subscribe_already">
	<img src="assets/img/subscribed1.png"/>
</div>
<!-- end here -->

<!-- subscribe new -->
<div id="subscribe_new">
	<img src="assets/img/subscribed.png"/>
</div>
<!-- end here -->

<!-- forgot password -->
<div id="forgot_password">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class" style="width:400px;"> <img src="assets/img/btn_close.png" id="logo-close" onclick="fp_close()" height="20" width="20" style="top:10px; right:10px;" />
      <div style="text-align:center; margin-top:35px; font-size:20px; font-weight:bold">Please provide email address</div>
      <div style="text-align:center; margin-top:20px; font-family: calibri;"><input type="text" style="" placeholder="Email" class="fp_text" /></div>
      <div style="margin-top:15px; width:250px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="forgot_password()">Ok</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="fp_close()">Cancel</span> </div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- signup prompt -->
<div id="signup-container">
  <div class="signup-container-wrapper">
    <div class="admin-delete-class"><img src="assets/img/btn_close.png" id="logo-close" onclick="closeSignup()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Join UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:25px; height:395px;">
      	<div style="height:55px;width:100%;">
        	<input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; float:left; outline:none" placeholder="First Name" class="txtFirstName" />
            <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; margin-left:20px; float:left; outline:none" placeholder="Last Name" class="txtLastName" />
        </div>
      	<input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="username" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Password (Atleast 4 characters)" class="password" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Confirm Password" class="confirm_password" />
        <div style="margin-bottom:10px; width:370px; text-align:left">
            <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15"/><span style="margin-top:-3px;">I agree to Ufundoo's term of use and privacy policy.</span>
        </div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUp()" />
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
      	<div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Already member ?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;"><input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Sign In"  onclick="openLogin()"/></div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- login prompt -->
<div id="login-container">
  <div class="login-container-wrapper">
  	<div class="admin-delete-class">
      <?php if($_REQUEST['reset']=="yes"){?>
      <img src="assets/img/btn_close.png" id="logo-close" onclick="window.open('index.php','_self')" height="20" width="20" />
      <?php } else { ?>
      <img src="assets/img/btn_close.png" id="logo-close" onclick="closeLogin()" height="20" width="20" />
      <?php } ?>
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Login UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:15px; height:405px;">
      	<input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmail" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpassword" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:25px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:8px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signIn()"/>
        <div style="margin-bottom:10px; width:370px; height:15px;">
           <span style="">OR</span>
        </div>
        <a href="googleLogin1.php?status=user" style="text-decoration:none;color:white"><input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
        <span onclick="FBLogin('user');"><input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
      	<div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Not a member yet?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;"><input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join Now"  onclick="openSignup()" /></div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- signup prompt -->
<div id="promoter-container">
  <div class="promoter-container-wrapper">
  	<div class="admin-delete-class-promoter">
      <?php if($_REQUEST['reset']=="yes"){?>
      <img src="assets/img/btn_close.png" id="logo-close" onclick="window.open('index.php','_self')" height="20" width="20" />
      <?php } else { ?>
      <img src="assets/img/btn_close.png" id="logo-close" onclick="closePromoter()" height="20" width="20" />
      <?php } ?>
    	<!-- login -->
    	<div style="float:left; width:50%; height:520px; border-right:2px solid #ccc; margin-top:25px; text-align:center;">
        	<div style="text-align:center;font-size:35px; font-weight:bold">Already a Promoter?</div>
            <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Login</div>
            <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmailPost" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpasswordPost" />
            <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:75px;" onclick="fp_open()">Forgot Password?</div>
            <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signInPost()"/>
            <!--<div style="margin-bottom:10px;height:15px;">
               <span style="">OR</span>
            </div>
            <a href="googleLogin1.php?status=promoter" style="text-decoration:none;color:white"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
            <span onclick="FBLogin('promoter');"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>-->
        </div>
        <!-- end here -->
        
        <!-- sign up -->
        <div style="float:left; width:50%; height:520px; margin-top:25px; text-align:center">
        	<div style="text-align:center;font-size:35px; font-weight:bold">Become a Promoter?</div>
            <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Sign Up</div>
            <div>
            	<input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Firstname" class="firstname" />
                <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none; margin-left:4%;" placeholder="Lastname" class="lastname" />
            </div>
            <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Email" class="username_post" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Password (Atleast 4 characters)" class="password_post" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:15px; outline:none" placeholder="Confirm Password" class="confirm_password_post" />
            <div style="margin-bottom:10px; width:75%; margin-left:auto; margin-right:auto">
                <img src="assets/img/unchecked.png" name="fainMail" onclick="check_single(this)" status="0" class="fainMail_post" style="cursor: pointer; float: left; margin-top:3px;" height="15" id="fainMail"/>Send me FainMail on everything events and entertainment.
            </div>
            <div style="margin-bottom:10px; width:70%; margin-left:64px; text-align:left">
                <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree_post" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15" id="agree"/>I agree to Ufundoo's term of use and privacy policy.
            </div>
            <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUpPost()" />
        </div>
        <!-- end here -->
        
    </div>
  </div>
</div>
<!-- end here -->

<div class="wrapper">
	<div class="row" style="margin:0px;">
    	<div style="padding:0px; height:100%; width:100%;">
        	<!-- slider container -->
        	<div class="sliderContainer">
            	<!--slider-->
                <div class="slider">
                    <div class="jquery-reslider">
                    	<?php while($resultSliderImage=mysqli_fetch_assoc($querySliderImage)){ ?>
                        <div class="slider-block" data-url="<?php echo $resultSliderImage["image"]; ?>"></div>
    					<?php } ?>
                        <div class="slider-dots"><ul></ul></div>
                    </div>
                </div>
                <!--end slider-->
            </div>
            
            <!-- black overlay -->
            <div class="overlaySlider"></div>
            <!--end slider-->
            
            <!-- end slider container -->
            
            <!-- header -->
            <div style="height:100px; width:100%; top:0;left:0; position:absolute; z-index:100; display:table">
            	<div style="margin-left:15px;margin-top:15px; display:inline-block"><img src="assets/img/logo.png" onclick="window.open('index.php','_self')" style="cursor:pointer" /></div>
                <div style="display: inline-block;right: 0;float: right;margin-top: 33px;" align="right">
                	<?php if((!isset($_SESSION["userId"])) && (!isset($_SESSION["promoterId"]))){ ?>
                	<div class="headerBtn" onclick="openPromoter()">PROMOTER</div>
                    <div class="headerBtn" onclick="openSignup()">SIGN UP</div>
                    <div class="headerBtn" onclick="openLogin()">LOGIN</div>
                    <?php } else { 
						if(isset($_SESSION["userId"]) || isset($_SESSION["userEmail"])){
					?>
                    <div class="headerBtn">Hi, <span style="margin-left:5px;"><?php echo strtoupper($_SESSION["username"]); ?></span></div>
                    <?php } else { ?>
                    <div class="headerBtn">Hi, <span style="margin-left:5px;"><?php echo strtoupper($_SESSION["username"]); ?></span></div>
                    <div class="headerBtn" onclick="window.open('Promoter/promoterDashboard.php','_self')">DASHBOARD</div>
                    <div class="headerBtn" onclick="window.open('uploadEvent.php','_self')">POST EVENT</div>
					<?php }?>
                    <div class="headerBtn" onclick="window.open('logout.php','_self');">LOGOUT</div>
                    <?php } ?>
                </div>
            </div>
            <!-- end header -->
            
            <!-- search bar -->
            <div class="searchBar">
            	<div class="searchBarContainer">
                	<div class="searchBarWrapper">
                    	<div class="inheritHeight searchLocation">
                        	<input type="text" class="searchBox inheritHeight search-location" placeholder="Search for event or location" />
                        </div>
                        <div class="inheritHeight" style=" width:35%; display:inline-block; padding-left:1%;">
                          	<input type="text" class="searchBox inheritHeight search-date" id="dp1"  placeholder="All dates"/>
                        </div>
                        <div class="inheritHeight searchBtnWrapper">
                        	<div class="searchBtn inheritHeight" onclick="searchData('searchBtn','')">Search</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end search bar -->
            
            <!-- popular events -->
            <div class="popularEvents" id="popularEvents">
            
            	<div align="center" class="mostPopEvents">MOST POPULAR EVENTS</div>
                <div align="center" class="mostPopEventsLine"></div>
                <div class="eachBlockWrapper">
                	<!-- each block -->
                    <?php 
						if(mysqli_num_rows($queryEvent)>0){ 
						while($result=mysqli_fetch_assoc($queryEvent)){
						$time_bt = date_create($result['startTime']);	
						$date_bt = date_create($result['eventDateFb']);	
					?>
                	<div class="inheritHeight eachBlockWrapperCont">
                    	<div class="eachBlock inheritHeight">
                        <?php if($result['ticketTypeId']=="2"){ ?>
                        <div style="position: absolute;color: #fff; text-align: center; padding: 1px;background: rgba(255,255,255,0.5);border-radius: 12px; width: 55px; margin-top: 5px;margin-left: 5px; font-size:12px;">FREE</div>
                        
                        <?php } ?>
                        	<?php
							//$queryImage=mysqli_query($mysqli,"select * from eventImage where eventImage.eventId='".$result['id']."' ORDER BY id LIMIT 1");
							//if(mysqli_num_rows($queryImage)>0){
							$resultImage=mysqli_fetch_assoc($queryImage);
								//if($resultImage['path']!='')
								if($result['coverImage']!='')
								{
									//$fbimage=$resultImage['path'];
									$fbimage=$result['coverImage'];
									$cover=$result['coverImage'];
								}
								else
								{
									$fbimage='https://ufundoo.com/assets/img/no_image.png';
									$cover='https://ufundoo.com/assets/img/no_image.png';
								}
							?>
                           	<img src="<?php echo $cover; ?>" height="247" style="width:100%;background: #E1E1E1; cursor:pointer;border: 1px solid #e1e1e1;" onclick="window.open('eventDetail.php?eventId=<?php echo $result['id'];?>&fbimage=<?php echo $fbimage; ?>','_self');"/>
                            <?php //} else { ?>
                            <!--<img src="https://ufundoo.com/assets/img/no_image.png" height="247" style="width:100%;background: #E1E1E1; cursor:pointer;border: 1px solid #e1e1e1;" onclick="window.open('eventDetail.php?eventId=<?php echo $result['id'];?>&fbimage=<?php echo $fbimage; ?>','_self');"/>-->
                            <?php //} ?>
                            <div class="eventDataWrapper">
                            	<div style="display:table-row">
                                	<div class="eventData">
                                        	<div style="display:inline-block; width:60%; height:65px; vertical-align:top;">
                                            	<div style="font-weight:600;cursor:pointer; height:65px;" class="eventDataEach" onclick="window.open('eventDetail.php?eventId=<?php echo $result['id'];?>&fbimage=<?php echo $fbimage; ?>','_self');" title="<?php echo $result['eventName']; ?>" ><?php echo $result['eventName']; ?></div>
                                            </div>
                                            <div style="display:inline-block; width:39%; height:65px;vertical-align:top;">
                                            	<div class="bookNowWrapper">
                                                		<?php if($result['totalNumber']!=0){?>
														<?php if($result['ticketTypeId']=="2"){ ?>
                                                        <div align="center" class="buyNow" onclick="window.open('seatChart.php?eventId=<?php echo $result['id']; ?>&eventName=<?php echo urlencode($result['eventName']); ?>&eventDate=<?php echo date_format($date_bt, 'M jS, Y'); ?>&eventTime=<?php echo date_format($time_bt, 'G:i a'); ?>&locationName=<?php echo urlencode($result['locationName']); ?>&seatChart=<?php echo $result['seatChart']; ?>&ticketTypeId=<?php echo $result['ticketTypeId']; ?>','_self');">BOOK NOW</div>
                                                        <?php } else { ?>
                                                        <div align="center" class="buyNow" onclick="window.open('seatChart.php?eventId=<?php echo $result['id']; ?>&eventName=<?php echo urlencode($result['eventName']); ?>&eventDate=<?php echo date_format($date_bt, 'M jS, Y'); ?>&eventTime=<?php echo date_format($time_bt, 'G:i a'); ?>&locationName=<?php echo urlencode($result['locationName']); ?>&seatChart=<?php echo $result['seatChart']; ?>&ticketTypeId=<?php echo $result['ticketTypeId']; ?>','_self');">BUY TICKET</div>
                                                    	<?php }}else { ?>
                                                        <div align="center" class="buyNow" onclick="window.open('eventDetail.php?eventId=<?php echo $result['id'];?>&fbimage=<?php echo $fbimage; ?>','_self');">SOLD OUT</div>
                                                        <?php } ?>
                                            	</div>
                                        </div>
                                        <div style="margin-top:7px; display:inline-block; width:100%;" class="eventDataEach"><?php echo date_format($date_bt, 'M jS,y').' '.date_format($time_bt, 'G:i a'); ?></div>
                                        <div style="margin-top:5px; display:inline-block; width:100%; height:42px;" class="eventDataEach"><?php echo $result['locationName'];?></div>
                                    </div>
                                </div>
                                <div style="display:table-row">
                                	<div class="iconsWrapper" align="right">
                                    	<div align="right" class="eachIcon">
                                        	<a href="https://www.facebook.com/dialog/feed?app_id=705873652868650&redirect_uri=https://ufundoo.com&link=https://ufundoo.com&picture=<?php echo $fbimage; ?>&caption=<?php echo $result['eventName']; ?>&description= Date : <?php echo $result['eventDateFb']?><center></center> Location : <?php echo $result['locationName'];?>" onclick="window.open(this.href); return false;">
                                                <img src="assets/img/mostp_fb.png" title="Facebook"/>
                                            </a>
                                        </div>
                                        <div align="right" class="eachIcon">
                                        	<a href="http://twitter.com/share?text=Ufundoo - <?php echo $result['eventName']; ?>%0aDate : <?php echo $result['eventDateFb']?>%0aLocation : <?php echo $result['locationName'];?>" onclick="window.open(this.href); return false;">
                                            	<img src="assets/img/mostp_tweet.png" title="Twitter" />
                                            </a>
                                        </div>
                                        <div align="right" class="eachIcon">
                                        	<a href="http://pinterest.com/pin/create/button/?url=https://ufundoo.com&media=<?php echo $fbimage; ?>&description=Event Name : <?php echo $result['eventName']; ?>%0aDate : <?php echo $result['eventDateFb']?>%0aLocation : <?php echo $result['locationName'];?>" onclick="window.open(this.href); return false;">
                                            	<img src="assets/img/mostp_pin.png" title="Pin It" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php }} else { ?>
                    <div align="center" style="margin-top:30px; color:#000">No Data Found</div>
                    <?php } ?>
                    <!-- end each block -->
                </div>
                <div align="center" class="seeMoreBtnWrapper">
                	<div align="center" class="seeMoreBtn" onclick="searchData('seemore','')">See More</div>
                </div>
            </div>
            <!-- end popular events -->
            
            <!-- browse events -->
            <div class="browseEvents" id="browseEvents">
            	<div align="center" class="mostPopEvents">BROWSE EVENTS</div>
                <div align="center" class="mostPopEventsLine"></div>
                <!-- grid layout first row -->
                <div class="eachBlockBrowseWrapper">
                	<div class="inheritHeight eventTypes">
                    	<div class="eachBlockBrowse inheritHeight" style="background-image:url(assets/img/event_all.png);" onclick="searchData('event','all')"></div>
                        <p class="eachBlockBrowseText">ALL</p>
                    </div>
                    <div class="inheritHeight eventTypes">
                    	<div class="eachBlockBrowse inheritHeight" style="background-image:url(assets/img/event_movies.png);" onclick="searchData('event','Movie')"></div>
                        <p class="eachBlockBrowseText">MOVIE</p>
                    </div>
                    <div class="inheritHeight eventTypes" style="width:25%;">
                    	<div class="eachBlockBrowse inheritHeight" style="background-image:url(assets/img/event_event.png);" onclick="searchData('event','Event')"></div>
                        <p class="eachBlockBrowseText">EVENT</p>
                    </div>
                    <div class="inheritHeight eventTypes" style="width:33%;">
                    	<div class="eachBlockBrowse inheritHeight" style="background-image:url(assets/img/event_dance.png);" onclick="searchData('event','Dance')"></div>
                        <p class="eachBlockBrowseText">DANCE</p>
                    </div>
                    <div class="inheritHeight eventTypes" style="width:10%;">
                    	<div class="eachBlockBrowse inheritHeight" style="background-image:url(assets/img/event_drama.png);" onclick="searchData('event','Drama')"></div>
                        <p class="eachBlockBrowseText">DRAMA</p>
                    </div>
                </div>
                <!-- end grid layout first row -->
                
                <!-- grid layout second row -->
                <div class="eachBlockBrowseWrapper" style="margin-top:5px;">
                	<div class="inheritHeight eventTypes" style="width:25%;">
                    	<div class="eachBlockBrowse inheritHeight" style="background-image:url(assets/img/event_festival.png);" onclick="searchData('event','Festival')"></div>
                        <p class="eachBlockBrowseText">FESTIVAL</p>
                    </div>
                    <div class="inheritHeight eventTypes" style="width:10%;">
                    	<div class="eachBlockBrowse inheritHeight" style="background-image:url(assets/img/event_spiritual.png);" onclick="searchData('event','Spiritual')"></div>
                        <p class="eachBlockBrowseText">SPIRITUAL</p>
                    </div>
                    <div class="inheritHeight eventTypes">
                    	<div class="eachBlockBrowse inheritHeight" style="background-image:url(assets/img/event_cultural.png);" onclick="searchData('event','Cultural')"></div>
                        <p class="eachBlockBrowseText">CULTURAL</p>
                    </div>
                    <div class="inheritHeight eventTypes">
                    	<div class="eachBlockBrowse inheritHeight" style="background-image:url(assets/img/event_exhibit.png);" onclick="searchData('event','Exhibit')"></div>
                        <p class="eachBlockBrowseText">EXHIBIT</p>
                    </div>
                    <div class="inheritHeight eventTypes" style="width:33%;">
                    	<div class="eachBlockBrowse inheritHeight" style="background-image:url(assets/img/event_sport.png);" onclick="searchData('event','Sport')"></div>
                        <p class="eachBlockBrowseText">SPORT</p>
                    </div>
                </div>
                <!-- end grid layout second row -->
            </div>
            <!-- end browse events -->
            
            <!-- share page strip -->
            <div class="sharePage">
               	<div class="sharePageTitle">SHARE THIS PAGE WITH YOUR FRIENDS</div>
                <div class="shareIconWrapper">
                	<a href="https://www.facebook.com/dialog/feed?app_id=705873652868650&redirect_uri=https://ufundoo.com&link=https://ufundoo.com&picture=https://ufundoo.com/assets/img/logo.png&caption=A brand new entertainment&description=A brand new entertainment experience is coming soon." onclick="window.open(this.href); return false;">
                    	<img src="assets/img/share_fb.png" title="Facebook" />
                    </a>
                    <a href="http://twitter.com/share?text=A brand new entertainment experience is coming soon%0a&url=https://ufundoo.com" onclick="window.open(this.href); return false;">
                    	<img src="assets/img/share_tweet.png" title="Twitter"/>
                    </a>
                    <a href="http://pinterest.com/pin/create/button/?url=https://ufundoo.com&media=https://ufundoo.com/assets/img/logo.png&description=Ufundoo - A brand new entertainment experience is coming soon" onclick="window.open(this.href); return false;">
                    	<img src="assets/img/share_pin.png" title="Pin It" />
                    </a>
                </div>
            </div>
            <!-- end share page strip -->
             
            <!-- testimonials -->
            <div class="testimonials">
            	<div align="center" class="testimonialTitle">NEWS</div>
                <div align="center" class="testimonialLine"></div>
                <div class="testimonialBlock">
                	<?php 
						$i=0; 
						while($result=mysqli_fetch_assoc($queryNews)){ 
						$i++;
						if($i%2==0){ 
					?>
                    <div style="background:#ee534f; margin-left:3.5%;" class="testimonialBlockEach">
                    <?php } else { ?>
                    <div style="background:#01bfa5;" class="testimonialBlockEach">
                    <?php } ?>
                    	<div style="display:inline-block; width:24%; margin-left:2%; float:left; height:100%; padding-top:30px; text-align:center">
                        	<img src="assets/img/testimonial_quotes.png" />
                        </div>
                        <div style="display:inline-block; width:65%; margin-left:3%;margin-right:6%; float:left; height:100%;padding-top:50px; padding-bottom:30px; color:#fff; font-size:18px; line-height:25px; text-align:justify;"><?php echo $result['content']; ?></div>
                    </div>
                    <?php } ?>
                    <?php if($noOfNews>2){ ?>
                    <div align="center" class="seeMoreBtnWrapper">
                        <div align="center" class="seeMoreBtn" onclick="window.open('news.php','_self')" style="border: 2px solid #fff;color: #fff;">See More</div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <!-- end testimonials -->
            
            <!-- footer -->
            <div class="footer">
            	<!-- previous footer -->
            	<div class="footerCont" style="display:none;">
                    <?php if(!isset($_SESSION["userId"])){ ?>
                    <div class="footerEach" id="promoter_footer_tab">
                    	<div align="left">
                            <div align="left" class="footerTitle">PROMOTER</div>
                            <div align="left" class="footerLine"></div>
                        </div>
                        <div class="footerEachData">
                        	<?php if(isset($_SESSION["promoterId"])){ ?>
                           	<div onclick="window.open('uploadEvent.php','_self')">Post events</div>
                            <?php } else { ?>
                            <div onclick="openPromoter()">Post events</div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="footerEach">
                    	<div align="left">
                            <div align="left" class="footerTitle">ABOUT US</div>
                            <div align="left" class="footerLine" style="margin-left:10px;"></div>
                        </div>
                        <div class="footerEachData">
                           	<div onclick="window.open('callUs.php','_self')">Feedback</div>
                            <div onclick="window.open('career.php','_self')">Jobs</div>
                        </div>
                    </div>
                    <div style="width:20%" class="footerEach">
                    	<div align="left">
                            <div align="left" class="footerTitle">CONTACT US</div>
                            <div align="left" class="footerLine" style="margin-left:20px;"></div>
                        </div>
                        <div class="footerEachData">
                           	<div style="line-height: 25px;"><img src="assets/img/contactus_phone.png"/>+1-510-818-0468</div>
                            <div style="line-height: 30px;"><img src="assets/img/contactus_location.png"/>Fremont, CA</div>
                        </div>
                    </div>
                    <div class="footerEach">
                    	<div align="left">
                            <div align="left" class="footerTitle">FOLLOW US</div>
                            <div align="left" class="footerLine"></div>
                        </div>
                        <div class="footerEachData">
                           	<a href="https://www.facebook.com/dialog/feed?app_id=705873652868650&redirect_uri=https://ufundoo.com&link=https://ufundoo.com&picture=https://ufundoo.com/assets/img/logo.png&caption=A brand new entertainment&description=A brand new entertainment experience is coming soon." onclick="window.open(this.href); return false;">
                           		<img src="assets/img/mostp_fb.png" title="Facebook"/>
                            </a>
                            <a href="http://twitter.com/share?text=A brand new entertainment experience is coming soon%0a&url=https://ufundoo.com" onclick="window.open(this.href); return false;">
                            	<img src="assets/img/mostp_tweet.png" title="Twitter"/>
                            </a>
                            <a href="http://pinterest.com/pin/create/button/?url=https://ufundoo.com&media=https://ufundoo.com/assets/img/logo.png&description=Ufundoo - A brand new entertainment experience is coming soon" onclick="window.open(this.href); return false;">
                            	<img src="assets/img/mostp_pin.png" style="margin-right:0px;" title="Pin It"/>
                            </a>
                        </div>	
                    </div>
                    <div style="width:34%;padding-top:25px;" class="footerEach">
                    	<div align="center" class="footerTitle" style="font-size:23px;">Get updates for all events</div>
                        <div style="font-size:10px; color:#fff">Subscribe here</div>
                        <div class="subscribeCont">
                        	<div class="inputSubscribeWrapper">
                            	<input type="text" class="subscribeInput" placeholder="Enter email ID here" />
                            </div>
                            <div class="subscribeBtn" onclick="subscribe()">SUBMIT</div>
                        </div>
                    </div>
                </div>
                <!-- End -->
                 <?php include('include/footer.php'); ?>
            </div>
            <!-- end footer -->
        </div>
    </div>
</div>
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/jquery.reslider.js"></script>
<script src="assets/js/ufundoo.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>

<script>
var windowHeight=$(window).height();
var windowWidth=$(window).width();
var noOfNews='<?php echo $noOfNews; ?>';
var sliderHeight=0;
var resetLink='';
var resetRole='';
resetLink='<?php echo $_REQUEST['reset'];?>';
resetRole='<?php echo $_REQUEST['resetRole'];?>';
$(document).ready(function(e) {
	if(resetLink=="yes")
	{
		if(resetRole=="user")
		{
			openLogin();
		}
		else
		{
			openPromoter();
		}
	}
	if(noOfNews==0)
	{
		$('.testimonials').css('display','none');
	}
	/* disable previous date */
	var date = new Date();
	date.setDate(date.getDate());
	$('#dp1').datepicker({
		startDate: date,
		format: 'yyyy-mm-dd'
	});
	/* hide datepicker on date select */
	$('#dp1').on('changeDate', function(){
		$(this).datepicker('hide');
	});
	
	sliderHeight=windowHeight-150;
    $('.sliderContainer').css('height',sliderHeight+'px');
	$('.slider .jquery-reslider .slider-block').css('height',sliderHeight+'px');
	$('.slider .jquery-reslider .slider-dots').css('margin-top',(sliderHeight-50)+'px');
	$('.overlaySlider').css('height',sliderHeight+'px');
	$('#sliderContent').css('margin-top',(sliderHeight/2.5)+'px');
});
	$(function(){
    	$('.jquery-reslider').reSlider({
        	speed:1000,
            delay:5000,
            imgCount:<?php echo $noOfSlides; ?>,
            dots:true,
            autoPlay:true
        });
    });
function switchRole(_this)
{
	$('.testRole').removeClass('testimonialOptActive');
	$('.testRole').addClass('testimonialOpt');
	$(_this).removeClass('testimonialOpt');
	$(_this).addClass('testimonialOptActive');
}
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	if($("#signup-container").css('display')=="block")
	{
		signUp();
	}
	if($("#login-container").css('display')=="block")
	{
		signIn();
	}
}});
</script>
</body>
</html>