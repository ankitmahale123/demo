<?php
session_start(); 
require_once("config/conn.php");
require_once('assets/resource/pdf/html2pdf.class.php');

$date=date('Y-m-d H:i:s');
if(!isset($_SESSION["userId"]) || !isset($_SESSION["promoterId"]))
{
	header('Location : index.php');
}
if(isset($_REQUEST["gusetUserEmail"]) && $_REQUEST["gusetUserEmail"]!='')
{
	$email=$_REQUEST["gusetUserEmail"];
	$msg='Dear Guest';
}
else
{
			if(isset($_SESSION["userEmail"]))
			{
				$userTypeId=$_SESSION["userId"];
				$email=$_SESSION["userEmail"];
			}
			else
			{
				$userTypeId=$_SESSION["promoterId"];
				$email=$_SESSION["promoterEmail"];
			}
			$str=(explode(" ",$_SESSION['username']));
			$msg='Dear '.ucfirst($str[0]);
}
if($_REQUEST['ticketType']=='free')
{
	$queryEvent=mysqli_query($mysqli,"select event.name as eventName,event.date as date,event.startDateTime as startTime,event.description as description,event.coverImage as coverImage,event.seatChart as seatChart,organization.name as orgName,organization.contactNo as orgContact, location.name as locationName,ticketType.id as ticketTypeId,profile.firstName as firstName,promoter.email as promoterEmail,eventType.name as eventTypeName from event inner join promoter on promoter.id=event.promoterId inner join profile on profile.id=promoter.profileId inner join organization on organization.id=event.organizationId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId inner join eventType on eventType.id=event.eventTypeId where event.id='".$_REQUEST['eventId']."'");
	$result=mysqli_fetch_assoc($queryEvent);
	$time_bt = date_create($result['startTime']);	
	$date_bt = date_create($result['eventDateFb']);	
	$totalTicket=$_REQUEST['totalTicket'];
	//$email=$_REQUEST["gusetUserEmail"];
	
	/* generate pdf */
	$content='<style>
	td{border:1px solid #ccc;}
	td>div{}
	</style>
	<div style="width:100%; text-align:center; margin-left:auto; margin-right:auto; margin-top:10px; margin-bottom:20px;">
		<a href="https://ufundoo.com" target="_blank"><img src="assets/img/logo_pdf.png" style="cursor:pointer"></a>
	</div>
	<table style="width:100%; height:300px;" cellpadding="0" cellspacing="0">
	  <tr>
		<td style="width:75%; height:25px; vertical-align:top; max-height:25px;" colspan="2">
			<div style="color:#ccc; max-height:10px;">Event</div>
			<div style="color:#ccc; max-height:15px; font-size:20px; color:#000; font-weight:bold;">'.$result['eventName'].'</div>
		</td>
		<td rowspan="2" style="width:25%; height:50px; max-height:50px;vertical-align:top; ">
			<div style="color:#ccc; max-height:10px; margin-top:10px;">Name</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold; margin-top:10px; text-align:center">'.$_SESSION['username'].'</div>
			<div style="color:#ccc; max-height:10px; margin-top:10px;">Event Type</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold; margin-top:10px; text-align:center">'.$result['eventTypeName'].'</div>
		</td>
	  </tr>
	  <tr>
		<td style="width:37%; height:50px;border:none; border-right:1px solid #ccc; border-left:1px solid #ccc; max-height:50px;vertical-align:top;">
			<div style="color:#ccc; max-height:10px;">Date + Time</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold;">'.date_format($date_bt, 'M jS, Y').' - '.date_format($time_bt, 'h:i a').'</div>
		</td>
		<td style="width:37%; height:50px;border:none; max-height:50px; vertical-align:top; ">
			<div style="color:#ccc; max-height:10px;">Order Info</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold;">Order No. # '.$_REQUEST['orderNo'].'</div>
		</td>
	  </tr>
	  <tr>
		<td style="width:75%; height:50px; max-height:50px;vertical-align:top; " colspan="2">
			<div style="color:#ccc; max-height:15px;">Location</div>
			<div style="color:#ccc; max-height:35px; font-size:20px; color:#000; font-weight:bold;">'.$result['locationName'].'</div>
		</td>
		<td rowspan="2" style="width:25%; height:100px; max-height:100px;vertical-align:top; ">
			<div style="color:#ccc; max-height:10px; margin-top:10px;">Order Status</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold; margin-top:10px;text-align:center">Free Order</div>
			<div style="color:#ccc; max-height:10px;margin-top:10px;">Total Ticket(s)</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold; margin-top:10px;text-align:center">'.$totalTicket.'</div>
		</td>
	  </tr>
	  <tr>
		<td style="width:75%; height:50px; max-height:50px;vertical-align:top; " colspan="2">
			<div style="color:#ccc; max-height:15px;">Event Organization</div>
			<div style="max-height:35px; font-size:20px; color:#000; font-weight:bold;">'.$result['orgName'].'</div>
		</td>
	  </tr>
	</table>';
	
	unlink('pdf/'.$_REQUEST['orderNo'].'-ticket.pdf');
	   try
		{
			$html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', array(5, 5, 5, 5));
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->writeHTML($content);
		
			$html2pdf->Output('pdf/'.$_REQUEST['orderNo'].'-ticket.pdf',true);
			$array["pdfLink"]=curPageURL().'/pdf/'.$_REQUEST['orderNo'].'-ticket.pdf';
		}
		catch(HTML2PDF_exception $e) {
			exit;
		}
	/* end */
	
			/* mail */
								include("config/configEmail.php");
								$to_addresss1=$email;
								$to_addresss2="sales@ufundoo.com";
								
								$body1 =  '<div style="height:100%;width:100%;font-family: calibri;" align="left">
    									   <div style="width:644px; padding:0px; text-align:left; height:auto; min-height:100px;">
										   <div style="height:5px; width:644px; background-color:#ed258f;display: inline-block;"></div>
										   <div><img src="https://ufundoo.com/assets/img/logo.png" onclick="window.open("index.php","_self")" style="cursor:pointer"></div>
										   <div style="font-family: calibri;font-size:15px;">'.$msg.',<br/><br/>
										   This is your order confirmation for '.$result['eventName'].'.<br/><br/>
										   <b>Total purchased tickets : </b>'.$totalTicket.'<br/><br/>
										   <table style="width:644px;font-family: calibri; margin-bottom:15px;" border="1" bordercolor="#e4e4e4" cellpadding="10" cellspacing="0">
										   <tr>
										   	<td style="width:644px;font-family: calibri;">
										   <b>Event details :</b><br/>
										   Name : '.$result['eventName'].'<br/>
										   Date : '.date_format($date_bt, 'M jS, Y').'<br/>
										   Time : '.date_format($time_bt, 'h:i a').'<br/>
										   Venue : '.$result['locationName'].'<br/>
										   Event Organizers : '.$result['orgName'].'<br/>';
										   if($result['orgContact']!='') {
								$body1 .=  'Contact Organizers : '.$result['orgContact'].'<br/>';
										   }
								$body1 .=  '</td></tr></table>
								 			Thanks,<br/>Ufundoo Team</div>
											<div style="margin-top:15px; display:table;width:606px; height:85px !important;background:url(https://ufundoo.com/assets/img/email_bottom.png);">
										   	<div style="display:table-cell; width:450px;vertical-align:middle;">
										   	<div style="color:#4e4e4e; font-size:15px;margin-left: 110px; padding-top: 5px;">Still have a question? Call us on +1-510-818-0468</div>
											<div style="font-size:25px;margin-left: 155px; padding-top: 5px; font-weight:bold;cursor:pointer; "><a href="https://ufundoo.com" style="text-decoration:none;color:#ed258f;" target="_blank">www.ufundoo.com</a></div>
											</div>
											<div style="display:table-cell; width:190px; height:85px;vertical-align:middle;">
												<div style="display:table;">
													<div style="display:table-cell; width:50px; height:85px; vertical-align:middle;">
														<a href="https://www.facebook.com/dialog/feed?app_id=705873652868650&amp;redirect_uri=https://ufundoo.com&amp;link=https://ufundoo.com&amp;picture=https://ufundoo.com/fileupload/server/php/files/151224-150836.jpg&amp;caption=DanceSport Classic Summer Sizzler&amp;description= Date : 2016-03-31&lt;center&gt;&lt;/center&gt; Location : " onclick="window.open(this.href); return false;"><img src="https://ufundoo.com/assets/img/mostp_fb.png" title="Facebook"  onclick="window.open("","")"></a>
													</div>
													<div style="display:table-cell; width:50px; height:85px; vertical-align:middle;">
														<a href="http://twitter.com/share?text=Event Name - DanceSport Classic Summer Sizzler%0aDate : 2016-03-31%0aLocation : " onclick="window.open(this.href); return false;"><img src="https://ufundoo.com/assets/img/mostp_tweet.png" title="Twitter" onclick="window.open("","")"></a>
													</div>
													<div style="display:table-cell; width:50px; height:85px; vertical-align:middle;">
														<a href="http://pinterest.com/pin/create/button/?url=https://ufundoo.com&amp;media=https://ufundoo.com/fileupload/server/php/files/151224-150836.jpg&amp;description=Event Name : DanceSport Classic Summer Sizzler%0aDate : 2016-03-31%0aLocation : " onclick="window.open(this.href); return false;"><img src="https://ufundoo.com/assets/img/mostp_pin.png" title="Pin It"  onclick="window.open("","")"></a>
													</div>
												</div>
											</div>
										   </div></div></div>';
								$subject = "Your tickets for ".$result['eventName'];
								
								require_once('phpmailer/class.phpmailer.php');
								$mail = new PHPMailer();
								$mail->IsHTML(true);
								$mail->IsSMTP(); // telling the class to use SMTP
								$mail->Host       = $SmtpServer; // SMTP server
								//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
								$mail->SMTPSecure = "ssl";										   
								$mail->SMTPAuth   = true;                  // enable SMTP authentication
								$mail->Host       = $SmtpServer; // sets the SMTP server
								$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
								$mail->Username   = $SmtpUser; // SMTP account username
								$mail->Password   = $SmtpPass;        // SMTP account password
								$mail->From=$SmtpUser;
								$mail->FromName=$SmtpUser;
								$mail->Subject    = $subject;
								$mail->AddAttachment( 'pdf/'.$_REQUEST['orderNo'].'-ticket.pdf' );
								$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
								$mail->MsgHTML($body);
								$mail->Body=$body1;
								$mail->AddAddress($to_addresss1, $to_addresss1);
								$mail->AddAddress($to_addresss2, $to_addresss2);
								if(!$mail->Send()) {
								 //echo "error"; 
								} else {
								  //echo 'Email has been sent to your email address.'; 
								}
}
else
{
if((isset($_POST['stripeToken'])) && $_POST['stripeToken']!='')
{
try {
	require_once('Stripe/lib/Stripe.php');
	Stripe::setApiKey("sk_live_ojHsqmR2XkPwlQuaTBd9bpdL"); //Replace with your Secret Key
	//Stripe::setApiKey("sk_test_uzKvGBJEew8q5q9CFtsur6b9"); //Replace with your Secret Key
	
	$amount = $_REQUEST['total']*100;
	$charge = Stripe_Charge::create(array(
		"amount" => $amount,
		"currency" => "USD",
		"card" => $_POST['stripeToken']
	));
	$fetchTotal=mysqli_query($mysqli,'select ticketDetail.id as ticketDetailId,totalTicket,eventId from ticketDetail where id="'.$_REQUEST['merchant_order_id'].'"');
	$resultTotal=mysqli_fetch_assoc($fetchTotal);
	$query_trans_detail=mysqli_query($mysqli,'insert into ticketTransectionDetails set ticketDetailId="'.$_REQUEST['merchant_order_id'].'",txnAmount="'.$_REQUEST['total'].'",txnQty="'.$resultTotal['totalTicket'].'",txnCurrency="'.$_REQUEST['currency_code'].'",txtTransectionNo="'.$_POST['stripeToken'].'",txnStatus="SUCCESS",date="'.$date.'"');
	$transTicketId=mysqli_insert_id($mysqli);
	if($query_trans_detail==1)
	{
		$update_ticket_detail=mysqli_query($mysqli,'update ticketDetail set orderStatus="SUCCESS" where id="'.$_REQUEST['merchant_order_id'].'"');
		if($update_ticket_detail==1)
		{
			$queryEvent=mysqli_query($mysqli,"select event.name as eventName,event.date as date,event.startDateTime as startTime,event.description as description,event.coverImage as coverImage,event.seatChart as seatChart,organization.name as orgName,organization.contactNo as orgContact, location.name as locationName,ticketType.id as ticketTypeId,profile.firstName as firstName,promoter.email as promoterEmail,eventType.name as eventTypeName from event inner join promoter on promoter.id=event.promoterId inner join profile on profile.id=promoter.profileId inner join organization on organization.id=event.organizationId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId inner join eventType on eventType.id=event.eventTypeId where event.id='".$resultTotal['eventId']."'");
			$result=mysqli_fetch_assoc($queryEvent);
			$time_bt = date_create($result['startTime']);	
			$date_bt = date_create($result['date']);
			$totalTicket=$resultTotal['totalTicket'];
	
	/* generate pdf */
	$content='<style>
	td{border:1px solid #ccc;}
	td>div{}
	</style>
	<div style="width:100%; text-align:center; margin-left:auto; margin-right:auto; margin-top:10px; margin-bottom:20px;">
		<a href="https://ufundoo.com" target="_blank"><img src="assets/img/logo_pdf.png" style="cursor:pointer"></a>
	</div>
	<table style="width:100%; height:300px;" cellpadding="0" cellspacing="0">
	  <tr>
		<td style="width:75%; height:25px; vertical-align:top; max-height:25px;" colspan="2">
			<div style="color:#ccc; max-height:10px;">Event</div>
			<div style="color:#ccc; max-height:15px; font-size:20px; color:#000; font-weight:bold;">'.$result['eventName'].'</div>
		</td>
		<td rowspan="2" style="width:25%; height:50px; max-height:50px;vertical-align:top; ">
			<div style="color:#ccc; max-height:10px; margin-top:10px;">Name</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold; margin-top:10px; text-align:center">'.$_SESSION['username'].'</div>
			<div style="color:#ccc; max-height:10px; margin-top:10px;">Event Type</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold; margin-top:10px; text-align:center">'.$result['eventTypeName'].'</div>
		</td>
	  </tr>
	  <tr>
		<td style="width:37%; height:50px;border:none; border-right:1px solid #ccc; border-left:1px solid #ccc; max-height:50px;vertical-align:top;">
			<div style="color:#ccc; max-height:10px;">Date + Time</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold;">'.date_format($date_bt, 'M jS, Y').' - '.date_format($time_bt, 'h:i a').'</div>
		</td>
		<td style="width:37%; height:50px;border:none; max-height:50px; vertical-align:top; ">
			<div style="color:#ccc; max-height:10px;">Order Info</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold;">Order No. # '.$_REQUEST['orderNo'].'</div>
		</td>
	  </tr>
	  <tr>
		<td style="width:75%; height:50px; max-height:50px;vertical-align:top; " colspan="2">
			<div style="color:#ccc; max-height:15px;">Location</div>
			<div style="color:#ccc; max-height:35px; font-size:20px; color:#000; font-weight:bold;">'.$result['locationName'].'</div>
		</td>
		<td rowspan="2" style="width:25%; height:100px; max-height:100px;vertical-align:top; ">
			<div style="color:#ccc; max-height:10px; margin-top:10px;">Order Status</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold; margin-top:10px;text-align:center">Paid Order</div>
			<div style="color:#ccc; max-height:10px;margin-top:10px;">Total Ticket(s)</div>
			<div style="max-height:15px; font-size:20px; color:#000; font-weight:bold; margin-top:10px;text-align:center">'.$totalTicket.'</div>
		</td>
	  </tr>
	  <tr>
		<td style="width:75%; height:50px; max-height:50px;vertical-align:top; " colspan="2">
			<div style="color:#ccc; max-height:15px;">Event Organization</div>
			<div style="max-height:35px; font-size:20px; color:#000; font-weight:bold;">'.$result['orgName'].'</div>
		</td>
	  </tr>
	</table>';
	
	unlink('pdf/'.$_REQUEST['orderNo'].'-ticket.pdf');
	   try
		{
			$html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', array(5, 5, 5, 5));
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->writeHTML($content);
		
			$html2pdf->Output('pdf/'.$_REQUEST['orderNo'].'-ticket.pdf',true);
			$array["pdfLink"]=curPageURL().'/pdf/'.$_REQUEST['orderNo'].'-ticket.pdf';
		}
		catch(HTML2PDF_exception $e) {
			exit;
		}
	/* end */
	
			/* mail */
								include("config/configEmail.php");
								$to_addresss1=$email;
								$to_addresss2="sales@ufundoo.com";
								
								$body1 =  '<div style="height:100%;width:100%;font-family: calibri;" align="left">
    									   <div style="width:644px; padding:0px; text-align:left; height:auto; min-height:100px;">
										   <div style="height:5px; width:644px; background-color:#ed258f;display: inline-block;"></div>
										   <div><img src="https://ufundoo.com/assets/img/logo.png" onclick="window.open("index.php","_self")" style="cursor:pointer"></div>
										   <div style="font-family: calibri;font-size:15px;">'.$msg.',<br/><br/>
										   This is your order confirmation for '.$result['eventName'].'.<br/><br/>
										   <b>Total purchased tickets : </b>'.$totalTicket.'<br/><br/>
										   <table style="width:644px;font-family: calibri; margin-bottom:15px;" border="1" bordercolor="#e4e4e4" cellpadding="10" cellspacing="0">
										   <tr>
										   	<td style="width:321px;font-family: calibri;">
										   <b>Event details :</b><br/>
										   Name : '.$result['eventName'].'<br/>
										   Date : '.date_format($date_bt, 'M jS, Y').'<br/>
										   Time : '.date_format($time_bt, 'h:i a').'<br/>
										   Venue : '.$result['locationName'].'<br/>
										   Event Organizers : '.$result['orgName'].'<br/>';
										   if($result['orgContact']!='') {
								$body1 .=  'Contact Organizers : '.$result['orgContact'].'<br/>';
										   }
								$body1 .=  '</td><td style="width:321px;font-family: calibri;">
											<b>Order summary : </b><br/>
											Order number : '.$_REQUEST['orderNo'].'<br/>
											Transaction number : '.$_POST['stripeToken'].'<br/>
										    Transaction amount : '.$_REQUEST['total'].' '.$_REQUEST['currency_code'].'<br/>
										    Total Quantity : '.$totalTicket.'<br/>
										    Date : '.date_format($date_bt, 'M jS, Y').'<br/><br/>
											</td></tr></table>
										    Thanks,<br/>Ufundoo Team</div>
											<div style="margin-top:15px; display:table;width:606px; height:85px !important;background:url(https://ufundoo.com/assets/img/email_bottom.png);">
										   	<div style="display:table-cell; width:450px;vertical-align:middle;">
										   	<div style="color:#4e4e4e; font-size:15px;margin-left: 110px; padding-top: 5px;">Still have a question? Call us on +1-510-818-0468</div>
											<div style="font-size:25px;margin-left: 155px; padding-top: 5px; font-weight:bold;cursor:pointer; "><a href="https://ufundoo.com" style="text-decoration:none;color:#ed258f;" target="_blank">www.ufundoo.com</a></div>
											</div>
											<div style="display:table-cell; width:190px; height:85px;vertical-align:middle;">
												<div style="display:table;">
													<div style="display:table-cell; width:50px; height:85px; vertical-align:middle;">
														<a href="https://www.facebook.com/dialog/feed?app_id=705873652868650&amp;redirect_uri=https://ufundoo.com&amp;link=https://ufundoo.com&amp;picture=https://ufundoo.com/fileupload/server/php/files/151224-150836.jpg&amp;caption=DanceSport Classic Summer Sizzler&amp;description= Date : 2016-03-31&lt;center&gt;&lt;/center&gt; Location : " onclick="window.open(this.href); return false;"><img src="https://ufundoo.com/assets/img/mostp_fb.png" title="Facebook"  onclick="window.open("","")"></a>
													</div>
													<div style="display:table-cell; width:50px; height:85px; vertical-align:middle;">
														<a href="http://twitter.com/share?text=Event Name - DanceSport Classic Summer Sizzler%0aDate : 2016-03-31%0aLocation : " onclick="window.open(this.href); return false;"><img src="https://ufundoo.com/assets/img/mostp_tweet.png" title="Twitter" onclick="window.open("","")"></a>
													</div>
													<div style="display:table-cell; width:50px; height:85px; vertical-align:middle;">
														<a href="http://pinterest.com/pin/create/button/?url=https://ufundoo.com&amp;media=https://ufundoo.com/fileupload/server/php/files/151224-150836.jpg&amp;description=Event Name : DanceSport Classic Summer Sizzler%0aDate : 2016-03-31%0aLocation : " onclick="window.open(this.href); return false;"><img src="https://ufundoo.com/assets/img/mostp_pin.png" title="Pin It"  onclick="window.open("","")"></a>
													</div>
												</div>
											</div>
										   </div>
											</div></div>';
								$subject = "Your tickets for ".$result['eventName'];
								
								require_once('phpmailer/class.phpmailer.php');
								$mail = new PHPMailer();
								$mail->IsHTML(true);
								$mail->IsSMTP(); // telling the class to use SMTP
								$mail->Host       = $SmtpServer; // SMTP server
								//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
								$mail->SMTPSecure = "ssl";										   
								$mail->SMTPAuth   = true;                  // enable SMTP authentication
								$mail->Host       = $SmtpServer; // sets the SMTP server
								$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
								$mail->Username   = $SmtpUser; // SMTP account username
								$mail->Password   = $SmtpPass;        // SMTP account password
								$mail->From=$SmtpUser;
								$mail->FromName=$SmtpUser;
								$mail->Subject    = $subject;
								$mail->AddAttachment( 'pdf/'.$_REQUEST['orderNo'].'-ticket.pdf' );
								$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
								$mail->MsgHTML($body);
								$mail->Body=$body1;
								$mail->AddAddress($to_addresss1, $to_addresss1);
								$mail->AddAddress($to_addresss2, $to_addresss2);
								if(!$mail->Send()) {
								 //echo "error"; 
								} else {
								  //echo 'Email has been sent to your email address.'; 
								}
		}
	}
}

catch(Stripe_CardError $e) {
	header("location: https://www.ufundoo.com/failure.php", true, 301 ); exit;
}
//catch the errors in any way you like
catch (Stripe_InvalidRequestError $e) {
  // Invalid parameters were supplied to Stripe's API
  header("location: https://www.ufundoo.com/failure.php", true, 301 ); exit;
} catch (Stripe_AuthenticationError $e) {
  // Authentication with Stripe's API failed
  // (maybe you changed API keys recently)
  header("location: https://www.ufundoo.com/failure.php", true, 301 ); exit;
} catch (Stripe_ApiConnectionError $e) {
  // Network communication with Stripe failed
  header("location: https://www.ufundoo.com/failure.php", true, 301 ); exit;
} catch (Stripe_Error $e) {
  // Display a very generic error to the user, and maybe send
  // yourself an email
  header("location: https://www.ufundoo.com/failure.php", true, 301 ); exit;
} catch (Exception $e) {
  // Something else happened, completely unrelated to Stripe
  header("location: https://www.ufundoo.com/failure.php", true, 301 ); exit;
}
}
else
{
	header("location: https://www.ufundoo.com/failure.php", true, 301 ); exit;
}
}
function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 $path_parts =pathinfo($_SERVER["REQUEST_URI"]);
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$path_parts["dirname"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$path_parts["dirname"];
 }
 return $pageURL;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Ufundoo | Thank You</title>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="https://ufundoo.com" />
<meta name="twitter:title" content="Small Island Developing States Photo Submission" />
<meta name="twitter:description" content="View the album on Flickr." />
<meta name="twitter:image" content="https://ufundoo.com/img.png" />
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<link rel="stylesheet" href="assets/css/datepicker.css" type="text/css" />

<style>
html,body
{
	overflow:hidden;
}
.headerBtnActive
{
	border: 1px solid #2e302d;
	color:#2e302d;
}
.headerBtn
{
	color:#2e302d;
}
</style>
</head>

<body>
	<div class="wrapper">
			<!-- header -->
            <?php include('include/header.php'); ?>
    		<!-- end header -->
            
            <div id="container_area" style="overflow:auto">
            	<div style="height:500px; width:670px;display:table; margin-left:auto; margin-right:auto;" align="center">
                	<div style="font-size:80px; color:#ed258f; margin-top:30px; width:100%;text-align:center">Thank You!</div>
                    <div style="font-size:18px; color:#000; width:100%; text-align:center">for booking tickets on <span style="color:#ed258f; font-weight:bold; cursor:pointer" onclick="window.open('index.php','_self');">Ufundoo</span></div>
                    <div style="background:url(assets/img/thankyou_ticket.png); background-repeat:no-repeat; height:226px; width:662; margin-top:20px; margin-bottom:20px;" align="center">
                    	<div style="width:485px; height:226px; display: inline-block;float: left;text-align: left;padding-left: 50px; padding-right: 34px;padding-top: 25px;">
                        	<div style="color:#000; font-size:25px; font-weight:bold;"><?php echo ucfirst($result['eventName']); ?></div>
                            <div style="color:#000; font-size:18px;">By <?php echo ucfirst($result['orgName']); ?></div>
                            <div style="color:#000; font-size:18px;"><?php echo ucfirst($result['locationName']); ?></div>
                            <div style="color:#000; font-size:18px;"><?php echo date_format($date_bt, 'jS M, Y').'   '.date_format($time_bt, 'G:i a'); ?></div>
                        </div>
                        <div style="width:185px; height:226px; display: inline-block;float: left;text-align: center;padding-top: 65px;">
                        	<div style="color:#000; font-size:40px;font-weight:bold"><?php echo $totalTicket; ?></div>
                            <div style="color:#000; font-size:20px;">Tickets</div>
                        </div>
                    </div>
                    <div style="font-size:18px; color:#000; width:100%; text-align:center">We have just sent you an Email with complete information about your booking.</div>
                    <div class="searchBtn" style="width: 210px; font-size: 19px; padding-top: 8px; height: 45px; margin-top:20px; margin-bottom:20px;margin-left: auto;margin-right: auto;" onclick="window.open('index.php','_self');">Home page</div>
                </div>
             <!-- footer -->
             <?php include('include/footer.php'); ?>   
             <!-- end footer -->
      </div>
</div>
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/ufundoo.js"></script>
<script>
var windowHeight=$(window).height();
$(document).ready(function(e) {
	$('#dp').datepicker({
		format: 'yyyy-mm-dd'
	});
	listEvents=windowHeight-80;
    $('#container_area').css('height',listEvents+'px');
	$("#container_area").niceScroll({cursorcolor: "rgba(237, 37, 143, 0.5)",cursorwidth:"6px",cursorborderradius:"10px" ,cursorborder: "0px", railpadding: {right: 2}, zindex: 0});
	
});
</script>
</body>
</html>