<?php 
require_once("config/conn.php");
session_start();

//$urlfb = 'https://www.facebook.com/logout.php?next=http://www.test.alive-mind.com/ufundoo/actions.php&access_token='.$_SESSION['token'];
//$urlg='https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://www.test.alive-mind.com/ufundoo/';


$_SESSION['userId']='';
unset($_SESSION['userId']);
$_SESSION['userEmail']='';
unset($_SESSION['userEmail']);
$_SESSION['promoterId']='';
unset($_SESSION['promoterId']);
$_SESSION['promoterEmail']='';
unset($_SESSION['promoterEmail']);
$_SESSION['username']='';
unset($_SESSION['username']);

$fb=$_SESSION['fb'];
$google=$_SESSION['google'];

if($fb=='fb')
{
	$_SESSION['fb']='';
	unset($_SESSION['fb']);
	$_SESSION['logout']=1;
	//header('Location: '.$urlfb);
	header('Location: index.php');
}
else if($google=='google')
{
	require_once 'google/src/Google_Client.php';
	require_once 'google/src/contrib/Google_Oauth2Service.php';
	$gClient = new Google_Client();
	$gClient->revokeToken();
	unset($_SESSION['google_token']);
	$_SESSION['grole']='';
	unset($_SESSION['grole']);
	$_SESSION['google']='';
	unset($_SESSION['google']);
	session_destroy();
	//header('Location: '.$urlg);	
	header('Location: index.php');	
}
else
{
	session_destroy();
	header('Location: index.php');
}

?>