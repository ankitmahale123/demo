<?php
	session_start();
	require_once("../config/conn.php"); 
	
	if($_REQUEST['status']=='events')
	{
	?>
    <script>$('.btn-admin').eq(0).trigger('click');$('#submenuFill').css('visibility','visible');$('#eventsTab').css('display','table-cell');$('#promotersTab').css('display','none');</script>
    <?php
	}
	else if($_REQUEST['status']=='newpromoter')
	{
	?>
    <script>$('.btn-admin').eq(4).trigger('click');$('#submenuFill').css('visibility','visible');$('#eventsTab').css('display','none');$('#promotersTab').css('display','table-cell');</script>
    <?php
	}
	else if($_REQUEST['status']=='promotion')
	{
	?>
    <script>
		$('#submenuFill').css('visibility','hidden');
		$('div[default="true"]').trigger('click');$( '#editor1' ).ckeditor();
		/* for csv */
	var uploadButton;
	$(function () {
		'use strict';
		// Change this to the location of your server-side upload handler:
		var url = '../fileupload/server/php/';
		$('#csv_file').each(function () {
			$(this).on('click', function () {
				uploadButton = $(this).parent().parent().attr('id');
				//alert(uploadButton);
			});
		$(this).fileupload({
			url: url,
			dataType: 'json',
			maxFileSize: 5000000, // 5 MB
			// Enable image resizing, except for Android and Opera,
			// which actually support image resizing, but fail to
			// send Blob objects via XHR requests:
			disableImageResize: /Android(?!.*Chrome)|Opera/
				.test(window.navigator.userAgent),
			previewCrop: true
		}).on('fileuploadadd', function (e, data) {
			$(".popup-loader").fadeIn();
		}).on('fileuploadprogressall', function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			//$('#progress .progress-bar').css('width',progress + '%');
		}).on('fileuploaddone', function (e, data) {
			$.each(data.result.files, function (index, file) {
			$(".popup-loader").fadeOut();
			$('#csv_done').css('display','inline-block');
			$('#csv_hidden').val(file.url);
			});
		}).on('fileuploadfail', function (e, data) {
			$.each(data.files, function (index) {
				var error = $('<span class="text-danger"/>').text('File upload failed.');
				console.log(error);
			});
		}).prop('disabled', !$.support.fileInput)
			.parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
	});
    </script>
    <style>
	.email-form-td
	{
		display:table-cell;
		padding:10px;
		text-align: right;
		vertical-align:middle;color:#727272;font-family: lator;width:200px; font-size:16px;
	}
	input
	{
		padding: 8px;
		border: 1px solid #ccc;
		border-radius: 3px;
		margin-bottom: 10px;
		font-family: lator;
		color: #2C3E50;
		font-size: 13px;
	}
	tr td
	{
		font-family:lator;
		color:#727272;
		font-size:15px;
	}
	.select-wrapper
	{
		border:1px solid #ccc;
	}
	</style>
    <div id="email-att" class="comm-divs">
  	<table style="width:700px; height:auto;">
    	<tr style="height:40px;">
        	<td style="width:100px; padding-left:20px;">To</td>
            <td style="width:785px;"><input type="hidden" id="csv_hidden" value="" /><input type="button" style="-webkit-appearance: none;-moz-appearance: none;-ms-appearance: none;    -o-appearance: none; appearance: none;width:200px; padding:0px; height:35px; color:#fff; font-size:16px; background-color:#ed258f; border:1px solid #ed258f;font-family: lator; position:absolute; cursor:pointer;" value="Upload file" /><input type="file" id="csv_file" style="width:184px; border:none; opacity:0; cursor:pointer;"/><span id="csv_done" style="margin-left:10px; display:none">File Uploaded</span></td>
        </tr>
        <tr>
        	<td></td>
            <td style="font-size:14px;">Please upload only .csv file for bulk email list. <span style="cursor:pointer; text-decoration:underline;" onclick="window.open('../assets/csv/sample.csv','_self');">Click here to see sample file.</span> Kindly upload it in the same format.</td>
        </tr>
        <tr style="height:65px;">
        	<td style="width:100px; padding-left:20px;">Subject</td>
            <td style="width:635px;"><input type="text" style="width:500px;" id="email-subject" /></td>
        </tr>
        <tr>
        	<td colspan="2"><textarea class="ckeditor" id="editor1" style="height:400px; width:80%;margin-top:20px;resize:none;"></textarea></td>
        </tr>
        <tr style="width:700px;">
            <td style="width:700px; padding-top:20px;" align="center" colspan="2">
                <input type="button" value="SEND" onclick="sendEmailEventPromotion()" style="width:100px;border:none; color:#fff; background:#727272; margin-left:100px; cursor:pointer;" />
                <input type="button" value="CANCEL" style="width:100px;border:none; color:#fff; background:#727272; margin-left:50px; cursor:pointer;" onclick="window.open('adminDashboard.php','_self');"/>
            </td>
        </tr>
    </table>
    </div>
    <?php
	}
	else if($_REQUEST['status']=='subscriberMail')
	{
	?>
    <style>
    .email-form-td
    {
        display:table-cell;
        padding:10px;
        text-align: right;
        vertical-align:middle;color:#000;font-family: lator;width:200px; font-size:16px;
    }
    input
    {
        padding: 8px;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-bottom: 10px;
        font-family: lator;
        color: #000;
        font-size: 13px;
    }
    tr td
    {
        font-family:lator;
        color:#000;
        font-size:15px;
    }
    </style>
    <script>
		$('div[default="true"]').trigger('click');$( '#editor1' ).ckeditor();
	</script>
<div align="center" style="width:100%; font-size:25px; color:#727272;font-family: lator; margin:10px 0px;">
  <div id="email-att" class="comm-divs" style="display:block;">
  	<table style="width:700px; height:auto;">
    	<tr style="height:40px;">
        	<td style="width:100px; padding-left:20px;">To</td>
            <td style="width:600px;">All Subscriber</td>
        </tr>
        <tr style="height:65px;">
        	<td style="width:100px; padding-left:20px;">Subject</td>
            <td style="width:600px;"><input type="text" style="width:500px;" id="email-subject" /></td>
        </tr>
        <tr>
        	<td colspan="2"><textarea class="ckeditor" id="editor1" style="height:400px; width:80%;margin-top:20px;resize:none;"></textarea></td>
        </tr>
		<tr style="width:700px;">
            <td style="width:700px; padding-top:20px;" align="center" colspan="2"><input type="button" value="SEND" onclick="sendEmailSubscriber()" style="width:100px;border:none; color:#fff; background:#727272; margin-left:100px; cursor:pointer;" /><input type="button" value="CANCEL" style="width:100px;border:none; color:#fff; background:#727272; margin-left:50px; cursor:pointer;" onclick="window.open('adminDashboard.php','_self');" /></td>
        </tr>
    </table>
  </div>
</div>
    <?php
	}
	else if($_REQUEST['status']=='subscriber')
	{
		$queryEvent=mysqli_query($mysqli,"select * from subscribe");
	?>
    <script>$('#submenuFill').css('visibility','hidden');</script>
    <style>
    .dash_table {
    border-collapse: collapse;
    margin-bottom: 3em;
    width: 100%;
    background: #fff;
	}
	.dash_td, .dash_th {
		padding: 0.75em 1.5em;
		text-align: left;
	}
	.dash_td.err {
		background-color: #e992b9;
		color: #fff;
		font-size: 0.75em;
		text-align: center;
		line-height: 1;
	}
	.dash_th {
		background-color:#676767;
		font-weight: bold;
		color: #fff;
		white-space: nowrap;
	}
	tbody .dash_th {
		background-color: #676767;
	}
	tbody tr:nth-child(2n-1) {
		background-color: #f5f5f5;
		transition: all .125s ease-in-out;
	}
	tbody tr:hover {
		background-color: rgba(250,164,209,.3);
	}
    </style>
    <?php
		if(mysqli_num_rows($queryEvent)>0){
		?>
    <div class="btn-admin" style="width:160px; background:#ed258f; border:1px solid #ed258f; font-size:18px; margin-top:1%;" onClick="send_all_subscriber('subscriberMail')">Email All Subscriber</div>
    <div align="center" style="width:98%; font-size:15px; color:#727272;font-family: lator; margin-left:auto; margin-right:auto; margin-top:1%;">
    	
    	<table align="center" class="dash_table">
					<thead>
						<tr>
							<th class="dash_th">Sr No.</th>
							<th class="dash_th">Email</th>
						</tr>
					</thead>
					<tbody>
                    	<?php $i=1;
								while($result=mysqli_fetch_assoc($queryEvent)){ ?>
						<tr>
                            <td class="dash_td"><?php echo $i; ?></td>
                            <td class="dash_td"><?php echo $result['email']; ?></td>
                        </tr>
                        <?php $i++; } ?>
					</tbody>
				</table>
                <?php } else { ?>
                <div style="margin-top:150px;font-size:25px; color:#727272;font-family: lator; text-align:center;">No data found.</div>
                <?php } ?>
    </div>
    <?php
	}
	else if($_REQUEST['status']=='user')
	{
	$query=mysqli_query($mysqli,"select booked_ticket.userTypeId,user.email,CONCAT_WS(' ', profile.firstName, profile.lastName) as name from booked_ticket inner join user on user.id=booked_ticket.userTypeId inner join profile on profile.id=user.profileId where role='user' UNION select booked_ticket.userTypeId,promoter.email,CONCAT_WS(' ', profile.firstName, profile.lastName) as name from booked_ticket inner join promoter on promoter.id=booked_ticket.userTypeId inner join profile on profile.id=promoter.profileId where role='promoter'");
	?>
    <script>$('#submenuFill').css('visibility','hidden');</script>
    <style>
    .dash_table {
    border-collapse: collapse;
    margin-bottom: 3em;
    width: 100%;
    background: #fff;
	}
	.dash_td, .dash_th {
		padding: 0.75em 1.5em;
		text-align: left;
	}
	.dash_td.err {
		background-color: #e992b9;
		color: #fff;
		font-size: 0.75em;
		text-align: center;
		line-height: 1;
	}
	.dash_th {
		background-color:#676767;
		font-weight: bold;
		color: #fff;
		white-space: nowrap;
	}
	tbody .dash_th {
		background-color: #676767;
	}
	tbody tr:nth-child(2n-1) {
		background-color: #f5f5f5;
		transition: all .125s ease-in-out;
	}
	tbody tr:hover {
		background-color: rgba(131,176,172,.3);
	}
    </style>
    <?php
		if(mysqli_num_rows($query)>0){
		?>
    <div class="btn-admin" style="width:155px; background:#ed258f; border:1px solid #ed258f; font-size:18px; margin-top:1%;" onClick="send_all_attendee()">Email All Ateendee</div>
    <div align="center" style="width:98%; font-size:15px; color:#727272;font-family: lator; margin-left:auto; margin-right:auto; margin-top:1%;">
    	
    	<table align="center" class="dash_table">
					<thead>
						<tr>
							<th class="dash_th">Sr No.</th>
							<th class="dash_th">Name</th>
							<th class="dash_th">Email</th>
						</tr>
					</thead>
					<tbody>
                    	<?php $i=1;
								while($result=mysqli_fetch_assoc($query)){ ?>
						<tr>
                            <td class="dash_td"><?php echo $i; ?></td>
                            <td class="dash_td"><?php echo $result['name']; ?></td>
                            <td class="dash_td"><?php echo $result['email']; ?></td>
                        </tr>
                        <?php $i++; } ?>
					</tbody>
				</table>
                <?php } else { ?>
                <div style="margin-top:150px;font-size:25px; color:#727272;font-family: lator; text-align:center;">No data found..</div>
                <?php } ?>
    </div>
    <?php
	}
	else if($_REQUEST['status']=='inquiries')
	{
		$queryFeedback=mysqli_query($mysqli,"select * from feedback order by createdAt desc");	
	?>
     <script>$('#submenuFill').css('visibility','hidden');</script>
	<style>
    .dash_table {
    border-collapse: collapse;
    margin-bottom: 3em;
    width: 100%;
    background: #fff;
	}
	.dash_td, .dash_th {
		padding: 0.75em 1.5em;
		text-align: left;
	}
	.dash_td.err {
		background-color: #e992b9;
		color: #fff;
		font-size: 0.75em;
		text-align: center;
		line-height: 1;
	}
	.dash_th {
		background-color:#676767;
		font-weight: bold;
		color: #fff;
		white-space: nowrap;
	}
	tbody .dash_th {
		background-color: #676767;
	}
	tbody tr:nth-child(2n-1) {
		background-color: #f5f5f5;
		transition: all .125s ease-in-out;
	}
	tbody tr:hover {
		background-color: rgba(131,176,172,.3);
	}
    </style>
     <?php
		if(mysqli_num_rows($queryFeedback)>0){ ?>
        
        <div align="center" style="width:98%; font-size:15px; color:#727272;font-family: lator; margin-left:auto; margin-right:auto; margin-top:1%;">
    	
    	<table align="center" class="dash_table">
					<thead>
						<tr>
							<th class="dash_th">Sr No.</th>
							<th class="dash_th">Suggest</th>
							<th class="dash_th">Email</th>
                            <th class="dash_th">Contact No.</th>
                            <th class="dash_th">Suggestion</th>
						</tr>
					</thead>
					<tbody>
                    	<?php $i=1;
								while($resultFeedback=mysqli_fetch_assoc($queryFeedback)){ ?>
						<tr>
                            <td class="dash_td"><?php echo $i; ?></td>
                            <td class="dash_td"><?php echo $resultFeedback['subject']; ?></td>
                            <td class="dash_td"><?php echo $resultFeedback['email']; ?></td>
                            <td class="dash_td"><?php echo $resultFeedback['contact']; ?></td>
                            <td class="dash_td" style="padding-left: 5px;padding-right: 5px;"><div class="addScroll" style="max-height:100px;width:200px;padding: 0 3px;"><?php 
							/*if(strlen($resultFeedback['suggestion'])>35)
							{
								echo substr($resultFeedback['suggestion'],0,30);
							} 
							else
							{*/
								echo $resultFeedback['suggestion'];
							//}
							?></div></td>
                        </tr>
                        <?php $i++; } ?>
					</tbody>
				</table>
                <?php } else { ?>
                <div style="margin-top:150px;font-size:25px; color:#727272;font-family: lator; text-align:center;">No data found..</div>
                <?php } ?>
    </div>
	<?php 
	}
	else if($_REQUEST['status']=='jobs')
	{
		$queryJobs=mysqli_query($mysqli,"select * from jobs order by createdAt desc");	
		?>
         <script>$('#submenuFill').css('visibility','hidden');</script>
	<style>
    .dash_table {
    border-collapse: collapse;
    margin-bottom: 3em;
    width: 100%;
    background: #fff;
	}
	.dash_td, .dash_th {
		padding: 0.75em 1.5em;
		text-align: left;
	}
	.dash_td.err {
		background-color: #e992b9;
		color: #fff;
		font-size: 0.75em;
		text-align: center;
		line-height: 1;
	}
	.dash_th {
		background-color:#676767;
		font-weight: bold;
		color: #fff;
		white-space: nowrap;
	}
	tbody .dash_th {
		background-color: #676767;
	}
	tbody tr:nth-child(2n-1) {
		background-color: #f5f5f5;
		transition: all .125s ease-in-out;
	}
	tbody tr:hover {
		background-color: rgba(131,176,172,.3);
	}
    </style>	
    <?php
		if(mysqli_num_rows($queryJobs)>0){ ?>
        
        <div align="center" style="width:98%; font-size:15px; color:#727272;font-family: lator; margin-left:auto; margin-right:auto; margin-top:1%;">
    	
    	<table align="center" class="dash_table">
					<thead>
						<tr>
							<th class="dash_th">Sr No.</th>
							<th class="dash_th">Name</th>
							<th class="dash_th">Email</th>
                            <th class="dash_th">Contact No.</th>
                            <th class="dash_th">Intrested Area</th>
                            <th class="dash_th">Description</th>
						</tr>
					</thead>
					<tbody>
                    	<?php $i=1;
								while($resultJobs=mysqli_fetch_assoc($queryJobs)){ ?>
						<tr>
                            <td class="dash_td"><?php echo $i; ?></td>
                            <td class="dash_td" style="text-transform:capitalize;"><?php echo $resultJobs['fullname']; ?></td>
                            <td class="dash_td"><?php echo $resultJobs['email']; ?></td>
                            <td class="dash_td"><?php echo $resultJobs['contact']; ?></td>
                            <td class="dash_td" style="text-transform:capitalize;"><?php echo $resultJobs['intrestedArea']; ?></td>
                            <td class="dash_td" style="padding-left: 5px;padding-right: 5px;"><div class="addScroll" style="max-height:100px;width:200px;padding: 0 3px;"><?php echo $resultJobs['description'];?></div></td>
                        </tr>
                        <?php $i++; } ?>
					</tbody>
				</table>
                <?php } else { ?>
                <div style="margin-top:150px;font-size:25px; color:#727272;font-family: lator; text-align:center;">No data found..</div>
                <?php } ?>
    </div>     
	<?php }
	else if($_REQUEST['status']=='content')
	{
	?>
   	<script>
		$('#submenuFill').css('visibility','hidden');
		$('.subMenu').eq(0).trigger('click');
    </script> 
    <?php
	}
	else if($_REQUEST['status']=='communication')
	{
	?>
	<style>
    .email-form-td
    {
        display:table-cell;
        padding:10px;
        text-align: right;
        vertical-align:middle;color:#727272;font-family: lator;width:200px; font-size:16px;
    }
    input
    {
        padding: 8px;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-bottom: 10px;
        font-family: lator;
        color: #2C3E50;
        font-size: 13px;
    }
    tr td
    {
        font-family:lator;
        color:#727272;
        font-size:15px;
    }
    .select-wrapper
    {
        border:1px solid #ccc;
    }
    </style>
    <script>
		//$('#submenuFill').css('display','none');
		$('div[default="true"]').trigger('click');$( '#editor1' ).ckeditor();
		$(".custom-select").each(function(){
            $(this).wrap("<span class='select-wrapper'></span>");
            $(this).after("<span class='holder'></span>");
        });
        $(".custom-select").change(function(){
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        }).trigger('change');
	</script>
<div align="center" style="width:100%; font-size:25px; color:#727272;font-family: lator; margin:10px 0px;">
  <div id="news-updates" align="center" class="comm-divs" style="text-align:center; display:none;">
    <div>Updates</div>
    <div>
      <textarea style="height:400px; width:80%;margin-top:20px;resize:none; padding:10px;" id="txt-update"></textarea>
    </div>
    <div class="btn event" style="border-radius:3px; margin:10px 75px; font-size:15px; border:1px solid #ed258f;"  onclick="addNewsUpadtes()">Add</div>
  </div>
  <div id="email-att" class="comm-divs" style="display:block;">
  	<table style="width:700px; height:auto;">
    	<tr style="height:40px;">
        	<td style="width:100px; padding-left:20px;">To</td>
            <td style="width:600px;">
            	<select class="custom-select">
                    <option>&nbsp;All attendees</option>
                    <!--<option>&nbsp;Attendees by ticket type</option>
                    <option>&nbsp;Tickets purchased after certain date</option>-->
                  </select>
                  <span id="eventName" style="padding-left:15px; padding-top:6px; position:absolute;"></span>
            </td>
        </tr>
        <tr style="height:65px;">
        	<td style="width:100px; padding-left:20px;">Subject</td>
            <td style="width:600px;"><input type="text" style="width:500px;" id="email-subject" /></td>
        </tr>
        <tr>
        	<td colspan="2"><textarea class="ckeditor" id="editor1" style="height:400px; width:80%;margin-top:20px;resize:none;"></textarea></td>
        </tr>
<!--        <tr>
            <td style="width:640px; padding-top:10px" colspan="2"><span style="padding-left:20px; padding-right:20px;">Send Test Message To</span><input type="text" id="test-email-id" style="width:500px;" /><input type="button" value="SEND" onclick="sendEmail('test')" style="width:100px; margin-left:15px; border:none; color:#fff; background:#727272; cursor:pointer;" /></td>
        </tr>
-->        
		<input type="hidden" attrEventId="<?php echo $_REQUEST['eventId']; ?>" id="hiddenEventId" />
		<tr style="width:700px;">
            <td style="width:700px; padding-top:20px;" align="center" colspan="2"><input type="button" value="SEND" onclick="sendEmailEventAttendee()" style="width:100px;border:none; color:#fff; background:#727272; margin-left:100px; cursor:pointer;" /><input type="button" value="CANCEL" style="width:100px;border:none; color:#fff; background:#727272; margin-left:50px; cursor:pointer;" onclick="window.open('promoterDashboard.php','_self');" /></td>
        </tr>
    </table>
  </div>
</div>
	<?php }	else{ ?>
    <script>$('#submenuFill').css('visibility','hidden');$('#extra_top_menu_list').siblings('div').removeClass('menuActive').addClass('menuDefault');</script>
    <div align="center" style="width:600px; height:300px; margin-left:auto; margin-right:auto; margin-top:30px;">
    	<table style="width:500px; height:300px;" align="center" cellpadding="0" cellspacing="0">
        	<tr align="center" style="margin-top:20px;">
                <td style="width:250px;color:#727272;font-family: lator;"><input type="password" class="input_setting old_pass" placeholder="old password" /></td>
            </tr>
            <tr align="center">
                <td style="width:250px;color:#727272;font-family: lator;"><input type="password" class="input_setting new_pass" placeholder="New Password" /></td>
            </tr>
            <tr align="center">
                <td style="width:250px;color:#727272;font-family: lator;"><input type="password" class="input_setting confirm_pass" placeholder="Confirm Password" /></td>
            </tr>
            <tr align="center">
            	<td><input type="button" class="join" value="Update" style="width:130px; cursor:pointer;" onclick="updatePasswordPromoter('<?php echo $_SESSION['promoterEmail']; ?>')" /><input type="button" class="join" value="Cancel" style="width:130px; margin-left:20px;" onclick="window.open('adminDashboard.php','_self')"/></td>
            </tr>
        </table>
        <div class="error" align="center" style="display:none"></div>
    </div>
    <?php
	}
?>