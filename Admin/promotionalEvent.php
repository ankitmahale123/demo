<!DOCTYPE html>
<html>
<head>
<title>Promotional Event</title>
<style>
html {
        overflow: auto;
}

p.relative {
    left: 600px;
    top: 200px;
    width: 400px;
    height: 25px;
	
} 


div.start {
	vertical-align: middle;
    margin-left: 150px;
    width: 1200px;
    height: 60px;
	font-family: calibri;

}
div.start1 {
	vertical-align: middle;
    margin-left: 150px;
    width: 1200px;
    height: 200px;
	font-family: calibri;

}
div.dp {
	
	vertical-align: middle;
    margin-left: 150px;
	height: 50px;

}

textarea,select,dp{
 
    border: 3px ridge #FFA5A5; 
    -webkit-border-radius: 30px; 
    -moz-border-radius: 30px; 
    border-radius: 30px; 
    outline:0; 
    height:30px; 
	font-size: 18px;
	font-family: calibri;
    display:inline-block;
    vertical-align:middle;
	
}

</style>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="script.js"></script>
    <link rel="stylesheet" href="runnable.css" />

</head>

<script>
$(document).ready(
  
  /* This is the function that will get executed after the DOM is fully loaded */
  function () {
    $( "#datepicker" ).datepicker({
      changeMonth: true,//this option for allowing user to select month
      changeYear: true //this option for allowing user to select from year range
    });
  }

);


function myFunction() {
    var x = document.forms["form"]["first"].value;
	if (x == null || x == "") {
        alert("Organization Name must be filled out");
        return false;
    }
	var x = document.forms["form"]["first"].value;
	var regex=/[a-zA-Z]/;
    if (!x.match(regex))
	{
		alert("Organization Name should contain alphabets");
		return false;
	}
	var x = document.forms["form"]["third"].value;
    if (x == null || x == "") {
        alert("Organizer contact must be filled out");
        return false;
    }
	var x = document.forms["form"]["third"].value;
	var regex=/[0-9]{10}/;
    if (!x.match(regex))
	{
		alert("Wrong Contact number");
		return false;
	}
	var x = document.forms["form"]["fourth"].value;
    if (x == null || x == "") {
        alert("Name of event must be filled out");
        return false;
    }
	var x = document.forms["form"]["fifth"].value;
    if (x == null || x == "") {
        alert("Event type must be filled out");
        return false;
    }
	var x = document.forms["form"]["sixth"].value;
    if (x == null || x == "") {
        alert("Location must be picked");
        return false;
    }
	var x = document.forms["form"]["second"].value;
    if (x == null || x == "") {
        alert("Date must be picked");
        return false;
    }
	var x = document.forms["form"]["seventh"].value;
    if (x == null || x == "") {
        alert("Address must be picked");
        return false;
    }
	var x = document.forms["form"]["eighth"].value;
    if (x == null || x == "") {
        alert("Zipcode must be filled out");
        return false;
    }
	
	var x = document.forms["form"]["eighth"].value;
	var regex=/^[0-9]{5}(?:-[0-9]{4})?$/;
    if (!x.match(regex))
	{
		alert("Wrong Zipcode");
		return false;
	}
	
	var x = document.forms["form"]["nineth"].value;
    if (x == null || x == "") {
        alert("Event Description must be filled out");
        return false;
    }
	
	var x = document.forms["form"]["tenth"].value;
    if (x == null || x == "") {
        alert("Event URL must be filled out");
        return false;
    }

}


</script>


<body>

<!--<form action="javascript:window.close()" onsubmit="return myFunction();" onsubmit="javascript:alert('form submitted successfully')">-->

<form name="form" id="submit"  onsubmit="return myFunction();"  method="POST" action="addpromevent.php" enctype="multipart/form-data">


<img src="../assets/images/UfundooHeader.jpg" alt="header" style="width:1355px; height:100px;">

<div  style="margin-left: 600px; font-size: 25px; height: 80px" class="start"><b> PROMOTIONAL EVENT </b></div>


<div  class="start">Organization Name <span style="color:red"> *</span> <textarea  name="first" style="margin-left:10px" rows="1" cols="120"></textarea> </div>

<div class="start1">Description <textarea  name="oc" style="margin-left:72px; height:165px;" rows="5" cols="120"></textarea> </div>

<div class="start"> Organizer Contact <span style="color:red"> *</span> <textarea name="third" style="margin-left:18px" rows="1" cols="120">  </textarea> </div>

<div class="start"> Name of the event <span style="color:red"> *</span> <textarea name="fourth" style="margin-left:14px" rows="1" cols="120"></textarea> </div>

<div class="start"> Event type <span style="color:red"> *</span> 

<select style="margin-left:70px" id="menucategory" name="fifth">
        <option value = 1> Movie </option>
        <option value = 2> Dance </option>
        <option value = 3> Concert </option>
		<option value = 4> Drama </option>
		<option value = 5> Festival </option>
		<option value = 6> Cultural </option>
		<option value = 7> Exhibit </option>
		<option value = 8> Spiritual </option>
		<option value = 9> Sport </option>
		<option value = 10> Event </option>
    </select>  </div>

<div class="dp" >Pick a Date: <span style="color:red"> *</span> <input name="second" class="dp" style="margin-left:58px" value="MM-DD-YYYY" type="text" id="datepicker" /></div>
	
	
<div class="start"> Location Name <span style="color:red"> *</span> <textarea name="sixth" style="margin-left:36px" rows="1" cols="120"></textarea> </div>

<div class="start"> Address <span style="color:red"> *</span> <textarea name="seventh" style="margin-left:82px" rows="1" cols="120"></textarea> </div>

<div class="start"> Zipcode <span style="color:red"> *</span> <textarea name="eighth" style="margin-left:82px" rows="1" cols="120"></textarea> </div>
	
<div class="start1"> Event Description <span style="color:red"> *</span> <textarea name="nineth" style="margin-left:19px; height:160px; " rows="1" cols="120"></textarea> </div>
	
<div class="start"> Upload event Picture  <input style="margin-left:12px" type="file" name="file" id="file" ></div>

<div class="start">Event Url <span style="color:red"> *</span> <textarea  name="tenth" style="margin-left:78px" rows="1" cols="120"></textarea></div>


<input style="margin-left: 600px; outline:none;" type="image" src="../assets/images/submit.JPG" name="submit" value="Submit" alt="Submit" />

</form>





</body>
</html>