<?php
	session_start();
	require_once("../config/conn.php");
	
	/* event type */ 
	$queryType=mysqli_query($mysqli,'select * from eventType');
	 $queryImage=mysqli_query($mysqli,'select eventImage.id,eventImage.path from eventImage inner join event on event.id = eventImage.eventId where event.id="'.$_REQUEST['eventid'].'"');
 
 $queryOrganization=mysqli_query($mysqli,'select organization.id as orgId,organization.name as orgName,organization.description as orgDesc,organization.email,organization.fax,organization.contactNo,organization.website,event.id,event.name as eventTitle, event.date,TIME_FORMAT(event.startDateTime, "%H:%i") as startTime, TIME_FORMAT(event.endDateTime,"%H:%i") as endTime, location.id as locationId,location.name as locationName, location.address,location.zipcode,location.includeMap, event.description, event.video, event.seatChart,event.coverImage as coverImage, DATE_FORMAT(event.ticketFrom, "%Y-%m-%d %H:%i") as ticketFrom, DATE_FORMAT(event.ticketTo, "%Y-%m-%d %H:%i") as ticketTo, ticketType.name, event.status, event.lastUpdated, event.eventTypeId, event.organizationId, event.promoterId,eventType.name as eventName,featuredImage.image as fImage from event inner join organization on organization.id = event.organizationId inner join eventType on eventType.id = event.eventTypeId join location on location.id=event.locationId left join ticketType on ticketType.id=event.ticketTypeId left join featuredImage on featuredImage.eventId=event.id where event.id="'.$_REQUEST['eventid'].'"');
	$resultOrganization = mysqli_fetch_assoc($queryOrganization);
	
	$queryticket1=mysqli_query($mysqli,'select ticket.*, DATE_FORMAT(event.ticketFrom, "%Y-%m-%d %H:%i") as ticketFrom, DATE_FORMAT(event.ticketTo, "%Y-%m-%d %H:%i") as ticketTo, ticketType.name as ticketName from ticket inner join event on event.id = ticket.eventId inner join ticketType on ticketType.id=event.ticketTypeId  where event.id="'.$_REQUEST['eventid'].'"');
	$resultTicket1 = mysqli_fetch_assoc($queryticket1);
	
		 $queryticket=mysqli_query($mysqli,'select ticket.*, DATE_FORMAT(event.ticketFrom, "%Y-%m-%d %H:%i") as ticketFrom, DATE_FORMAT(event.ticketTo, "%Y-%m-%d %H:%i") as ticketTo, ticketType.name as ticketName from ticket inner join event on event.id = ticket.eventId inner join ticketType on ticketType.id=event.ticketTypeId  where event.id="'.$_REQUEST['eventid'].'"');
		 while($row = mysqli_fetch_assoc($queryticket)){ 
    		$itemsArrayTicket[] = $row;
		}
			?>

<div class="content" align="left" style="width:1140px; margin-left:60px;"> 
  <!-- main content -->
  <div style="display:table; width:1140px;margin-top:35px;" align="left"> 
    
    <!-- title -->
    <div align="left" style="font-size:30px;font-family: Helvetica; color:#ccc; margin-bottom:20px;">Edit Your Event</div>
    <div align="left" style="margin-top:20px; border-bottom:1px solid #ccc; height:1px; width:940px; margin-bottom:40px;"></div>
    <!-- end here --> 
    
    <!-- multistep form -->
    <form id="msform">
      <!-- progressbar -->
      <ul id="progressbar">
        <li class="active">Organization Details</li>
        <li>Event Details</li>
        <li>Create Tickets</li>
        <li>Preview</li>
      </ul>
      <!-- fieldsets -->
      <fieldset>
        <input type="hidden" value="<?php echo $resultOrganization['orgId']; ?>" id="org_id" />
        <input type="hidden" value="<?php echo $resultOrganization['id']; ?>" id="event_id" />
        <input type="hidden" value="<?php echo $resultOrganization['locationId']; ?>" id="location_id" />
        <div align="left">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Organization name<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="org_name" id="org_name" style="width:715px;" value="<?php echo $resultOrganization['orgName']?>" />
          </div>
        </div>
        <div align="left" style="margin-top:15px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Description</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <textarea name="org_desc" id="org_desc" style="width:715px; height:100px; resize:none;"><?php echo $resultOrganization['orgDesc']?></textarea>
          </div>
        </div>
        <div align="left" style="margin-top:15px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Organizer contact</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="org_email" id="org_email" style="width:340px;" placeholder="Enter Email" value="<?php echo $resultOrganization['email']?>"/>
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="org_contact_no" id="org_contact_no" style="width:340px; margin-left:15px;" placeholder="Enter Contact Number" value="<?php echo $resultOrganization['contactNo']?>"/>
          </div>
        </div>
        <div align="left" style="margin-top:15px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="org_fax" id="org_fax" style="width:340px;" placeholder="Enter Fax" value="<?php echo $resultOrganization['fax']?>"/>
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="org_website" id="org_website" style="width:340px; margin-left:15px;" placeholder="Enter Website" value="<?php echo $resultOrganization['website']?>"/>
          </div>
        </div>
        <input type="button" name="next" class="next" value="Next" style="margin-left:200px;" attrNum="first"/>
        <!--<input type="button" value="Save as draft" class="draft"/>-->
        <input type="button" value="Cancel" class="cancel" onclick="javascript:location.reload()" style="background-color: #ed258f;border: 1px solid #ed258f; cursor:pointer;"/>
        <div style="margin-top:100px; display:none; color:#727272; font-size:14px;font-family: Helvetica;" align="center" class="error"></div>
      </fieldset>
      <fieldset>
        <div align="left">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Name of your event<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="event_name" id="event_name" style="width:715px;" value="<?php echo $resultOrganization['eventTitle']?>" data-maxsize="65"/>
          </div>
        </div>
        <div align="left" style="margin-top:10px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle; color:#727272;font-family: Helvetica; font-size:16px;">Type of event<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;border:1px solid #ccc; border-radius:20px; padding-left:10px; height:35px;">
            <select name="timepass" class="custom-select" id="event_type"  style="margin-left:-10px;">
              <option style="font-size:14px; width:135px;"><?php echo $resultOrganization['eventName']?></option>
              <?php while($result=mysqli_fetch_assoc($queryType)){ ?>
              <option style="width:135px;"><?php echo $result['name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div align="left" style="margin-top:20px; height:45px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle; color:#727272;font-family: Helvetica; font-size:16px;">Date<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" style="width:200px; margin-right:10px;" id="datepick" value="<?php echo $resultOrganization['date']?>" readonly/>
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle; padding-left:75px; color:#727272"><span style="padding-right:10px;">Time<span style="color:#f00">*</span></span>
            <input type="text" style="width:100px; margin-right:10px;" id="datepicker_time1" value="<?php echo $resultOrganization['startTime']?>" readonly />
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle;padding-left:55px; color:#727272"><span style="padding-right:10px;">To<span style="color:#f00">*</span></span>
            <input type="text" style="width:100px; margin-right:10px;" id="datepicker_time2" value="<?php echo $resultOrganization['endTime']?>" readonly/>
          </div>
        </div>
        <div align="left">
          <div align="left" style="display:table-cell; vertical-align:middle; padding-left:557px; color:#727272; font-size:14px;">Start Time</div>
          <div align="left" style="display:table-cell; vertical-align:middle;padding-left:160px;color:#727272; font-size:14px;">End Time</div>
        </div>
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Location name<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="event_location" id="event_location" style="width:715px;" value="<?php echo $resultOrganization['locationName']?>" />
          </div>
        </div>
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Address<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <textarea name="event_address" id="event_address" style="width:715px; height:92px; resize:none;"><?php echo $resultOrganization['address']?></textarea>
          </div>
        </div>
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Zip code</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="zip_code" id="zip_code" style="width:215px;" value="<?php echo $resultOrganization['zipcode']?>"/>
            <span style="margin-left:20px;">
            <?php if($resultOrganization['includeMap'] == 0){ ?>
            <input type="checkbox" style="margin-right:20px;" onchange="includeMap()" id="mapCheck" name="mapCheck" value="0" />
            <?php } else { ?>
            <input type="checkbox" style="margin-right:20px;" onchange="includeMap()" id="mapCheck" name="mapCheck" checked="checked" value="1" />
            <?php } ?>
            <span style="color:#727272">Include Map</span></span></div>
        </div>
        <?php if($resultOrganization['address'] == "" ) {?>
        <div align="left" style="margin-top:20px; display:none;" id="include_map">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Generate Map<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell;vertical-align:middle;">
            <input type="text" style="width:600px;" id="address" placeholder="Location name (e.g : Los Angeles, CA, United States)"  />
          </div>
          <div align="left" style="display:table-cell; padding-left:17px;">
            <input type="button" style="width:100px; padding:0px; height:35px; color:#fff; font-size:16px; background-color:#ed258f; border:1px solid #ed258f;font-family: Helvetica; position:absolute; cursor:pointer;" value="Done" onclick="codeAddress()" />
          </div>
        </div>
        <?php } if($resultOrganization['includeMap'] == 1){  ?>
        <div align="left" style="margin-top:20px; " id="include_map">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Generate Map<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell;vertical-align:middle;">
            <input type="text" style="width:600px;" id="address" placeholder="Location name (e.g : Los Angeles, CA, United States)" value="<?php echo $resultOrganization['address']?>"/>
          </div>
          <div align="left" style="display:table-cell; padding-left:17px;">
            <input type="button" style="width:100px; padding:0px; height:35px; color:#fff; font-size:16px; background-color:#ed258f; border:1px solid #ed258f;font-family: Helvetica; position:absolute; cursor:pointer;" value="Done" onclick="codeAddress()" id="editLocation"/>
          </div>
        </div>
        <?php }  ?>
        <div align="left" style="margin-top:20px; display:none;" id="map_canvas_wrapper">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div align="left" style="display:table-cell;vertical-align:middle; width:739px; height:300px;">
            <div id="map-canvas" style="width:739px; height:300px;"></div>
          </div>
        </div>
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Event description</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <textarea name="address" style="width:715px; height:100px; resize:none;" id="event_desc" name="event_desc" >
            <?php echo $resultOrganization['description']?>
            </textarea>
          </div>
        </div>
        <?php if(mysqli_num_rows($queryImage)>0) { ?>
        <div align="left" style="margin-top:20px;">
          <div style="width:200px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div align="left" style="display:table-cell; vertical-align:middle;" id="eventImage">
            <?php while($resultImage=mysqli_fetch_assoc($queryImage)){ ?>
            <div class="show-image" style="float:left">
              <?php
				$str = $resultImage['path'];
				$str_container = explode("files/",$str);
				$imageContent.=$resultImage['path'].'$';
			?>
              <img src="<?php echo $resultImage['path']?>" height="60" style="border: 1px solid #ccc;" class="uplodedImg"/> <img src="https://ufundoo.com/assets/img/status-remove.png" style="text-align:right; vertical-align:top; position:relative; right:10px; top:-10px; cursor:pointer;" onClick="deleteUploadedImage(this,'<?php echo $resultImage['path']?>','<?php $str_container[0].'?file='.$str_container[1];?>')" class="removedImageIcon">
              <?php /*?><input class="deleteImgdelete" type="button" value="X" style="background-color:black;color:white;line-height:5px;font-size:14px;cursor:pointer" onclick="deleteUploadedImage(<?php echo $resultImage['id']?>,this)"/><?php */?>
            </div>
            <?php } ?>
          </div>
          <div align="left" style="vertical-align:middle; display:none;" id="event_image_wrapper"></div>
        </div>
        <?php } ?>
        <?php 
			if($resultOrganization['coverImage']!=''){ 
			$str = $resultOrganization['coverImage'];
			$str_container = explode("files/",$str);
		?>
        <div align="left" style="margin-top:20px;">
          <div style="width:200px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div align="left" style="vertical-align:middle; display:table-cell;margin-left:200px;" id="event_image_wrapper_cover">
            <div align="left" style="display:table-cell; vertical-align:middle;"> <img src="<?php echo $resultOrganization['coverImage']?>" height="60" style="border: 1px solid #ccc;" id="event_image_cover"/><img src="https://ufundoo.com/assets/img/status-remove.png" style="text-align:right; vertical-align:top; position:relative; right:10px; top:-10px; cursor:pointer; padding-right:20px;" class="removedImageIcon" id="deleteCover" onclick="deleteUploadedImageSingle(this,'<?php $str_container[0].'?file='.$str_container[1];?>','image')"></div>
          </div>
        </div>
        <?php } else { ?>
        <div align="left" style="margin-left:200px; margin-top:15px;" id="event_image_wrapper_cover"><img src="" id="event_image_cover" height="60" style="border: 1px solid #ccc;display:none;" /><img src="https://ufundoo.com/assets/img/status-remove.png" style="text-align:right; vertical-align:top; position:relative; right:10px; top:-10px; cursor:pointer; padding-right:20px;display:none;" class="removedImageIcon" id="deleteCover"></div>
        <?php } ?>
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Cover image</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="button" style="width:200px; padding:0px; height:35px; color:#fff; font-size:16px; background-color:#ed258f; border:1px solid #ed258f;font-family: Helvetica; position:absolute; cursor:pointer;" value="Upload file" />
            <input type="file" style="position:relative; top:0; right:0; z-index:2; padding-top:0px; padding-left:0px;opacity:0;height:25px;width:190px;cursor:pointer;border:none" id="fileupload-image-cover"/>
          </div>
        </div>
        <div align="left" style="margin-top:10px;">
          <div style="width:200px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:12px;">Upload any file with .jpg, .jpeg, .png format. You can upload max 1 imgaes for a particular event. Image size must be atleast 700x200.</div>
        </div>
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px; color:#727272;font-family: Helvetica; font-size:16px; float:left;padding-top:12px;">Event video</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <?php 
				if($resultOrganization['video']!=''){ 
			?>
            <div align="left" style="display:table-cell; vertical-align:middle;">
              <input type="text" name="event_video" id="event_video" style="width:715px;" value="<?php echo $resultOrganization['video']; ?>" placeholder="e.g : https://www.youtube.com/watch?v=eu7k0kSvnUo"/>
            </div>
            <?php } else { ?>
            <div align="left" style="display:table-cell; vertical-align:middle;">
              <input type="text" name="event_video" id="event_video" style="width:715px;" placeholder="e.g : https://www.youtube.com/watch?v=eu7k0kSvnUo"/>
            </div>
            <?php } ?>
          </div>
        </div>
        <div align="left" style="margin-top:10px;">
          <div style="width:200px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:12px;">Upload any file with .jpg, .jpeg, .png format. You can upload max 1 imgaes for a particular event. Image size must be atleast 1200x445.</div>
        </div>
        <input type="button" name="previous" class="previous" value="Previous" />
        <input type="button" name="next" class="next" value="Next" attrNum="second" />
        <!--<input type="button" value="Save as draft" class="draft"/>-->
        <input type="button" value="Cancel" class="cancel" onclick="javascript:location.reload()" style="background-color: #ed258f;border: 1px solid #ed258f; cursor:pointer;"/>
        <div style="margin-top:100px; display:none; color:#727272; font-size:14px;font-family: Helvetica;" align="center" class="error"></div>
      </fieldset>
      <fieldset>
        <div align="left">
          <?php if($resultTicket1['ticketName']=="paid"){ ?>
          <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:15px;width:200px;">
            <input type="radio" style="margin-right:10px;" name="ticket_type" value="1" class="ticket_type" checked="checked" onchange="freeTicket(this)" />
            Paid Ticket</div>
          <?php } else { ?>
          <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:15px;width:200px;">
            <input type="radio" style="margin-right:10px;" name="ticket_type" value="1" class="ticket_type" onchange="freeTicket(this)" />
            Paid Ticket</div>
          <?php } if($resultTicket1['ticketName']=="free"){ ?>
          <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:15px;width:200px;">
            <input type="radio" style="margin-right:10px;" name="ticket_type" value="2" class="ticket_type" onchange="freeTicket(this)" checked="checked" />
            Free Ticket</div>
          <?php } else { ?>
          <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:15px;width:200px;">
            <input type="radio" style="margin-right:10px;" name="ticket_type" value="2" class="ticket_type" onchange="freeTicket(this)" />
            Free Ticket</div>
          <?php } ?>
        </div>
        <?php 
			$id=0; 
		 	foreach ( $itemsArrayTicket as $resultTicket ){
			$ticketStatusMul=mysqli_query($mysqli,'select availableQty from ticketStatus where eventId="'.$_REQUEST['eventid'].'" and ticketId="'.$resultTicket['id'].'"');
			$resultTicketStatusMul=mysqli_fetch_assoc($ticketStatusMul);	
			$soldout=$resultTicket['totalQty']-$resultTicketStatusMul['availableQty'];
			$id++;
		 ?>
        <div class="all">
        <input type="hidden" id="ticket_id_<?php echo $resultTicket['id'] ?>" />
        <div align="left" style="margin-top:15px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Ticket name<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="ticket_name" id="ticket_name_<?php echo $id ?>" class="ticket_name check_rows" style="width:215px;" value="<?php echo $resultTicket['name']?>"/>
          </div>
        </div>
        <div align="left">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Total quantity</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="qty_ava" id="qty_ava_<?php echo $id ?>" style="width:215px;" class="qty_ava" value="<?php echo $resultTicket['totalQty']?>" readonly="readonly"/>
          </div>
        </div>
        <div align="left">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Sold out quantity</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" style="width:215px;" id="qty_sold_<?php echo $id ?>" value="<?php echo $soldout;?>" readonly="readonly"/>
          </div>
        </div>
        <div align="left">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Available quantity<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="number" style="width:215px;" id="qty_total_<?php echo $id ?>" value="<?php echo $resultTicketStatusMul['availableQty']?>" max="<?php echo $resultTicketStatusMul['availableQty']?>" onchange="changeTotalMax(this)" onkeyup="changeTotalMax(this)"/>
          </div>
        </div>
        <?php if($resultTicket['ticketName']=="free"){ ?>
        <div align="left" id="ticket_price_wrapper_single" style="display:none">
          <?php } else { ?>
          <div align="left" id="ticket_price_wrapper_single" >
            <?php } ?>
            <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Price<span style="color:#f00">*</span></div>
            <div align="left" style="display:table-cell; vertical-align:middle;">
              <?php 
			  	$str = $resultTicket['price'];
				$price = (explode(" ",$str));
				?>
              <input type="text" name="ticket_price" id="ticket_price_<?php echo $id ?>" class="ticket_price" style="width:215px;" onkeypress="return isNumberKeyFloat(event)" value="<?php echo $price[1]?>"/>
            </div>
          </div>
        </div>
        <?php } ?>
        <?php if($resultTicket['ticketName']=="paid"){ ?>
        <div id="multiTicketWrapper" align="left" style="margin-top:15px;"> </div>
        <div align="left" style="margin-top:15px; height:30px;" id="multi_ticket">
          <div style="width:200px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div style="width:200px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"><span style="color:#727272; font-size:14px;font-family: Helvetica; text-decoration:underline; cursor:pointer; " onclick="addMultiTicket()">Add more ticket</span></div>
        </div>
        <?php } ?>
        <div id="multiTicketWrapper" align="left" style="margin-top:15px; display:none"> </div>
        <div align="left" style="margin-top:15px; height:30px;display:none" id="multi_ticket">
          <div style="width:200px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div style="width:200px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"><span style="color:#727272; font-size:14px;font-family: Helvetica; text-decoration:underline; cursor:pointer; " onclick="addMultiTicket()">Add more ticket</span></div>
        </div>
        <div align="left" style="height:45px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle; color:#727272;font-family: Helvetica; font-size:16px;">Ticket sales timimg<span style="color:#f00">*</span></div>
          <?php $dateFrom  = $resultTicket['ticketFrom'];
			$dateFromPart = explode(" ", $dateFrom);
			$dateTo  = $resultTicket['ticketTo'];
			$dateToPart = explode(" ", $dateTo);
			 ?>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" style="width:100px; margin-right:10px;" id="datepick1" readonly value="<?php echo $dateFromPart[0]?>"/>
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle; padding-left:50px; color:#727272">
            <input type="text" style="width:100px; margin-right:10px;" id="datepicker_time3" value="<?php echo $dateFromPart[1]?>" readonly/>
            <span style="padding-right:20px; padding-left:55px;">to</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" style="width:100px; margin-right:10px;" id="datepick2" readonly value="<?php echo $dateToPart[0]?>"/>
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle;padding-left:50px; color:#727272">
            <input type="text" style="width:100px; margin-right:10px;" id="datepicker_time4" value="<?php echo $dateToPart[1] ?>" readonly/>
          </div>
        </div>
        <input type="button" name="previous" class="previous" value="Previous"/>
        <input type="button" name="next" class="next" value="Next" attrNum="third"/>
        <!--<input type="button" value="Save as draft" class="draft"/>-->
        <input type="button" value="Cancel" class="cancel" onclick="javascript:location.reload()" style="background-color: #ed258f;border: 1px solid #ed258f; cursor:pointer;"/>
        <div style="margin-top:100px; display:none; color:#727272; font-size:14px;font-family: Helvetica;" align="center" class="error"></div>
        
      </fieldset>
      <fieldset>
        <div align="left" style="margin-top:15px; margin-left:200px; color:#727272; font-size:15px;">Save your changes for this event.</div>
        <input type="button" name="previous" class="previous" value="Previous"/>
        <input type="submit" name="submit" class="submit" value="Update" onclick="submitEditEvent('promoter')" />
        <!--<input type="button" value="Save as draft" class="draft"/>-->
        <input type="button" value="Cancel" class="cancel" onclick="javascript:location.reload()" style="background-color: #ed258f;border: 1px solid #ed258f; cursor:pointer;"/>
        <div style="margin-top:100px; display:none; color:#727272; font-size:14px;font-family: Helvetica;" align="center" class="error"></div>
      </fieldset>
      <?php if($_REQUEST['page']=="normal") { ?>
      <input type="hidden" value="<?php echo $_SESSION['userId']; ?>" id="userId" />
      <?php } else { ?>
      <input type="hidden" value="<?php echo $_SESSION['promoterId']; ?>" id="userId" />
      <?php } ?>
   
    </form>
  </div>
  <!-- end here --> 
</div>
<script>
$(document).ready(function(e) {
   $('.btn-admin').eq(0).trigger('click');
	if($(window).width()<1200)
	{
		$('.logout').css({'right':'-8px'});
	}
	 $(function() {
				$( "#datepick" ).datepicker({
			      minDate: 0,
				  showOn: "button",
				  buttonImage: "../assets/img/calander.png",
				  buttonImageOnly: true,
				  buttonText: "Date",
				  dateFormat: 'yy-mm-dd',
			});
	  
  }); 
	  $(function() {
				$( "#datepick1" ).datepicker({
				  minDate: 0,
				  showOn: "button",
				  buttonImage: "../assets/img/calander.png",
				  buttonImageOnly: true,
				  dateFormat: 'yy-mm-dd',
			});
	  
  });
  $(function() {
				$( "#datepick2" ).datepicker({
				  minDate: $( "#datepick1" ).val(),
				  maxDate: $( "#datepick" ).val(),
				  showOn: "button",
				  buttonImage: "../assets/img/calander.png",
				  buttonImageOnly: true,
				  dateFormat: 'yy-mm-dd',
			});
	  
  });
  $(function() {
				$( "#datepicker_time1" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
				 
			});
	  
  }); 
  $(function() {
				$( "#datepicker_time2" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
				 
			});
	  
  });
  $(function() {
				$( "#datepicker_time3" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
				 
			});
	  
  }); 
  $(function() {
				$( "#datepicker_time4" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
				
			});
	  
  });
});
$(".custom-select").each(function(){
            $(this).wrap("<span class='select-wrapper' style='width:115px;'></span>");
            $(this).after("<span class='holder'></span>");
        });
        $(".custom-select").change(function(){
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        }).trigger('change');
		
function typeTicket(button)
{
	$(button).addClass('button_event_upload_color');
	$(button).removeClass('button_event_upload');
	$(button).parent('div').siblings('div').children('div').addClass('button_event_upload');
	$(button).parent('div').siblings('div').children('div').removeClass('button_event_upload_color');
}
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
	if($(this).attr('attrNum')=='first')
	{
		if($('#org_name').val()=='')
		{
			$('.error').html('Please Enter Organizaton Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#org_name').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('#org_email').val())) 
		{
			$('.error').html('Invalid Email Address').fadeIn().delay( 4000 ).fadeOut();
			$('#org_email').focus();
			return false;
		}
		$('#editLocation').trigger('click');
	}
	else if($(this).attr('attrNum')=='second')
	{
		if($('#event_name').val()=='')
		{
			$('.error').html('Please Enter Event Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_name').focus();
			return false;
		}
		if($('#event_type').val()=='Select')
		{
			$('.error').html('Please Select Event Type').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_type').focus();
			return false;
		}
		if($('#event_location').val()=='')
		{
			$('.error').html('Please Enter Event Location').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_location').focus();
			return false;
		}
		if($('#event_address').val()=='')
		{
			$('.error').html('Please Enter Event Address').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_address').focus();
			return false;
		}
		if($('#datepick').val()=='')
		{
			$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepick').focus();
			return false;
		}
		if($('#datepicker_time1').val()=='')
		{
			$('.error').html('Please Select Start Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time1').focus();
			return false;
		}
		if($('#datepicker_time2').val()=='')
		{
			$('.error').html('Please Select End Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time2').focus();
			return false;
		}
		var checkedValueMap=document.getElementById("mapCheck").checked;
		if(checkedValueMap)
		{
			if($('#address').val()=='')
			{
				$('.error').html('Please Enter Address To Generate Map').fadeIn().delay( 4000 ).fadeOut(); // empty username
				$('#address').focus();
				return false;
			}
		}	
	}
	else
	{
		var flagQty=true;
		$('.qty_ava').each(function(index, element) {
			if($(this).val()=='')
			{
				flagQty=false;
			}
		});
		if(!flagQty)
		{
			$('.error').html('Quantity must be minimum 1').fadeIn().delay( 4000 ).fadeOut(); // empty username
			return false;
		}
		var flagName=true;
		$('.ticket_name').each(function(index, element) {
			if($(this).val()=='')
			{
				flagName=false;
			}
		});
		if(!flagName)
		{
			$('.error').html('Please Enter Ticket Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
			return false;
		}
		var ticketTypeRadio=$("input:radio[name=ticket_type]:checked").val();
		if(ticketTypeRadio==1)
		{
			var flagPrice=true;
			$('.ticket_price').each(function(index, element) {
				if($(this).val()=='')
				{
					flagPrice=false;
				}
			});
			if(!flagPrice)
			{
				$('.error').html('Please Enter Ticket Price').fadeIn().delay( 4000 ).fadeOut(); // empty username
				return false;
			}
		}
		if($('#datepick1').val()=='')
		{
			$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepick1').focus();
			return false;
		}
		if($('#datepick2').val()=='')
		{
			$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepick2').focus();
			return false;
		}
		if($('#datepicker_time3').val()=='')
		{
			$('.error').html('Please Select Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time3').focus();
			return false;
		}
		if($('#datepicker_time4').val()=='')
		{
			$('.error').html('Please Select Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time4').focus();
			return false;
		}
	}
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'transform': 'scale('+scale+')'});
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
});
var uploadButton='';
var appendImgPath='';
var appendPath='';
var countNoOfFiles='';
countNoOfFiles='<?php echo mysqli_num_rows($queryImage); ?>';
var s='';
appendPath='<?php echo $imageContent; ?>';

/* for images */
/*$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '../fileupload/server/php/';
	$('#fileupload-image').each(function () {
		$(this).on('click', function () {
			uploadButton = $(this).parent().parent().attr('id');
			//alert(uploadButton);
		});
    $(this).fileupload({
        url: url,
        dataType: 'json',
        maxFileSize: 100000000, // 5 MB
		maxNumberOfFiles: 5,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
		$(".popup-loader").fadeIn();
	}).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        //$('#progress .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
		$(".popup-loader").fadeOut();
		$('#event_image_wrapper').css('display','table-cell');
        $.each(data.result.files, function (index, file) {
		if(countNoOfFiles>=5)
		{
			alert("You can upload max 5 imgaes for a particular event.");
		}
		else
		{
			countNoOfFiles=parseInt(countNoOfFiles)+1;
			appendImgPath=('<div style="margin-top:5px; margin-right:10px; margin-bottom:10px;float:left"><img src="'+file.thumbnailUrl+'" height="60" style="border:1px solid #ccc; max-width:170px;" /><img src="https://ufundoo.com/assets/img/status-remove.png" style="text-align:right; vertical-align:top; position:relative; right:10px; top:-10px; cursor:pointer;" onClick="deleteUploadedImage(this,\''+file.url+'\',\''+file.deleteUrl+'\')" class="removedImageIcon"></div>');
			appendPath+=file.url+'$';
			//alert(appendPath);
			//alert(countNoOfFiles);
		}
		});
		$('#event_image_wrapper').append(appendImgPath);
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
			console.log(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
});
*/

function deleteUploadedImage(imagePos,fileUrl,deleteUrl)
{
	    countNoOfFiles=parseInt(countNoOfFiles)-1;
		$(imagePos).parent('div').remove();
		var str1 = appendPath;
		var str2 = fileUrl;
		appendPath = str1.replace(str2+'$','');	
		
	$.ajax({
    	dataType: 'json',
        type: "DELETE",
        url: deleteUrl,
    }).done(function (data) {
    	console.log(data);
	});
}

function deleteUploadedImageSingle(imagePos,deleteUrl,type)
{
	if(type=="video")
	{
		$(imagePos).siblings('video').attr('src','');
	}
	else
	{
		$(imagePos).siblings('img').attr('src','');
	}
	$(imagePos).parents('#event_image_wrapper_cover').css('display','none');
	$.ajax({
    	dataType: 'json',
        type: "DELETE",
        url: deleteUrl,
    }).done(function (data) {
    	console.log(data);
	});
}


/* for cover image */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '../fileupload/server/php/';
	$('#fileupload-image-cover').each(function () {
		$(this).on('click', function () {
			uploadButton = $(this).parent().parent().attr('id');
			//alert(uploadButton);
		});
    $(this).fileupload({
        url: url,
        dataType: 'json',
        maxFileSize: 5000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
		$(".popup-loader").fadeIn();
	}).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        //$('#progress .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
		$(".popup-loader").fadeOut();
		//$('#event_image_wrapper_cover').hide();
		//$('#event_image_wrapper_cover_new').show();
		//$('#event_image_cover_new').attr('src',file.url);

		$('#event_image_cover').attr('src','');
		$('#event_image_wrapper_cover').show();
		$('#event_image_cover').show();
		$('#deleteCover').show();
		$('#event_image_cover').attr('src',file.url);
		$('#deleteCover').attr('onClick','deleteUploadedImageSingle(this,\''+file.deleteUrl+'\',"image")');
		});
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
			console.log(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
});

function includeMap()
{
	$('#address').val('');
	var checkedValue=document.getElementById("mapCheck").checked;
	if(checkedValue)
	{
		$('#include_map').fadeIn();
		var locationVal=$('#event_location').val();
		$('#address').val(locationVal);
		codeAddress();
	}
	else
	{
		$('#include_map').fadeOut();
		$('#map_canvas_wrapper').fadeOut();
	}
}
function freeTicket(type)
{
	if($(type).val()==2)
	{
		$('#ticket_price_wrapper_single').fadeOut();
		$('#multi_ticket').css('display','none');
		$('#multiTicketWrapper').css('display','none');
		$('.all').hide();
		$('.all').first().show();
	}
	else
	{
		$('#ticket_price_wrapper_single').fadeIn();
		$('#multi_ticket').show();
		$('#multiTicketWrapper').css('display','table-cell');
		$('.all').show();
	}
}
var id=<?php echo $id; ?>;
function addMultiTicket()
{
	$('#ticket_price_wrapper_single').css('margin-bottom','10px');
	$('#multiTicketWrapper').css('display','table-cell');
	++id;
	$('#multiTicketWrapper').append('<div style="border-top:1px solid #ccc; width:940px; padding-top:5px;"><div align="left" style="margin-top:15px;"><div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Ticket name<span style="color:#f00">*</span></div><div align="left" style="display:table-cell; vertical-align:middle;"><input type="text" name="ticket_name" id="ticket_name_'+id+'" class="ticket_name check_rows" style="width:215px;"/></div></div><div align="left" style="margin-top:15px;"><div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Total quantity<span style="color:#f00">*</span></div><div align="left" style="display:table-cell; vertical-align:middle;"><input type="text" name="qty_ava" id="qty_ava_'+id+'" style="width:215px;" onkeypress="return isNumberKey(event)" class="qty_ava"/></div></div><div align="left" style="margin-top:15px; margin-bottom:10px;" class="ticket_price_wrapper"><div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Price<span style="color:#f00">*</span></div><div align="left" style="display:table-cell; vertical-align:middle;"><input type="text" name="ticket_price" id="ticket_price_'+id+'" class="ticket_price" style="width:215px;" onkeypress="return isNumberKeyFloat(event)"/></div></div></div>');
}

/* load map */
var geocoder;
var map;
function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-34.397, 150.644);
  var mapOptions = {
    zoom: 10,
    center: latlng
  }
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
}

function codeAddress() {
	if($('#address').val()=='')
	{
		$('.error').html('Please Enter Address To Generate Map').fadeIn().delay( 4000 ).fadeOut(); // empty username
		$('#address').focus();
		return false;
	}
	else
	{
	$('#map_canvas_wrapper').fadeIn();
	initialize();
	var address = document.getElementById('address').value;
  	geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
    } else {
      //alert('Geocode was not successful for the following reason: ' + status);
    }
  });
  }
}
function editCodeAddress() {
	if($('#editaddress').val()=='')
	{
		$('.error').html('Please Enter Address To Generate Map').fadeIn().delay( 4000 ).fadeOut(); // empty username
		$('#editaddress').focus();
		return false;
	}
	else
	{
	$('#map_canvas_wrapper').fadeIn();
	initialize();
	var address = document.getElementById('editaddress').value;
  	geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
    } else {
      //alert('Geocode was not successful for the following reason: ' + status);
    }
  });
  }
}
</script>