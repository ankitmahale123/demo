<?php
	session_start();
	require_once("../config/conn.php"); 
	$date=date('Y-m-d');
	if($_REQUEST['status']=='disapprove')
	{
		$queryEvent=mysqli_query($mysqli,"select event.id,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate, event.date as eventDateBlock,event.name as eventName,event.coverImage,organization.name as organizationName from event inner join organization on organization.id=event.organizationId where event.status='disapprove' order by id desc");
	}
	else if($_REQUEST['status']=='approve')
	{
		$queryEvent=mysqli_query($mysqli,"select event.id,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate, event.date as eventDateBlock,event.name as eventName,event.coverImage,organization.name as organizationName from event inner join organization on organization.id=event.organizationId where event.status='approve' and event.date>'".$date."' order by id desc");
	}
	else if($_REQUEST['status']=='past')
	{
		$queryEvent=mysqli_query($mysqli,"select event.id,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate, event.date as eventDateBlock,event.name as eventName,event.coverImage,organization.name as organizationName from event inner join organization on organization.id=event.organizationId where event.status='approve' and event.date<'".$date."' order by id desc");
	}
	else
	{
		$queryEvent=mysqli_query($mysqli,"select event.id,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate, event.date as eventDateBlock,event.name as eventName,event.coverImage,organization.name as organizationName from event inner join organization on organization.id=event.organizationId where event.status='pending' order by id desc");
	}
	?>
	<ul id="view-table" style="margin-top:0px; padding:0px;">
					<?php if(mysqli_num_rows($queryEvent)>0){
					while($result=mysqli_fetch_assoc($queryEvent)){
					$date_bt = date_create($result['eventDateBlock']);
					$date_footer = date_create($result['eventDateBlock']);	
					?>
                <!-- event detail wrapper -->
                <li style="list-style-type:none; padding:0px; border:1px solid #cacaca; margin:15px;-webkit-box-shadow: 0 0 5px rgba(0,0,0,0.4);box-shadow: 0 0 5px rgba(0,0,0,0.4); margin-top:10px; margin-bottom:30px;">
            	<div style="height:180px;margin-left:25px;">
                	<!-- image -->
        			<div style="height:180px;width:150px;float:left;position:relative;">
              			<div class="date_banner">
            				<div style="margin: 0px auto; text-align: center;font-family: Helvetica; line-height:23px; margin-top:2px; "><?php echo date_format($date_bt, 'M jS'); ?></span></div>
            				<div style="text-align:center; color:#fff;font-family: Helvetica;"><span><?php echo $result['eventDay']; ?></span></div>
          				</div>
              			<div style="height:140px;width:140px;margin-left:5px;margin-right:5px;vertical-align:middle;margin-top:20px;border-radius:15px;">
                        	<?php 
								/*$queryImage=mysqli_query($mysqli,"select * from eventImage where eventImage.eventId='".$result['id']."' ORDER BY id LIMIT 1");
								if(mysqli_num_rows($queryImage)>0){
								while($resultImage=mysqli_fetch_assoc($queryImage)){
						 	?>
                        	<img src="<?php echo $resultImage['path']; ?>" height="140" width="140" style="border-radius:15px; cursor:pointer;" onError="this.src = '../assets/img/no_image.png'" ondragstart="return false;"/>
                            <?php }}else { ?>
                            <img src="../assets/img/no_image.png" height="140" width="140" />*/
								if($result['coverImage']!='')
							{
								echo '<img src='.$result['coverImage'].' height="140" width="140" style="border-radius:15px; cursor:pointer;" ondragstart="return false;"/>';
							}
							else
							{
								echo '<img src="../assets/img/no_image.png" height="140" width="140" />';
							}
							  ?>
                        </div>
            		</div>
                    
                    <!-- middle content -->
        			<div style="height:180px;width:335px;float:left;margin-left:30px;margin-top:0px;">
              			<div class="event-title" title="<?php echo $result['eventName']; ?>" style="white-space: nowrap; width: 330px;overflow: hidden;text-overflow: ellipsis; cursor:default; "><?php echo $result['eventName']; ?></div>
              			<div style="height:30px;width:335px;font-size:14px;color:#111;"> By <?php echo $result['organizationName']; ?></div>
                        <?php 
							$queryTicket=mysqli_query($mysqli,"select * from ticketStatus where eventId='".$result['id']."'");
						 ?>
              			<div style="height:75px;color:#444;font-size:15px">
                        	<?php while($resultTicket=mysqli_fetch_assoc($queryTicket)){?>
                            	<div style="height:22px;//width:300px;"><?php echo $resultTicket['ticketName']; ?> 
                                <?php if($resultTicket['price']!=''){?>
                                - <?php echo $resultTicket['price']; ?><span style="margin-left:10px;">(Total: <?php echo $resultTicket['totalQty']; ?><span style="margin-left:5px; margin-right:5px;">|</span>Sold: <?php echo ($resultTicket['totalQty']-$resultTicket['availableQty']); ?>)</span> 
                                <?php } else { ?>
                                - Free Ticket<span style="margin-left:10px;">(Total: <?php echo $resultTicket['totalQty']; ?><span style="margin-left:5px; margin-right:5px;">|</span>Sold: <?php echo ($resultTicket['totalQty']-$resultTicket['availableQty']); ?>)</span>
                                <?php } ?>
                                </div>
                            <?php } ?>
          				</div>
            		</div>
                    
                    <!-- buttons -->
        			<div style="height:180px;float:left;">
                    	<?php if($_REQUEST['status']=='pending') { ?>
                            <div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:25px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px; line-height:14px;" onclick="adminEditEvent('<?php echo $result['id']; ?>')">Edit</div>
              			<div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:15px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px; line-height:14px;" onclick="approveEvent('<?php echo $result['id']; ?>')">Approve</div>
              			<div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:15px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px; line-height:14px;" onclick="dispproveEventPopup('<?php echo $result['id']; ?>')">Disapprove</div>
                        <?php } else if($_REQUEST['status']=='approve'){ ?>
                          <div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:25px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px; line-height:14px;" onclick="adminEditEvent('<?php echo $result['id']; ?>')">Edit</div>
                        <div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:15px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px; line-height:14px;" onclick="view_attendee_admin('<?php echo $result['id']; ?>')">View Attendee</div>
                        <div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:15px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px; line-height:14px;" onclick="archive('<?php echo $result['id']; ?>')">Delete</div>
          <?php      } else if($_REQUEST['status']=='disapprove'){?>
						<div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:75px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px; line-height:14px;" onclick="approveEvent('<?php echo $result['id']; ?>')">Approve</div>	
						<?php } else { ?>
                        <?php } ?>
            		</div>
      			</div>
                <div style="height:50px; background:#ebebeb">
                	<div style="display:table-cell; width:139px; height:50px; border-right:1px solid #999;">
                    	<div style="display:table-row;">
                        	<div style="display:table-cell; vertical-align:middle; height:25px; color:#ed258f; font-size:15px; width:139px; text-align:center">Date</div>
                        </div>
                        <div style="display:table-row;">
                        	<div style="display:table-cell; vertical-align:middle; height:25px; color:#727272; font-size:15px;width:139px; text-align:center"><?php echo date_format($date_footer, 'm-d-Y'); ?></div>
                        </div>
                    </div>
                    <?php if($_REQUEST['status']=='pending') { }else {?>
                    <div style="display:table-cell; width:119px; height:50px; border-right:1px solid #999;">
                    	<div style="display:table-row;">
                        	<div style="display:table-cell; vertical-align:middle; height:25px; color:#ed258f; font-size:15px;width:139px; text-align:center">Total Views</div>
                        </div>
                        <?php 
							$queryEventIdViews=mysqli_query($mysqli,"select id,totalViews from event where id='".$result['id']."'");
							$resultViews=mysqli_fetch_assoc($queryEventIdViews);
						?>
                        <div style="display:table-row;">
                        	<div style="display:table-cell; vertical-align:middle; height:25px; color:#727272; font-size:15px;width:139px; text-align:center"><?php echo $resultViews['totalViews']; ?></div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php /*?><div style="display:table-cell; width:119px; height:50px; border-right:1px solid #999;">
                    	<div style="display:table-row;">
                        	<div style="display:table-cell; vertical-align:middle; height:25px; color:#ed258f; font-size:15px;width:139px; text-align:center">Total RSVP</div>
                        </div>
                        <?php $queryEventId=mysqli_query($mysqli,"select * from rsvp where eventId='".$result['id']."'"); ?>
                        <div style="display:table-row;">
                        	<div style="display:table-cell; vertical-align:middle; height:25px; color:#727272; font-size:15px;width:139px; text-align:center"><?php echo mysqli_num_rows($queryEventId);?></div>
                        </div>
                     </div><?php */?>
                     <?php /*?><div style="display:table-cell; width:179px; height:50px; border-right:1px solid #999;">
                    	<div style="display:table-row;">
                        	<div style="display:table-cell; vertical-align:middle; height:25px; color:#ed258f; font-size:15px;width:179px; text-align:center">Total Comments/Reviews</div>
                        </div>
                        <?php $queryEventReview=mysqli_query($mysqli,"select * from review where eventId='".$result['id']."'"); ?>
                        <div style="display:table-row;">
                        	<div style="display:table-cell; vertical-align:middle; height:25px; color:#727272; font-size:15px;width:179px; text-align:center"><?php echo mysqli_num_rows($queryEventReview);?></div>
                        </div>
                     </div><?php */?>
                </div>
                <!-- end here -->
                <?php } } else { ?>
                	<div align="center" style="width:100%; font-size:25px; color:#727272;font-family: Helvetica; margin-top:180px;">There is no events in this category.</div>
                    <script>$('.holder-wrapper').fadeOut();</script>
				<?php } ?>
                </li>
                </ul>
				<div class="holder-wrapper" align="center" style="margin-bottom:20px;height:30px; width:720px;"></div>