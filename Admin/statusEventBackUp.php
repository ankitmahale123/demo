<?php
	session_start();
	require_once("../config/conn.php"); 
	$date=date('Y-m-d');
	if($_REQUEST['status']=='disapprove')
	{
		$queryEvent=mysqli_query($mysqli,"select event.id,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.name as eventName,organization.name as organizationName from event inner join organization on organization.id=event.organizationId where event.status='disapprove'");
	}
	else if($_REQUEST['status']=='approve')
	{
		$queryEvent=mysqli_query($mysqli,"select event.id,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.name as eventName,organization.name as organizationName from event inner join organization on organization.id=event.organizationId where event.status='approve' and event.date>'".$date."'");
	}
	else if($_REQUEST['status']=='past')
	{
		$queryEvent=mysqli_query($mysqli,"select event.id,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.name as eventName,organization.name as organizationName from event inner join organization on organization.id=event.organizationId where event.status='approve' and event.date<'".$date."'");
	}
	else
	{
		$queryEvent=mysqli_query($mysqli,"select event.id,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.name as eventName,organization.name as organizationName from event inner join organization on organization.id=event.organizationId where event.status='pending'");
	}
	?>
	<ul id="view-table" style="margin-top:0px; padding:0px;">
					<?php if(mysqli_num_rows($queryEvent)>0){
					while($result=mysqli_fetch_assoc($queryEvent)){?>
                <!-- event detail wrapper -->
                <li style="list-style-type:none; padding:0px;">
            	<div style="height:265px;margin-left:40px;">
                	<!-- image -->
        			<div style="height:265px;width:150px;float:left;position:relative;">
              			<div class="date_banner">
            				<div style="margin: 0px auto; text-align: center;font-family: Helvetica; "><?php echo $result['eventMonth']; ?> &nbsp;<span style="font-size:23px;font-family: Helvetica; "><?php echo $result['eventDate']; ?></span></div>
            				<div style="text-align:center; color:#000;font-family: Helvetica;"><span><?php echo $result['eventDay']; ?></span></div>
          				</div>
              			<div style="height:140px;width:140px;margin-left:5px;margin-right:5px;vertical-align:middle;margin-top:55px;border-radius:15px;">
                        	<?php 
								$queryImage=mysqli_query($mysqli,"select * from eventImage where eventImage.eventId='".$result['id']."' ORDER BY id LIMIT 1");
								if(mysqli_num_rows($queryImage)>0){
								while($resultImage=mysqli_fetch_assoc($queryImage)){
						 	?>
                        	<img src="<?php echo $resultImage['path']; ?>" height="140" width="140" style="border-radius:15px; cursor:pointer;" onError="this.src = '../images/no_image.png'" ondragstart="return false;"/>
                            <?php }}else { ?>
                            <img src="../images/no_image.png" height="140" width="140" />
                            <?php } ?>
                        </div>
            		</div>
                    
                    <!-- middle content -->
        			<div style="height:235px;width:490px;float:left;margin-left:40px;margin-top:30px;">
              			<div class="event-title"><?php echo $result['eventName']; ?></div>
              			<div style="height:35px;width:300px;font-size:14px;color:#111;"> By <?php echo $result['organizationName']; ?></div>
                        <?php 
							$queryTicket=mysqli_query($mysqli,"select ticket.name as ticketName,ticket.price as ticketPrice from ticket where ticket.eventId='".$result['id']."'");
						 ?>
              			<div style="height:75px;//width:300px;color:#444;font-size:15px">
                        	<?php while($resultTicket=mysqli_fetch_assoc($queryTicket)){?>
                            	<div style="height:22px;//width:300px;"><?php echo $resultTicket['ticketName']; ?> 
                                <?php if($resultTicket['ticketPrice']!=''){?>
                                - <?php echo $resultTicket['ticketPrice']; ?> 
                                <?php } else { ?>
                                - Free Ticket
                                <?php } ?>
                                </div>
                            <?php } ?>
          				</div>
            		</div>
                    
                    <!-- buttons -->
        			<div style="height:235px;float:left;">
                    	<?php if($_REQUEST['status']=='pending') { ?>
              			<div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:70px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px" onclick="approveEvent('<?php echo $result['id']; ?>')">Approve</div>
              			<div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:15px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px" onclick="dispproveEvent('<?php echo $result['id']; ?>')">Disapprove</div>
                        <?php } else if($_REQUEST['status']=='approve'){ ?>
                        <div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:70px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px" onclick="archive('<?php echo $result['id']; ?>')">Delete</div>
                        <?php $queryImage2=mysqli_query($mysqli,"select * from featuredImage where eventId='".$result['id']."'");
								$resultImage2=mysqli_fetch_assoc($queryImage2);
								if($resultImage2['status']==0){ 
						?>
                        <div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:15px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px" onclick="featureEventImage('<?php echo $result['id']; ?>',this)" class="feature_image">Feature Image</div>
                        <?php } else { ?>
                        <div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:15px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px" onclick="featureEventImage('<?php echo $result['id']; ?>',this)" class="feature_image">Featured Image</div>
                        <?php }} else { ?>
                        <!--<div style="height:25px;width:140px;border-radius:4px;background:#727272;margin-left:auto;margin-right:auto;margin-top:105px;cursor:pointer;color:white;text-align:center;padding-top:9px;font-size:14px" onclick="approveEvent('<?php /*?><?php echo $result['id']; ?><?php */?>')">Approve</div>-->
                        <?php } ?>
            		</div>
      			</div>
                <!-- end here -->
                <?php } } else { ?>
                	<div align="center" style="width:100%; font-size:25px; color:#727272;font-family: Helvetica; margin-top:180px;">There is no events in this category.</div>
                    <script>$('.holder-wrapper').fadeOut();</script>
				<?php } ?>
                </li>
                </ul>
				<div class="holder-wrapper" align="center" style="margin-bottom:20px;height:30px; width:720px;"></div>