<?php
	session_start();
	require_once("../config/conn.php"); 	
	if(!isset($_SESSION["adminId"]))
	{
		header("Location:index.php");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UFundoo | Admin</title>
<link rel="shortcut icon" href="../assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="../assets/css/ufundoo.css" type="text/css" />
<link rel="stylesheet" href="../assets/css/jPages.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="../assets/css/tooltipster.css" />
<script src="../assets/js/jquery-1.9.1.min.js"></script>
<script src="../assets/js/jquery-ui.js"></script>
<script src="../assets/js/ufundoo.js"></script>
<script src="../assets/js/jPages.js"></script>
<script src="../assets/js/jquery.nicescroll.js"></script>
<script src="../fileupload/js/vendor/jquery.ui.widget.js"></script> 
<!-- The Load Image plugin is included for the preview images and image resizing functionality --> 
<script src="../fileupload/js/load-image.all.min.js"></script> 
<!-- The Canvas to Blob plugin is included for image resizing functionality --> 
<script src="../fileupload/js/canvas-to-blob.min.js"></script> 
<!-- The Iframe Transport is required for browsers without support for XHR file uploads --> 
<script src="../fileupload/js/jquery.iframe-transport.js"></script> 
<!-- The basic File Upload plugin --> 
<script src="../fileupload/js/jquery.fileupload.js"></script> 
<!-- The File Upload processing plugin --> 
<script src="../fileupload/js/jquery.fileupload-process.js"></script> 
<!-- The File Upload image preview & resize plugin --> 
<script src="../fileupload/js/jquery.fileupload-image.js"></script> 
<!-- The File Upload validation plugin --> 
<script src="../fileupload/js/jquery.fileupload-validate.js"></script> 
<script src="ckeditor/ckeditor.js"></script> 
<script src="ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="../assets/js/jquery.tooltipster.js"></script> 


<link rel="stylesheet" href="../assets/css/cal.css" type="text/css" />

<link rel="stylesheet" href="../assets/css/uploadEvent.css" type="text/css" />

<script src="../assets/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['corechart']}]}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>

<!-- jQuery easing plugin -->
<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
<script src="ckeditor/ckeditor.js"></script> 
<script src="ckeditor/adapters/jquery.js"></script> 
<!-- maxlength.js -->
<script src="../assets/js/maxlength.js"></script> 
<style>
.ui-datepicker-trigger{margin-top:4px; height:27px; position:absolute; cursor:pointer}
.ui-datepicker-div{padding-left:7px;}
.ui-timepicker-div{padding-left:7px; height:140px;}
.ui-slider .ui-slider-handle{height:0.8em; width:0.8em;top:-.4em}
input[type=button],textarea
{
	outline:none;
	-webkit-appearance: none; /*Safari/Chrome*/
    -moz-appearance: none; /*Firefox*/
    -ms-appearance: none; /*IE*/
    -o-appearance: none; /*Opera*/
    appearance: none;
}
.select-wrapper{
		float: left;
		display: inline-block;
		background:url(../assets/img/arrow.png) no-repeat right 8px center;
		cursor: pointer;
		
	}
	.select-wrapper, .select-wrapper select{
		width: 125px;
		//height: 40px;
		height:30px;
		//line-height: 26px;
		line-height: 30px;
		font-size:15px;
	}
	
	.select-wrapper .holder{
		display: block;
		//margin: 0 35px 0 5px;
		white-space: nowrap;            
		overflow: hidden;
		cursor: pointer;
		position: relative;
		//padding-top:7px;
		z-index: -1;
		color:#444;
		width:125px;
	}
	.select-wrapper select{
		margin: 0;
		position: absolute;
		z-index: 2;            
		cursor: pointer;
		outline: none;
		opacity: 0;
		/* CSS hacks for older browsers */
		_noFocusLine: expression(this.hideFocus=true); 
		-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
		filter: alpha(opacity=0);
		-khtml-opacity: 0;
		-moz-opacity: 0;
		width:125px;
	}
	.custom-select
	{
		background-color:#fff;
	}
	.headerBtn {
    color: #2e302d;
}
/* admin button */
.admin-btn
{
	width:150px;
	height:30px;
	padding-left:5px;
	padding-right:5px;
	background-color:#7da60a;
	display:table;
	text-align:center;
	cursor:pointer;
	font-family: calibri;
}
.admin-btn span
{
	color:#fff;
	font-size:18px;
	display:table-cell;
	vertical-align:middle;
}
#admin-popup-delete,#admin-popup-delete-featured-image,#admin-popup-delete-disapprove
{
	position:fixed;
	height:100%;
	width:100%;
	z-index:99999;
	display:none;
	background:rgba(0,0,0,0.7);
}
#admin-popup-deleteAll
{
	position:fixed;
	height:100%;
	width:100%;
	z-index:99999;
	display:none;
	background:rgba(0,0,0,0.7);
}
.admin-popup-delete-container
{
	position:fixed;
	width:330px;
	height:150px;
	left:50%;
	top:50%;
	margin-left:-205px;
	margin-top:-75px;
	z-index:100000;
	background:#fff;
}
.admin-delete-class
{
	width:330px;
	height:150px;
	margin-right:10px;
}
.admin-popup-delete-container-disapprove
{
	position:fixed;
	width:500px;
	height:300px;
	left:50%;
	top:50%;
	margin-left:-250px;
	margin-top:-150px;
	z-index:100000;
	background:#fff;
}
.admin-delete-class-disapprove
{
	width:500px;
	height:240px;
	margin-right:10px;
}
#logo-close
{
	position:absolute;
	right:5px;
	top:5px;
	cursor:pointer;
}
.select-wrapper{
		float: left;
		display: inline-block;
		background:url(../assets/img/arrow.png) no-repeat right 8px center;
		cursor: pointer;
		
	}
	.select-wrapper, .select-wrapper select{
		width: 125px;
		//height: 40px;
		height:30px;
		//line-height: 26px;
		line-height: 30px;
		font-size:15px;
	}
	
	.select-wrapper .holder{
		display: block;
		//margin: 0 35px 0 5px;
		white-space: nowrap;            
		overflow: hidden;
		cursor: pointer;
		position: relative;
		//padding-top:7px;
		z-index: -1;
		color:#444;
		width:125px;
	}
	.select-wrapper select{
		margin: 0;
		position: absolute;
		z-index: 2;            
		cursor: pointer;
		outline: none;
		opacity: 0;
		/* CSS hacks for older browsers */
		_noFocusLine: expression(this.hideFocus=true); 
		-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
		filter: alpha(opacity=0);
		-khtml-opacity: 0;
		-moz-opacity: 0;
		width:125px;
	}
	.custom-select
	{
		background-color:#fff;
	}
.headerBtn {
    color: #2e302d;
}
.btn-admin {
    padding: 9px 18px;
}
</style>
</head>
<body>
<!-- loader -->
<div class="loading" style="display:none">
	<?php 
    	include('../loader.php');
    ?>
</div>
<!-- end here -->

<div id="admin-popup-delete">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class"> <img src="../assets/img/btn_close.png" id="logo-close" onclick="show_delete_close()" height="20" width="20" />
      <div style="text-align:center; margin-top:40px; font-family: calibri;">Are you sure you want to delete this event?</div>
      <div style="margin-top:30px; width:200px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:50px; border:1px solid #ed258f; background:#ed258f; border-radius:15px; color:#fff"> <span onclick="delete_yes()">Yes</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:50px; border:1px solid #ed258f; background:#ed258f; border-radius:15px; color:#fff"> <span onclick="delete_no()">No</span> </div>
      </div>
    </div>
  </div>
</div>
<div id="admin-popup-delete-featured-image">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class"> <img src="../assets/img/btn_close.png" id="logo-close" onclick="close_image()" height="20" width="20" />
      <div style="text-align:center; margin-top:40px; font-family: calibri;">Are you sure you want to delete this image?</div>
      <div style="margin-top:30px; width:200px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:50px; border:1px solid #ed258f; background:#ed258f; border-radius:15px; color:#fff"> <span onclick="delete_yes_image()">Yes</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:50px; border:1px solid #ed258f; background:#ed258f; border-radius:15px; color:#fff"> <span onclick="delete_no_image()">No</span> </div>
      </div>
    </div>
  </div>
</div>
<div id="admin-popup-delete-disapprove">
  <div class="admin-popup-delete-container-disapprove">
    <div class="admin-delete-class-disapprove"> <img src="../assets/img/btn_close.png" id="logo-close" onclick="close_event()" height="20" width="20" />
      <div style="text-align:center; margin-top:25px; font-family: calibri; font-size:19px;">Please give reason why you are disapproving this event?</div>
      <div style="text-align:center; margin-top:20px; font-family: calibri;">
      	<textarea style="padding: 10px; border: 1px solid #ccc; border-radius: 20px; margin-bottom: 10px; font-family: lator; color: #2C3E50; font-size: 13px; line-height: 19px; width:390px; height:100px; resize:none; outline:none" id="disapprove_reason"></textarea>
      </div>
      <div style="margin-top:10px; width:350px; margin-left:auto; margin-right:auto; height:30px;">
        <div id="yes-btn" class="admin-btn" style="float:left; height:35px; border-radius:18px; border:1px solid #ed258f; background:#ed258f;color:#fff"> <span onclick="delete_yes_event()">Confirm</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right;  height:35px; border-radius:18px; border:1px solid #ed258f; background:#ed258f;color:#fff"> <span onclick="delete_no_event()">Cancel</span> </div>
      </div>
    </div>
    <div class="errorLogin" style="text-align:center"></div>
  </div>
</div>
<div class="wrapper">
    <!-- header -->
	<div class="header">
        <?php include('../include/header-admin.php'); ?>
    </div>
    <!-- end here -->
    
    <!-- container -->
  	<div class="content" align="center">
    	
        <div style="display:table;height:60px; width:950px; margin-top:10px;margin-left: auto; margin-right: auto;" id="submenuFill">
        	<!-- top menu -->
            <div style="display:table-cell; vertical-align:middle;height:60px; height:auto; width:250px;">
            </div>
            <div style="display:table-cell; vertical-align:middle; width:700px;height:60px; height:auto;" id="eventsTab">
            	<div style="display:table-row">
                	<div style="display:table-cell; vertical-align:middle; width:150px; height:60px; font-size:15px; padding-right:10px;">
                    	<div class="btn-admin" align="center" onclick="fetchEvent(this,'pending')">NEW</div>
                    </div>
                    <div style="display:table-cell; vertical-align:middle; width:150px; height:60px;  font-size:15px; padding-right:10px;">
                    	<div class="btn-admin" align="center" onclick="fetchEvent(this,'approve')">CURRENT</div>
                    </div>
                    <div style="display:table-cell; vertical-align:middle; width:150px; height:60px; font-size:15px; padding-right:10px;">
                    	<div class="btn-admin" align="center" onclick="fetchEvent(this,'past')">PAST</div>
                    </div>
                    <div style="display:table-cell; vertical-align:middle; width:150px; height:60px; font-size:15px;">
                    	<div class="btn-admin" align="center" onclick="fetchEvent(this,'disapprove')">DISAPPROVED</div>
                    </div>
                </div>
            </div>
            <div style="display:none; vertical-align:middle; width:700px;height:60px; height:auto;" id="promotersTab">
            	<div style="display:table-row">
                	<div style="display:table-cell; vertical-align:middle; width:150px; height:60px; font-size:15px; padding-right:10px;">
                    	<div class="btn-admin" align="center" onclick="fetchPromoter(this,'pending')">NEW</div>
                    </div>
                    <div style="display:table-cell; vertical-align:middle; width:150px; height:60px;  font-size:15px; padding-right:10px;">
                    	<div class="btn-admin" align="center" onclick="fetchPromoter(this,'approve')">APPROVED</div>
                    </div>
                    <div style="display:table-cell; vertical-align:middle; width:150px; height:60px; font-size:15px;">
                    	<div class="btn-admin" align="center" onclick="fetchPromoter(this,'disapprove')">DISAPPROVED</div>
                    </div>
                </div>
            </div>
            <!-- end here -->
        </div>
    
    	<!-- main content -->
        <div style="display:table; min-height:400px; height:auto; width:950px;margin-left: auto; margin-right: auto;">
                
        	<!-- left side list -->
            <div style="display:table-cell; vertical-align:top; width:220px;min-height:400px; height:auto; padding-right:12px;">
            	<div style="width:200px; margin-left:15px; border-radius:6px; min-height:400px; text-transform:uppercase; height:auto; background:#ebebeb; display:table;">
                	<div style="height:20px; padding-top:0px;" id="extra_top_menu_list"></div>
                	<div class="menuActive" onclick="fetchMenu(this,'events')">EVENTS</div>
                    <div class="menuDefault" onclick="fetchMenu(this,'promotion')">PROMOTION</div>
                    <div class="menuDefault admin-content" onclick="fetchContentMenu(this,'image')">featured image</div>
                    <div class="menuDefault admin-update" onclick="fetchContentMenu(this,'newsUpdate')" >News & Updates</div>
                    <div class="menuDefault" onclick="fetchMenu(this,'newpromoter')">PROMOTER</div>
                    <div class="menuDefault" onclick="fetchMenu(this,'subscriber')">SUBSCRIBER</div>
                    <div class="menuDefault" onclick="fetchMenu(this,'user')">USERS</div>
                    <div class="menuDefault" onclick="fetchMenu(this,'inquiries')">INQUIRIES/SUGGESTION</div>
                    <div class="menuDefault" onclick="fetchMenu(this,'jobs')">JOB POSTING</div>
					<div class="menuDefault"  onclick="window.open('promotionalEvent.php');" target="_blank"  >PROMOTE AN EVENT</div>
                </div>
            </div>
            <!-- end here -->
            
            <!-- right side list -->
            <div style="display:table-cell;width:700px;min-height:400px; height:auto; border-left:2px solid #ebebeb;" id="dataFill">
            </div>
            <!-- end here -->
        </div>
        <!-- end here -->
           
    </div>
    <!-- end here -->
    
    <!-- header -->
<?php /*?>	<div class="footer">
        <?php include('../include/footer.php'); ?>
    </div>
<?php */?>    <!-- end here -->
</div>
<script>
$(document).ready(function(e) {
    $('.btn-admin').eq(0).trigger('click');
	if($(window).width()<1200)
	{
		$('.logout').css({'right':'-8px'});
	}
	$('.tooltip').tooltipster({
				interactive: true
				});
});
var global_id;
var global_id_image;
function archive(id)
{
	global_id=id;
	$("#admin-popup-delete").fadeIn();
}
function archiveImage(id)
{
	global_id_image=id;
	$("#admin-popup-delete-featured-image").fadeIn();
}
function show_delete_close()
{
	$("#admin-popup-delete").fadeOut();
}
function close_image()
{
	$("#admin-popup-delete-featured-image").fadeOut();
}
function delete_no()
{
	$("#admin-popup-delete").fadeOut();
}
function delete_no_image()
{
	$("#admin-popup-delete-featured-image").fadeOut();
}
function delete_yes()
{
	$.ajax({
					url: "../Ajax/deleteEvent.php",
					type:"POST",
					data:"id="+encodeURIComponent(global_id),
					dataType : "json",
					success: function(json) {
						console.log(json.msg);
          				if(json.success==1)
						{
							$("#admin-popup-delete").fadeOut();
							alert('Successfully deleted event');
							$('.btn-admin').eq(1).trigger('click');
						}
						else
						{
							alert('Error');
						}
						}
					});
}
function delete_yes_image()
{
	$.ajax({
		url: '../Ajax/deleteFeaturedImage.php',
		type:"POST",
		data:"imageId="+global_id_image,
		dataType : "json",
		success: function(json) {
			if(json.success==1)
			{
				$("#admin-popup-delete-featured-image").fadeOut();
				alert('Successfully deleted image');
				$('.btn-admin').eq(3).trigger('click');
			}
			else
			{
				alert('error');
			}
		}
	});
}

/* disapprove event */
var global_id_event;
function dispproveEventPopup(disapproveId)
{
	global_id_event=disapproveId;
	$("#admin-popup-delete-disapprove").fadeIn();
}
function delete_yes_event(disapproveId)
{
	if($('#disapprove_reason').val()=='')
	{
		$('.errorLogin').html('Please enter reason for disapproving event').fadeIn().delay( 4000 ).fadeOut();
		return false;
	}
	else
	{
		$.ajax({
						url: "../Ajax/eventAction.php",
						type:"POST",
						data:"actionId="+encodeURIComponent(global_id_event)+"&reason="+encodeURIComponent($('#disapprove_reason').val())+"&action=disapprove",
						dataType : "json",
						success: function(json) {
							console.log(json.success);
							if(json.success==1)
							{
								alert("Disapproved Event Successfully");
								location.reload();
							}
							else
							{
								alert("Error");
							}
						}
			});
	}
}
function delete_no_event()
{
	$("#admin-popup-delete-disapprove").fadeOut();
}
function close_event()
{
	$("#admin-popup-delete-disapprove").fadeOut();
}

/* end here */

</script>
</body>
</html>