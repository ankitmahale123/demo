<?php 
require_once("config/conn.php");
session_start();
$_SESSION['promoterId']='';
unset($_SESSION['promoterId']);
$_SESSION['promoterEmail']='';
unset($_SESSION['promoterEmail']);
$_SESSION['username']='';
unset($_SESSION['username']);
if(isset($_SESSION['page']))
{
	$_SESSION['page']='';
	unset($_SESSION['page']);
	session_destroy();
	header("Location: Promoter/index.php");
}
session_destroy();
header("Location: ../index.php");
?>