<?php
	session_start();
	require_once("config/conn.php"); 
	
	//  ticketStatus is a view
	$sql_ticket = mysqli_query($mysqli,"SELECT * FROM ticketStatus WHERE eventId='".$_REQUEST['eventId']."' and availableQty!=0");
	$sql_ticket1 = mysqli_query($mysqli,"SELECT * FROM ticketStatus WHERE eventId='".$_REQUEST['eventId']."' and availableQty!=0");
	$result1=mysqli_fetch_assoc($sql_ticket1);
	
	// get service charge from ticket table
	$service_charge = mysqli_query($mysqli,"SELECT serviceCharge FROM ticket WHERE eventId='".$_REQUEST['eventId']."'");
	$service=mysqli_fetch_assoc($service_charge);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Ufundoo | Seat Chart</title>
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<link rel="stylesheet" href="assets/css/datepicker.css" type="text/css" />
<style>
html, body {
	background: #fff;
	overflow:hidden;
}
.headerBtnActive {
	border: 1px solid #2e302d;
	color:#2e302d;
}
.headerBtn {
	color:#2e302d;
}
.searchBar {
	height:80px;
	background:none;
	margin-bottom:25px;
}
.searchBarContainer {
	height:80px;
	border-radius:80px;
	width:95%;
}
.searchBarWrapper {
	height: 60px;
	margin-top: 3px;
}
.searchLocation, .category {
	padding-left:0.7%;
	width: 20%;
	padding-right:1%;
	display: inline-block;
}
.searchBox {
	font-size:17px;
	padding-left:20px;
}
.searchBtn {
	padding-top:10px;
	margin-top:5px;
}
.eachBlockWrapper {
	margin-top:0px;
	height:auto;
}
.eachBlockWrapperCont {
	margin-bottom:2%;
	margin-left:1%;
	margin-right:1%;
	width:22.7%;
	position: relative;
}
.mostPopEventsLine {
	margin-bottom:25px;
}
.form-control:focus {
	border-color: #9a9a9a;
	outline: 0;
	-webkit-box-shadow:none;
	box-shadow:none;
}
.styled-select #categories {
	background: transparent;
	width: 268px;
	padding: 5px 5px 5px 30px;
	font-size: 16px;
	line-height: 1;
	border: 0;
	border-radius: 0;
	height: 34px;
	-webkit-appearance: none;
}
.styled-select {
	width: 240px;
	height: 34px;
	overflow: hidden;
	background: url(http://cdn.bavotasan.com/wp-content/uploads/2011/05/down_arrow_select.jpg) no-repeat left #ddd;
	border: 1px solid #ccc;
}
@media screen and (max-width: 1230px) {
 .eachBlockWrapper div:nth-child(4) {
 display:inline-block;
}
 .eachBlockWrapperCont {
 width:31%;
}
 .searchLocation, .category {
 padding-left:0.5%;
 padding-right:0.5%;
}
 .searchLocation {
 padding-left:1%;
}
 .searchBtnWrapper {
 padding-right:5px;
}
 .footerCont {
 width:93%;
}
}
.proceed_to_payment_enable {
	height: 40px;
	width: 240px;
	border-radius:20px;
	background:#ed258f;
	cursor:pointer;
	color: white;
	text-align:center;
	padding-top:11px;
	font-size:14px;
	font-family:lator;
	display:none;
}
.proceed_to_payment_disable {
	height: 40px;
	width: 240px;
	border-radius:20px;
	background:#ccc;
	cursor:default;
	color: white;
	text-align:center;
	padding-top:11px;
	font-size:14px;
	font-family:lator;
}
/* choose account for checkout */
.admin-btn-acct {
	width:150px;
	height:30px;
	padding-left:5px;
	padding-right:5px;
	background-color:#7da60a;
	display:table;
	text-align:center;
	cursor:pointer;
	font-family: calibri;
}
.admin-btn-acct span {
	color:#fff;
	font-size:18px;
	display:table-cell;
	vertical-align:middle;
}
#acct-user-popup {
	position:fixed;
	height:100%;
	width:100%;
	z-index:99999;
	display:none;
	background:rgba(0, 0, 0, 0.7);
}
#admin-popup-deleteAll-acct {
	position:fixed;
	height:100%;
	width:100%;
	z-index:99999;
	display:none;
	background:rgba(0, 0, 0, 0.7);
}
.admin-popup-delete-container-acct {
	position:fixed;
	width:400px;
	height:200px;
	left:50%;
	top:50%;
	margin-left:-200px;
	margin-top:-100px;
	z-index:100000;
	background:#fff;
	border-radius:35px;
}
.admin-delete-class-acct {
	width:400px;
	height:200px;
	margin-right:10px;
}
#logo-close-acct {
	position:absolute;
	right:10px;
	top:10px;
	cursor:pointer;
}
/* guest button */
.admin-btn-guest {
	width:150px;
	height:30px;
	padding-left:5px;
	padding-right:5px;
	background-color:#7da60a;
	display:table;
	text-align:center;
	cursor:pointer;
	font-family: calibri;
}
.admin-btn-guest span {
	color:#fff;
	font-size:18px;
	display:table-cell;
	vertical-align:middle;
}
#guest-user-popup {
	position:fixed;
	height:100%;
	width:100%;
	z-index:99999;
	display:none;
	background:rgba(0, 0, 0, 0.7);
}
#admin-popup-deleteAll-guest {
	position:fixed;
	height:100%;
	width:100%;
	z-index:99999;
	display:none;
	background:rgba(0, 0, 0, 0.7);
}
.admin-popup-delete-container-guest {
	position:fixed;
	width:360px;
	height:160px;
	left:50%;
	top:50%;
	margin-left:-180px;
	margin-top:-80px;
	z-index:100000;
	background:#fff;
	border-radius:35px;
}
.admin-delete-class-guest {
	width:360px;
	height:135px;
	margin-right:10px;
}
#logo-close-guest {
	position:absolute;
	right:10px;
	top:10px;
	cursor:pointer;
}
#signin-popup-checkout {
	height: 100%;
	width: 100%;
	position:fixed;
	z-index: 999;
	background:hsla(0, 0%, 0%, 0.7) none repeat scroll 0 0;
	display:none;
}
#signin_page_checkout {
	position:fixed;
	width:520px;
	height:340px;
	left:50%;
	top:50%;
	margin-left:-260px;
	margin-top:-170px;
	z-index:100000;
	background:#fff;
	border-radius:35px;
}
</style>
</head>
<body>
<!-- loader -->
<div class="loading" style="display:none">
  <?php 
    	include('loader.php');
    ?>
</div>
<!-- end here --> 

<!-- forgot password -->
<div id="forgot_password">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class" style="width:400px;"> <img src="assets/img/btn_close.png" id="logo-close" onclick="fp_close()" height="20" width="20" style="top:10px; right:10px;" />
      <div style="text-align:center; margin-top:35px; font-size:20px; font-weight:bold">Please provide email address</div>
      <div style="text-align:center; margin-top:20px; font-family: calibri;">
        <input type="text" style="" placeholder="Email" class="fp_text" />
      </div>
      <div style="margin-top:15px; width:250px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="forgot_password()">Ok</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="fp_close()">Cancel</span> </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- signup prompt -->
<div id="signup-container">
  <div class="signup-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeSignup()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Join UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:25px; height:395px;">
        <div style="height:55px;width:100%;">
          <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; float:left; outline:none" placeholder="First Name" class="txtFirstName" />
          <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; margin-left:20px; float:left; outline:none" placeholder="Last Name" class="txtLastName" />
        </div>
        <input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="username" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Password (Atleast 4 characters)" class="password" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Confirm Password" class="confirm_password" />
        <div style="margin-bottom:10px; width:370px; text-align:left"> <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15"/><span style="margin-top:-3px;">I agree to Ufundoo's term of use and privacy policy.</span> </div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUp()" />
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
        <div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Already member ?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;">
          <input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Sign In"  onclick="openLogin()"/>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- login prompt -->
<div id="login-container">
  <div class="login-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeLogin()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Login UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:15px; height:405px;">
        <input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmail" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpassword" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:25px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:8px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signIn()"/>
        <div style="margin-bottom:10px; width:370px; height:15px;"> <span style="">OR</span> </div>
        <a href="googleLogin1.php?status=user" style="text-decoration:none;color:white">
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" />
        </a> <span onclick="FBLogin('user');">
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" />
        </span> </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
        <div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Not a member yet?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;">
          <input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join Now"  onclick="openSignup()" />
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- signup prompt -->
<div id="promoter-container">
  <div class="promoter-container-wrapper">
    <div class="admin-delete-class-promoter"><img src="assets/img/btn_close.png" id="logo-close" onclick="closePromoter()" height="20" width="20" /> 
      
      <!-- login -->
      <div style="float:left; width:50%; height:520px; border-right:2px solid #ccc; margin-top:25px; text-align:center;">
        <div style="text-align:center;font-size:35px; font-weight:bold">Already a Promoter?</div>
        <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Login</div>
        <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmailPost" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpasswordPost" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:75px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signInPost()"/>
        <!--<div style="margin-bottom:10px;height:15px;">
               <span style="">OR</span>
            </div>
            <a href="googleLogin1.php?status=promoter" style="text-decoration:none;color:white"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
            <span onclick="FBLogin('promoter');"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>--> 
      </div>
      <!-- end here --> 
      
      <!-- sign up -->
      <div style="float:left; width:50%; height:520px; margin-top:25px; text-align:center">
        <div style="text-align:center;font-size:35px; font-weight:bold">Become a Promoter?</div>
        <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Sign Up</div>
        <div>
          <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Firstname" class="firstname" />
          <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none; margin-left:4%;" placeholder="Lastname" class="lastname" />
        </div>
        <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Email" class="username_post" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Password (Atleast 4 characters)" class="password_post" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:15px; outline:none" placeholder="Confirm Password" class="confirm_password_post" />
        <div style="margin-bottom:10px; width:75%; margin-left:auto; margin-right:auto"> <img src="assets/img/unchecked.png" name="fainMail" onclick="check_single(this)" status="0" class="fainMail_post" style="cursor: pointer; float: left; margin-top:3px;" height="15" id="fainMail"/>Send me FainMail on everything events and entertainment. </div>
        <div style="margin-bottom:10px; width:70%; margin-left:64px; text-align:left"> <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree_post" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15" id="agree"/>I agree to Ufundoo's term of use and privacy policy. </div>
        <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUpPost()" />
      </div>
      <!-- end here --> 
      
    </div>
  </div>
</div>
<!-- end here --> 

<!-- choose account -->
<div id="acct-user-popup">
  <div class="admin-popup-delete-container-acct">
    <div class="admin-delete-class-acct"> <img src="assets/img/btn_close.png" id="logo-close-acct" onclick="acct_user_close()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-family: lator; font-size:20px; color:#000">Please choose account for checkout</div>
      <div style="text-align:center; margin-top:15px; font-family: lator; font-size:17px; width:400px; color:#000;">
        <input type="radio" name="acct" id="ufundoo_acct" value="1" checked="checked" style="margin-right:10px;" />
        I have ufundoo account </div>
      <div style="text-align:center; margin-top:10px; font-family: lator; font-size:17px; width:400px; color:#000;">
        <input type="radio" name="acct" id="guest_acct" value="0" style="margin-right:10px;" />
        I prefer guest checkout </div>
      <div style="margin-top:20px; width:200px; margin-left:auto; margin-right:auto;">
        <div class="admin-btn-acct" style="width:70px; background-color:#ed258f;text-align: center; margin-left: auto;margin-right: auto; border-radius:15px;"><span onclick="proceed_acct_user()">OK</span></div>
      </div>
    </div>
  </div>
</div>
<!--  end here --> 

<!-- guest user checkout -->
<div id="guest-user-popup">
  <div class="admin-popup-delete-container-guest">
    <div class="admin-delete-class-guest"> <img src="assets/img/btn_close.png" id="logo-close-guest" onclick="guest_user_close()" height="20" width="20" />
      <div style="text-align:center; margin-top:25px; font-family: lator; font-size:15px;">Please provide us your email address</div>
      <div style="text-align:center; margin-top:15px; font-family: lator; font-size:15px;">
        <input type="text" placeholder="Email" class="txtbox" id="guest_user_email" style="height:40px; width:290px; margin-bottom:12px; padding:10px;border: 1px solid #9a9a9a;border-radius:25px; outline:none;" />
      </div>
      <div style="margin-top:0px; width:200px; margin-left:auto; margin-right:auto;">
        <?php if($_REQUEST['ticketTypeId']=="2"){ ?>
        <div id="proceed-guest-user" class="admin-btn-guest" style="width:70px; background-color:#ed2590;text-align: center; margin-left: auto;margin-right: auto; border-radius:15px;"><span onclick="proceed_guest_user('free')">OK</span></div>
        <?php } else { ?>
        <div id="proceed-guest-user" class="admin-btn-guest" style="width:70px; background-color:#ed2590;text-align: center; margin-left: auto;margin-right: auto; border-radius:15px;"><span onclick="proceed_guest_user('paid')">OK</span></div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- signin checkout -->
<div id="signin-popup-checkout">
  <div id="signin_page_checkout" style="height:340px; width:520px;background-color:#fff;border-radius:35px;border:0;z-index:1000;position: absolute;"> <img src="assets/img/btn_close.png" id="logo-close-guest" onclick="signin_checkout_close()" height="20" width="20" style="right:15px; top:15px;" />
    <div style="clear:both;"></div>
    <div style="width: 500px; margin-top: 25px;margin-left: auto; margin-right: auto;text-align: center;">
      <p style="font-size:24px;width:100%; text-align:center;margin:5px 0 15px 0;color:#494949">Login UFUNDOO</p>
      <input type="text" placeholder="Email" class="txtbox txtLoginmailCheckout" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" />
      <input type="password" placeholder="Password (At least 4 characters)" class="txtbox txtLoginpasswordCheckout"  style="margin-bottom:5px;width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none"/>
      <div style="height:20px; color:#494949; font-size:12px; text-align:right; width:435px; margin-bottom:5px;"><span style="cursor:pointer" onclick="forgot_password('user')">Forgot Password?</span></div>
      <div style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; border:1px solid #ed2590; outline:none; margin-left:50px; cursor:pointer" onclick="signInCheckout()">Login</div>
    </div>
    <div class="errorLogin" style="text-align:center"></div>
  </div>
</div>
<!-- end here-->

<div class="wrapper">
  <?php 
			include('include/header.php'); 
		?>
  <div id="container_area" style="overflow:auto;">
    <div class="content" style="height: auto; width: 950px;margin-left: auto; margin-right: auto; min-height: 100px;margin-top: 30px;"> 
      <!-- main content -->
      <div id="buy_ticket1">
        <div class="buy_ticket_container1">
          <div class="admin-delete-class1">
            <div style="height:60px; width:950px; border-bottom:1px solid #000;  margin-top:25px;font-family:lator;color:#000;"> 
              <!-- event name -->
              <div style="font-size:20px; text-transform:uppercase; line-height:20px; margin-bottom:10px; font-weight:600;" id="bt_event_name"><?php echo $_REQUEST['eventName']; ?></div>
              <!-- event date, time, location -->
              <div style="font-size:16px;"> <span id="bt_event_date"><?php echo $_REQUEST['eventDate']; ?></span><span style="margin-left:25px;" id="bt_event_time"><?php echo $_REQUEST['eventTime']; ?></span><span style="margin-left:20px; margin-right:20px;">|</span><span id="bt_event_location" style="text-transform:capitalize"><?php echo $_REQUEST['locationName']; ?></span> </div>
            </div>
            <div style="height:60px; width:950px; display:table;font-family:lator;color:#000;">
              <div style="margin-top:5%; margin-bottom:5%; width:950px;height:40px;">
                <input type="hidden" id="eventId" value="<?php echo $_REQUEST['eventId']?>" />
                <!-- ticket type -->
                <?php if($_REQUEST['ticketTypeId']=="2"){ ?>
                <input type="hidden" id="status" value="free" />
                <input type="hidden" id="ticketId" value="<?php echo $result1['ticketId']; ?>" />
                <input type="hidden" id="ticketName" value="<?php echo $result1['ticketName']; ?>" />
                <div style="width:650px; height:auto; float:left">
                  <div style="width:650px; height:auto;">
                    <div style="font-size:35px; line-height:35px; display:inline-block; float:left">1.</div>
                    <div style="font-size:15px;display:inline-block; float:left; margin-left:10px; min-height:34px; margin-top:8px; width:150px;"> <?php echo $result1['ticketName']; ?> </div>
                    <div style="font-size:15px;display:inline-block; float:left">
                      <input type="number" onkeyup="isNumberKey(event)" style="width:70px; border-radius:4px; border:1px solid #000; height:32px;margin-left:10px; outline:none; padding-left:10px; font-size:15px;" placeholder="Qty" min="0" id="qty_free" value="0" maxRange="<?php echo $result1['availableQty']; ?>" onChange="changeTotalFree()" onKeyUp="changeTotalFree()" max="<?php echo $result1['availableQty']; ?>"/>
                    </div>
                  </div>
                </div>
                <?php } else { ?>
                <input type="hidden" id="status" value="paid" />
                <div style="width:450px; height:auto; float:left">
                  <?php if(mysqli_num_rows($sql_ticket)>0){
										$i=1;
                                        while($result=mysqli_fetch_assoc($sql_ticket)){
								?>
                  <div style="width:450px; height:auto; min-height:45px;">
                    <?php if($i==1){?>
                    <div style="font-size:35px; line-height:35px; display:inline-block; float:left; width:35px; height:35px;"><?php echo $i; ?>.</div>
                    <?php } else { ?>
                    <div style="font-size:35px; line-height:35px; display:inline-block; float:left; width:35px; height:35px;"></div>
                    <?php } ?>
                    <div style="font-size:15px;display:inline-block; float:left; margin-left:10px; min-height:34px; width:150px; margin-top:8px;" maxRange="<?php echo $result['availableQty']; ?>" ticketId="<?php echo $result['ticketId']; ?>">
                      <input type="hidden" id="ticketId<?php echo $i; ?>" value="<?php echo $result['ticketId']; ?>" />
                      <input type="hidden" id="ticketName<?php echo $i; ?>" value="<?php echo $result['ticketName']; ?>" />
                      <?php echo $result['ticketName']; ?> </div>
                    <div style="font-size:15px;display:inline-block; float:left">
                      <input type="number" class="paidQuant" onkeyup="isNumberKey(event)" style="width:70px; border-radius:4px; border:1px solid #000; height:32px;margin-left:10px; outline:none; padding-left:10px; font-size:15px; margin-top:6px;" placeholder="Qty"  min="0" max="<?php echo $result["availableQty"]; ?>" id="qty<?php echo $i; ?>" value="0" onChange="changeTotal(this,<?php echo $i; ?>)" onKeyUp="changeTotal(this,<?php echo $i; ?>)" />
                    </div>
                    <?php
									$priceNew=explode(' ',$result['price']);
									$pricePer=$priceNew[1];
									?>
                    <input type="hidden" id="price_per_unit<?php echo $i; ?>" value="<?php echo $pricePer; ?>" />
                    <input type="hidden" id="service_per_unit<?php echo $i; ?>" value="<?php echo $service['serviceCharge']; ?>" />
                    <div style="font-size:15px;display:inline-block; float:left; margin-top:10px; margin-left:10px;">x $ <span id="price_per<?php echo $i; ?>"><?php echo $pricePer; ?></span></div>
                    <div style="font-size:15px;display:inline-block; float:left; margin-top:10px; margin-left:10px;">=</div>
                    <div style="font-size:15px;display:inline-block; float:left; margin-top:10px; margin-left:10px;">$ <span id="total_amt<?php echo $i; ?>" class="check_rows">00.00</span></div>
                  </div>
                  <?php $i++; }} ?>
                </div>
                <div style="width:200px; height:auto; float:left; min-height:45px;">
                  <div style="font-size:35px; line-height:35px; display:inline-block; float:left">2.</div>
                  <div style="display:inline-block;float:left;margin-left: 10px;">
                  <div style="padding: 5px 0;">Ticket Total : $ <span id="grand_amt">00.00</span></div>
                  <div style="padding: 5px 0;">Service Charges : $ <span id="service_amt">00.00</span></div>
                  <div style="padding: 5px 0;">Total : $ <span id="tkt_Totalamt">00.00</span></div>
                  </div>
                </div>
                <?php } ?>
                <!-- payment button -->
                <?php if($_REQUEST['ticketTypeId']=="2"){ ?>
                <div style="width:300px;height:40px; float:right" align="right"> 
                  <!-- payment button disable -->
                  <div id="proceedTicket_disable" class="proceed_to_payment_disable">CONFIRM TICKETS</div>
                  <!-- user/promoter login or not -->
                  <?php if((isset($_SESSION["userEmail"])) || (isset($_SESSION["promoterEmail"]))){ ?>
                  <div id="proceedTicket_enable" class="proceed_to_payment_enable" onClick="direct_checkout('free')">CONFIRM TICKETS</div>
                  <?php } else { ?>
                  <div id="proceedTicket_enable" class="proceed_to_payment_enable" onClick="choose_acct('<?php echo $_REQUEST['eventId']; ?>','free')">CONFIRM TICKETS</div>
                  <?php } ?>
                </div>
                <?php } else { ?>
                <div style="width:300px;height:40px; float:right" align="right"> 
                  <!-- payment button disable -->
                  <div id="proceedTicket_disable" class="proceed_to_payment_disable">CONFIRM TICKETS</div>
                  <!-- user login or not -->
                  <?php if((isset($_SESSION["userEmail"])) || (isset($_SESSION["promoterEmail"]))){ ?>
                  <div id="proceedTicket_enable" class="proceed_to_payment_enable" onClick="direct_checkout('paid')">CONFIRM TICKETS</div>
                  <?php } else { ?>
                  <div id="proceedTicket_enable" class="proceed_to_payment_enable" onClick="choose_acct('<?php echo $_REQUEST['eventId']; ?>','paid')">CONFIRM TICKETS</div>
                  <?php } ?>
                </div>
                <?php } ?>
              </div>
            </div>
            <!-- seat chart --> 
            <!-- <div style="height:auto; min-height:400px; width:950px; border-top:1px solid #000;text-align:center" id="bt_image_wrapper">
                    	<?php if($_REQUEST['seatChart']==''){ ?>
                        	<img src="assets/img/no_seat_chart.png" style="margin-top:10px; max-width:950px; max-height:380px; "/>
                        <?php } else { ?>
                        	<img src="<?php echo $_REQUEST['seatChart']; ?>" style="margin-top:10px; max-width:950px; max-height:380px; " id="bt_event_seat_chart" onerror="this.src = 'assets/img/no_seat_chart.png';"/>
                        <?php } ?>
                    </div>--> 
          </div>
        </div>
      </div>
      <!-- end here --> 
    </div>
    <!-- end here --> 
    
    <!-- footer -->
    <div class="footer">
      <?php include('include/footer.php'); ?>
    </div>
    <!-- end here -->
    <input type="hidden" id="address" value="<?php echo $resultEvent['address'];?>" />
  </div>
</div>
<script src="assets/js/jquery-1.9.1.min.js"></script> 
<script src="assets/js/jquery-ui.js"></script> 
<script src="assets/js/bootstrap.js"></script> 
<script src="assets/js/ufundoo.js"></script> 
<script src="assets/js/bootstrap-datepicker.js"></script> 
<script src="assets/js/jquery.nicescroll.js"></script> 
<script>
var windowHeight=$(window).height();
$(document).ready(function(e) {
	var listEvents=windowHeight-80;
    $('#container_area').css('height',listEvents+'px');
	$("#container_area").niceScroll({cursorcolor: "rgba(237, 37, 143, 0.5)",cursorwidth:"6px",cursorborderradius:"10px" ,cursorborder: "0px", railpadding: {right: 2}, zindex: 0});
	
});

/* facebook */
window.fbAsyncInit = function() {
		FB.init({
		appId      : '705873652868650', // replace your app id here
		channelUrl : '', 
		status     : true, 
		cookie     : true, 
		xfbml      : true  
		});
	};
	(function(d){
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	}(document));
	
	function FBLogin(type){
		FB.login(function(response){
			if(response.authResponse){
				window.location.href = "actions.php?status="+type;
			}
		}, {scope: 'email,user_likes'});
	}
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	if($("#signup-container").css('display')=="block")
	{
		signUp();
	}
	if($("#login-container").css('display')=="block")
	{
		signIn();
	}
}});
</script>
</body>
</html>