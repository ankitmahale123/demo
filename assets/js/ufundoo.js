var fpType;
/* search data */
function searchData(searchBtn,eventName)
{
	if(searchBtn=='searchBtn')
	{
		if($('.search-location').val()=='' && $('.search-date').val()=='')
		{
			alert('Please enter criteria on which you want to search');
			return false;
		}
	}
	window.open('eventList.php?search_location='+$('.search-location').val()+'&search_date='+$('.search-date').val()+'&event_name='+eventName+'&type='+searchBtn,'_self');
}
/* end here */

/* sign up open prompt */
function openSignup()
{
	$('.txtFirstName').val('');
	$('.txtLastName').val('');
	$('.username').val('');
	$('.password').val('');
	$('.confirm_password').val('');
	$('.agree').attr('status',0);
	$('.agree').attr('src','assets/img/unchecked.png');
	$('#login-container').fadeOut();
	$('#signup-container').fadeIn();
}
/* end here */

/* sign up close prompt */
function closeSignup()
{
	$('#signup-container').fadeOut();
}
/* end here */

/* login open prompt */
function openLogin()
{
	fpType="user";
	$('.txtLoginmail').val('');
	$('.txtLoginpassword').val('');
	$('#signup-container').fadeOut();
	$('#login-container').fadeIn();
}
/* end here */

/* login close prompt */
function closeLogin()
{
	fpType="";
	$('#login-container').fadeOut();
}
/* end here */

/* promoter open prompt */
function openPromoter()
{
	fpType="promoter";
	$('#promoter-container').fadeIn();
}
/* end here */

/* promoter close prompt */
function closePromoter()
{
	fpType="";
	$('#promoter-container').fadeOut();
}
/* end here */

/* checkbox value in login */
  function check_single(img)
	{
		if($(img).attr('status')==0)
		{
			$(img).attr('src','assets/img/checked.png');
			$(img).attr('status',1);
		}
		else
		{
			$(img).attr('src','assets/img/unchecked.png');
			$(img).attr('status',0);
		}
	}
/* end here */

/* login */
function signIn()
{
	var username=$('.txtLoginmail').val();
	var password=$('.txtLoginpassword').val();
		if(username=='')
		{
			alert('Please Enter Email Address'); // empty username
			$('.txtLoginmail').focus();
			return false;
		}
		if(password=='')
		{
			alert('Please Enter Password'); // empty password
			$('.txtLoginpassword').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('.txtLoginmail').val())) 
		{
			alert('Invalid Email Address');
			return false;
		}
		else
		{
		$(".loading").fadeIn();
		$.ajax({
						url: "Ajax/login.php",
						type:"POST",
						data:"username="+encodeURIComponent($(".txtLoginmail").val())
						+"&password="+encodeURIComponent($(".txtLoginpassword").val())
						+"&role=customer",
						dataType : "json",
						success: function(json) {
							console.log(json);
							$(".loading").fadeOut();
							if(json.success==1)
							{
								//alert("You Have Successfully Logged In");
								$('#login-container').fadeOut();
								window.open('index.php','_self');
							}
							else
							{
								alert(json.msg).fadeIn();
							}
						}
			});
		}
	}
	
function signInPost()
{
	var username=$('.txtLoginmailPost').val();
	var password=$('.txtLoginpasswordPost').val();
		if(username=='')
		{
			alert('Please Enter Email Address'); // empty username
			$('.txtLoginmailPost').focus();
			return false;
		}
		if(password=='')
		{
			alert('Please Enter Password'); // empty password
			$('.txtLoginpasswordPost').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('.txtLoginmailPost').val())) 
		{
			alert('Invalid Email Address');
			return false;
		}
		else
		{
			$(".loading").fadeIn();
			$.ajax({
						url: "Ajax/login.php",
						type:"POST",
						data:"username="+encodeURIComponent($(".txtLoginmailPost").val())
						+"&password="+encodeURIComponent($(".txtLoginpasswordPost").val())
						+"&role=promoter",
						dataType : "json",
						success: function(json) {
							console.log(json);
							$(".loading").fadeOut();
							if(json.success==1)
							{
								$('#promoter-container').fadeOut();
								window.open('https://ufundoo.com/Promoter/promoterDashboard.php','_self');
								//window.open('http://192.168.2.3/developer/ufundoo/Promoter/promoterDashboard.php','_self');
							}
							else
							{
								alert(json.msg);
							}
						}
			});
		}
	}
/* end here */

/* sign up */
function signUp()
{
	var firstname=$('.txtFirstName').val();
	var lastname=$('.txtLastName').val();
	var username=$('.username').val();
	var password=$('.password').val();
	var confirm_password=$('.confirm_password').val();
	var terms_cond=$('.agree').attr('status');
		if(firstname=='')
		{
			alert('Please Enter First Name');
			$('.txtFirstName').focus();
			return false;
		}
		if(lastname=='')
		{
			alert('Please Enter Last Name');
			$('.txtLastName').focus();
			return false;
		}
		if(username=='')
		{
			alert('Please Enter Email Address');
			$('.username').focus();
			return false;
		}
		if(password=='')
		{
			alert('Please Enter Password');
			$('.password').focus();
			return false;
		}
		if(confirm_password=='')
		{
			alert('Please Enter Confirm Password');
			$('.confirm_password').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('.username').val())) 
		{
			alert('Invalid Email Address');
			return false;
		}
		if(password!=confirm_password)
		{
			alert('Confirm Password And Password Must Be Same');//dose not match password with confirm password
			return false;
		}
		if (/\d/.test(firstname)== true)
		{
			alert('First Name must not contain numbers');
			return false;
		}
		if (/\d/.test(lastname)== true)
		{
			alert('Last Name must not contain numbers');
			return false;
		}
		if(terms_cond==0)
		{
			alert('You Must Have to Accept Terms And Condition To Join Us');//dose not match password with confirm password
			return false;
		}
		else
		{
			$(".loading").fadeIn();
			$.ajax({
						url: "Ajax/signup.php",
						type:"POST",
						data:"firstname="+encodeURIComponent($(".txtFirstName").val())
						+"&lastname="+encodeURIComponent($(".txtLastName").val())
						+"&username="+encodeURIComponent($(".username").val())
						+"&password="+encodeURIComponent($(".password").val())
						+"&role=customer",
						dataType : "json",
						success: function(json) {
							console.log(json);
							$(".loading").fadeOut();
							if(json.success==1)
							{
								//alert(json.msg);
								$('#signup-container').fadeOut();
								//openLogin();
							}
							else
							{
								alert(json.msg);
							}
						}
			});
		}
}

function signUpPost()
{
	var firstname=$('.firstname').val();
	var lastname=$('.lastname').val();
	var username=$('.username_post').val();
	var password=$('.password_post').val();
	var confirm_password=$('.confirm_password_post').val();
	var terms_cond=$('.agree_post').attr('status');
	var fainmail=$(".fainMail_post").attr('status');
		if(firstname=='')
		{
			alert('Please Enter Firstname'); // empty username
			$('.firstname').focus();
			return false;
		}
		if(lastname=='')
		{
			alert('Please Enter Lastname'); // empty username
			$('.lastname').focus();
			return false;
		}
		if(username=='')
		{
			alert('Please Enter Email Address'); // empty username
			$('.username_post').focus();
			return false;
		}
		if(password=='')
		{
			alert('Please Enter Password'); // empty password
			$('.password_post').focus();
			return false;
		}
		if(confirm_password=='')
		{
			alert('Please Enter Confirm Password'); // empty confirm password
			$('.confirm_password_post').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('.username_post').val())) 
		{
			alert('Invalid Email Address');
			return false;
		}
		if(password!=confirm_password)
		{
			alert('Confirm Password And Password Must Be Same');//dose not match password with confirm password
			return false;
		}
		if (/\d/.test(firstname)== true)
		{
			alert('Firstname must not contain numbers');
			return false;
		}
		if (/\d/.test(lastname)== true)
		{
			alert('Lastname must not contain numbers');
			return false;
		}
		if(terms_cond==0)
		{
			alert('You Must Have to Accept Terms And Condition To Join Us');//dose not match password with confirm password
			return false;
		}
		else
		{
			$(".loading").fadeIn();
		$.ajax({
						url: "Ajax/signup.php",
						type:"POST",
						data:"username="+encodeURIComponent($(".username_post").val())
						+"&password="+encodeURIComponent($(".password_post").val())
						+"&firstname="+encodeURIComponent($(".firstname").val())
						+"&lastname="+encodeURIComponent($(".lastname").val())
						+"&email_notification="+fainmail
						+"&role=promoter",
						dataType : "json",
						success: function(json) {
								console.log(json);
								$(".loading").fadeOut();
								if(json.success==1)
								{
										alert(json.msg);
										$(".username_post").val('');
										$(".password_post").val('');
										$(".firstname").val('');
										$(".lastname").val('');
										$(".confirm_password_post").val('');
										$('.agree_post').attr('status','0');
										$('#agree').attr('src','assets/img/unchecked.png');
										$(".fainMail_post").attr('status','0');
										$('#fainMai').attr('src','assets/img/unchecked.png');
										$('#promoter-container').fadeOut();
								}
								else
								{
									alert(json.msg);
								}
							}
			});
		}
	}
/* end here */

/* forgot password */
function forgot_password()
{
			var username=$('.fp_text').val();
			if(username=='')
			{
				alert('Please Enter Email Address'); // empty username
				$('.fp_text').focus();
				return false;
			}
			var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
			
			if( !userEmail.test( $('.fp_text').val())) 
			{
				alert('Invalid Email Address');
				return false;
			}
			$(".loading").fadeIn();
			$.ajax({
						url: "Ajax/forgotPassword.php",
						type:"POST",
						data:"username="+encodeURIComponent(username)+"&role="+fpType,
						dataType : "json",
						success: function(json) {
							if(json.success==1)
							{
								$(".loading").fadeOut();
								$('.fp_text').val('');
								alert(json.msg);
								fp_close();
								if(fpType=="user")
								{
									closeLogin();
								}
								else
								{
									closePromoter();
								}
							}
							else
							{
								alert(json.msg);
								location.reload();
							}
						}
			});
}
/* end here */

/* reset password */
function reset_password_link(id,role)
{
	var password=document.getElementById('txtcpassword').value;
	var passwordConfirm=document.getElementById('txtcpasswordconfirm').value;
		if(password=='')
		{
			alert('Please Enter Password'); // empty username
			$('#txtcpassword').focus();
			return false;
		}
		if(passwordConfirm=='')
		{
			alert('Please Enter Confirm Password'); // empty username
			$('#txtcpasswordconfirm').focus();
			return false;
		}
		if(password!=passwordConfirm)
		{
			alert('Password Does Not Match With Confirm Password'); // empty username
			$('#txtcpasswordconfirm').focus();
			return false;
		}
		else
		{
			$(".loading").fadeIn();
			$.ajax({
						url: "Ajax/reset_password.php",
						type:"POST",
						data:"password="+encodeURIComponent($("#txtcpassword").val())
						+"&id="+id
						+"&role="+role,
						dataType : "json",
						success: function(json) {
								console.log(json);
								$(".loading").fadeOut();
								if(json.success==1)
								{
									$('.reset_container').fadeOut(800, function(){
										$('.errorSignUp').html("Your Password Has Been Changed Successfully.");
										$('#after_reset').css('display','table');
									});
								}
								else if(json.success==2)
								{
									$("#txtcpasswordconfirm").val('');
									$("#txtcpassword").val('');
									alert(json.msg);
								}
								else
								{
									alert(json.msg);
								}
							}
			});
		}
}
/* end here */

/* search from event list page */
function searchByAll()
{
	var price=0;
	if($("#price").val()=='Paid')
	{
		price=1;
	}
	if($("#price").val()=='Free')
	{
		price=2;
	}
	$(".loading").fadeIn();
        $.ajax({
            url: "Ajax/searchByAll.php",
            type: "POST",
            data: "eventname_location=" + encodeURIComponent($("#eventname_location").val())+"&dp=" + encodeURIComponent($("#dp").val())+"&event_type=" + encodeURIComponent($("#event_type").val())+"&price=" + encodeURIComponent(price),
            dataType: "html",
            success: function (json) {
				$(".loading").fadeOut();
				$("#event_container").html('');
				$("#event_container").html(json);
            },
            error: function (data) {
				$(".loading").fadeOut();
                alert('Server Problem');
            }
        });
}
/* end here */

/* post feedback */
function feedback()
{
	var suggest=$('#suggest_fb').val();
	var email=$('#email_fb').val();
	var box=$('#suggestion_fb').val();
		if(suggest=='')
		{
			$('.errorLogin').html('Please Enter Suggestion').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#suggest_fb').focus();
			return false;
		}
		if(email=='')
		{
			$('.errorLogin').html('Please Enter Email Address').fadeIn().delay( 4000 ).fadeOut(); // empty password
			$('#email_fb').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('#email_fb').val())) 
		{
			$('.errorLogin').html('Invalid Email Address').fadeIn().delay( 4000 ).fadeOut();
			return false;
		}
		if(box=='')
		{
			$('.errorLogin').html('Please Enter Suggestion').fadeIn().delay( 4000 ).fadeOut(); // empty password
			$('#suggestion_fb').focus();
			return false;
		}
		else
		{
			$(".loading").fadeIn();
		$.ajax({
			url: 'Ajax/sendFeedback.php',
			type:"POST",
			data:"suggest="+encodeURIComponent($("#suggest_fb").val())
			+"&email="+encodeURIComponent($("#email_fb").val())
			+"&contact="+encodeURIComponent($("#contact_fb").val())
			+"&suggestion="+encodeURIComponent($("#suggestion_fb").val()),
			dataType : "json",
			success: function(json) {
				$(".loading").fadeOut();
				if(json.success==1)
				{
					$("#suggest_fb").val('');
					$("#email_fb").val('');
					$("#contact_fb").val('');
					$('#suggestion_fb').val('');
					alert("Your query has been submitted. Soon our team will answer your query.");			
				}
				else
				{
					alert("Error");
				}
				
			}
		});
		}
}
/* end here */

/* carrer */
function career()
{
	var areaSelect=$("input:radio[name=area]:checked").val();
	var areaOtherText=$('#other_text_area').val();
	var areaOtherTextVal='';
	if(areaSelect=="other")
	{
		areaOtherTextVal=1;
	}
	else
	{
		areaOtherTextVal=0;
	}
	var fname=$('#cont_fname').val();
	var lname=$('#cont_lname').val();
	var emailCareer=$('#cont_email').val();
	if(fname=='')
	{
		$('.error').html('Please Enter First Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
		$('#cont_fname').focus();
		return false;
	}
	if(lname=='')
	{
		$('.error').html('Please Enter Last Name').fadeIn().delay( 4000 ).fadeOut(); // empty password
		$('#cont_lname').focus();
		return false;
	}
	if(emailCareer=='')
	{
		$('.error').html('Please Enter Email Address').fadeIn().delay( 4000 ).fadeOut(); // empty password
		$('#cont_email').focus();
		return false;
	}
	var emailCareer = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
	if( !emailCareer.test( $('#cont_email').val())) 
	{
		$('.error').html('Invalid Email Address').fadeIn().delay( 4000 ).fadeOut();
		return false;
	}
	else
	{
		$(".loading").fadeIn();
	$.ajax({
		url: 'Ajax/career.php',
		type:"POST",
		data:"fname="+encodeURIComponent($("#cont_fname").val())
		+"&lname="+encodeURIComponent($("#cont_lname").val())
		+"&email="+encodeURIComponent($("#cont_email").val())
		+"&phNumber="+encodeURIComponent($("#cont_ph").val())
		+"&area="+encodeURIComponent(areaSelect)
		+"&reasonVal="+encodeURIComponent(areaOtherTextVal)
		+"&reason="+encodeURIComponent(areaOtherText)
		+"&whyUfundoo="+encodeURIComponent($("#cont_why").val()),
		dataType : "json",
		success: function(json) {
			$(".loading").fadeOut();
			if(json.success==1)
			{
				alert("Thank you for your interest. Our representative will reach you soon.");		
				location.reload();
			}
			else
			{
				alert("Error");
			}
			
		}
	});
	}
}
/* end here */

/* sign in promoter */
function signInPromoter()
{
	var username=$('.txtLoginmail').val();
	var password=$('.txtLoginpassword').val();
		if(username=='')
		{
			$('.errorLogin').html('Please Enter Email Address').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.txtLoginmail').focus();
			return false;
		}
		if(password=='')
		{
			$('.errorLogin').html('Please Enter Password').fadeIn().delay( 4000 ).fadeOut(); // empty password
			$('.txtLoginpassword').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('.txtLoginmail').val())) 
		{
			$('.errorLogin').html('Invalid Email Address').fadeIn().delay( 4000 ).fadeOut();
			return false;
		}
		else
		{
			$(".loading").fadeIn();
			$.ajax({
						url: "../Ajax/promoterLoginCheck.php",
						type:"POST",
						data:"username="+encodeURIComponent($(".txtLoginmail").val())+"&password="+encodeURIComponent($(".txtLoginpassword").val()),
						dataType : "json",
						success: function(json) {
							console.log(json);
							$(".loading").fadeOut();
							if(json.success==1)
							{
								//alert("You Have Successfully Logged In");
								window.open('promoterDashboard.php','_self');
							}
							else
							{
								$(".errorLogin").html(json.msg).fadeIn().delay( 5000 ).fadeOut();
							}
						}
			});
		}
	}
/* end here */

/* fetch menu */
function fetchMenu(type,status)
{
	$('.subMenu').css('color','#727272');
	if(status!="setting")
	{
		$(type).siblings('div').removeClass('menuActive').addClass('menuDefault');
		$(type).removeClass('menuDefault').addClass('menuActive');
	}
	else
	{
		$(".logout").css('display','none');
	}
	$(".loading").fadeIn();
	$.ajax({
		url: 'fetchMenu.php',
		type:"POST",
		data:"status="+status,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
			if(status=='inquiries' || status=='jobs')
			{
				$(".addScroll").niceScroll({cursorcolor: "rgba(237, 37, 143, 0.5)",cursorwidth:"2px",cursorborderradius:"2px" ,cursorborder: "0px", railpadding: {right: 0}, zindex: 0});
			}
		}
	});
}
/* end here */

/* update setting */
function updatePassword(adminEmail)
{
	var oldPass=$('.old_pass').val();
	var newPass=$('.new_pass').val();
	var confirmPass=$('.confirm_pass').val();
		if(oldPass=='')
		{
			$('.error').html('Please Enter Old Password').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.old_pass').focus();
			return false;
		}
		if(newPass=='')
		{
			$('.error').html('Please Enter New Password').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.old_pass').focus();
			return false;
		}
		if(confirmPass=='')
		{
			$('.error').html('Please Enter Confirm Password').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.new_pass').focus();
			return false;
		}
		if(newPass!=confirmPass)
		{
			$('.error').html('New Password And Confirm Password Must be Same').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.confirmPass').focus();
			return false;
		}
		else
		{
			$(".loading").fadeIn();
			$.ajax({
				url: '../Ajax/updatePasswordAdmin.php',
				type:"POST",
				data:"adminEmail="+adminEmail+"&oldPass="+encodeURIComponent(oldPass)+"&newPass="+encodeURIComponent(newPass),
				dataType : "json",
				success: function(json) {
					$(".loading").fadeOut();
							if(json.success==1)
							{
								$(".error").html(json.msg).fadeIn().delay( 2000 ).fadeOut("slow",function(){
									window.open('../logoutAdmin.php','_self');
								});
							}
							else
							{
								$(".error").html(json.msg).fadeIn().delay( 4000 ).fadeOut();
							}
				}
			});
		}
}
/* end here */

/* request of new promoter */
function fetchPromoter(color,type)
{
	$(color).parent('div').siblings('div').children('div').removeClass('activeEvent');
	$(color).addClass('activeEvent');
	$(".loading").fadeIn();
	$.ajax({
		url: 'fetchPromoter.php',
		type:"POST",
		data:"type="+type,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
		}
	});
}
/* end here */

/* approve promoter */
function approvePromoter(approveId)
{
	$(".loading").fadeIn();
		$.ajax({
						url: "../Ajax/promoterAction.php",
						type:"POST",
						data:"actionId="+encodeURIComponent(approveId)+"&action=approve",
						dataType : "json",
						success: function(json) {
							$(".loading").fadeOut();
							console.log(json);
							if(json.success==1)
							{
								//alert("Approved Promoter Successfully");
								location.reload();
							}
							else
							{
								alert("Error");
								$(".errorLogin").html(json.msg).fadeIn().delay( 4000 ).fadeOut();
							}
						}
			});
}
/* end here */

/* disapprove promoter */
function dispprovePromoter(disapproveId)
{
	$(".loading").fadeIn();
		$.ajax({
						url: "../Ajax/promoterAction.php",
						type:"POST",
						data:"actionId="+encodeURIComponent(disapproveId)+"&action=disapprove",
						dataType : "json",
						success: function(json) {
							$(".loading").fadeOut();
							console.log(json);
							if(json.success==1)
							{
								//alert("Disapproved Promoter..!!");
								location.reload();
							}
							else
							{
								alert("Error");
								$(".errorLogin").html(json.msg).fadeIn().delay( 4000 ).fadeOut();
							}
						}
			});
}
/* end here */

/* promoter */

/* upload event */
function submitEvent(userType)
{
	var ticketArray=Array();
	var count=1;
	$('.check_rows').each(function(index, element) {
		if($(this).val()!='')
        {
			if($('#ticket_price_'+count).val()!='')
			{
				ticketArray.push([$(this).val(),$('#qty_ava_'+count).val(),'$ '+$('#ticket_price_'+count).val()]);
			}
			else
			{
				ticketArray.push([$(this).val(),$('#qty_ava_'+count).val(),$('#ticket_price_'+count).val()]);
			}
		}
		count++;
    });
	var ticketDetail=JSON.stringify(ticketArray);
	var ticketType=$("input:radio[name=ticket_type]:checked").val();
	var mapCheck=0;
	if($('#mapCheck').is(':checked'))
	{
		mapCheck=1;
	}
	$(".loading").fadeIn();
	if(userType=="promoter")
	{
		var userTypePath="../Ajax/addEvent.php";
	}
	else
	{
		var userTypePath="Ajax/addEvent.php";
	}
		$.ajax({
						url: userTypePath,
						type:"POST",
						data:"org_name="+encodeURIComponent($("#org_name").val())
						+"&org_id="+encodeURIComponent($("#org_id").val())
						+"&org_desc="+encodeURIComponent($("#org_desc").val())
						+"&org_email="+encodeURIComponent($("#org_email").val())
						+"&org_contact_no="+encodeURIComponent($("#org_contact_no").val())
						+"&org_fax="+encodeURIComponent($("#org_fax").val())
						+"&org_website="+encodeURIComponent($("#org_website").val())
						+"&event_name="+encodeURIComponent($("#event_name").val())
						+"&event_type="+encodeURIComponent($("#event_type").val())
						+"&datepick="+encodeURIComponent($("#datepick").val())
						+"&datepicker_time1="+encodeURIComponent($("#datepick").val()+' '+$("#datepicker_time1").val()+':00')
						+"&datepicker_time2="+encodeURIComponent($("#datepick").val()+' '+$("#datepicker_time2").val()+':00')
						+"&event_location="+encodeURIComponent($("#event_location").val())
						+"&event_address="+encodeURIComponent($("#event_address").val())
						+"&zip_code="+encodeURIComponent($("#zip_code").val())
						+"&mapCheck="+encodeURIComponent(mapCheck)
						+"&event_desc="+encodeURIComponent($("#event_desc").val())
						//+"&event_image="+encodeURIComponent(appendPath)
						+"&event_video="+encodeURIComponent($('#event_video').val())
						//+"&event_seat_chart="+encodeURIComponent($('#event_seat_chart').attr('src'))
						+"&event_image_cover="+encodeURIComponent($('#event_image_cover').attr('src'))
						//+"&event_image_featured="+encodeURIComponent($('#event_image_featured').attr('src'))
						+"&ticket_type="+encodeURIComponent(ticketType)
						+"&ticket_detail="+encodeURIComponent(ticketDetail)
						+"&datepick1="+encodeURIComponent($("#datepick1").val())
						+"&datepick2="+encodeURIComponent($("#datepick2").val())
						+"&datepicker_time3="+encodeURIComponent($("#datepicker_time3").val()+':00')
						+"&datepicker_time4="+encodeURIComponent($("#datepicker_time4").val()+':00'),
						dataType : "json",
						success: function(json) {
								$(".loading").fadeOut();
								console.log(json);
								if(json.success==1)
								{
									alert("Your event has been sent for approval.");
									if(userType=="promoter")
									{
										location.reload();
									}
									else
									{
										window.open('index.php','_self');
									}
								}
								else
								{
									alert("Error");
								}
							}
			});
}
/* end here */

/* search type data filling */
function searchMenu(particular,eventType)
{
	$(particular).siblings('span').removeClass('active');
	$(particular).addClass('active');
	$('.no_data_found').css('display','none');	
	$('.All').hide();
	$('#menu_type').css('border-bottom','none');
	$('#menu_type').html(eventType);
	if($('.'+eventType).length>0)
	{
		$('.'+eventType).fadeIn("slow");
		$('.holder-wrapper').fadeIn();
		pagging();
	}
	else
	{
		$('#menu_type').css('border-bottom','1px solid #e8e8e0');
		$('.no_data_found').css('display','table');	
		$('.holder-wrapper').css('display','none');
	}
}
/* end here */

/* edit promoter */
function editEvent(eventId)
{
	$(".loading").fadeIn();
	$.ajax({
		url: '../include/postEditEventForm.php',
		type:"POST",
		data:"eventid="+eventId,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('.content').html(json);
			$('.wrapper').children('div.footer').css('display','none');
		}
	});
}
function deleteUploadedImage(id,parameter)
{
	$( "#basicModal" ).dialog({
        modal: true,
        title: "Are you sure?",
        buttons: {
            "YES": function() {   
	$.ajax({
		url: '../Ajax/deleteUploadedImage.php',
		type:"POST",
		data:"imgid="+id,
		dataType : "json",
		success: function(json) {
			$( "#basicModal" ).dialog( "close" );
			if(json.success==1)
			{
				$(parameter).parent().css('display','none');
			}
			else
			{					
				console.log(json.success)
			}
			
		}
		});
	 },
            "NO": function() {
                $( this ).dialog( "close" );
            }
        }
    });
}
function submitEditEvent(userType)
{
	var ticketArray=Array();
	var count=1;
	var mapCheck=0;
	var ticketType=$("input:radio[name=ticket_type]:checked").val();
	if($('#mapCheck').is(':checked'))
	{
		mapCheck=1;
	}
	if($('#qty_total_'+count).val()=='')
	{
		$('#qty_total_'+count).val()=0;
	}
	if(ticketType == 1)
	{
	$('.check_rows').each(function(index, element) {
		if($(this).val()!='')
        {
		
			if($('#ticket_price_'+count).val().indexOf('$') === -1 && $('#ticket_price_'+count).val()!='')
			{
				if(ticketType == 2)
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
			 		ticketArray.push([$(this).val(),remaningQty,'']);
				}
				else
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
					ticketArray.push([$(this).val(),remaningQty,'$ '+$('#ticket_price_'+count).val()]);
				}
			}
			else if($('#ticket_price_'+count).val()!='')
			{
				if(ticketType == 2)
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
			 		ticketArray.push([$(this).val(),remaningQty,'']);
				}
				else
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
					ticketArray.push([$(this).val(),remaningQty,$('#ticket_price_'+count).val()]);
				}
			}
			else
			{
				if(ticketType == 2)
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
			 		ticketArray.push([$(this).val(),remaningQty,'']);
				}
				else
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
					ticketArray.push([$(this).val(),remaningQty,$('#ticket_price_'+count).val()]);
				}
			}
		}
		count++;
    });
	}
	else
		{
			if($('#ticket_price_'+count).val().indexOf('$') === -1 && $('#ticket_price_'+count).val()!='')
			{
				if(ticketType == 2)
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
			 		ticketArray.push([$('.check_rows').val(),remaningQty,'']);
				}
				else
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
					ticketArray.push([$('.check_rows').val(),remaningQty,'$ '+$('#ticket_price_'+count).val()]);
				}
				
			}else if($('#ticket_price_'+count).val()!='')
			{
				if(ticketType == 2)
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
			 		ticketArray.push([$('.check_rows').val(),remaningQty,'']);
				}
				else
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
					ticketArray.push([$('.check_rows').val(),remaningQty,$('#ticket_price_'+count).val()]);
				}
			}
			else
			{
				if(ticketType == 2)
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
			 		ticketArray.push([$('.check_rows').val(),remaningQty,'']);
				}
				else
				{
					var remaningQty=parseInt($('#qty_sold_'+count).val())+parseInt($('#qty_total_'+count).val());
					ticketArray.push([$('.check_rows').val(),remaningQty,$('#ticket_price_'+count).val()]);
				}
			}
	}
	var ticketDetail=JSON.stringify(ticketArray);
	if($('#event_seat_chart').attr('src')=="undefined")
	{
		$('#event_seat_chart').attr('src','');
	}
	if($('#event_image_cover').attr('src')=="undefined")
	{
		$('#event_image_cover').attr('src','');
	}
	if($('#event_image_featured').attr('src')=="undefined")
	{
		$('#event_image_featured').attr('src','');
	}
	$(".loading").fadeIn();
		$.ajax({
						url: "../Ajax/editEvent.php",
						type:"POST",
						data:"event_id="+encodeURIComponent($("#event_id").val())
						+"&org_name="+encodeURIComponent($("#org_name").val())
						+"&org_id="+encodeURIComponent($("#org_id").val())
						+"&org_desc="+encodeURIComponent($("#org_desc").val())
						+"&org_email="+encodeURIComponent($("#org_email").val())
						+"&org_contact_no="+encodeURIComponent($("#org_contact_no").val())
						+"&org_fax="+encodeURIComponent($("#org_fax").val())
						+"&org_website="+encodeURIComponent($("#org_website").val())
						+"&event_name="+encodeURIComponent($("#event_name").val())
						+"&event_type="+encodeURIComponent($("#event_type").val())
						+"&datepick="+encodeURIComponent($("#datepick").val())
						+"&datepicker_time1="+encodeURIComponent($("#datepick").val()+' '+$("#datepicker_time1").val()+':00')
						+"&datepicker_time2="+encodeURIComponent($("#datepick").val()+' '+$("#datepicker_time2").val()+':00')
						+"&event_location="+encodeURIComponent($("#event_location").val())
						+"&event_address="+encodeURIComponent($("#event_address").val())
						+"&zip_code="+encodeURIComponent($("#zip_code").val())
						+"&mapCheck="+encodeURIComponent(mapCheck)
						+"&location_id="+encodeURIComponent($("#location_id").val())
						+"&event_desc="+encodeURIComponent($("#event_desc").val())
						//+"&event_image="+encodeURIComponent(appendPath)
						+"&event_video="+encodeURIComponent($('#event_video').val())
						//+"&event_seat_chart="+encodeURIComponent($('#event_seat_chart').attr('src'))
						+"&event_image_cover="+encodeURIComponent($('#event_image_cover').attr('src'))
						//+"&event_image_featured="+encodeURIComponent($('#event_image_featured').attr('src'))
						+"&ticket_type="+encodeURIComponent(ticketType)
						+"&ticket_detail="+encodeURIComponent(ticketDetail)
						+"&datepick1="+encodeURIComponent($("#datepick1").val())
						+"&datepick2="+encodeURIComponent($("#datepick2").val())
						+"&datepicker_time3="+encodeURIComponent($("#datepicker_time3").val()+':00')
						+"&datepicker_time4="+encodeURIComponent($("#datepicker_time4").val()+':00')
						+"&userId="+encodeURIComponent($("#userId").val()),
						dataType : "json",
						success: function(json) {
								$(".loading").fadeOut();
								console.log(json);
								console.log(json.msg);
								if(json.success==1)
								{
									//alert("Successfully Updated Event.");
									location.reload();
								}
								else
								{
									console.log(json.success);
									alert("Error");
								}
							}
			});
}
/* end here */

/* fetch review and comment of event */
function reviewCommentEvent(eventId)
{
	$(".loading").fadeIn();
	$.ajax({
		url: 'fetchReviewCommentEvent.php',
		type:"POST",
		data:"eventId="+eventId,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
		}
	});
}
/* end here */

/* end here */

/* admin */
 
/* admin login */
function signInAdmin()
{
	var username=$('.txtLoginmail').val();
	var password=$('.txtLoginpassword').val();
		if(username=='')
		{
			$('.errorLogin').html('Please Enter Email Address').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.txtLoginmail').focus();
			return false;
		}
		if(password=='')
		{
			$('.errorLogin').html('Please Enter Password').fadeIn().delay( 4000 ).fadeOut(); // empty password
			$('.txtLoginpassword').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('.txtLoginmail').val())) 
		{
			$('.errorLogin').html('Invalid Email Address').fadeIn().delay( 4000 ).fadeOut();
			return false;
		}
		else
		{
		$(".loading").fadeIn();
		$.ajax({
						url: "../Ajax/adminLoginCheck.php",
						type:"POST",
						data:"username="+encodeURIComponent($(".txtLoginmail").val())+"&password="+encodeURIComponent($(".txtLoginpassword").val()),
						dataType : "json",
						success: function(json) {
							$(".loading").fadeOut();
							console.log(json);
							if(json.success==1)
							{
								//alert("You Have Successfully Logged In");
								window.open('adminDashboard.php','_self');
							}
							else
							{
								alert("Error");
								$(".errorLogin").html(json.msg).fadeIn().delay( 4000 ).fadeOut();
							}
						}
			});
		}
	}
/* end here */
function closeAdminAppove()
{
	$('#admin_approve').fadeOut();
	$("#txtServiceCharge").val('');
}
/* approve event */
function approveEvent(approveId)
{
		$('#admin_approve').css({'left':'0','top':'0'});
		$('#admin_approve').fadeIn();
		$('#admin_approve').attr('eventId',approveId);
		//approveEventFinal(approveId,txtServiceCharge);

}
function approveEventFinal()
{
	var approveId=$('#admin_approve').attr('eventId');
	var txtServiceCharge=document.getElementById("txtServiceCharge").value;
		if(txtServiceCharge=='')
		{
			alert('Please Enter Service Charge'); // empty username
			$('#txtServiceCharge').focus();
			return false;
		}
		else
		{
		$(".loading").fadeIn();
		$.ajax({
						url: "../Ajax/eventAction.php",
						type:"POST",
						data:"actionId="+encodeURIComponent(approveId)+"&serviceCharge="+encodeURIComponent(txtServiceCharge)+"&action=approve",
						dataType : "json",
						success: function(json) {
							$(".loading").fadeOut();
							console.log(json);
							if(json.success==1)
							{
								$('#admin_approve').fadeOut();
								location.reload();
							}
							else
							{
								alert("Error");
								$(".errorLogin").html(json.msg).fadeIn().delay( 4000 ).fadeOut();
							}
						}
			});
		}
}
/* end here */

/* fetch event status */
function fetchEvent(type,status)
{
	$('.subMenu').css('color','#727272');
	$(type).parent('div').siblings('div').children('div').removeClass('activeEvent');
	$(type).addClass('activeEvent');
	$('.disapproveMenu').css('color','#727272');
	if($(type).attr('status')=='disapprove')
	{
		$('#submenuFill').css('visibility','hidden');
		$('.disapproveMenu').css('color','#0db3a5');
	}
	$(".loading").fadeIn();
	$.ajax({
		url: 'statusEvent.php',
		type:"POST",
		data:"status="+status,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
			$('.tooltip').tooltipster({
				interactive: true
				});
			pagging();
		}
	});
}
/* end here */

/* post new event button */
function fetchEventForm()
{
	$(".loading").fadeIn();
	$.ajax({
		url: '../include/postNewEventForm.php',
		type:"POST",
		data:"page=promoter",
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('.content').html(json);
			/* append code */
			$(function() {
				$( "#datepick1" ).datepicker({
				  minDate: 0,
				  showOn: "button",
				  buttonImage: "https://ufundoo.com/assets/img/calander.png",
				  buttonImageOnly: true,
				  dateFormat: 'yy-mm-dd',
			});
	  
  });
  $(function() {
				$( "#datepick2" ).datepicker({
				  minDate: $( "#datepick1" ).val(),
				  maxDate: $( "#datepick" ).val(),
				  showOn: "button",
				  buttonImage: "https://ufundoo.com/assets/img/calander.png",
				  buttonImageOnly: true,
				  dateFormat: 'yy-mm-dd',
			});
	  
  });
  $(function() {
				$( "#datepick" ).datepicker({
				  minDate: 0,
				  showOn: "button",
				  buttonImage: "https://ufundoo.com/assets/img/calander.png",
				  buttonImageOnly: true,
				  buttonText: "Date",
				  dateFormat: 'yy-mm-dd',
			});
	  
  }); 
  $(function() {
				$( "#datepicker_time1" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  }); 
  $(function() {
				$( "#datepicker_time2" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  });
  $(function() {
				$( "#datepicker_time3" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  }); 
  $(function() {
				$( "#datepicker_time4" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  });
			$(".next").click(function(){
			
				if($(this).attr('attrNum')=='first')
				{
					if($('#org_name').val()=='')
					{
						$('.error').html('Please Enter Organizaton Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#org_name').focus();
						return false;
					}
					var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
					
					if( !userEmail.test( $('#org_email').val())) 
					{
						$('.error').html('Invalid Email Address').fadeIn().delay( 4000 ).fadeOut();
						$('#org_email').focus();
						return false;
					}
				}
				else if($(this).attr('attrNum')=='second')
				{
					if($('#event_name').val()=='')
					{
						$('.error').html('Please Enter Event Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#event_name').focus();
						return false;
					}
					if($('#event_type').val()=='Select')
					{
						$('.error').html('Please Select Event Type').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#event_type').focus();
						return false;
					}
					if($('#event_location').val()=='')
					{
						$('.error').html('Please Enter Event Location').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#event_location').focus();
						return false;
					}
					if($('#event_address').val()=='')
					{
						$('.error').html('Please Enter Event Address').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#event_address').focus();
						return false;
					}
					if($('#datepick').val()=='')
					{
						$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#datepick').focus();
						return false;
					}
					if($('#datepicker_time1').val()=='')
					{
						$('.error').html('Please Select Start Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#datepicker_time1').focus();
						return false;
					}
					if($('#datepicker_time2').val()=='')
					{
						$('.error').html('Please Select End Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#datepicker_time2').focus();
						return false;
					}
					var checkedValueMap=document.getElementById("mapCheck").checked;
					if(checkedValueMap)
					{
						if($('#address').val()=='')
						{
							$('.error').html('Please Enter Address To Generate Map').fadeIn().delay( 4000 ).fadeOut(); // empty username
							$('#address').focus();
							return false;
						}
					}	
				}
				else
				{
					var flagQty=true;
					$('.qty_ava').each(function(index, element) {
						if($(this).val()=='')
						{
							flagQty=false;
						}
					});
					if(!flagQty)
					{
						$('.error').html('Quantity must be minimum 1').fadeIn().delay( 4000 ).fadeOut(); // empty username
						return false;
					}
					var flagName=true;
					$('.ticket_name').each(function(index, element) {
						if($(this).val()=='')
						{
							flagName=false;
						}
					});
					if(!flagName)
					{
						$('.error').html('Please Enter Ticket Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
						return false;
					}
					if($('#ticket_price_wrapper_single').css('display')!="none")
					{
						var flagPrice=true;
						$('.ticket_price').each(function(index, element) {
							if($(this).val()=='')
							{
								flagPrice=false;
							}
						});
						if(!flagPrice)
						{
							$('.error').html('Please Enter Ticket Price').fadeIn().delay( 4000 ).fadeOut(); // empty username
							return false;
						}
					}
					if($('#datepick1').val()=='')
					{
						$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#datepick1').focus();
						return false;
					}
					if($('#datepick2').val()=='')
					{
						$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#datepick2').focus();
						return false;
					}
					if($('#datepicker_time3').val()=='')
					{
						$('.error').html('Please Select Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#datepicker_time3').focus();
						return false;
					}
					if($('#datepicker_time4').val()=='')
					{
						$('.error').html('Please Select Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
						$('#datepicker_time4').focus();
						return false;
					}
				}
				if(animating) return false;
				animating = true;
				
				current_fs = $(this).parent();
				next_fs = $(this).parent().next();
				//activate next step on progressbar using the index of next_fs
				$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
				
				//show the next fieldset
				next_fs.show(); 
				//hide the current fieldset with style
				current_fs.animate({opacity: 0}, {
					step: function(now, mx) {
						//as the opacity of current_fs reduces to 0 - stored in "now"
						//1. scale current_fs down to 80%
						scale = 1 - (1 - now) * 0.2;
						//2. bring next_fs from the right(50%)
						left = (now * 50)+"%";
						//3. increase opacity of next_fs to 1 as it moves in
						opacity = 1 - now;
						current_fs.css({'transform': 'scale('+scale+')'});
						next_fs.css({'left': left, 'opacity': opacity});
					}, 
					duration: 800, 
					complete: function(){
						current_fs.hide();
						animating = false;
					}, 
					//this comes from the custom easing plugin
					easing: 'easeInOutBack'
				});
			});
			$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
});
$(".custom-select").each(function(){
            $(this).wrap("<span class='select-wrapper' style='width:115px;'></span>");
            $(this).after("<span class='holder'></span>");
        });
        $(".custom-select").change(function(){
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        }).trigger('change');
			/* end */
			$('.wrapper').children('div.footer').css('display','none');
		}
	});
}
/* end here */

/* fetch menu */
function fetchMenuPromoter(type,status,email,eventNameAttendee,eventId)
{
	$('.admin-content').css('color','#727272');
	$('#submenuFill').css('visibility','hidden');
	$('.draftMenu').css('color','#727272');
	$('.pendingMenu').css('color','#727272');
	$('.disapproveMenu').css('color','#727272');
	if(status!="setting")
	{
		$(type).siblings('div').removeClass('menuActive').addClass('menuDefault');
		$(type).removeClass('menuDefault').addClass('menuActive');
	}
	else
	{
		$(".logout").css('display','none');
	}
	if(status=="communication")
	{
		$("#submenuFill").css("visibility","visible");
		$(".events_sub").css("display","none");
		$(".communication_sub").css("display","table-cell");
	}
	else
	{
		$(".communication_sub").css("display","none");
		$(".events_sub").css("display","table-cell");
	}
	if(status=="events")
	{
		$('.dashboard_sub').hide();
		$('.btn-admin').eq(1).addClass('activeEvent');
		$('.events_sub').show();
	}
	if(status=="dashboard")
	{
		$('.events_sub').hide();
		$('.btn-admin').eq(0).css({'background':'#0db3a5','border':'1px solid #0db3a5'});
		$('.dashboard_sub').show();
	}
	
	$(".loading").fadeIn();
	$.ajax({
		url: 'fetchMenu.php',
		type:"POST",
		data:"status="+status,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
			if(email=="email-at")
			{
				$('#eventName').html('<span>of'+' '+eventNameAttendee+'</span>');
				$('#hiddenEventId').attr('attrEventId',eventId);
				$('#attendee_sub').trigger('click');
				$('.left_menu_wrapper').children('div').removeClass('menuActive').addClass('menuDefault');
				$('.left_menu_wrapper div:last-child').removeClass('menuDefault').addClass('menuActive');
			}
		}
	});
}
/* end here */

/* update setting */
function updatePasswordPromoter(promoterEmail)
{
	var oldPass=$('.old_pass').val();
	var newPass=$('.new_pass').val();
	var confirmPass=$('.confirm_pass').val();
		if(oldPass=='')
		{
			$('.error').html('Please Enter Old Password').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.old_pass').focus();
			return false;
		}
		if(newPass=='')
		{
			$('.error').html('Please Enter New Password').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.new_pass').focus();
			return false;
		}
		if(confirmPass=='')
		{
			$('.error').html('Please Enter Confirm Password').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.confirm_pass').focus();
			return false;
		}
		if(newPass!=confirmPass)
		{
			$('.error').html('New Password And Confirm Password Must be Same').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.confirmPass').focus();
			return false;
		}
		else
		{
			$(".loading").fadeIn();
			$.ajax({
				url: '../Ajax/updatePasswordPromoter.php',
				type:"POST",
				data:"promoterEmail="+promoterEmail+"&oldPass="+encodeURIComponent(oldPass)+"&newPass="+encodeURIComponent(newPass),
				dataType : "json",
				success: function(json) {
					$(".loading").fadeOut();
							if(json.success==1)
							{
								$(".error").html(json.msg).fadeIn().delay( 2000 ).fadeOut("slow",function(){
									window.open('../logoutPromoter.php','_self');
								});
							}
							else
							{
								$(".error").html(json.msg).fadeIn().delay( 4000 ).fadeOut();
							}
				}
			});
		}
}
/* end here */

/* fetch event status */
function fetchEventPromoter(type,status,eventType)
{
	$('.activeEvent').removeClass('activeEvent');
	$(type).addClass('activeEvent');
	$('.draftMenu').css('color','#727272');
	$('.pendingMenu').css('color','#727272');
	$('.disapproveMenu').css('color','#727272');
	if(eventType=='draft')
	{
		$('#submenuFill').css('visibility','hidden');
		$('.draftMenu').css('color','#0db3a5');
		$('.pendingMenu').css('color','#727272');
		$('.disapproveMenu').css('color','#727272');
	}
	$(".loading").fadeIn();
	$.ajax({
		url: 'statusEvent.php',
		type:"POST",
		data:"status="+status+"&eventType="+eventType,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
			pagging();
		}
	});
}
/* end here */
function fetchContentMenu(_this,conentType)
{
	//$('.activeEvent').removeClass('activeEvent');
	$('.subMenu').css('color','#727272');
	if(conentType!="setting")
	{
		$(_this).siblings('div').removeClass('menuActive').addClass('menuDefault');
		$(_this).removeClass('menuDefault').addClass('menuActive');
	}
	else
	{
		$(".logout").css('display','none');
	}
	$(".loading").fadeIn();
	$.ajax({
		url: 'fetchContentMenu.php',
		type:"POST",
		data:"conentType="+conentType,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
		}
	});
}
function saveFeaturedImage()
{
	if($('#image-url').val()=="")
	{
		alert('Please upload the image first.');
		return false;
	}
	$(".loading").fadeIn();
	$.ajax({
		url: '../Ajax/saveFeaturedImage.php',
		type:"POST",
		data:"image="+$('#image-url').val(),
		dataType : "json",
		success: function(json) {
			$(".loading").fadeOut();
			if(json.success==1)
			{
				$(".admin-content").trigger('click');
			}
			else
			{
				console.log(json.msg);
				alert('error');
			}
		}
	});
}
function featureEventImage(eventId,_this)
{
	$(".loading").fadeIn();
	$.ajax({
		url: '../Ajax/toggleFeaturedImage.php',
		type:"POST",
		data:"eventId="+eventId,
		dataType : "json",
		success: function(json) {
			$(".loading").fadeOut();
			if(json.success==1)
			{
				$(_this).html(json.msg);
			}
			else
			{
				alert('error');
			}
		}
	});
}
function deleteReviews(id)
{
	$(".loading").fadeIn();
	var eventId=$('#select-event').val();
	$.ajax({
		url: '../Ajax/deleteReviews.php',
		type:"POST",
		data:"reviewId="+id,
		dataType : "json",
		success: function(json) {
			$(".loading").fadeOut();
			if(json.success==1)
			{
				location.reload();
			}else
			{
				alert('error');
			}
		}
	});
}
function deleteNewsUpadtes(id)
{
	$(".loading").fadeIn();
	$.ajax({
		url: '../Ajax/deleteNewsUpadtes.php',
		type:"POST",
		data:"id="+id,
		dataType : "json",
		success: function(json) {
			$(".loading").fadeOut();
			if(json.success==1)
			{
				$(".admin-update").trigger('click');
			}else
			{
				alert('error');
			}
		}
	});
}
function fetchCommunication(_this,comm_divs)
{
	$('.activeEvent').removeClass('activeEvent');
	$(_this).addClass('activeEvent');
	$('.comm-divs').hide();
	$('#'+comm_divs).fadeIn();
}
function viewNews(_this)
{
	$(_this).parent().hide();
	$(_this).parent().siblings('div').show();
}
/* end here */
function editNewsUpadtes(_this,id)
{
	$(".loading").fadeIn();
	$.ajax({
		url: '../Ajax/editNewsUpdates.php',
		type:"POST",
		dataType : "json",
		data:"updateContent="+escape($(_this).siblings('textarea').val())+"&id="+id,
		success: function(json) {
			$(".loading").fadeOut();
			if(json.success==1)
			{
				$(".admin-update").trigger('click');
			}else
			{
				alert("Error");
			}
		}
	});
}
/* add news and updates */
function addNewsUpadtes(accType)
{
	$(".loading").fadeIn();
	$.ajax({
		url: '../Ajax/saveNewsUpdates.php',
		type:"POST",
		dataType : "json",
		data:"updateContent="+escape($("#txt-update").val()),
		success: function(json) {
			$(".loading").fadeOut();
			if(json.success==1)
			{
				//alert("Updates save successfully.");
				$("#txt-update").val('');
				if(accType=='admin')
				{
					$(".admin-update").trigger('click');
				}
			}else
			{
				alert("Error");
			}
		}
	});
}
/* end here */

/* view attendee */
function viewAttendee(attendeeTab)
{
	$('.draftMenu').css('color','#727272');
	$('.pendingMenu').css('color','#727272');
	$('.disapproveMenu').css('color','#727272');
	$('.left_menu_wrapper').children('div').removeClass('menuActive').addClass('menuDefault');
	$( ".left_menu_wrapper" ).children('div').last().addClass( "menuActive" );
	$('.events_sub').css('display','none');
	$('.attendee_sub').css('display','table-cell');
	$(".loading").fadeIn();
	$.ajax({
		url: 'fetchMenu.php',
		type:"POST",
		data:"status="+attendeeTab,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
		}
	});
}
/* end here */

/* view sales on dashboard */
function view_sales(eventId)
{
	$('#submenuFill').css('visibility','hidden');
	$(".loading").fadeIn();
	$.ajax({
		url: 'chart.php',
		type:"POST",
		data:"eventId="+eventId,
		dataType : "json",
		success: function(json) {
			//console.log(JSON.stringify(json));
			$(".loading").fadeOut();
			$('#dataFill').html('<div id="piechart" style="width: 800px; height: 300px;"></div>');
			google.setOnLoadCallback(drawChart);
    		drawChart(json);
		}
	});
}
/* end here */

/* view attendee list to promoter */
function view_attendee(eventId)
{
	$('#submenuFill').css('visibility','hidden');
	$(".loading").fadeIn();
	$.ajax({
		url: 'viewAttendee.php',
		type:"POST",
		data:"eventId="+eventId,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
		}
	});
}
/* end here */

/* view attendee list to promoter */
function open_promote(eventId)
{
	$('#submenuFill').css('visibility','hidden');
	$(".loading").fadeIn();
	$.ajax({
		url: 'fetchMenu.php',
		type:"POST",
		data:"eventId="+eventId+"&status=promotion",
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
		}
	});
}
/* end here */

function open_attendees(eventNameAttendee,eventId,email)
{
		$(".loading").fadeIn();
		$.ajax({
			url: 'fetchMenu.php',
			type:"POST",
			data:"status=communication"+"&eventId="+eventId,
			dataType : "html",
			success: function(json) {
				//console.log(email);
				$(".loading").fadeOut();
				$('#dataFill').html(json);
				if(email=="email-at")
				{
					$('#eventName').html('<span>of'+' '+eventNameAttendee+'</span>');
					$('#hiddenEventId').attr('attrEventId',eventId);
					$('#email-att').css('display','block');
				}
			}
		});
}

function send_all_subscriber(status)
{
		$(".loading").fadeIn();
		$.ajax({
			url: 'fetchMenu.php',
			type:"POST",
			data:"status="+status,
			dataType : "html",
			success: function(json) {
				console.log(status);
				$(".loading").fadeOut();
				$('#dataFill').html(json);
			}
		});
}
/* admin edit event */
function adminEditEvent(eventId)
{
	$(".loading").fadeIn();
	$.ajax({
		url: 'adminPostEditEventForm.php',
		type:"POST",
		data:"eventid="+eventId,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('.content').html(json);
			$('.wrapper').children('div.footer').css('display','none');
		}
	});
}
/* end here */
/* view attendee list to promoter */
function view_attendee_admin(eventId)
{
	$(".loading").fadeIn();
	$.ajax({
		url: 'viewAttendee.php',
		type:"POST",
		data:"eventId="+eventId,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
		}
	});
}
/* end here */

/* view all attendee (admin) */
function send_all_attendee()
{
	$(".loading").fadeIn();
	$.ajax({
		url: 'sendAllAttendee.php',
		type:"POST",
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('#dataFill').html(json);
		}
	});
}
/* end here */

/* send email to all attendee (admin) */
function sendEmailAllAttendee()
{
	$(".loading").fadeIn();
	$.ajax({
		url: '../Ajax/sendEmailAllAttendee.php',
		type:"POST",
		data:"emailContent="+escape($("#editor1").val())+"&subject="+encodeURIComponent($("#email-subject").val()),
		dataType : "json",
		success: function(json) {
			$(".loading").fadeOut();
			if(json.success==1)
			{
				$("#editor1").val('');
				$("#email-subject").val('');
			}
			else
			{
				alert("Error");
			}
		}
	});
}
/* end here */

/* for chart */
function drawChart(jsonData) {
	var data = new google.visualization.DataTable();
						data.addColumn('string', 'Name');
						data.addColumn('number', 'qty');
                $.each(jsonData, function(i,v) {
					 data.addRow([i,parseInt(v)]);
                });
        var options = {
          title: 'Ticket Details', titleTextStyle: { color: '#676767', fontSize: '18', fontWeight:'normal'} 
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
      }
/* end here */

/* end here */

/* pagging */
function pagging()
	{
	$("div.holder-wrapper").jPages({
				containerID  : "view-table",
				perPage      : 5,
				startPage    : 1
			});	
	}
/* end here */

/* send email */
function sendEmailEventAttendee()
{
	var eventId=$('#hiddenEventId').attr('attrEventId');
	$(".loading").fadeIn();
	$.ajax({
		//url: '../Ajax/sendEmailEventAttendeeRsvp.php',
		url: '../Ajax/sendEmailUser.php',
		type:"POST",
		data:"emailContent="+escape($("#editor1").val())+"&subject="+encodeURIComponent($("#email-subject").val())+"&eventId="+eventId,
		dataType : "json",
		success: function(json) {
			$(".loading").fadeOut();
			if(json.success==1)
			{
				alert(json.msg);
				$("#editor1").val('');
				$("#email-subject").val('');
				$("#test-email-id").val('');
			}
			else
			{
				alert(json.msg);
				$("#editor1").val('');
				$("#email-subject").val('');
				$("#test-email-id").val('');
			}
		}
	});
}
function sendEmailEventAttendeeUser()
{
	var eventId=$('#hiddenEventId').attr('attrEventId');
	$(".loading").fadeIn();
	$.ajax({
		url: '../Ajax/sendEmailUser.php',
		type:"POST",
		data:"emailContent="+escape($("#editor1").val())+"&subject="+encodeURIComponent($("#email-subject").val())+"&eventId="+eventId,
		dataType : "json",
		success: function(json) {
			$(".loading").fadeOut();
			if(json.success==1)
			{
				alert(json.msg);
				$("#editor1").val('');
				$("#email-subject").val('');
				$("#test-email-id").val('');
			}
			else
			{
				alert(json.msg);
				$("#editor1").val('');
				$("#email-subject").val('');
				$("#test-email-id").val('');
			}
		}
	});
}
/* end here */

/* send email promtion of particular event (promotion) */
function sendEmailEventPromotion()
{
	$(".loading").fadeIn();
	$.ajax({
		url: '../Ajax/sendEmailEventAttendee.php',
		type:"POST",
		data:"emailContent="+escape($("#editor1").val())+"&subject="+encodeURIComponent($("#email-subject").val())+"&people="+encodeURIComponent($("#csv_hidden").val()),
		dataType : "json",
		success: function(json) {
			$(".loading").fadeOut();
			console.log(json);
			if(json.success==1)
			{
				$("#editor1").val('');
				$("#email-subject").val('');
				$("#csv_hidden").val('');
				$('#csv_done').css('display','none');
				//alert("Email has been sent successfully.");			
			}
			else
			{
				alert("Error");
			}
			
		}
	});
}
/* end here */

/* send email to subscriber (admin) */
function sendEmailSubscriber()
{
	$(".loading").fadeIn();
	$.ajax({
		url: '../Ajax/sendEmailSubscriber.php',
		type:"POST",
		data:"emailContent="+escape($("#editor1").val())+"&subject="+encodeURIComponent($("#email-subject").val())+"&people="+encodeURIComponent($("#csv_hidden").val()),
		dataType : "json",
		success: function(json) {
			$(".loading").fadeOut();
			console.log(json.success);
			if(json.success==1)
			{
				$("#editor1").val('');
				$("#email-subject").val('');
				window.open('adminDashboard.php','_self');		
			}
			else
			{
				alert("Error");
			}
		}
	});
}
/* end here */

/* total views */
function totalViews(eventId)
{
	$.ajax({
					url: "Ajax/totalViews.php",
					type:"POST",
					data:"eventId="+eventId,
					dataType : "json",
					success: function(json) {
						}
					});
}
/* end here */

/* buy ticket popup */
function buy_ticket_close()
{
	$('#buy_ticket').fadeOut();
}
function buy_ticket_btn(eventName,eventDate,eventTime,eventLocation,eventSeatChart,ticketTypeId,eventId)
{
	$(".loading").fadeIn();
	$.ajax({
		url: 'Ajax/buyTicketsPopup.php',
		type:"POST",
		data:"eventName="+encodeURIComponent(eventName)
		+"&eventDate="+encodeURIComponent(eventDate)
		+"&eventTime="+encodeURIComponent(eventTime)
		+"&eventLocation="+encodeURIComponent(eventLocation)
		+"&eventSeatChart="+encodeURIComponent(eventSeatChart)
		+"&ticketTypeId="+encodeURIComponent(ticketTypeId)
		+"&eventId="+encodeURIComponent(eventId),
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
				$('#buy_ticket').fadeIn();
				$('.buy_ticket_container').html(json);
				$('#qty').prop('disabled', true);
				$('#proceedTicket_enable').fadeOut(function(){ $('#proceedTicket_disable').fadeIn();});
		}
	});
}
/* onchange and keyup of quantity value */
function changeTotalFree()
{
   var qty=$("#qty_free").val();
   var maxValue=$("#qty_free").attr('maxRange');//get the quantity value from 
   if(parseInt(qty) > parseInt(maxValue))//enter value > total quantity available
   {
	   alert('Maximum no of tickets available is '+maxValue+'.');//set total quantity available
	   $("#qty_free").val(maxValue);
   }
   if(qty=="0")
   {
	   $('#proceedTicket_enable').fadeOut(function(){ $('#proceedTicket_disable').fadeIn();});
   }
   else
   {
	   $('#proceedTicket_disable').fadeOut(function(){ $('#proceedTicket_enable').fadeIn();});
   }
}
var grandTotal=0;
var countVar=0;
var tot=0;
function changeTotal(_this,countId){
	
   var qty_old=$("#qty"+countId).val();
   var qty_new=parseFloat(qty_old).toFixed(2);//parse float with two decimal places
   var price_per_old=$('#price_per'+countId).html();
   var price_per_new=parseFloat(price_per_old).toFixed(2);//parse float with two decimal places
   var total_old=(qty_new) * (price_per_new);
   var total_new=parseFloat(total_old).toFixed(2);//multiply two digits of having datatype float
   $('#total_amt'+countId).html(total_new);//parse float with two decimal places - set value
   /* dynamic grand total count */
   countVar=0;
   $('.check_rows').each(function(index, element) {
        countVar=parseFloat(countVar)+parseFloat($(this).text());
		if(countVar=="00.00" || countVar=="0.00")
	    {
		   $('#proceedTicket_enable').fadeOut(function(){ $('#proceedTicket_disable').fadeIn();});
	    }
	    else
	    {
		   $('#proceedTicket_disable').fadeOut(function(){ $('#proceedTicket_enable').fadeIn();});
	    }
   });
   
   // service charge
   totalQuantBox=0;
   $('.paidQuant').each(function(index, element) {
	   totalQuantBox=totalQuantBox+parseFloat($(this).val());
	});

	var service_per_unit = $('#service_per_unit'+countId).val();
	var serviceCharge=parseFloat(service_per_unit).toFixed(2);//parse float with two decimal places
	//var totalServiceCharge =  (qty_new) * (serviceCharge);
	var totalServiceCharge =  (totalQuantBox) * (serviceCharge);
	var total_new_ServiceCharge=parseFloat(totalServiceCharge).toFixed(2);//multiply two digits of having datatype float
   
   $('#service_amt').html(total_new_ServiceCharge);
   $('#grand_amt').html(countVar.toFixed(2));
   
   // grand_amt + service_amt
   var totalCharge = totalServiceCharge+countVar;
   var totalChargeNew=parseFloat(totalCharge).toFixed(2);//multiply two digits of having datatype float
   $('#tkt_Totalamt').html(totalChargeNew);
 
   
   /* end */
   var maxValue=$("#qty"+countId).attr('max');//get the quantity value from 
   if(parseInt(qty_old) > parseInt(maxValue))//enter value > total quantity available
   {
	   alert('Maximum no of tickets available is '+maxValue+'.');//set total quantity available
	   $("#qty"+countId).val(maxValue);
   }
   
}
function changePrice(price)
{
	if($(price).val()=="Select")
	{
		alert('Please select category');
		$('#qty').prop('disabled', true);
		$('#price_per').html('00.00');
		$('#qty').attr('max','');
		$('#qty').val('0');
		$('#total_amt').html('00.00');
		$('#proceedTicket_enable').fadeOut(function(){ $('#proceedTicket_disable').fadeIn();});
	}
	else
	{
		var selectedPrice=$('option:selected', price).attr('price');
		var res = selectedPrice.split(" ");
		$('#price_per').html(res[1]);
		var maxRange=$('option:selected', price).attr('maxRange');
		$('#qty').attr('max',maxRange);
		$('#qty').prop('disabled', false);
		changeTotal();
	}
}
function proceedTicket(eventId)
{
	var gusetUserEmail=$('#userTypeEmail').val();
	var manual=$('#userType').val();
	var ticketId=$('#ticketId').val();
	var totalNum=$('#qty_free').val();
	var ticketDetail='';
	$(".loading").fadeIn();
	$.ajax({
		url: 'Ajax/proceedTicket.php',
		type:"POST",
		data:"eventId="+encodeURIComponent(eventId)
		+"&ticketId="+encodeURIComponent(ticketId)
		+"&totalNum="+encodeURIComponent(totalNum)
		+"&ticket_detail="+encodeURIComponent(ticketDetail)
		+"&gusetUserEmail="+encodeURIComponent(gusetUserEmail)
		+"&type="+encodeURIComponent(manual),
		dataType : "json",
		success: function(json) {
			$(".loading").fadeOut();
			console.log(json.msg);
			if(json.success==1)
			{
				window.open('thankyou.php?ticketType=free&eventId='+eventId+'&totalTicket='+totalNum+'&gusetUserEmail='+gusetUserEmail+'&orderNo='+json.orderNo,'_self');
			}
			else
			{
				console.log(json.msg);
				alert("Error");
			}
			
		}
	});
}
/* set global variable for ticket is free or paid when user go via login */
var userTicketType='free';
function choose_acct(eventId,status)
{
	userTicketType=status;
	$('#acct-user-popup').fadeIn();
}
function acct_user_close()
{
	$('#acct-user-popup').fadeOut();
}
function signin_checkout_close()
{
	$('#signin-popup-checkout').fadeOut();
}
function proceed_acct_user()
{
	var acctType=$("input:radio[name=acct]:checked").val();
	if(acctType==1)
	{
		$('#acct-user-popup').fadeOut();
		$('#signin-popup-checkout').fadeIn();
	}
	else
	{
		$('#acct-user-popup').fadeOut();
		$('#guest-user-popup').fadeIn();
	}
}
function signInCheckout()
{
	var eventId=$('#eventId').val();
	if(userTicketType=="free")
	{
		var ticketId=$('#ticketId').val();
		var totalNum=$('#qty_free').val();
		var ticketName=$('#ticketName').val();
		var ticketDetail='';
		var grandAmt='';
	}
	else
	{
		var ticketArray=Array();
		var count=1;
		$('.check_rows').each(function(index, element) {
			ticketArray.push([$('#ticketId'+count).val(),$('#ticketName'+count).val(),$('#qty'+count).val(),$('#total_amt'+count).text(),$('#price_per_unit'+count).val()]);
			count++;
		});
		var ticketDetail=JSON.stringify(ticketArray);
		var grandAmt=$('#grand_amt').text();
	}
	var username=$('.txtLoginmailCheckout').val();
	var password=$('.txtLoginpasswordCheckout').val();
		if(username=='')
		{
			$('.errorLogin').html('Please Enter Email Address').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.txtLoginmailCheckout').focus();
			return false;
		}
		if(password=='')
		{
			$('.errorLogin').html('Please Enter Password').fadeIn().delay( 4000 ).fadeOut(); // empty password
			$('.txtLoginpasswordCheckout').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('.txtLoginmailCheckout').val())) 
		{
			$('.errorLogin').html('Invalid Email Address').fadeIn().delay( 4000 ).fadeOut();
			return false;
		}
		else
		{
			$(".loading").fadeIn();
		$.ajax({
						url: "Ajax/loginTicketCheckout.php",
						type:"POST",
						data:"username="+encodeURIComponent($(".txtLoginmailCheckout").val())
						+"&password="+encodeURIComponent($(".txtLoginpasswordCheckout").val()),
						dataType : "json",
						success: function(json) {
							$(".loading").fadeOut();
							console.log(json.success);
							if(json.success==1)
							{
								$('#signin-popup-checkout').fadeOut();
								$('#signin_page_checkout').fadeOut();
								window.open('checkout.php?eventId='+eventId+'&grandAmt='+grandAmt+'&ticketId='+ticketId+'&ticketName='+ticketName+'&totalNum='+totalNum+'&ticket_detail='+ticketDetail+'&gusetUserEmail=','_self');
							}
							else
							{
								$(".errorLogin").html(json.msg).fadeIn().delay( 9000 ).fadeOut();
							}
						}
			});
		}
}
function direct_checkout(userTicketType)
{
	var eventId=$('#eventId').val();
	if(userTicketType=="free")
	{
		var ticketId=$('#ticketId').val();
		var totalNum=$('#qty_free').val();
		var ticketName=$('#ticketName').val();
		var ticketDetail='';
		var grandAmt='';
	}
	else
	{
		var ticketArray=Array();
		var count=1;
		$('.check_rows').each(function(index, element) {
			ticketArray.push([$('#ticketId'+count).val(),$('#ticketName'+count).val(),$('#qty'+count).val(),$('#total_amt'+count).text(),$('#price_per_unit'+count).val()]);
			count++;
		});
		var ticketDetail=JSON.stringify(ticketArray);
		var subTotal=$('#grand_amt').text();
		var grandAmt=$('#tkt_Totalamt').text();
		var ticketId='';
		var ticketName='';
		var totalNum='';
	}
	window.open('checkout.php?eventId='+eventId+'&subTotal='+subTotal+'&grandAmt='+grandAmt+'&totalServiceCharge='+$('#service_amt').text()+'&ticketId='+ticketId+'&ticketName='+ticketName+'&totalNum='+totalNum+'&ticket_detail='+ticketDetail+'&gusetUserEmail=','_self');
}
/* end here */

/* sign up guset user */
function open_signup_guest()
{
	$('#signin-popup-guest').fadeOut();
	$('#signin_page_guest').fadeOut();
	$(".errorSignUp").html('');
	$('input').val('');
	$(".fainMail").attr('status','0');
	$(".fainMail").attr('src',"images/unchecked.png");
	$(".agree").attr('status','0');
	$(".agree").attr('src',"images/unchecked.png");	
	$('#join-popup').fadeIn();
	$('#join_page').fadeIn();
}

/* guest user prompt */
function guest_user()
{
	$('#signin-popup-guest').fadeOut();
	$('#signin_page_guest').fadeOut();
	$('#guest-user-popup').fadeIn();
}
function guest_user_close()

{
	$('#guest-user-popup').fadeOut();
}
function proceed_guest_user(status)
{
	var eventId=$('#eventId').val();
	if($("#guest_user_email").val()=='')
	{
		alert('Please enter email address.');
	}
	else
	{
		if(status=="free")
		{
			var ticketId=$('#ticketId').val();
			var totalNum=$('#qty_free').val();
			var ticketName=$('#ticketName').val();
			var ticketDetail='';
			var grandAmt='';
		}
		else
		{
			var ticketArray=Array();
			var count=1;
			$('.check_rows').each(function(index, element) {
				ticketArray.push([$('#ticketId'+count).val(),$('#ticketName'+count).val(),$('#qty'+count).val(),$('#total_amt'+count).text(),$('#price_per_unit'+count).val(),$('#service_amt').text()]);
				count++;
			});
			console.log(ticketArray);
			var ticketDetail=JSON.stringify(ticketArray);
			var grandAmt=$('#tkt_Totalamt').text();
		}
		$('#guest-user-popup').fadeOut();
		window.open('checkout.php?eventId='+eventId+'&grandAmt='+grandAmt+'&ticketId='+ticketId+'&ticketName='+ticketName+'&totalNum='+totalNum+'&ticket_detail='+ticketDetail+'&gusetUserEmail='+encodeURIComponent($("#guest_user_email").val()),'_self');
	}
}
/* end here */

/* rsvp */
function rsvp(eventId)
{
	$('#username').val('');
	$('#email').val('');
	$('#contact_no').val('');
	$('.rsvp_step_1').fadeIn();
	$('.rsvp_step_2').css('display','none');
	$(".loading").fadeIn();
	$.ajax({
		url: 'Ajax/proceedRsvp.php',
		type:"POST",
		data:"eventId="+encodeURIComponent(eventId),
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$("#admin-popup-delete").fadeIn();
			$('.admin-popup-delete-container').html(json);
		}
	});
}
function show_delete_close()
{
	$("#admin-popup-delete").fadeOut();
}
function submit_rsvp(global_id,type)
{
	var typeSubmit;
	if(type=="add")
	{
		typeSubmit="add";
	}
	else
	{
		typeSubmit="edit";
	}
	var username=$('#username').val();
	var email=$('#email').val();
		if(username=='')
		{
			$('.errorRsvp').html('Please Enter Your Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#username').focus();
			return false;
		}
		if(email=='')
		{
			$('.errorRsvp').html('Please Enter Your Email').fadeIn().delay( 4000 ).fadeOut(); // empty email
			$('#email').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('#email').val())) 
		{
			$('.errorRsvp').html('Invalid Email Format').fadeIn().delay( 4000 ).fadeOut();
			return false;
		}
		else
		{
		$(".loading").fadeIn();
		$.ajax({
					url: "Ajax/addRsvp.php",
					type:"POST",
					data:"id="+encodeURIComponent(global_id)+"&username="+encodeURIComponent($('#username').val())+"&email="+encodeURIComponent($('#email').val())+"&contact_no="+encodeURIComponent($('#contact_no').val())+"&typeSubmit="+typeSubmit,
					dataType : "json",
					success: function(json) {
						$(".loading").fadeOut();
          				if(json.success==1)
						{
							if(typeSubmit=="add")
							{
								buy_ticket_btn(json.eventName,json.eventDate,json.eventTime,json.eventLocation,json.eventSeatChart,json.ticketTypeId,json.eventId);
							}
							$("#admin-popup-delete").fadeOut();
						}
						else
						{
							$(".errorRsvp").html(json.msg);
							$(".errorRsvp").fadeIn();
						}
						}
					});
		}
}
function edit_rsvp_info(rsvpId,eventId)
{
	$(".loading").fadeIn();
	$.ajax({
		url: 'Ajax/editRsvpInfo.php',
		type:"POST",
		data:"rsvpId="+rsvpId+"&eventId="+eventId,
		dataType : "html",
		success: function(json) {
			$(".loading").fadeOut();
			$('.rsvp_step_1').fadeOut(function(){
				$('.rsvp_step_3').fadeIn();
				$('.rsvp_step_3').html(json);
			});
		}
	});
}
/* end here */

/* sign in with ticket data */
function signInWithData()
{
	var username=$('.txtLoginmail').val();
	var password=$('.txtLoginpassword').val();
		if(username=='')
		{
			$('.errorLogin').html('Please Enter Email Address').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('.txtLoginmail').focus();
			return false;
		}
		if(password=='')
		{
			$('.errorLogin').html('Please Enter Password').fadeIn().delay( 4000 ).fadeOut(); // empty password
			$('.txtLoginpassword').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('.txtLoginmail').val())) 
		{
			$('.errorLogin').html('Invalid Email Address').fadeIn().delay( 4000 ).fadeOut();
			return false;
		}
		else
		{
		$(".loading").fadeIn();
		$.ajax({
						url: "Ajax/login.php",
						type:"POST",
						data:"username="+encodeURIComponent($(".txtLoginmail").val())
						+"&password="+encodeURIComponent($(".txtLoginpassword").val())
						+"&role=customer",
						dataType : "json",
						success: function(json) {
							$(".loading").fadeOut();
							console.log(json);
							if(json.success==1)
							{
								//alert("Ticket Booked");
								$('#signin-popup').fadeOut();
								window.open('index.php','_self');
							}
							else
							{
								$(".errorLogin").html(json.msg).fadeIn().delay( 9000 ).fadeOut();
							}
						}
			});
		}
	}
/* end here */

function changeTotalMax(remainVal)
{
   var qty_old=$(remainVal).val();//get the quantity value 
   var maxValue=$(remainVal).attr('max');//get the quantity value from 
   if(parseInt(qty_old) > parseInt(maxValue))//enter value > total quantity available
   {
	   alert('Maximum no of tickets available must be only '+maxValue+'.');//set total quantity available
	   $(remainVal).val(maxValue);
   }
}

/* post subscribe */
function subscribe()
{
	var email=$('.subscribeInput').val();
		if(email=='')
		{
			alert('Please Enter Email Address'); // empty password
			$('.subscribeInput').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('.subscribeInput').val())) 
		{
			alert('Invalid Email Address');
			return false;
		}
		else
		{
			$(".loading").fadeIn();
			$.ajax({
				url: 'Ajax/subscribe.php',
				type:"POST",
				data:"email="+encodeURIComponent($(".subscribeInput").val()),
				dataType : "json",
				success: function(json) {
					$(".loading").fadeOut();
					console.log(json.msg);
					if(json.success==2)//already
					{
						$('.subscribeInput').val('');
						$('#subscribe_already').fadeIn().delay(3000).fadeOut();
						//alert(json.msg);			
					}
					else if(json.success==1)//new
					{
						$('.subscribeInput').val('');
						$('#subscribe_new').fadeIn().delay(3000).fadeOut();
						//alert(json.msg);			
					}
					else
					{
						alert(json.msg);
					}
				}
			});
		}
}
/* end here */

/* number validation */
function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if ((charCode > 31) && (charCode < 48 || charCode > 57))
		 {
			 //return false;
			 $('#qty_free').val($('#qty_free').val().replace(/[A-Za-z$-]/g, ""));
		 }
		 else
		 {
         	return true;
		 }
      }
function isNumberKeyFloat(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
		// alert(charCode);
         if ((charCode > 31) && (charCode < 44 || charCode == 45 || charCode == 47|| charCode > 57))
		 {
            return false;
		 }
		 else
		 {
         	return true;
		 }
      }
/* end here */

/* forgot password */
function fp_close()
{
	$('#forgot_password').fadeOut();
	if(fpType=="user")
	{
	$('#login-container').fadeIn();
	}
	else
	{
	$('#promoter-container').fadeIn();
	}
}
function fp_open()
{
	if(fpType=="user")
	{
		$('#login-container').fadeOut();
	}
	else
	{
		$('#promoter-container').fadeOut();
	}
	$('#forgot_password').fadeIn();
}

/* video */
function open_video(videoLink)
{
	//alert();
	$('#video_wrapper').attr('src',videoLink);
	$('#video_container').fadeIn();
}
function close_video()
{
	$('#video_wrapper').attr('src','');
	$('#video_container').fadeOut();
}