<?php
session_start(); 
require_once("../config/conn.php");
$date=date('Y-m-d H:i:s');

if($_REQUEST['action']=="approve")
{
		$query_event_update=mysqli_query($mysqli,'update ticket set serviceCharge="'.trim($_REQUEST['serviceCharge']).'" where eventId="'.trim($_REQUEST['actionId']).'"');
			if($query_event_update==1)
			{
	$query=mysqli_query($mysqli,'select event.name as eventName,event.date as date,event.startDateTime as startTime,event.description as description,event.coverImage as coverImage,event.seatChart as seatChart,organization.name as orgName,organization.contactNo as orgContact, location.name as locationName,ticketType.id as ticketTypeId,profile.firstName as firstName,promoter.email as promoterEmail from event inner join promoter on promoter.id=event.promoterId inner join profile on profile.id=promoter.profileId inner join organization on organization.id=event.organizationId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.id="'.trim($_REQUEST['actionId']).'"');
	$result=mysqli_fetch_assoc($query);
	$time_bt = date_create($result['startTime']);	
	$date_bt = date_create($result['date']);
	if(mysqli_num_rows($query)>0)
	{
		$query_update=mysqli_query($mysqli,'update event set lastUpdated="'.$date.'",status="approve" where id="'.trim($_REQUEST['actionId']).'"');
		if($query_update==1)
		{
								include("../config/configEmail.php");
								$to_addresss1=$result['promoterEmail'];
								
								$body1 =  '<div style="font-family: calibri;font-size:15px;">Hi '.trim($result['firstName']).',<br/><br/>
										   Your event is live!<br/><br/>
										   <b>Event details :</b><br/>
										   Name : '.$result['eventName'].'<br/>
										   Date : '.date_format($date_bt, 'M jS, Y').'<br/>
										   Time : '.date_format($time_bt, 'G:i a').'<br/>
										   Venue : '.$result['locationName'].'<br/>
										   Event Organizers : '.$result['orgName'].'<br/>';
										   if($result['orgContact']!='') {
								$body1 .=  'Contact Organizers : '.$result['orgContact'].'<br/>';
										   }
								$body1 .=  '<br/><table border="0" bordercolor="#0db3a5" cellpadding="10" cellspacing="0" width="50%" style="border:2px solid #2e2e2e; font-size:14px;font-family: calibri;">
												<tr style="background:#2e2e2e; color:#fff; height:30px" align="center">
													<td>Ticket Type</td>
													<td>Total No Of Ticket</td>';
													if($result['ticketTypeId']!="2"){
								$body1 .=  '		<td>Price</td>';
													}
								$body1 .=  '	</tr>';
											$queryTicket=mysqli_query($mysqli,"select ticket.name as ticketName,ticket.price as ticketPrice,ticket.totalQty as ticketQty from ticket where ticket.eventId='".$_REQUEST['actionId']."'");
										if(mysqli_num_rows($queryTicket)>0){
											while($resultTicket=mysqli_fetch_assoc($queryTicket)){
								$body1 .=  '	<tr style="height:30px;" align="center">
													<td>'.$resultTicket['ticketName'].'</td>
													<td>'.$resultTicket['ticketQty'].'</td>';
													if($result['ticketTypeId']!="2"){
								$body1 .=  '		<td>'.$resultTicket['ticketPrice'].'</td>';
													}
								$body1 .=  '	</tr>';
												}} else {
								$body1 .=  '	<tr>
													<td colspan="3" align="center">No Data Available</td>
												</tr>';
												} 
								$body1 .=  '	</table><br/><br/>
											Log in <a href="https://ufundoo.com/Promoter">here</a> to track and manage your event.<br/><br/>
										   Please email us at <a href="mailto:admin@ufundoo.com" target="_blank">admin@ufundoo.com</a> with any questions.<br/><br/>Thanks,<br/>Ufundoo Team</div>';
								$subject = "Congrats! Your event is live : (".$result['eventName'].")";
								
								require_once('../phpmailer/class.phpmailer.php');
								$mail = new PHPMailer();
								$mail->IsHTML(true);
								$mail->IsSMTP(); // telling the class to use SMTP
								$mail->Host       = $SmtpServer; // SMTP server
								//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
								$mail->SMTPSecure = "ssl";										   
								$mail->SMTPAuth   = true;                  // enable SMTP authentication
								$mail->Host       = $SmtpServer; // sets the SMTP server
								$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
								$mail->Username   = $SmtpUser; // SMTP account username
								$mail->Password   = $SmtpPass;        // SMTP account password
								
								$mail->From=$SmtpUser;
								$mail->FromName=$SmtpUser;
								
								$mail->Subject    = $subject;
								
								$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
								$mail->MsgHTML($body);
								$mail->Body=$body1;
								
								//smtp_host,smtp_port,username,password,from_address,to_address
								
								$mail->AddAddress($to_addresss1, $to_addresss1);
								
								if(!$mail->Send()) {
								  $arr = array("success"=>0); 
								} else {
								  $arr = array("success"=>1); 
								}
		}
		else
		{
			$arr=array("success"=>0);
		}
	}
	else
	{
		$arr=array("success"=>0);
	}
			}
			else
	{
		$arr=array("success"=>0);
	}
			
}
else
{
	$query=mysqli_query($mysqli,'select event.name as eventName,event.date as date,event.startDateTime as startTime,event.description as description,event.coverImage as coverImage,event.seatChart as seatChart,organization.name as orgName,organization.contactNo as orgContact, location.name as locationName,ticketType.id as ticketTypeId,profile.firstName as firstName,promoter.email as promoterEmail from event inner join promoter on promoter.id=event.promoterId inner join profile on profile.id=promoter.profileId inner join organization on organization.id=event.organizationId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.id="'.trim($_REQUEST['actionId']).'"');
	$result=mysqli_fetch_assoc($query);
	$time_bt = date_create($result['startTime']);	
	$date_bt = date_create($result['date']);
	if(mysqli_num_rows($query)>0)
	{
		$query_update=mysqli_query($mysqli,'update event set lastUpdated="'.$date.'", status="disapprove",reason="'.mysqli_real_escape_string($mysqli,$_REQUEST['reason']).'" where id="'.trim($_REQUEST['actionId']).'"');
		if($query_update==1)
		{
								include("../config/configEmail.php");
								$to_addresss1=$result['promoterEmail'];
								
								$body1 =  '<div style="font-family: calibri;font-size:15px;">Hi '.trim($result['firstName']).',<br/><br/>
										   Unfortunately, we were not able to approve the following event:<br/><br/>
										   <b>Event details :</b><br/>
										   Name : '.$result['eventName'].'<br/>
										   Date : '.date_format($date_bt, 'M jS, Y').'<br/>
										   Time : '.date_format($time_bt, 'G:i a').'<br/>
										   Venue : '.$result['locationName'].'<br/>
										   Event Organizers : '.$result['orgName'].'<br/>';
										   if($result['orgContact']!='') {
								$body1 .=  'Contact Organizers : '.$result['orgContact'].'<br/>';
										   }
								$body1 .=  '<br/><table border="0" bordercolor="#0db3a5" cellpadding="10" cellspacing="0" width="50%" style="border:2px solid #2e2e2e; font-size:14px;font-family: calibri;">
												<tr style="background:#2e2e2e; color:#fff; height:30px" align="center">
													<td>Ticket Type</td>
													<td>Total No Of Ticket</td>';
													if($result['ticketTypeId']!="2"){
								$body1 .=  '		<td>Price</td>';
													}
								$body1 .=  '	</tr>';
											$queryTicket=mysqli_query($mysqli,"select ticket.name as ticketName,ticket.price as ticketPrice,ticket.totalQty as ticketQty from ticket where ticket.eventId='".$_REQUEST['actionId']."'");
										if(mysqli_num_rows($queryTicket)>0){
											while($resultTicket=mysqli_fetch_assoc($queryTicket)){
								$body1 .=  '	<tr style="height:30px;" align="center">
													<td>'.$resultTicket['ticketName'].'</td>
													<td>'.$resultTicket['ticketQty'].'</td>';
													if($result['ticketTypeId']!="2"){
								$body1 .=  '		<td>'.$resultTicket['ticketPrice'].'</td>';
													}
								$body1 .=  '	</tr>';
												}} else {
								$body1 .=  '	<tr>
													<td colspan="3" align="center">No Data Available</td>
												</tr>';
												} 
								$body1 .=  '</table><br/><br/>
										   Please correct the following details and resubmit your event for approval:<br/><b>Reason</b> : '.mysqli_real_escape_string($mysqli,$_REQUEST['reason']).'<br/><br/>Please email us at <a href="mailto:admin@ufundoo.com" target="_blank">admin@ufundoo.com</a> with any questions.<br/><br/>Thanks,<br/>Ufundoo Team</div>';
								$subject = "Your event was not approved : (".$result['eventName'].")";
								
								require_once('../phpmailer/class.phpmailer.php');
								$mail = new PHPMailer();
								$mail->IsHTML(true);
								$mail->IsSMTP(); // telling the class to use SMTP
								$mail->Host       = $SmtpServer; // SMTP server
								//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
								$mail->SMTPSecure = "ssl";										   
								$mail->SMTPAuth   = true;                  // enable SMTP authentication
								$mail->Host       = $SmtpServer; // sets the SMTP server
								$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
								$mail->Username   = $SmtpUser; // SMTP account username
								$mail->Password   = $SmtpPass;        // SMTP account password
								
								$mail->From=$SmtpUser;
								$mail->FromName=$SmtpUser;
								
								$mail->Subject    = $subject;
								
								$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
								$mail->MsgHTML($body);
								$mail->Body=$body1;
								
								//smtp_host,smtp_port,username,password,from_address,to_address
								
								$mail->AddAddress($to_addresss1, $to_addresss1);
								
								if(!$mail->Send()) {
								  $arr = array("success"=>0); 
								} else {
								  $arr = array("success"=>1); 
								}
		}
		else
		{
			$msg='update event set lastUpdated="'.$date.'", status="disapprove",reason="'.$_REQUEST['reason'].'" where id="'.trim($_REQUEST['actionId']).'"';
			$arr=array("success"=>0,"msg"=>$msg);
		}
	}
	else
	{
		$msg='select event.name as eventName,profile.firstName as firstName inner join promoter on promoter.id=event.id inner join profile on profile.id=promoter.profileId from event where id="'.trim($_REQUEST['actionId']).'"';
			$arr=array("success"=>0,"msg"=>$msg);
	}
}
echo json_encode($arr);
?>