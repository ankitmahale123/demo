<?php
session_start(); 
require_once("../config/conn.php");
$date=date('Y-m-d H:i:s');

if($_REQUEST['action']=="approve")
{
	$query=mysqli_query($mysqli,'select promoter.*,profile.firstName from promoter inner join profile on profile.id=promoter.profileId where promoter.id="'.trim($_REQUEST['actionId']).'"');
	$result=mysqli_fetch_assoc($query);
	if(mysqli_num_rows($query)>0)
	{
		$query_update=mysqli_query($mysqli,'update promoter set lastLogin="'.$date.'",status="approve" where id="'.trim($_REQUEST['actionId']).'"');
		if($query_update==1)
		{
						include("../config/configEmail.php");
						$to_addresss1=$result['email'];
						
						$body1 =  'Hi '.ucfirst($result['firstName']).',<br/><br/>
								  Congrats! Your ufundoo event promoter account has been approved!<br/><br/>You may now <a href="https://ufundoo.com/Promoter">login</a> and post your first event.<br/><br/>Please email us at admin@ufundoo.com with any questions or for assistance setting up your event!<br/><br/>Thanks,<br/>Ufundoo Team';
						$subject = "Your promoter account is ready!";
						
						require_once('../phpmailer/class.phpmailer.php');
						$mail = new PHPMailer();
						$mail->IsHTML(true);
						$mail->IsSMTP(); // telling the class to use SMTP
						$mail->Host       = $SmtpServer; // SMTP server
						//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
						$mail->SMTPSecure = "ssl";										   
						$mail->SMTPAuth   = true;                  // enable SMTP authentication
						$mail->Host       = $SmtpServer; // sets the SMTP server
						$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
						$mail->Username   = $SmtpUser; // SMTP account username
						$mail->Password   = $SmtpPass;        // SMTP account password
						
						$mail->From=$SmtpUser;
						$mail->FromName=$SmtpUser;
						
						$mail->Subject    = $subject;
						
						$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
						$mail->MsgHTML($body);
						$mail->Body=$body1;
				
						
						//smtp_host,smtp_port,username,password,from_address,to_address
						
						$mail->AddAddress($to_addresss1, $to_addresss1);
						if(!$mail->Send()) {
						  $msg = "Error in request to admin"; 
						  $arr = array("success"=>0,'msg' => $msg);
						} else {
						  $msg = "Your details have been sent to admin. Kindly wait for approval.";
						  $arr = array("success"=>1,'msg' => $msg); 
						}
		}
		else
		{
			$arr=array("success"=>0);
		}
	}
	else
	{
		$arr=array("success"=>0);
	}
}
else
{
	$query=mysqli_query($mysqli,'select * from promoter where id="'.trim($_REQUEST['actionId']).'"');
	$result=mysqli_fetch_assoc($query);
	if(mysqli_num_rows($query)>0)
	{
		$query_update=mysqli_query($mysqli,'update promoter set lastLogin="'.$date.'", status="disapprove" where id="'.trim($_REQUEST['actionId']).'"');
		if($query_update==1)
		{
			$arr=array("success"=>1);
		}
		else
		{
			$arr=array("success"=>0);
		}
	}
	else
	{
		$arr=array("success"=>0);
	}
}
echo json_encode($arr);
?>