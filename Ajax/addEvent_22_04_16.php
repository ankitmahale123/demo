<?php
session_start(); 
require_once("../config/conn.php");
$date=date('Y-m-d H:i:s');

		/* fetch eventid */
		$queryType=mysqli_query($mysqli,'select id from eventType where name="'.trim($_REQUEST['event_type']).'"');
		$resultType=mysqli_fetch_assoc($queryType);
		
		/* find if promoter post event for a first time or not */
		$orgNumber=mysqli_query($mysqli,'select promoterId,id from organization where promoterId="'.trim($_SESSION['promoterId']).'"');
		$resultOrgNumber=mysqli_fetch_assoc($orgNumber);
		if(mysqli_num_rows($orgNumber)>0)
		{
			$query_org=mysqli_query($mysqli,'update organization set name="'.trim($_REQUEST['org_name']).'",description="'.mysqli_real_escape_string($mysqli,$_REQUEST['org_desc']).'",email="'.trim($_REQUEST['org_email']).'",fax="'.trim($_REQUEST['org_fax']).'",contactNo="'.trim($_REQUEST['org_contact_no']).'",website="'.trim($_REQUEST['org_website']).'" where promoterId="'.trim($_SESSION['promoterId']).'"');
			$orgLastId=$resultOrgNumber['id'];
		}
		else
		{
			$query_org=mysqli_query($mysqli,'insert into organization set name="'.trim($_REQUEST['org_name']).'",description="'.mysqli_real_escape_string($mysqli,$_REQUEST['org_desc']).'",email="'.trim($_REQUEST['org_email']).'",fax="'.trim($_REQUEST['org_fax']).'",contactNo="'.trim($_REQUEST['org_contact_no']).'",website="'.trim($_REQUEST['org_website']).'",promoterId="'.trim($_SESSION['promoterId']).'"');
			$orgLastId=mysqli_insert_id($mysqli);
		}
		if($query_org==1)
		{
			/* add location */
			$query_location=mysqli_query($mysqli,'insert into location set name="'.trim($_REQUEST['event_location']).'",address="'.mysqli_real_escape_string($mysqli,$_REQUEST['event_address']).'",zipcode="'.trim($_REQUEST['zip_code']).'",includeMap="'.trim($_REQUEST['mapCheck']).'"');
			$locationLastId=mysqli_insert_id($mysqli);
			if($query_location==1)
			{
				/* find ticket type */
				$query_ticket_type=mysqli_query($mysqli,'select name,id from ticketType where id="'.trim($_REQUEST['ticket_type']).'"');
				$resultTicketType=mysqli_fetch_assoc($query_ticket_type);
				/* embed video link */
				
				if($_REQUEST['event_video']!='')
				{
				$url = trim($_REQUEST['event_video']);
				preg_match('/[\\?\\&]v=([^\\?\\&]+)/',$url,$matches);
				$videoid = $matches[1];
  				$videoLink = 'https://www.youtube.com/v/' . $videoid . '&amp;hl=en_US&amp;fs=1?rel=0' ;
				}
				else
				{
					$videoLink='';
				}
					/* end embed video link */
				/* add event */
				$query_event=mysqli_query($mysqli,'insert into event set name="'.trim($_REQUEST['event_name']).'",date="'.trim($_REQUEST['datepick']).'",startDateTime="'.trim($_REQUEST['datepicker_time1']).'",endDateTime="'.trim($_REQUEST['datepicker_time2']).'",locationId="'.$locationLastId.'",description="'.mysqli_real_escape_string($mysqli,trim($_REQUEST['event_desc'])).'",video="'.$videoLink.'",seatChart="'.trim($_REQUEST['event_seat_chart']).'",coverImage="'.trim($_REQUEST['event_image_cover']).'",ticketFrom="'.trim($_REQUEST['datepick1']).' '.trim($_REQUEST['datepicker_time3']).'",ticketTo="'.trim($_REQUEST['datepick2']).' '.trim($_REQUEST['datepicker_time4']).'",ticketTypeId="'.$resultTicketType['id'].'",organizationId="'.$orgLastId.'",eventTypeId="'.$resultType['id'].'",lastUpdated="'.$date.'",createdAt="'.$date.'",promoterId="'.trim($_SESSION['promoterId']).'"');
				$eventLastId=mysqli_insert_id($mysqli);
				if($query_event==1)
				{
					
							/* add ticket details */
							$ticket=json_decode($_REQUEST['ticket_detail']);
							for($j=0;$j<sizeof($ticket);$j++)
							{
								$query_ticket_insert=mysqli_query($mysqli,'insert into ticket set name="'.$ticket[$j][0].'",totalQty="'.$ticket[$j][1].'",price="'.$ticket[$j][2].'",eventId="'.$eventLastId.'"');
							}
							if($query_ticket_insert==1)
							{
								include("../config/configEmail.php");
								$to_addresss1="admin@ufundoo.com";
								
								$body1 =  '<div style="font-family: calibri;font-size:15px;">Hi Admin,<br/><br/>
										   New event posting: '.trim($_REQUEST['event_name']).'<br/><br/>Click <a href="https://ufundoo.com/Admin">here</a> to approve or reject this request.<br/><br/>Thanks,<br/>Ufundoo Team</div>';
								$subject = "New event request: ".trim($_REQUEST['event_name']);
								
								require_once('../phpmailer/class.phpmailer.php');
								$mail = new PHPMailer();
								$mail->IsHTML(true);
								$mail->IsSMTP(); // telling the class to use SMTP
								$mail->Host       = $SmtpServer; // SMTP server
								//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
								$mail->SMTPSecure = "ssl";										   
								$mail->SMTPAuth   = true;                  // enable SMTP authentication
								$mail->Host       = $SmtpServer; // sets the SMTP server
								$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
								$mail->Username   = $SmtpUser; // SMTP account username
								$mail->Password   = $SmtpPass;        // SMTP account password
								
								$mail->From=$SmtpUser;
								$mail->FromName=$SmtpUser;
								
								$mail->Subject    = $subject;
								
								$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
								$mail->MsgHTML($body);
								$mail->Body=$body1;
								
								//smtp_host,smtp_port,username,password,from_address,to_address
								
								$mail->AddAddress($to_addresss1, $to_addresss1);
								
								if(!$mail->Send()) {
								  $msg = "error";
								  $arr = array("success"=>0,'msg' => $msg); 
								} else {
								  $flag=true;
								}
								if($flag)
								{
									/* fetch promoter data */
									
									$query=mysqli_query($mysqli,'select event.name as eventName,event.date as date,event.startDateTime as startTime,event.description as description,event.coverImage as coverImage,event.seatChart as seatChart,organization.name as orgName,organization.contactNo as orgContact, location.name as locationName,ticketType.id as ticketTypeId,profile.firstName as firstName,promoter.email as promoterEmail from event inner join promoter on promoter.id=event.promoterId inner join profile on profile.id=promoter.profileId inner join organization on organization.id=event.organizationId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.id="'.$eventLastId.'"');
									$result=mysqli_fetch_assoc($query);
									$time_bt = date_create($result['startTime']);	
									$date_bt = date_create($result['date']);
	
									include("../config/configEmail.php");
									$to_addresss1=$result['promoterEmail'];
									
									$body1 =  '<div style="font-family: calibri;font-size:15px;">Hi '.trim($result['firstName']).',<br/><br/>
											   Thanks for submitting your event to ufundoo.<br/><br/>
											   <b>Event details :</b><br/>
											   Name : '.$result['eventName'].'<br/>
											   Date : '.date_format($date_bt, 'M jS, Y').'<br/>
											   Time : '.date_format($time_bt, 'G:i a').'<br/>
											   Venue : '.$result['locationName'].'<br/>
											   Event Organizers : '.$result['orgName'].'<br/>';
											   if($result['orgContact']!='') {
									$body1 .=  'Contact Organizers : '.$result['orgContact'].'<br/>';
											   }
									$body1 .=  '<br/><table border="0" bordercolor="#0db3a5" cellpadding="10" cellspacing="0" width="50%" style="border:2px solid #2e2e2e; font-size:14px;font-family: calibri;">
													<tr style="background:#2e2e2e; color:#fff; height:30px" align="center">
														<td>Ticket Type</td>
														<td>Total No Of Ticket</td>';
														if($result['ticketTypeId']!="2"){
									$body1 .=  '		<td>Price</td>';
														}
									$body1 .=  '	</tr>';
												$queryTicket=mysqli_query($mysqli,"select ticket.name as ticketName,ticket.price as ticketPrice,ticket.totalQty as ticketQty from ticket where ticket.eventId='".$eventLastId."'");
											if(mysqli_num_rows($queryTicket)>0){
												while($resultTicket=mysqli_fetch_assoc($queryTicket)){
									$body1 .=  '	<tr style="height:30px;" align="center">
														<td>'.$resultTicket['ticketName'].'</td>
														<td>'.$resultTicket['ticketQty'].'</td>';
														if($result['ticketTypeId']!="2"){
									$body1 .=  '		<td>'.$resultTicket['ticketPrice'].'</td>';
														}
									$body1 .=  '	</tr>';
													}} else {
									$body1 .=  '	<tr>
														<td colspan="3" align="center">No Data Available</td>
													</tr>';
													} 
									$body1 .=  '</table><br/><br/>
												You will get an email from us as soon as your event is approved.<br/><br/>
											   Please email us at <a href="mailto:admin@ufundoo.com" target="_blank">admin@ufundoo.com</a> with any questions.<br/><br/>Thanks,<br/>Ufundoo Team</div>';
									$subject = "Awaiting approval: (".$result['eventName'].")";
									
									require_once('../phpmailer/class.phpmailer.php');
									$mail = new PHPMailer();
									$mail->IsHTML(true);
									$mail->IsSMTP(); // telling the class to use SMTP
									$mail->Host       = $SmtpServer; // SMTP server
									//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
									$mail->SMTPSecure = "ssl";										   
									$mail->SMTPAuth   = true;                  // enable SMTP authentication
									$mail->Host       = $SmtpServer; // sets the SMTP server
									$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
									$mail->Username   = $SmtpUser; // SMTP account username
									$mail->Password   = $SmtpPass;        // SMTP account password
									
									$mail->From=$SmtpUser;
									$mail->FromName=$SmtpUser;
									
									$mail->Subject    = $subject;
									
									$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
									$mail->MsgHTML($body);
									$mail->Body=$body1;
									
									//smtp_host,smtp_port,username,password,from_address,to_address
									
									$mail->AddAddress($to_addresss1, $to_addresss1);
									
									if(!$mail->Send()) {
							    	  $msg = 'error'; 
									  $arr = array("success"=>0,'msg' => $msg);
									} else {
									  $msg = 'Successfully Added Event'; 
									  $arr = array("success"=>1,'msg' => $msg);
									}
								}
							}
							else
							{
								$msg = 'insert into ticket set name="'.$ticket[$j][0].'",totalQty="'.$ticket[$j][1].'",price="'.$ticket[$j][2].'",eventId="'.$eventLastId.'"';
								$arr=array("success"=>0,'msg' => $msg);
							}
				}
				else
				{
					$msg = 'insert into event set name="'.trim($_REQUEST['event_name']).'",date="'.trim($_REQUEST['datepick']).'",startDateTime="'.trim($_REQUEST['datepicker_time1']).'",endDateTime="'.trim($_REQUEST['datepicker_time2']).'",locationId="'.$locationLastId.'",description="'.trim($_REQUEST['event_desc']).'",video="'.$videoLink.'",seatChart="'.trim($_REQUEST['event_seat_chart']).'",coverImage="'.trim($_REQUEST['event_image_cover']).'",ticketFrom="'.trim($_REQUEST['datepick1']).' '.trim($_REQUEST['datepicker_time3']).'",ticketTo="'.trim($_REQUEST['datepick2']).' '.trim($_REQUEST['datepicker_time4']).'",ticketTypeId="'.$resultTicketType['id'].'",organizationId="'.$orgLastId.'",eventTypeId="'.$resultType['id'].'",lastUpdated="'.$date.'",createdAt="'.$date.'",promoterId="'.trim($_SESSION['promoterId']).'"';
					$arr=array("success"=>0,'msg' => $msg);
				}
			}
			else
			{
				$msg = 'insert into location set name="'.trim($_REQUEST['event_location']).'",address="'.mysqli_real_escape_string($mysqli,$_REQUEST['event_address']).'",zipcode="'.trim($_REQUEST['zip_code']).'"'; 
				$arr=array("success"=>0,'msg' => $msg);
			}
		}
		else
		{
			$msg = 'insert into organization set name="'.trim($_REQUEST['org_name']).'",description="'.mysqli_real_escape_string($mysqli,$_REQUEST['org_desc']).'",email="'.trim($_REQUEST['org_email']).'",fax="'.trim($_REQUEST['org_fax']).'",contactNo="'.trim($_REQUEST['org_contact_no']).'",website="'.trim($_REQUEST['org_website']).'",promoterId="'.trim($_SESSION['promoterId']).'"'; 
			$arr=array("success"=>0,'msg' => $msg);
		}
echo json_encode($arr);
?>