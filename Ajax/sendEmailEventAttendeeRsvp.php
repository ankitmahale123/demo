<?php
session_start(); 
require_once("../config/conn.php");
$queryEmailId=mysqli_query($mysqli,'select * from rsvp where eventId="'.$_REQUEST["eventId"].'"');

include("../config/configEmail.php");

if(mysqli_num_rows($queryEmailId))
{						
$body =  $_REQUEST["emailContent"];
$subject =  $_REQUEST["subject"];
						
require_once('../phpmailer/class.phpmailer.php');
$mail = new PHPMailer();
$mail->IsHTML(true);
$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = $SmtpServer; // SMTP server
//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
$mail->SMTPSecure = "ssl";										   
$mail->SMTPAuth   = true;                  // enable SMTP authentication
$mail->Host       = $SmtpServer; // sets the SMTP server
$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
$mail->Username   = $SmtpUser; // SMTP account username
$mail->Password   = $SmtpPass;        // SMTP account password
$mail->From=$SmtpUser;
$mail->FromName=$SmtpUser;
$mail->Subject    = $subject;
$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
$mail->MsgHTML($body);
$mail->Body=$body;
	//smtp_host,smtp_port,username,password,from_address,to_address
	while($resultEmail=mysqli_fetch_assoc($queryEmailId))
	{	
		$mail->AddBCC($resultEmail["email"], '');
	}
	if(!$mail->Send()) {
		$arr = array("success"=>0,"msg"=>"Technical Error");
	} else {
		$arr = array("success"=>1);
	}
}
else
{
	$arr = array("success"=>0,"msg"=>"No attendees exist");
}
echo json_encode($arr);
?>
