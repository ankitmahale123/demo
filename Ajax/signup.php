<?php
session_start(); 
require_once("../config/conn.php");
include("../config/configEmail.php");
require_once('../phpmailer/class.phpmailer.php');
$date=date('Y-m-d H:i:s');
$flag=false;


if($_REQUEST['role']=="customer")
{
	$queryEmailCheck=mysqli_query($mysqli,'select id from user where email="'.strtolower(trim($_REQUEST['username'])).'"');
	if(mysqli_num_rows($queryEmailCheck)==0)
	{
		$query_profile_insert=mysqli_query($mysqli,'insert into profile set firstName="'.trim($_REQUEST['firstname']).'",lastName="'.trim($_REQUEST['lastname']).'"');
		$profileLastId=mysqli_insert_id($mysqli);
		if($query_profile_insert==1)
		{
			$query_user_insert=mysqli_query($mysqli,'insert into user set email="'.strtolower(trim($_REQUEST['username'])).'",password="'.sha1($_REQUEST['password']).'",lastLogin="'.$date.'",createdAt="'.$date.'",profileId="'.$profileLastId.'"');
			$userId=mysqli_insert_id($mysqli);
			if($query_user_insert==1)
			{
						$to_addresss1=strtolower(trim($_REQUEST['username']));
						$body1 =  '<div style="font-family: calibri;font-size:15px;">Hi '.$_REQUEST['firstname'].',</div><br/>
								   <div style="font-family: calibri;font-size:15px;">Thanks for signing up at ufundoo. Please click <a href="https://ufundoo.com/confirmUser.php?id='.$userId.'&role='.$_REQUEST['role'].'">here</a> to activate your account.<div style="font-family: calibri;"></div><br/>
											  <div style="font-family: calibri;font-size:15px;">Thanks,<br/>Ufundoo Team</div>';
						$subject = "Activate your ufundoo account";
						//echo $body1;
						//exit();
						$mail = new PHPMailer();
						$mail->IsHTML(true);
						$mail->IsSMTP(); // telling the class to use SMTP
						$mail->Host       = $SmtpServer; // SMTP server
						//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
						$mail->SMTPSecure = "ssl";										   
						$mail->SMTPAuth   = true;                  // enable SMTP authentication
						$mail->Host       = $SmtpServer; // sets the SMTP server
						$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
						$mail->Username   = $SmtpUser; // SMTP account username
						$mail->Password   = $SmtpPass;        // SMTP account password
						
						$mail->From=$SmtpUser;
						$mail->FromName=$SmtpUser;
						
						$mail->Subject    = $subject;
						
						$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
						$mail->MsgHTML($body);
						$mail->Body=$body1;
				
						
						//smtp_host,smtp_port,username,password,from_address,to_address
						
						$mail->AddAddress($to_addresss1, $to_addresss1);
						
						if(!$mail->Send()) {
						  $msg = "Error in request to admin"; 
						  $arr = array("success"=>0,'msg' => $msg);
						} else {
						  $msg = "Check your mail for email verification process.";
						  $arr = array("success"=>1,'msg' => $msg); 
						}
			}
			else
			{
				$msg = "Technical Error.";
				$arr=array("success"=>0,'msg' => $msg);
			}
		}
		else
		{
			$msg = "Technical Error.";
			$arr=array("success"=>0,'msg' => $msg);
		}
	}
	else
	{
		$msg = "Email Address Already Exist.";
		$arr=array("success"=>0,'msg' => $msg);
	}
}
else
{
	$queryEmailCheck=mysqli_query($mysqli,'select id from promoter where email="'.strtolower(trim($_REQUEST['username'])).'"');
	if(mysqli_num_rows($queryEmailCheck)==0)
	{
		$query_profile_insert=mysqli_query($mysqli,'insert into profile set firstName="'.trim($_REQUEST['firstname']).'",lastname="'.trim($_REQUEST['lastname']).'"');
		$profileLastId=mysqli_insert_id($mysqli);
		$email=strtolower(trim($_REQUEST['username']));
		if($query_profile_insert==1)
		{
			$query_user_insert=mysqli_query($mysqli,'insert into promoter set email="'.strtolower(trim($_REQUEST['username'])).'",password="'.sha1($_REQUEST['password']).'",alertEmail="'.trim($_REQUEST['email_notification']).'",lastLogin="'.$date.'",createdAt="'.$date.'",profileId="'.$profileLastId.'"');
			if($query_user_insert==1)
			{
						$to_addresss1=strtolower(trim($_REQUEST['username']));
						
						$body1 =  '<div style="font-family: calibri;font-size:15px;">Hi '.$_REQUEST['firstname'].',<br/><br/>
								  Thanks for requesting an account as an event promoter on ufundoo.com<br/><br/>
								  Your request is being reviewed by our team. We will get back to you as soon as your account is approved!<br/><br/>
								  If you have any questions, or do not hear from us within 3 business days, please send us an email at <a href="mailto:admin@ufundoo.com" target="_blank">admin@ufundoo.com</a>.<br/><br/>
								  Thanks,<br/>Ufundoo Team</div>';
						$subject = "Ufundoo Promoter: Awaiting approval";
						
						$mail = new PHPMailer();
						$mail->IsHTML(true);
						$mail->IsSMTP(); // telling the class to use SMTP
						$mail->Host       = $SmtpServer; // SMTP server
						//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
						$mail->SMTPSecure = "ssl";										   
						$mail->SMTPAuth   = true;                  // enable SMTP authentication
						$mail->Host       = $SmtpServer; // sets the SMTP server
						$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
						$mail->Username   = $SmtpUser; // SMTP account username
						$mail->Password   = $SmtpPass;        // SMTP account password
						
						$mail->From=$SmtpUser;
						$mail->FromName=$SmtpUser;
						
						$mail->Subject    = $subject;
						
						$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
						$mail->MsgHTML($body);
						$mail->Body=$body1;
				
						
						//smtp_host,smtp_port,username,password,from_address,to_address
						
						$mail->AddAddress($to_addresss1, $to_addresss1);
						if(!$mail->Send()) {
						  $msg = "Error in request to admin"; 
						  $arr = array("success"=>0,'msg' => $msg);
						} else {
							$flag=true;
						}
						if($flag)
						{
							$to_addresss1="admin@ufundoo.com";
							
							$body1 =  '<div style="font-family: calibri;font-size:15px;">Hi Admin,<br/><br/>
									   New promoter signup: '.$_REQUEST['firstname'].'<br/><br/>
									  Click <a href="https://ufundoo.com/Admin">here</a> to approve or reject this request.<br/><br/>
									  Thanks,<br/>Ufundoo Team</div>';
							$subject = "New promoter signup request: ".$_REQUEST['firstname'];
							
							$mail = new PHPMailer();
							$mail->IsHTML(true);
							$mail->IsSMTP(); // telling the class to use SMTP
							$mail->Host       = $SmtpServer; // SMTP server
							//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
							$mail->SMTPSecure = "ssl";										   
							$mail->SMTPAuth   = true;                  // enable SMTP authentication
							$mail->Host       = $SmtpServer; // sets the SMTP server
							$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
							$mail->Username   = $SmtpUser; // SMTP account username
							$mail->Password   = $SmtpPass;        // SMTP account password
							
							$mail->From=$SmtpUser;
							$mail->FromName=$SmtpUser;
							
							$mail->Subject    = $subject;
							
							$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
							$mail->MsgHTML($body);
							$mail->Body=$body1;
					
							
							//smtp_host,smtp_port,username,password,from_address,to_address
							
							$mail->AddAddress($to_addresss1, $to_addresss1);
							if(!$mail->Send()) {
							  $msg = "Error in request to admin"; 
							  $arr = array("success"=>0,'msg' => $msg);
							} else {
							  $msg = "Your details have been sent to admin. Kindly wait for approval.";
							  $arr = array("success"=>1,'msg' => $msg); 
							}
						}
			}
			else
			{
				$msg = "Technical Error.";
				$arr=array("success"=>0,'msg' => $msg);
			}
		}
		else
		{
			$msg = "Technical Error.";
			$arr=array("success"=>0,'msg' => $msg);
		}
	}
	else
	{
		$msg = "Email Address Already Exist.";
		$arr=array("success"=>0,'msg' => $msg);
	}
}
echo json_encode($arr);
?>
