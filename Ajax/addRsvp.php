<?php
session_start(); 
require_once("../config/conn.php");
$date=date('Y-m-d');
if($_REQUEST['typeSubmit']=="add")
{
	$query_event_select=mysqli_query($mysqli,'select id as rsvpId,email,eventId from rsvp where email="'.trim($_REQUEST['email']).'" AND eventId="'.$_REQUEST['id'].'"');
	$result_event_select=mysqli_fetch_assoc($query_event_select);
	if(mysqli_num_rows($query_event_select)>0)
	{
		$msg="You are already going to this event. <span style='text-decoration:underline; cursor:pointer;' onclick='edit_rsvp_info(".$result_event_select['rsvpId'].",".$_REQUEST['id'].")'>Click here to edit info</span>";
		$arr=array("success"=>0,"msg"=>$msg);
	}
	else
	{
		$query_event_insert=mysqli_query($mysqli,'insert into rsvp set name="'.trim($_REQUEST['username']).'",email="'.trim($_REQUEST['email']).'",contactNo="'.trim($_REQUEST['contact_no']).'",eventId="'.$_REQUEST['id'].'",addedDate="'.$date.'"');
		if($query_event_insert==1)
		{
			$query_event=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,organization.name as organizationName,location.name as locationName,ticketType.id as ticketTypeId from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.id='".$_REQUEST['id']."'");
			$result_event=mysqli_fetch_assoc($query_event);
			
			//date time conversion in specific format
			$time_bt = date_create($result_event['startTime']);	
			$date_bt = date_create($result_event['eventDateFb']);	
			
			//values for parameter
			$eventName=$result_event['eventName'];
			$eventDate=date_format($date_bt,'jS M Y');
			$eventTime=date_format($time_bt,'G:i a');
			$eventLocation=$result_event['locationName'];
			$eventSeatChart=$result_event['seatChart'];
			$ticketTypeId=$result_event['ticketTypeId'];
			$eventId=$_REQUEST['id'];
			
			$arr=array("success"=>1,"eventName"=>$eventName,"eventDate"=>$eventDate,"eventTime"=>$eventTime,"eventLocation"=>$eventLocation,"eventSeatChart"=>$eventSeatChart,"ticketTypeId"=>$ticketTypeId,"eventId"=>$eventId);
		}
		else
		{
			$msg="Technical Error.";
			$arr=array("success"=>0,"msg"=>$msg);
		}
	}
}
else
{
	$query_event_insert=mysqli_query($mysqli,'update rsvp set name="'.trim($_REQUEST['username']).'",contactNo="'.trim($_REQUEST['contact_no']).'" where email="'.trim($_REQUEST['email']).'"');
		if($query_event_insert==1)
		{
			$query_event=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,organization.name as organizationName,location.name as locationName,ticketType.id as ticketTypeId from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.id='".$_REQUEST['id']."'");
			$result_event=mysqli_fetch_assoc($query_event);
			
			//date time conversion in specific format
			$time_bt = date_create($result_event['startTime']);	
			$date_bt = date_create($result_event['eventDateFb']);	
			
			//values for parameter
			$eventName=$result_event['eventName'];
			$eventDate=date_format($date_bt,'jS M Y');
			$eventTime=date_format($time_bt,'G:i a');
			$eventLocation=$result_event['locationName'];
			$eventSeatChart=$result_event['seatChart'];
			$ticketTypeId=$result_event['ticketTypeId'];
			$eventId=$_REQUEST['id'];
			
			$arr=array("success"=>1,"eventName"=>$eventName,"eventDate"=>$eventDate,"eventTime"=>$eventTime,"eventLocation"=>$eventLocation,"eventSeatChart"=>$eventSeatChart,"ticketTypeId"=>$ticketTypeId,"eventId"=>$eventId);
			//$arr=array("success"=>2);
		}
		else
		{
			$msg="Technical Error.";
			$arr=array("success"=>0,"msg"=>$msg);
		}
}
echo json_encode($arr);
?>