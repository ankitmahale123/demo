<?php
	session_start();
	require_once("../config/conn.php"); 
	$date=date('Y-m-d');
	if($_REQUEST['event_type']=='Category - Any')
	$_REQUEST['event_type']='';
	
	if($_REQUEST['eventname_location']=='' && $_REQUEST['dp']!='' && $_REQUEST['event_type']!='')
	{
		$queryString="(eventType.name LIKE '%".$_REQUEST['event_type']."%' AND event.date LIKE '%".$_REQUEST['dp']."%')";
	}
	if($_REQUEST['dp']=='' && $_REQUEST['eventname_location']!='' && $_REQUEST['event_type']!='')
	{
		$queryString="((event.name LIKE '%".$_REQUEST['eventname_location']."%' OR location.name LIKE '%".$_REQUEST['eventname_location']."%') AND eventType.name LIKE '%".$_REQUEST['event_type']."%')";
	}
	if($_REQUEST['event_type']=='' && $_REQUEST['eventname_location']!='' && $_REQUEST['dp']!='')
	{
		$queryString="((event.name LIKE '%".$_REQUEST['eventname_location']."%' OR location.name LIKE '%".$_REQUEST['eventname_location']."%') AND event.date LIKE '%".$_REQUEST['dp']."%')";
	}
	if($_REQUEST['eventname_location']=='' && $_REQUEST['dp']=='' && $_REQUEST['event_type']!='')
	{
		$queryString="(eventType.name LIKE '%".$_REQUEST['event_type']."%')";
	}
	if($_REQUEST['dp']=='' && $_REQUEST['event_type']=='' && $_REQUEST['eventname_location']!='')
	{
		$queryString="(event.name LIKE '%".$_REQUEST['eventname_location']."%' OR location.name LIKE '%".$_REQUEST['eventname_location']."%')";
	}
	if($_REQUEST['event_type']=='' && $_REQUEST['eventname_location']=='' && $_REQUEST['dp']!='')
	{
		$queryString="(event.date LIKE '%".$_REQUEST['dp']."%')";
	}
	if($_REQUEST['event_type']!='' && $_REQUEST['eventname_location']!='' && $_REQUEST['dp']!='')
	{
		$queryString="((event.name LIKE '%".$_REQUEST['eventname_location']."%' OR location.name LIKE '%".$_REQUEST['eventname_location']."%') AND eventType.name LIKE '%".$_REQUEST['event_type']."%' AND event.date LIKE '%".$_REQUEST['dp']."%')";
	}
	
	/* if all parameters are empty */
	if($_REQUEST['event_type']=='' && $_REQUEST['eventname_location']=='' && $_REQUEST['dp']=='')
	{
		if($_REQUEST['price']==0)
		{
			$queryEvent=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,organization.name as organizationName,location.name as locationName, ticketType.id as ticketTypeId from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.status='approve' AND event.date>='".$date."' ORDER BY id DESC");
		}
		else
		{
			$queryEvent=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,organization.name as organizationName,location.name as locationName, ticketType.id as ticketTypeId from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.status='approve' AND (ticketTypeId='".$_REQUEST['price']."') AND event.date>='".$date."' ORDER BY id DESC");
		}
	}else
	{
	
	/* if ticket type could be anything */
	if($_REQUEST['price']==0)
	{
		$queryEvent=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,organization.name as organizationName,location.name as locationName, ticketType.id as ticketTypeId from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where ".$queryString." AND (event.status='approve') AND event.date>='".$date."' ORDER BY id DESC");
	}
	
	/* if ticket type could be some specific type */
	else
	{
		$queryEvent=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,organization.name as organizationName,location.name as locationName, ticketType.id as ticketTypeId from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where ".$queryString." AND (event.status='approve') AND (ticketTypeId='".$_REQUEST['price']."') AND event.date>='".$date."' ORDER BY id DESC");
		
	}
	}
	if(mysqli_num_rows($queryEvent)>0){ 
?>
                <div align="center" class="mostPopEvents">EVENTS</div>
                <div align="center" class="mostPopEventsLine"></div>
                <div style="width:100%; min-height:10px;">
                	<div class="eachBlockWrapper" style="width:100%;">
                	<!-- each block -->
                    <?php 
						while($result=mysqli_fetch_assoc($queryEvent)){
						$time_bt = date_create($result['startTime']);	
						$date_bt = date_create($result['eventDateFb']);	
					?>
                	<div class="inheritHeight eachBlockWrapperCont">
                    	 <?php if($result['ticketTypeId']=="2"){ ?>
                        <div style="position: absolute;color: #fff; text-align: center; padding: 1px;background: rgba(255,255,255,0.5);border-radius: 12px; width: 55px; margin-top: 10px;margin-left: 10px; font-size:12px;">FREE</div>
                        <?php } ?>
                    	<div class="eachBlock inheritHeight">
                        	<?php
							$queryImage=mysqli_query($mysqli,"select * from eventImage where eventImage.eventId='".$result['id']."' ORDER BY id LIMIT 1");
							if(mysqli_num_rows($queryImage)>0){
							$resultImage=mysqli_fetch_assoc($queryImage);
								if($resultImage['path']!='')
								{
									$fbimage=$resultImage['path'];
								}
								else
								{
									$fbimage='https://ufundoo.com/assets/img/no_image.png';
								}
							?>
                           	<img src="<?php echo $fbimage; ?>" height="247" style="width:100%;background: #E1E1E1;cursor:pointer;border: 1px solid #e1e1e1;" onclick="window.open('eventDetail.php?eventId=<?php echo $result['id'];?>&fbimage=<?php echo $fbimage; ?>','_self');" />
                            <?php } else { ?>
                            <img src="https://ufundoo.com/assets/img/no_image.png" height="247" style="width:100%;background: #E1E1E1;cursor:pointer;border: 1px solid #e1e1e1;" onclick="window.open('eventDetail.php?eventId=<?php echo $result['id'];?>&fbimage=<?php echo $fbimage; ?>','_self');" />
                            <?php } ?>
                            <div class="eventDataWrapper">
                            	<div style="display:table-row">
                                	<div class="eventData">
                                        	<div style="display:inline-block; width:60%; height:65px; vertical-align:top;">
                                            	<div style="font-weight:600;cursor:pointer; height:65px;" class="eventDataEach" onclick="window.open('eventDetail.php?eventId=<?php echo $result['id'];?>&fbimage=<?php echo $fbimage; ?>','_self');" title="<?php echo $result['eventName']; ?>" ><?php echo $result['eventName']; ?></div>
                                            </div>
                                            <div style="display:inline-block; width:39%; height:65px;vertical-align:top;">
                                            	<div class="bookNowWrapper">
														<?php if($result['ticketTypeId']=="2"){ ?>
                                                        <div align="center" class="buyNow" onclick="window.open('seatChart.php?eventId=<?php echo $result['id']; ?>&eventName=<?php echo urlencode($result['eventName']); ?>&eventDate=<?php echo date_format($date_bt, 'jS M Y'); ?>&eventTime=<?php echo date_format($time_bt, 'G:i a'); ?>&locationName=<?php echo urlencode($result['locationName']); ?>&seatChart=<?php echo $result['seatChart']; ?>&ticketTypeId=<?php echo $result['ticketTypeId']; ?>','_self');">BOOK NOW</div>
                                                        <?php } else { ?>
                                                        <div align="center" class="buyNow" onclick="window.open('seatChart.php?eventId=<?php echo $result['id']; ?>&eventName=<?php echo urlencode($result['eventName']); ?>&eventDate=<?php echo date_format($date_bt, 'jS M Y'); ?>&eventTime=<?php echo date_format($time_bt, 'G:i a'); ?>&locationName=<?php echo urlencode($result['locationName']); ?>&seatChart=<?php echo $result['seatChart']; ?>&ticketTypeId=<?php echo $result['ticketTypeId']; ?>','_self');">BUY TICKET</div>
                                                    <?php } ?>
                                            	</div>
                                        </div>
                                        <div style="margin-top:7px; display:inline-block; width:100%;" class="eventDataEach"><?php echo date_format($date_bt, 'jS M,y').' '.date_format($time_bt, 'G:i a'); ?></div>
                                        <div style="margin-top:5px; display:inline-block; width:100%; height:42px;" class="eventDataEach"><?php echo $result['locationName'];?></div>
                                    </div>
                                </div>
                                <div style="display:table-row">
                                	<div class="iconsWrapper" align="right">
                                    	<div align="right" class="eachIcon">
                                        	<a href="https://www.facebook.com/dialog/feed?app_id=705873652868650&redirect_uri=https://ufundoo.com&link=https://ufundoo.com&picture=<?php echo $fbimage; ?>&caption=<?php echo $result['eventName']; ?>&description= Date : <?php echo $result['eventDateFb']?><center></center> Location : <?php echo $result['locationName'];?>" onclick="window.open(this.href); return false;">
                                                <img src="assets/img/mostp_fb.png" title="Facebook"/>
                                            </a>
                                        </div>
                                        <div align="right" class="eachIcon">
                                        	<a href="http://twitter.com/share?text=Ufundoo - <?php echo $result['eventName']; ?>%0aDate : <?php echo $result['eventDateFb']?>%0aLocation : <?php echo $result['locationName'];?>" onclick="window.open(this.href); return false;">
                                            	<img src="assets/img/mostp_tweet.png" title="Twitter" />
                                            </a>
                                        </div>
                                        <div align="right" class="eachIcon">
                                        	<a href="http://pinterest.com/pin/create/button/?url=https://ufundoo.com&media=<?php echo $fbimage; ?>&description=Event Name : <?php echo $result['eventName']; ?>%0aDate : <?php echo $result['eventDateFb']?>%0aLocation : <?php echo $result['locationName'];?>" onclick="window.open(this.href); return false;">
                                            	<img src="assets/img/mostp_pin.png" title="Pin It" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php } ?>
                    <!-- end each block -->
                </div>
            </div>
            <?php } else { ?>
                    <div align="center" style="margin-top:30px; color:#000; font-size:25px; margin-bottom:25px;">Oops... No listing available for this category right now.</div>
                    <?php } ?>