<?php
session_start(); 
require_once("../config/conn.php");
$date=date('Y-m-d H:i:s');

	$queryType=mysqli_query($mysqli,'select id from eventType where name="'.trim($_REQUEST['event_type']).'"');
	$resultType=mysqli_fetch_assoc($queryType);
	
	/* find ticket type */
	$query_ticket_type=mysqli_query($mysqli,'select name,id from ticketType where id="'.trim($_REQUEST['ticket_type']).'"');
	$resultTicketType=mysqli_fetch_assoc($query_ticket_type);
		
	/* find location */
	$query_org=mysqli_query($mysqli,'update location set name="'.trim($_REQUEST['event_location']).'",address="'.trim($_REQUEST['event_address']).'",zipcode="'.trim($_REQUEST['zip_code']).'",includeMap="'.trim($_REQUEST['mapCheck']).'" where id="'.trim($_REQUEST['locationId']).'"');
			
		$query_org_insert=mysqli_query($mysqli,'update organization set name="'.trim($_REQUEST['org_name']).'",description="'.mysqli_real_escape_string($mysqli,$_REQUEST['org_desc']).'",email="'.trim($_REQUEST['org_email']).'",fax="'.trim($_REQUEST['org_fax']).'",contactNo="'.trim($_REQUEST['org_contact_no']).'",website="'.trim($_REQUEST['org_website']).'" where id="'.trim($_REQUEST['org_id']).'"');
		
		if($query_org_insert==1)
		{
			if($_REQUEST['event_video']!='')
			{
				if (preg_match('/watch/',$_REQUEST['event_video']))
				{
						$url = trim($_REQUEST['event_video']);
						preg_match('/[\\?\\&]v=([^\\?\\&]+)/',$url,$matches);
						$videoid = $matches[1];
						$videoLink = 'https://www.youtube.com/v/' . $videoid . '&amp;hl=en_US&amp;fs=1?rel=0' ;
				}
				else
				{	
						$videoLink = $_REQUEST['event_video'] ;
				}
			}
			else
			{
				$videoLink ='';
			}
			$query_event_insert=mysqli_query($mysqli,'update event set name="'.trim($_REQUEST['event_name']).'",date="'.trim($_REQUEST['datepick']).'",startDateTime="'.trim($_REQUEST['datepicker_time1']).'",endDateTime="'.trim($_REQUEST['datepicker_time2']).'",locationId="'.$_REQUEST['location_id'].'",description="'.trim($_REQUEST['event_desc']).'",video="'.$videoLink.'",seatChart="'.trim($_REQUEST['event_seat_chart']).'",coverImage="'.trim($_REQUEST['event_image_cover']).'",ticketFrom="'.trim($_REQUEST['datepick1']).' '.trim($_REQUEST['datepicker_time3']).'",ticketTo="'.trim($_REQUEST['datepick2']).' '.trim($_REQUEST['datepicker_time4']).'",ticketTypeId="'.$resultTicketType['id'].'",organizationId="'.$_REQUEST['org_id'].'",eventTypeId="'.$resultType['id'].'",lastUpdated="'.$date.'" where id="'.trim($_REQUEST['event_id']).'"');
						
			if($query_event_insert==1)
			{
					$query_ticket_insert=mysqli_query($mysqli,'delete from ticket where eventId="'.trim($_REQUEST['event_id']).'"');
					$ticket=json_decode($_REQUEST['ticket_detail']);
					for($j=0;$j<sizeof($ticket);$j++)
					{
						$query_ticket_insert=mysqli_query($mysqli,'insert into ticket set name="'.$ticket[$j][0].'",totalQty="'.$ticket[$j][1].'",price="'.$ticket[$j][2].'",eventId="'.trim($_REQUEST['event_id']).'"');
					}
					$arr=array("success"=>1,'msg' => $msg);
			}
			else
			{
				$arr=array("success"=>0,'msg' => $msg);
			}
		}
		else
		{
			$arr=array("success"=>0,'msg' => $msg);
		}
echo json_encode($arr);
?>
