<?php
session_start(); 
require_once("../config/conn.php");

include("../config/configEmail.php");
						
$body =  $_REQUEST["emailContent"];
$subject =  $_REQUEST["subject"];
						
require_once('../phpmailer/class.phpmailer.php');
$mail = new PHPMailer();
$mail->IsHTML(true);
$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = $SmtpServer; // SMTP server
//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
$mail->SMTPSecure = "ssl";										   
$mail->SMTPAuth   = true;                  // enable SMTP authentication
$mail->Host       = $SmtpServer; // sets the SMTP server
$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
$mail->Username   = $SmtpUser; // SMTP account username
$mail->Password   = $SmtpPass;        // SMTP account password
$mail->From=$SmtpUser;
$mail->FromName=$SmtpUser;
$mail->Subject    = $subject;
$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
$mail->MsgHTML($body);
$mail->Body=$body;
$file_handle = fopen($_REQUEST["people"], "r");
while ( ($data = fgetcsv($file_handle) ) !== FALSE ) {

$bccEmail=trim($data[0]);
$mail->AddBCC($bccEmail,'');
}
fclose($file_handle);
if(!$mail->Send()) {
	$arr = array("success"=>0);
} else {
	$arr = array("success"=>1);
}
echo json_encode($arr);
?>
