<?php
	session_start();
	require_once("config/conn.php"); 
	//$queryEvent=mysqli_query($mysqli,"select event.name as eventName,event.date as date,event.startDateTime as startTime,event.description as description,event.coverImage as coverImage,event.seatChart as seatChart,organization.name as orgName,organization.contactNo as orgContact, location.name as locationName,location.address as eventAddress,ticketType.id as ticketTypeId,event.video, SUM(ticketStatus.availableQty) as totalNumber from event inner join organization on organization.id=event.organizationId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.id='".$_REQUEST['eventId']."' GROUP BY event.id");
	
$queryEvent=mysqli_query($mysqli,"select event.ticketTypeId as ttypeId,event.name as eventName,event.date as date,event.startDateTime as startTime,event.description as description,event.coverImage as coverImage,event.seatChart as seatChart,organization.name as orgName,organization.contactNo as orgContact, location.name as locationName,location.address as eventAddress,ticketType.id as ticketTypeId,event.video, SUM(ticketStatus.availableQty) as totalNumber from event inner join organization on organization.id=event.organizationId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId  inner join ticketStatus on ticketStatus.eventId=event.id where event.id='".$_REQUEST['eventId']."' GROUP BY event.id");
	$resultEvent=mysqli_fetch_assoc($queryEvent);
	
	// fetch from view
	//$queryView = mysqli_query($mysqli,"SELECT SUM(ticketStatus.availableQty) as totalNumber FROM `ticketStatus` where eventId='".$_REQUEST['eventId']."'");
	//$resultView=mysqli_fetch_assoc($queryView); 
	
	$ttypeId= $resultEvent['ttypeId'];
	
	$time_bt = date_create($resultEvent['startTime']);	
	$date_bt = date_create($resultEvent['date']);
	$queryReview=mysqli_query($mysqli,"select * from review where eventId = '".$_REQUEST['eventId']."' ORDER BY date DESC");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Ufundoo | Event List</title>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="https://ufundoo.com" />
<meta name="twitter:title" content="Small Island Developing States Photo Submission" />
<meta name="twitter:description" content="View the album on Flickr." />
<meta name="twitter:image" content="https://ufundoo.com/img.png" />
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<link rel="stylesheet" href="assets/css/datepicker.css" type="text/css" />
<style>
html,body
{
	background: #fff;
	overflow:hidden;
}
.headerBtnActive
{
	border: 1px solid #2e302d;
	color:#2e302d;
}
.headerBtn
{
	color:#2e302d;
}
.searchBar
{
	height:80px;
	background:none;
	margin-bottom:25px;
}
.searchBarContainer
{
	height:80px;
	border-radius:80px;
	width:95%;
}
.searchBarWrapper {
    height: 60px;
    margin-top: 3px;
}
.searchLocation,.category
{
	padding-left:0.7%;
	width: 20%;
	padding-right:1%;
	display: inline-block;
}
.searchBox
{
	font-size:17px;
	padding-left:20px;
}
.searchBtn
{
	padding-top:10px;
	margin-top:5px;
}
.eachBlockWrapper
{
	margin-top:0px;
	height:auto;
}
.eachBlockWrapperCont
{
	margin-bottom:2%;
	margin-left:1%;
	margin-right:1%;
	width:22.7%;
	position: relative;
}
.mostPopEventsLine
{
	margin-bottom:25px;
}
.form-control:focus {
    border-color: #9a9a9a;
    outline: 0;
    -webkit-box-shadow:none;
    box-shadow:none;
}
.styled-select #categories {
  background: transparent;
  width: 268px;
  padding: 5px 5px 5px 30px;
  font-size: 16px;
  line-height: 1;
  border: 0;
  border-radius: 0;
  height: 34px;
  -webkit-appearance: none;
}
.styled-select {
  width: 240px;
  height: 34px;
  overflow: hidden;
  background: url(http://cdn.bavotasan.com/wp-content/uploads/2011/05/down_arrow_select.jpg)  no-repeat left #ddd;
  border: 1px solid #ccc;
}
@media screen and (max-width: 1230px) {
	.eachBlockWrapper div:nth-child(4)
	{
		display:inline-block;
	}
	.eachBlockWrapperCont
	{
		width:31%;
	}
	.searchLocation, .category
	{
		padding-left:0.5%;
		padding-right:0.5%;
	}
	.searchLocation
	{
		padding-left:1%;
	}
	.searchBtnWrapper
	{
		padding-right:5px;
	}
	.footerCont
	{
		width:93%;
	}
}
</style>
</head>

<body>
<!-- loader -->
<div class="loading" style="display:none">
	<?php 
    	include('loader.php');
    ?>
</div>
<!-- end here -->

<!-- video -->
<div id="video_container">
	<img src="assets/img/btn_close_white.png" id="logo-close" onclick="close_video()" height="20" width="20" style="top:10px; right:10px;" />
    <!--<iframe src="https://www.youtube.com/embed/nkS_Ar0Yad0" frameborder="0" allowfullscreen id="video_wrapper"></iframe>-->
    <iframe width="420" height="315" src="" frameborder="0" allowfullscreen id="video_wrapper"></iframe>
</div>
<!-- end here -->

<!-- forgot password -->
<div id="forgot_password">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class" style="width:400px;"> <img src="assets/img/btn_close.png" id="logo-close" onclick="fp_close()" height="20" width="20" style="top:10px; right:10px;" />
      <div style="text-align:center; margin-top:35px; font-size:20px; font-weight:bold">Please provide email address</div>
      <div style="text-align:center; margin-top:20px; font-family: calibri;"><input type="text" style="" placeholder="Email" class="fp_text" /></div>
      <div style="margin-top:15px; width:250px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="forgot_password()">Ok</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="fp_close()">Cancel</span> </div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- signup prompt -->
<div id="signup-container">
  <div class="signup-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeSignup()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Join UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:25px; height:395px;">
      	<div style="height:55px;width:100%;">
        	<input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; float:left; outline:none" placeholder="First Name" class="txtFirstName" />
            <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; margin-left:20px; float:left; outline:none" placeholder="Last Name" class="txtLastName" />
        </div>
      	<input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="username" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Password (Atleast 4 characters)" class="password" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Confirm Password" class="confirm_password" />
        <div style="margin-bottom:10px; width:370px; text-align:left">
            <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15"/><span style="margin-top:-3px;">I agree to Ufundoo's term of use and privacy policy.</span>
        </div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUp()" />
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
      	<div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Already member ?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;"><input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Sign In"  onclick="openLogin()"/></div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- login prompt -->
<div id="login-container">
  <div class="login-container-wrapper">
  	<div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeLogin()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Login UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:15px; height:405px;">
      	<input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmail" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpassword" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:25px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:8px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signIn()"/>
        <div style="margin-bottom:10px; width:370px; height:15px;">
           <span style="">OR</span>
        </div>
        <a href="googleLogin1.php?status=user" style="text-decoration:none;color:white"><input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
        <span onclick="FBLogin('user');"><input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
      	<div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Not a member yet?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;"><input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join Now"  onclick="openSignup()" /></div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- signup prompt -->
<div id="promoter-container">
  <div class="promoter-container-wrapper">
  	<div class="admin-delete-class-promoter"><img src="assets/img/btn_close.png" id="logo-close" onclick="closePromoter()" height="20" width="20" />
    
    	<!-- login -->
    	<div style="float:left; width:50%; height:520px; border-right:2px solid #ccc; margin-top:25px; text-align:center;">
        	<div style="text-align:center;font-size:35px; font-weight:bold">Already a Promoter?</div>
            <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Login</div>
            <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmailPost" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpasswordPost" />
            <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:75px;" onclick="fp_open()">Forgot Password?</div>
            <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signInPost()"/>
            <!--<div style="margin-bottom:10px;height:15px;">
               <span style="">OR</span>
            </div>
            <a href="googleLogin1.php?status=promoter" style="text-decoration:none;color:white"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
            <span onclick="FBLogin('promoter');"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>-->
        </div>
        <!-- end here -->
        
        <!-- sign up -->
        <div style="float:left; width:50%; height:520px; margin-top:25px; text-align:center">
        	<div style="text-align:center;font-size:35px; font-weight:bold">Become a Promoter?</div>
            <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Sign Up</div>
            <div>
            	<input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Firstname" class="firstname" />
                <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none; margin-left:4%;" placeholder="Lastname" class="lastname" />
            </div>
            <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Email" class="username_post" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Password (Atleast 4 characters)" class="password_post" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:15px; outline:none" placeholder="Confirm Password" class="confirm_password_post" />
            <div style="margin-bottom:10px; width:75%; margin-left:auto; margin-right:auto">
                <img src="assets/img/unchecked.png" name="fainMail" onclick="check_single(this)" status="0" class="fainMail_post" style="cursor: pointer; float: left; margin-top:3px;" height="15" id="fainMail"/>Send me FainMail on everything events and entertainment.
            </div>
            <div style="margin-bottom:10px; width:70%; margin-left:64px; text-align:left">
                <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree_post" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15" id="agree"/>I agree to Ufundoo's term of use and privacy policy.
            </div>
            <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUpPost()" />
        </div>
        <!-- end here -->
        
    </div>
  </div>
</div>
<!-- end here -->

	<div class="wrapper">
			<!-- header -->
				<?php include ('include/header.php'); ?>
    		<!-- end header -->
            
            <div id="container_area" style="overflow:auto;">                
            <div style="height:auto; width:92%; margin-left:auto; margin-right:auto; min-height:100px; margin-top:30px;" id="event_container">
            	<div style="display:table; min-height:500px; width:100%; margin-bottom:25px; height:auto; ">
                	<div style="min-height:450px; height:auto" id="event_detail_left">
                    	<div style="min-height:160px; float:left; width:100%;">
                        	<?php
							
							 if($resultEvent['coverImage']==''){ ?>
                           
                          <div style="width:30%; min-height:160px; float:left;position:relative">
                            <img src="https://ufundoo.com/assets/img/no_image.png" class="img-responsive"  style="max-width: 200px;max-height: 200px;"/>
                            
                            
                            <?php } else { ?>
                           <!-- <img src="<?php echo $resultEvent['coverImage']; ?>" class="img-responsive"  style="max-width: 200px;max-height: 200px;float:left;"/>-->
                            
                        	<div style="width:30%; min-height:160px; float:left;position:relative">
                            	<img src="<?php echo $resultEvent['coverImage']; ?>" class="img-responsive"  style="max-width: 200px;max-height: 200px;"/>
                            <?php } ?>
                            	<?php if($resultEvent['ticketTypeId']=="2"){ ?>
                            	<div style="position: absolute;color: #fff; text-align: center; padding: 1px;background: rgba(255,255,255,0.5);border-radius: 12px; width: 55px; margin-top: 5px;margin-left: 5px; font-size:12px;top: 0;">FREE</div>
                                <?php } ?>
                            </div>
                            <div style="width:70%; min-height:160px; float:left;padding-left:2%; padding-right:2%;">
                            	<div style="float:left; min-height:100px; width:100%;border-bottom:2px solid #ededed">
                                	<div style="float:left; width:100%; height:auto; font-size:40px;font-family: leagueSpartan; text-transform:uppercase"><?php echo $resultEvent['eventName']; ?></div>
                                    <div style="float:left; width:100%; height:auto; font-size:20px;">By <?php echo $resultEvent['orgName']; ?></div>
                                    <div class="iconsWrapper" align="right" style="margin-top:0px; height:45px; width:100%;">
                                    	<?php /*if( $resultEvent['video']!=''){ ?>
                                    	<div style="width:40%; float:left;">
                                        	<div style="padding-top:10px; font-size:16px; text-align:left; margin-left:20px;cursor:pointer;" onclick="open_video('<?php echo $resultEvent['video']; ?>')"><img src="assets/img/video.png" height="30" style="margin-right:10px;" />View video</div>
                                        </div>
                                        <?php }*/ ?>
                                        <div style="width:50%; float:right;">
                                            <div align="right" class="eachIcon" style="display:inline-block">
                                                <a href="https://www.facebook.com/dialog/feed?app_id=705873652868650&redirect_uri=https://ufundoo.com&link=https://ufundoo.com&picture=<?php echo $_REQUEST['fbimage']; ?>&caption=<?php echo $resultEvent['eventName']; ?>&description= Date : <?php echo $resultEvent['date']?><center></center> Location : <?php echo $resultEvent['eventAddress'];?>" onclick="window.open(this.href); return false;"><img src="assets/img/mostp_fb.png"title="Facebook"/></a>
                                            </div>
                                            <div align="right" class="eachIcon" style="display:inline-block">
                                                <a href="http://twitter.com/share?text=Event Name - <?php echo $resultEvent['eventName']; ?>%0aDate : <?php echo $resultEvent['date']?>%0aLocation : <?php echo $resultEvent['eventAddress'];?>" onclick="window.open(this.href); return false;"><img src="assets/img/mostp_tweet.png" title="Twitter"/></a>
                                            </div>
                                            <div align="right" class="eachIcon" style="display:inline-block">
                                                <a href="http://pinterest.com/pin/create/button/?url=https://ufundoo.com&media=<?php echo $_REQUEST['fbimage']; ?>&description=Event Name : <?php echo $resultEvent['eventName']; ?>%0aDate : <?php echo $resultEvent['date']?>%0aLocation : <?php echo $resultEvent['eventAddress'];?>" onclick="window.open(this.href); return false;"><img src="assets/img/mostp_pin.png" title="Pin It"/></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="float:left; min-height:10px; width:100%;">
                                	<div style="float:left; width:100%; height:auto; font-size:15px; margin-top:3px;">
                                    	<div style="font-size:16px; width:33.5%; height:auto; float:left"><span style="font-weight:bold">Date</span>: <?php echo date_format($date_bt, 'M jS, Y'); ?></div>
                                        <div style="font-size:16px; width:23.5%; height:auto; float:left"><span style="font-weight:bold">Time</span>: <?php echo date_format($time_bt, 'G:i a'); ?></div>
                                        <div style="font-size:16px; width:43%; height:auto; float:left"><span style="font-weight:bold">Venue</span>: <?php echo $resultEvent['locationName']; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="float:left; width:100%; min-height:10px; height:auto; font-size:16px; line-height:22px;margin-top:10px;">
                           	<span style="font-weight:bold">Address</span>: <?php echo $resultEvent['eventAddress']; ?>
                        </div>
                        <?php if($resultEvent['description']!=''){ ?>
                        <div style="float:left; width:100%; min-height:10px; height:auto; font-size:16px; line-height:22px;margin-top:10px;">
                           	<span style="font-weight:bold">Description</span>: <?php echo $resultEvent['description']; ?>
                        </div>
                        <?php } ?>
						<div style="min-height:275px; float:left; width:98%;margin-top:30px;">
                        	
							
							
							
							<?php if($resultEvent['ticketTypeId']=="2" || $resultEvent['ticketTypeId']=="1" ) {   ?>
							<div style="width:100%; float:left; height:auto; min-height:60px; background:#fff">
                            	<table border="0" bordercolor="#0db3a5" cellpadding="10" cellspacing="0" width="100%" style="border:2px solid #2e2e2e; font-size:16px;">
                                    <tr style="background:#2e2e2e; color:#fff; height:30px" align="center">
                                        <td>Ticket Type</td>
                                        <td>Total No Of Ticket</td>
                                        <?php if($resultEvent['ticketTypeId']!="2"){ ?>
                                        <td>Price</td>
                                        <?php } ?>
                                    </tr>
                                    <?php 
								$queryTicket=mysqli_query($mysqli,"select ticket.name as ticketName,ticket.price as ticketPrice,ticket.totalQty as ticketQty from ticket where ticket.eventId='".$_REQUEST['eventId']."'");
							if(mysqli_num_rows($queryTicket)>0){
								while($resultTicket=mysqli_fetch_assoc($queryTicket)){ ?>
                                    <tr style="height:30px;" align="center">
                                        <td><?php echo $resultTicket['ticketName']; ?></td>
                                        <td><?php echo $resultTicket['ticketQty']; ?></td>
                                        <?php if($resultEvent['ticketTypeId']!="2"){ ?>
                                        <td><?php echo $resultTicket['ticketPrice']; ?></td>
                                        <?php } ?>
                                    </tr>
                                    <?php }} else { ?>
                                    <tr>
                                        <td colspan="3" align="center">No Data Available</td>
                                    </tr>
							<?php } ?>
                                </table>
                            </div>
							<?php  } ?>
							
							
							
							
                            <div style="width:100%; float:left; height:auto; height:100px; margin-top:-10px;">
                            	<!--<div style="float:left; width:100%; height:30px; font-size:16px;"><span style="font-weight:bold">RSVP</span> : I am going (Be the first one to go)</div>-->
                                <?php if($resultEvent['totalNumber']!=0){?>
                                
								<?php if($resultEvent['ticketTypeId']=="2"){ ?>
                                <div align="center" class="buyNow" style="width:150px; margin-bottom:15px;" onclick="window.open('seatChart.php?eventId=<?php echo $_REQUEST['eventId']; ?>&eventName=<?php echo urlencode($resultEvent['eventName']); ?>&eventDate=<?php echo date_format($date_bt, 'M jS, Y'); ?>&eventTime=<?php echo date_format($time_bt, 'G:i a'); ?>&locationName=<?php echo urlencode($resultEvent['locationName']); ?>&seatChart=<?php echo $resultEvent['seatChart']; ?>&ticketTypeId=<?php echo $resultEvent['ticketTypeId']; ?>','_self');">BOOK NOW</div>
                                <?php } else if($resultEvent['ticketTypeId']=="3"){ ?>
                                <div align="center" class="buyNow" style="width:150px; margin-bottom:15px;" onclick="window.open('seatChart.php?eventId=<?php echo $_REQUEST['eventId']; ?>&eventName=<?php echo urlencode($resultEvent['eventName']); ?>&eventDate=<?php echo date_format($date_bt, 'M jS, Y'); ?>&eventTime=<?php echo date_format($time_bt, 'G:i a'); ?>&locationName=<?php echo urlencode($resultEvent['locationName']); ?>&seatChart=<?php echo $resultEvent['seatChart']; ?>&ticketTypeId=<?php echo $resultEvent['ticketTypeId']; ?>','_self');">BUY TICKET</div>
                                <?php }}
								
								else { if($resultEvent['ticketTypeId']=="2" || $resultEvent['ticketTypeId']=="1" ){?>
                                <div align="center" class="buyNow" style="width:150px; margin-bottom:15px; cursor:default;">SOLD OUT</div>
                                <?php }} ?>
                                <div style="float:left; width:100%; height:30px; font-size:16px;"><span style="font-weight:bold">Event Organizers</span> : <?php echo $resultEvent['orgName']; ?></div>
                                <?php if($resultEvent['orgContact']!='') { ?>
                                <div style="float:left; width:100%; height:30px; font-size:16px;"><span style="font-weight:bold">Contact Organizers</span> : <?php echo $resultEvent['orgContact'];?></div>
                                <?php } else { ?>
                                <div style="float:left; width:100%; height:30px; font-size:16px;"><span style="font-weight:bold">Contact Organizers</span> : No information available</div>
                                <?php } ?>
                            </div>
							
                            <?php 
							
							
							
							
							
							//----------------------------------------------------------------------------------------------------------
							if($resultEvent['video']!='' && $ttypeId!=3){?> 
                            <div style="width:100%; float:left; height:auto;margin-top:15px;font-size:16px">
                            <span style="font-weight:bold;">Promotion/Preview Video</span> <br/>
                            <iframe width="420" height="315" src="<?php echo $resultEvent['video']; ?>" frameborder="0" allowfullscreen style="margin-top:5px;"></iframe>
                            </div>
                            <?php } ?>
							
							
							<?php 
							//----------------------------------------------------------------------------------------------------------
							if($resultEvent['video']!='' && $ttypeId==3){?> 
                            <div style="width:100%; float:left; height:auto;margin-top:-35px;font-size:16px">
                            <span style="font-weight:bold;">Promotional Event URL</span> 
                            <a href="<?php echo $resultEvent['video'];?>">Click here</a>
                            </div>
                            <?php } ?>
							
                        </div>
                    </div>
                    <div style="height:450px; background:#ccc;" id="map-canvas"></div>
                </div>
            </div>
            
            <!-- footer -->
            <?php include('include/footer.php'); ?>
            <!-- end footer -->
            
            <!--<input type="hidden" id="address" value="<?php //echo $resultEvent['locationName'];?>"> /-->
             <input type="hidden" id="address" value="<?php echo $resultEvent['eventAddress'];?>">
      </div>
</div>
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/ufundoo.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<script>
var windowHeight=$(window).height();
var mapAddress='<?php echo $resultEvent['locationName']; ?>';
$(document).ready(function(e) {
	if(mapAddress!='')
	{
		google.maps.event.addDomListener(window, 'load', initialize);
	}
	else
	{
		$('#map-canvas').css('background','#fff');
	}
	$('#dp').datepicker({
		format: 'dd-mm-yyyy'
	});
	listEvents=windowHeight-80;
    $('#container_area').css('height',listEvents+'px');
	$("#container_area").niceScroll({cursorcolor: "rgba(237, 37, 143, 0.5)",cursorwidth:"6px",cursorborderradius:"10px" ,cursorborder: "0px", railpadding: {right: 2}, zindex: 0});
	totalViews(<?php echo $_REQUEST['eventId']; ?>);
	
});

/* load map */
var geocoder;
var map;
function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-34.397, 150.644);
  var mapOptions = {
    zoom: 10,
    center: latlng
  }
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  var address = document.getElementById('address').value;
  	geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
    } else {
      //alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}

/* facebook */
window.fbAsyncInit = function() {
		FB.init({
		appId      : '705873652868650', // replace your app id here
		channelUrl : '', 
		status     : true, 
		cookie     : true, 
		xfbml      : true  
		});
	};
	(function(d){
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	}(document));
	
	function FBLogin(type){
		FB.login(function(response){
			if(response.authResponse){
				window.location.href = "actions.php?status="+type;
			}
		}, {scope: 'email,user_likes'});
	}
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	if($("#signup-container").css('display')=="block")
	{
		signUp();
	}
	if($("#login-container").css('display')=="block")
	{
		signIn();
	}
}});
</script>
</body>
</html>