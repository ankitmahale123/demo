<?php
	session_start();
	require_once("config/conn.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UFundoo</title>
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/ufundoo.js"></script>
<style>
.headerBtn {
	color: #2e302d;
}
.description {
	font-size:18px;
	padding: 0 15px;
}
</style>
<script>
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	if($("#signup-container").css('display')=="block")
	{
		signUp();
	}
	else if($("#login-container").css('display')=="block")
	{
		signIn();
	}
	else
	{
		feedback();
	}
}});
</script>
</head>

<body>
<!-- loader -->
<div class="loading" style="display:none">
  <?php 
    	include('loader.php');
    ?>
</div>
<!-- end here --> 

<!-- forgot password -->
<div id="forgot_password">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class" style="width:400px;"> <img src="assets/img/btn_close.png" id="logo-close" onclick="fp_close()" height="20" width="20" style="top:10px; right:10px;" />
      <div style="text-align:center; margin-top:35px; font-size:20px; font-weight:bold">Please provide email address</div>
      <div style="text-align:center; margin-top:20px; font-family: calibri;">
        <input type="text" style="" placeholder="Email" class="fp_text" />
      </div>
      <div style="margin-top:15px; width:250px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="forgot_password()">Ok</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="fp_close()">Cancel</span> </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- signup prompt -->
<div id="signup-container">
  <div class="signup-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeSignup()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Join UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:25px; height:395px;">
        <div style="height:55px;width:100%;">
          <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; float:left; outline:none" placeholder="First Name" class="txtFirstName" />
          <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; margin-left:20px; float:left; outline:none" placeholder="Last Name" class="txtLastName" />
        </div>
        <input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="username" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Password (Atleast 4 characters)" class="password" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Confirm Password" class="confirm_password" />
        <div style="margin-bottom:10px; width:370px; text-align:left"> <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15"/><span style="margin-top:-3px;">I agree to Ufundoo's term of use and privacy policy.</span> </div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUp()" />
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
        <div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Already member ?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;">
          <input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Sign In"  onclick="openLogin()"/>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- login prompt -->
<div id="login-container">
  <div class="login-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeLogin()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Login UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:15px; height:405px;">
        <input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmail" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpassword" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:25px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:8px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signIn()"/>
        <div style="margin-bottom:10px; width:370px; height:15px;"> <span style="">OR</span> </div>
        <a href="googleLogin1.php?status=user" style="text-decoration:none;color:white">
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" />
        </a> <span onclick="FBLogin('user');">
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" />
        </span> </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
        <div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Not a member yet?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;">
          <input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join Now"  onclick="openSignup()" />
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- signup prompt -->
<div id="promoter-container">
  <div class="promoter-container-wrapper">
    <div class="admin-delete-class-promoter"><img src="assets/img/btn_close.png" id="logo-close" onclick="closePromoter()" height="20" width="20" /> 
      
      <!-- login -->
      <div style="float:left; width:50%; height:520px; border-right:2px solid #ccc; margin-top:25px; text-align:center;">
        <div style="text-align:center;font-size:35px; font-weight:bold">Already a Promoter?</div>
        <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Login</div>
        <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmailPost" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpasswordPost" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:75px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signInPost()"/>
        <!--<div style="margin-bottom:10px;height:15px;">
               <span style="">OR</span>
            </div>
            <a href="googleLogin1.php?status=promoter" style="text-decoration:none;color:white"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
            <span onclick="FBLogin('promoter');"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>--> 
      </div>
      <!-- end here --> 
      
      <!-- sign up -->
      <div style="float:left; width:50%; height:520px; margin-top:25px; text-align:center">
        <div style="text-align:center;font-size:35px; font-weight:bold">Become a Promoter?</div>
        <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Sign Up</div>
        <div>
          <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Firstname" class="firstname" />
          <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none; margin-left:4%;" placeholder="Lastname" class="lastname" />
        </div>
        <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Email" class="username_post" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Password (Atleast 4 characters)" class="password_post" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:15px; outline:none" placeholder="Confirm Password" class="confirm_password_post" />
        <div style="margin-bottom:10px; width:75%; margin-left:auto; margin-right:auto"> <img src="assets/img/unchecked.png" name="fainMail" onclick="check_single(this)" status="0" class="fainMail_post" style="cursor: pointer; float: left; margin-top:3px;" height="15" id="fainMail"/>Send me FainMail on everything events and entertainment. </div>
        <div style="margin-bottom:10px; width:70%; margin-left:64px; text-align:left"> <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree_post" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15" id="agree"/>I agree to Ufundoo's term of use and privacy policy. </div>
        <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUpPost()" />
      </div>
      <!-- end here --> 
      
    </div>
  </div>
</div>
<!-- end here --> 

<!-- header -->
<div class="header">
  <?php include('include/header.php'); ?>
</div>
<!-- end here --> 

<!-- wrapper -->
<div class="wrapper"> 
  
  <!-- container -->
  <div class="content"> 
    
    <!-- main content -->
    <div style="display:table; min-height:100px; height:auto; width:1024px; margin-top:30px; margin-left:auto; margin-right:auto; margin-bottom:30px;">
      <div style="display:table-row">
        <div style="display:table-cell; width:550px; height:auto; padding-left:25px;"> 
          <h3>Fulfillment Policy</h3>
          <div class="description">
            <p>Event Manager and Event/Movie Ticket Policy: 
              The following are the terms and conditions under which Ufundoo is providing the ticketing service and under which this website is to be used.</p>
            <p>Introduction and Definitions: Ufundoo Event Manager is the on-line service operated by uFundoo , INC., a CA corporation with its principal place of business in Fremont (the "Company"), on the World Wide Web of the Internet, consisting of ticketing services, ticketing resources, and related content provided by the Company, and by third parties. "Subscriber" means every person who establishes a connection for access to and use of Ufundoo. </p>
            <p>Agreement with Terms and Conditions: This Agreement sets forth the terms and conditions that apply to use of Ufundoo by Subscriber. By using Ufundoo, Subscriber agrees to comply with all of the terms and conditions hereof.</p>
            <p>Changes in Terms and Conditions: The Company has the right to change or discontinue any aspect or feature of Ufundoo, including, but not limited to, content, hours of availability, and equipment needed for access or use, at any time. The Company has the right to change or modify the terms and conditions applicable to Subscriber's use of Ufundoo, or any part thereof, or to impose new conditions, including, but not limited to, adding fees and charges for use at any time. Such changes, modifications, additions or deletions shall be effective immediately upon notice thereof, which may be given by means including, but not limited to, posting on Ufundoo, or by electronic or conventional mail, or by any other means. Any use of Ufundoo by Subscriber subsequent to such notice shall be deemed to constitute acceptance by Subscriber of such changes, modifications or additions. </p>
            <p>Ticketing : Ufundoo sells tickets on behalf of promoters, teams, bands and venues which means Ufundoo does not set the ticket prices or determine seating locations. All ticket sales are final and no exchanges or refunds are allowed after a purchase has been made. Where tickets are mailed to the Subscriber, neither the client nor Ufundoo will replace lost, stolen, damaged or destroyed tickets. When you receive your tickets, please keep them in a safe place. Please note that direct sunlight or heat can sometimes damage tickets. Where "Ticket Delivery" feature is available and chosen, please allow enough time for the delivery of your ticket order. If you have selected delivery by mail, please allow at least fourteen business days after your order is placed to receive your tickets. For express delivery allow at least 2 business days. These are approximate times, and Ufundoo is not responsible for deliveries that take longer. Will call orders can be processed up to the day of the event. Where "Will Call" feature is available and chosen, the Subscriber must show proof of identification and a printed copy of the electronic receipt to receive the actual ticket. Occasionally, events are cancelled, delayed or postponed by the promoter, team, performer or venue for a variety of reasons. If the event is cancelled, delayed or postponed for any reason please contact the organizer for information on receiving a refund. If the event was moved or rescheduled, the venue or promoter may set refund limitations. Ufundoo assumes no responsibility for such refunds and processes refunds only as instructed by the event organizer. Ufundoo will send a confirmation of the order for tickets via email to the subscriber upon successful processing of the order. It is the responsibility of the Subscriber to ensure that the email address provided is correct and accurate.</p>
            <p>There are absolutely No Refunds or Exchanges or Transfers for Event and Movie ticket purchases. You purchase event and movie tickets at your own discretion. If you encounter any problems with your ticket purchase, you must resolve the matter directly with the event or movie theater management. If the event or movie theater management instructs us to refund or exchange your ticket purchase, only then can we process such a request.</p>
            <p>For Print Out Tickets: If you are purchasing more than 1 ticket, you must be present at the box office along with your guests for entry. Your guests will not be allowed entry if you are not physically present with them to present the order receipt and your ID.</p>
            <p>ALL TICKETS ARE NON-REFUNDABLE AND NON-TRANSFERABLE. THE PERSON WHOSE NAME THE TICKET IS ISSUED IN, MUST ALSO BE PRESENT AT THE DOOR ALONG WITH A VALID PHOTO ID. PLEASE NOTE THAT YOU WILL HAVE TO TAKE A PRINT OUT OF THE ORDER RECEIPT AND SHOW IT AT THE VENUE. ANY FAILURE TO DO THIS, MAY RESULT IN THE DENIAL OF YOUR ADMISSION, WHICH WE ARE NOT RESPONSIBLE FOR.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- end here --> 
  </div>
  <!-- end here --> 
  
  <!-- footer -->
  <div class="footer">
    <?php include('include/footer.php'); ?>
  </div>
  <!-- end here --> 
</div>
<!-- end here -->
</body>
</html>