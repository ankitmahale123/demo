<?php
	session_start();
	require_once("config/conn.php"); 
	$date=date('Y-m-d');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Ufundoo | Thank You</title>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="https://ufundoo.com" />
<meta name="twitter:title" content="Small Island Developing States Photo Submission" />
<meta name="twitter:description" content="View the album on Flickr." />
<meta name="twitter:image" content="https://ufundoo.com/img.png" />
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<link rel="stylesheet" href="assets/css/datepicker.css" type="text/css" />

<style>
html,body
{
	overflow:hidden;
}
.headerBtnActive
{
	border: 1px solid #2e302d;
	color:#2e302d;
}
.headerBtn
{
	color:#2e302d;
}
</style>
</head>

<body>
<!-- loader -->
<div class="loading" style="display:none">
	<?php 
    	include('loader.php');
    ?>
</div>
<!-- end here -->

	<div class="wrapper">
<?php $url = 'https://www.youtube.com/watch?v=nLUP4RfSbZI';
preg_match(
        '/[\\?\\&]v=([^\\?\\&]+)/',
        $url,
        $matches
    );
$id = $matches[1];
  echo 'http://www.youtube.com/v/' . $id . '&amp;hl=en_US&amp;fs=1?rel=0' ;
$width = '640';
$height = '385';
echo '<object width="' . $width . '" height="' . $height . '"><param name="movie" value="https://www.youtube.com/v/' . $id . '&amp;hl=en_US&amp;fs=1?rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/' . $id . '&amp;hl=en_US&amp;fs=1?rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="' . $width . '" height="' . $height . '"></embed></object>';
?>

      </div>
</div>
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/ufundoo.js"></script>

</body>
</html>