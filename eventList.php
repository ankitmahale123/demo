<?php
	session_start();
	require_once("config/conn.php"); 
	$date=date('Y-m-d');
	
	/* search button hit from index page */
	if($_REQUEST['type']=='searchBtn')
	{
		/* text empty and date non-empty */
		if($_REQUEST['search_location']=='' && $_REQUEST['search_date']!='')
		{
			$queryString="(event.date LIKE '%".$_REQUEST['search_date']."%')";
		}
		
		/* text non-empty and date empty */
		if($_REQUEST['search_date']=='' && $_REQUEST['search_location']!='')
		{
			$queryString="(event.name LIKE '%".$_REQUEST['search_location']."%' OR location.name LIKE '%".$_REQUEST['search_location']."%' OR eventType.name LIKE '%".$_REQUEST['search_location']."%')";
		}
		
		/* text date both non-empty */
		if($_REQUEST['search_location']!='' && $_REQUEST['search_date']!='')
		{
			$queryString="(event.name LIKE '%".$_REQUEST['search_location']."%' OR location.name LIKE '%".$_REQUEST['search_location']."%' OR eventType.name LIKE '%".$_REQUEST['search_location']."%') AND event.date LIKE '%".$_REQUEST['search_date']."%'";
		}
		
		
		/* query string append */
		$queryEvent=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.coverImage,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,organization.name as organizationName,location.name as locationName, ticketType.id as ticketTypeId from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where ".$queryString." AND (event.status='approve') AND event.date>='".$date."' ORDER BY id DESC");
		
	}
	
	/* search button hit from see more */
	if($_REQUEST['type']=='seemore')
	{
		
		$queryEvent=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.coverImage,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,organization.name as organizationName,location.name as locationName, ticketType.id as ticketTypeId from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.status='approve' AND event.date>='".$date."' ORDER BY event.date ASC");
	}
	
	/* search button hit from event type block */
	if($_REQUEST['type']=='event')
	{
		if($_REQUEST['event_name']!="all")
		{
			$queryEvent=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.coverImage,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,organization.name as organizationName,location.name as locationName, ticketType.id as ticketTypeId from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.status='approve' AND eventType.name='".$_REQUEST['event_name']."' AND event.date>='".$date."' ORDER BY id DESC");
		}
		else
		{
			$queryEvent=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.coverImage,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,organization.name as organizationName,location.name as locationName, ticketType.id as ticketTypeId from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.status='approve' AND event.date>='".$date."' ORDER BY id DESC");
		}
	}
	
	/* event type list */
	$eventType=mysqli_query($mysqli,"select * from eventType");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Ufundoo | Event List</title>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="https://ufundoo.com" />
<meta name="twitter:title" content="Small Island Developing States Photo Submission" />
<meta name="twitter:description" content="View the album on Flickr." />
<meta name="twitter:image" content="https://ufundoo.com/img.png" />
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<link rel="stylesheet" href="assets/css/datepicker.css" type="text/css" />

<style>
html,body
{
	background: url(assets/img/mostp_bk.png) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
	overflow:hidden;
}
.headerBtnActive
{
	border: 1px solid #2e302d;
	color:#2e302d;
}
.headerBtn
{
	color:#2e302d;
}
.searchBar
{
	height:80px;
	background:none;
}
.searchBarContainer
{
	height:80px;
	border-radius:80px;
	width:95%;
}
.searchBarWrapper {
    height: 60px;
    margin-top: 3px;
}
.searchLocation,.category
{
	padding-left:0.7%;
	width: 20%;
	padding-right:1%;
	display: inline-block;
}
.searchBox
{
	font-size:17px;
	padding-left:20px;
}
.searchBtn
{
	padding-top:10px;
	margin-top:5px;
}
.eachBlockWrapper
{
	margin-top:0px;
	height:auto;
}
.eachBlockWrapperCont
{
	margin-bottom:2%;
	margin-left:1%;
	margin-right:1%;
	width:22.7%;
	position: relative;
}
.mostPopEventsLine
{
	margin-bottom:25px;
}
.form-control:focus {
    border-color: #9a9a9a;
    outline: 0;
    -webkit-box-shadow:none;
    box-shadow:none;
}
.styled-select #categories {
  background: transparent;
  width: 268px;
  padding: 5px 5px 5px 30px;
  font-size: 16px;
  line-height: 1;
  border: 0;
  border-radius: 0;
  height: 34px;
  -webkit-appearance: none;
}
.styled-select {
  width: 240px;
  height: 34px;
  overflow: hidden;
  background: url(http://cdn.bavotasan.com/wp-content/uploads/2011/05/down_arrow_select.jpg)  no-repeat left #ddd;
  border: 1px solid #ccc;
}
@media screen and (max-width: 1230px) {
	.eachBlockWrapper div:nth-child(4)
	{
		display:inline-block;
	}
	.eachBlockWrapperCont
	{
		width:31%;
	}
	.searchLocation, .category
	{
		padding-left:0.5%;
		padding-right:0.5%;
	}
	.searchLocation
	{
		padding-left:1%;
	}
	.searchBtnWrapper
	{
		padding-right:5px;
	}
	.footerCont
	{
		width:93%;
	}
}
</style>
</head>

<body>
<!-- loader -->
<div class="loading" style="display:none">
	<?php 
    	include('loader.php');
    ?>
</div>
<!-- end here -->

<!-- forgot password -->
<div id="forgot_password">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class" style="width:400px;"> <img src="assets/img/btn_close.png" id="logo-close" onclick="fp_close()" height="20" width="20" style="top:10px; right:10px;" />
      <div style="text-align:center; margin-top:35px; font-size:20px; font-weight:bold">Please provide email address</div>
      <div style="text-align:center; margin-top:20px; font-family: calibri;"><input type="text" style="" placeholder="Email" class="fp_text" /></div>
      <div style="margin-top:15px; width:250px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="forgot_password()">Ok</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="fp_close()">Cancel</span> </div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- signup prompt -->
<div id="signup-container">
  <div class="signup-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeSignup()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Join UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:25px; height:395px;">
      	<div style="height:55px;width:100%;">
        	<input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; float:left; outline:none" placeholder="First Name" class="txtFirstName" />
            <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; margin-left:20px; float:left; outline:none" placeholder="Last Name" class="txtLastName" />
        </div>
      	<input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="username" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Password (Atleast 4 characters)" class="password" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Confirm Password" class="confirm_password" />
        <div style="margin-bottom:10px; width:370px; text-align:left">
            <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15"/><span style="margin-top:-3px;">I agree to Ufundoo's term of use and privacy policy.</span>
        </div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUp()" />
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
      	<div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Already member ?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;"><input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Sign In"  onclick="openLogin()"/></div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- login prompt -->
<div id="login-container">
  <div class="login-container-wrapper">
  	<div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeLogin()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Login UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:15px; height:405px;">
      	<input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmail" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpassword" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:25px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:8px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signIn()"/>
        <div style="margin-bottom:10px; width:370px; height:15px;">
           <span style="">OR</span>
        </div>
        <a href="googleLogin1.php?status=user" style="text-decoration:none;color:white"><input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
        <span onclick="FBLogin('user');"><input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
      	<div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Not a member yet?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;"><input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join Now"  onclick="openSignup()" /></div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- signup prompt -->
<div id="promoter-container">
  <div class="promoter-container-wrapper">
  	<div class="admin-delete-class-promoter"><img src="assets/img/btn_close.png" id="logo-close" onclick="closePromoter()" height="20" width="20" />
    
    	<!-- login -->
    	<div style="float:left; width:50%; height:520px; border-right:2px solid #ccc; margin-top:25px; text-align:center;">
        	<div style="text-align:center;font-size:35px; font-weight:bold">Already a Promoter?</div>
            <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Login</div>
            <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmailPost" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpasswordPost" />
            <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:75px;" onclick="fp_open()">Forgot Password?</div>
            <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signInPost()"/>
            <!--<div style="margin-bottom:10px;height:15px;">
               <span style="">OR</span>
            </div>
            <a href="googleLogin1.php?status=promoter" style="text-decoration:none;color:white"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
            <span onclick="FBLogin('promoter');"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>-->
        </div>
        <!-- end here -->
        
        <!-- sign up -->
        <div style="float:left; width:50%; height:520px; margin-top:25px; text-align:center">
        	<div style="text-align:center;font-size:35px; font-weight:bold">Become a Promoter?</div>
            <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Sign Up</div>
            <div>
            	<input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Firstname" class="firstname" />
                <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none; margin-left:4%;" placeholder="Lastname" class="lastname" />
            </div>
            <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Email" class="username_post" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Password (Atleast 4 characters)" class="password_post" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:15px; outline:none" placeholder="Confirm Password" class="confirm_password_post" />
            <div style="margin-bottom:10px; width:75%; margin-left:auto; margin-right:auto">
                <img src="assets/img/unchecked.png" name="fainMail" onclick="check_single(this)" status="0" class="fainMail_post" style="cursor: pointer; float: left; margin-top:3px;" height="15" id="fainMail"/>Send me FainMail on everything events and entertainment.
            </div>
            <div style="margin-bottom:10px; width:70%; margin-left:64px; text-align:left">
                <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree_post" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15" id="agree"/>I agree to Ufundoo's term of use and privacy policy.
            </div>
            <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUpPost()" />
        </div>
        <!-- end here -->
        
    </div>
  </div>
</div>
<!-- end here -->

	<div class="wrapper">
			<!-- header -->
            <?php include('include/header.php'); ?>
    		<!-- end header -->
            
            <div id="container_area" style="overflow:auto">
			<!-- search bar -->
            <div class="searchBar">
            	<div class="searchBarContainer">
                	<div class="searchBarWrapper">
                    	<div class="inheritHeight searchLocation">
                        	<input type="text" class="searchBox inheritHeight" placeholder="Search for event or location" id="eventname_location" />
                        </div>
                        <div class="inheritHeight" style="width: 20%;display: inline-block;">
                          	<input type="text" class="searchBox inheritHeight" id="dp"  placeholder="All dates"/>
                        </div>
                        <div class="inheritHeight category">
                            <div class="form-group">
                              <select class="form-control searchBox inheritHeight" style="height:60px;" id="event_type">
                              	<option id="0">Category - Any</option>
                              	<?php while($eventTypeResult=mysqli_fetch_assoc($eventType)){ ?>
                                <option id="<?php echo $eventTypeResult['id'];?>"><?php echo $eventTypeResult['name'];?></option>
                                <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="inheritHeight" style="width: 20%; display: inline-block;">
                          	<div class="form-group">
                              <select class="form-control searchBox inheritHeight" style="height:60px;" id="price">
                              	<option id="0">Price - Any</option>
                                <option id="1">Paid</option>
                                <option id="2">Free</option>
                              </select>
                            </div>
                        </div>
                        <div class="inheritHeight searchBtnWrapper">
                        	<div class="searchBtn inheritHeight" onclick="searchByAll()">Search</div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end search bar -->    
            <div style="height:auto; width:95%; margin-left:auto; margin-right:auto" id="event_container">
            	<?php if($_REQUEST['type']=='seemore'){ ?>
                <div align="center" class="mostPopEvents">MOST POPULAR EVENTS</div>
                <?php } else if($_REQUEST['type']=='searchBtn') {?>
                <div align="center" class="mostPopEvents">EVENTS</div>
                <?php } else if($_REQUEST['type']=='event') {?>
                <div align="center" class="mostPopEvents"><?php echo strtoupper($_REQUEST['event_name']);?></div>
                <?php } else { ?>
                <?php } ?>
                <div align="center" class="mostPopEventsLine"></div>
                <div style="width:100%; min-height:10px;">
                	<div class="eachBlockWrapper" style="width:100%;">
                	<!-- each block -->
                    <?php 
						if(mysqli_num_rows($queryEvent)>0){ 
						while($result=mysqli_fetch_assoc($queryEvent)){
							
						$time_bt = date_create($result['startTime']);	
						$date_bt = date_create($result['eventDateFb']);	
					?>
                	<div class="inheritHeight eachBlockWrapperCont">
                    	 <?php if($result['ticketTypeId']=="2"){ ?>
                        <div style="position: absolute;color: #fff; text-align: center; padding: 1px;background: rgba(255,255,255,0.5);border-radius: 12px; width: 55px; margin-top: 10px;margin-left: 10px; font-size:12px;">FREE</div>
                        <?php } ?>
                    	<div class="eachBlock inheritHeight">
                        	<?php
							//$queryImage=mysqli_query($mysqli,"select * from eventImage where eventImage.eventId='".$result['id']."' ORDER BY id LIMIT 1");
							//if(mysqli_num_rows($queryImage)>0){
							//$resultImage=mysqli_fetch_assoc($queryImage);
								//if($resultImage['path']!='')
								if($result['coverImage']!='')
								{
									$fbimage=$result['coverImage'];
									$cover=$result['coverImage'];
								}
								else
								{
									$fbimage='https://ufundoo.com/assets/img/no_image.png';
									$cover='https://ufundoo.com/assets/img/no_image.png';
								}
							?>
                           	<img src="<?php echo $cover; ?>" height="247" style="width:100%;background: #E1E1E1;cursor:pointer;border: 1px solid #e1e1e1;" onclick="window.open('eventDetail.php?eventId=<?php echo $result['id'];?>&fbimage=<?php echo $fbimage; ?>','_self');" />
                            <?php //} else { ?>
                            <!--<img src="https://ufundoo.com/assets/img/no_image.png" height="247" style="width:100%;background: #E1E1E1;cursor:pointer;border: 1px solid #e1e1e1;" onclick="window.open('eventDetail.php?eventId=<?php echo $result['id'];?>&fbimage=<?php echo $fbimage; ?>','_self');" />-->
                            <?php //} ?>
                            <div class="eventDataWrapper">
                            	<div style="display:table-row">
                                	<div class="eventData">
                                        	<div style="display:inline-block; width:60%; height:65px; vertical-align:top;">
                                            	<div style="font-weight:600;cursor:pointer; height:65px;" class="eventDataEach" onclick="window.open('eventDetail.php?eventId=<?php echo $result['id'];?>&fbimage=<?php echo $fbimage; ?>','_self');" title="<?php echo $result['eventName']; ?>" ><?php echo $result['eventName']; ?></div>
                                            </div>
                                            <div style="display:inline-block; width:39%; height:65px;vertical-align:top;">
                                            	<div class="bookNowWrapper">
														<?php if($result['ticketTypeId']=="2"){ ?>
                                                        <div align="center" class="buyNow" onclick="window.open('seatChart.php?eventId=<?php echo $result['id']; ?>&eventName=<?php echo urlencode($result['eventName']); ?>&eventDate=<?php echo date_format($date_bt, 'M jS, Y'); ?>&eventTime=<?php echo date_format($time_bt, 'G:i a'); ?>&locationName=<?php echo urlencode($result['locationName']); ?>&seatChart=<?php echo $result['seatChart']; ?>&ticketTypeId=<?php echo $result['ticketTypeId']; ?>','_self');">BOOK NOW</div>
                                                        <?php } else { ?>
                                                        <div align="center" class="buyNow" onclick="window.open('seatChart.php?eventId=<?php echo $result['id']; ?>&eventName=<?php echo urlencode($result['eventName']); ?>&eventDate=<?php echo date_format($date_bt, 'M jS, Y'); ?>&eventTime=<?php echo date_format($time_bt, 'G:i a'); ?>&locationName=<?php echo urlencode($result['locationName']); ?>&seatChart=<?php echo $result['seatChart']; ?>&ticketTypeId=<?php echo $result['ticketTypeId']; ?>','_self');">BUY TICKET</div>
                                                    <?php } ?>
                                            	</div>
                                        </div>
                                        <div style="margin-top:7px; display:inline-block; width:100%;" class="eventDataEach"><?php echo date_format($date_bt, 'M jS,y').' '.date_format($time_bt, 'G:i a'); ?></div>
                                        <div style="margin-top:5px; display:inline-block; width:100%; height:42px;" class="eventDataEach"><?php echo $result['locationName'];?></div>
                                    </div>
                                </div>
                                <div style="display:table-row">
                                	<div class="iconsWrapper" align="right">
                                    	<div align="right" class="eachIcon">
                                        	<a href="https://www.facebook.com/dialog/feed?app_id=705873652868650&redirect_uri=https://ufundoo.com&link=https://ufundoo.com&picture=<?php echo $fbimage; ?>&caption=<?php echo $result['eventName']; ?>&description= Date : <?php echo $result['eventDateFb']?><center></center> Location : <?php echo $result['locationName'];?>" onclick="window.open(this.href); return false;">
                                                <img src="assets/img/mostp_fb.png" title="Facebook"/>
                                            </a>
                                        </div>
                                        <div align="right" class="eachIcon">
                                        	<a href="http://twitter.com/share?text=Ufundoo - <?php echo $result['eventName']; ?>%0aDate : <?php echo $result['eventDateFb']?>%0aLocation : <?php echo $result['locationName'];?>" onclick="window.open(this.href); return false;">
                                            	<img src="assets/img/mostp_tweet.png" title="Twitter" />
                                            </a>
                                        </div>
                                        <div align="right" class="eachIcon">
                                        	<a href="http://pinterest.com/pin/create/button/?url=https://ufundoo.com&media=<?php echo $fbimage; ?>&description=Event Name : <?php echo $result['eventName']; ?>%0aDate : <?php echo $result['eventDateFb']?>%0aLocation : <?php echo $result['locationName'];?>" onclick="window.open(this.href); return false;">
                                            	<img src="assets/img/mostp_pin.png" title="Pin It" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php }} else { ?>
                    <div align="center" style="margin-top:30px; color:#000; font-size:25px; margin-bottom:30px;">Oops... No listing available for this category right now.</div>
                    <?php } ?>
                    <!-- end each block -->
                </div>
            </div>
         </div>
         
         <!-- footer -->
         <?php include('include/footer.php'); ?>   
         <!-- end footer -->
      </div>
</div>
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/ufundoo.js"></script>
<script>
var windowHeight=$(window).height();
$(document).ready(function(e) {
	$('#dp').datepicker({
		format: 'yyyy-mm-dd'
	});
	listEvents=windowHeight-80;
    $('#container_area').css('height',listEvents+'px');
	$("#container_area").niceScroll({cursorcolor: "rgba(237, 37, 143, 0.5)",cursorwidth:"6px",cursorborderradius:"10px" ,cursorborder: "0px", railpadding: {right: 2}, zindex: 0});
	
});
/* facebook */
window.fbAsyncInit = function() {
		FB.init({
		appId      : '705873652868650', // replace your app id here
		channelUrl : '', 
		status     : true, 
		cookie     : true, 
		xfbml      : true  
		});
	};
	(function(d){
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	}(document));
	
	function FBLogin(type){
		FB.login(function(response){
			if(response.authResponse){
				window.location.href = "actions.php?status="+type;
			}
		}, {scope: 'email,user_likes'});
	}
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	if($("#signup-container").css('display')=="block")
	{
		signUp();
	}
	if($("#login-container").css('display')=="block")
	{
		signIn();
	}
}});
</script>
</body>
</html>