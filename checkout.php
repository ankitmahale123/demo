<?php
	session_start();
	require_once("config/conn.php");
	if(!isset($_SESSION["userId"]) || !isset($_SESSION["promoterId"]))
	{
		header('Location : index.php');
	}
	$queryEvent=mysqli_query($mysqli,"select eventType.name as eventTypeName,event.id,event.seatChart,event.date AS eventDateFb,DAYNAME(event.date) as eventDay,MONTHNAME(event.date) as eventMonth,DATE_FORMAT(event.date, '%d') as eventDate,event.startDateTime as startTime,event.name as eventName,organization.name as organizationName,location.name as locationName,ticketType.id as ticketTypeId from event inner join organization on organization.id=event.organizationId inner join eventType on eventType.id=event.eventTypeId inner join location on location.id=event.locationId inner join ticketType on ticketType.id=event.ticketTypeId where event.id='".$_REQUEST['eventId']."'");
	$result=mysqli_fetch_assoc($queryEvent);
	$time_bt = date_create($result['startTime']);	
	$date_bt = date_create($result['eventDateFb']);	
	$ticket=json_decode($_REQUEST['ticket_detail']);
	
	/* for hidden type checkout values */
	$ticket_hidden=json_decode($_REQUEST['ticket_detail']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Ufundoo | Checkout</title>
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<link rel="stylesheet" href="assets/css/datepicker.css" type="text/css" />
<style>
html,body
{
	background: #fff;
	overflow:hidden;
}
.headerBtnActive
{
	border: 1px solid #2e302d;
	color:#2e302d;
}
.headerBtn
{
	color:#2e302d;
}
.searchBar
{
	height:80px;
	background:none;
	margin-bottom:25px;
}
.searchBarContainer
{
	height:80px;
	border-radius:80px;
	width:95%;
}
.searchBarWrapper {
    height: 60px;
    margin-top: 3px;
}
.searchLocation,.category
{
	padding-left:0.7%;
	width: 20%;
	padding-right:1%;
	display: inline-block;
}
.searchBox
{
	font-size:17px;
	padding-left:20px;
}
.searchBtn
{
	padding-top:10px;
	margin-top:5px;
}
.eachBlockWrapper
{
	margin-top:0px;
	height:auto;
}
.eachBlockWrapperCont
{
	margin-bottom:2%;
	margin-left:1%;
	margin-right:1%;
	width:22.7%;
	position: relative;
}
.mostPopEventsLine
{
	margin-bottom:25px;
}
.form-control:focus {
    border-color: #9a9a9a;
    outline: 0;
    -webkit-box-shadow:none;
    box-shadow:none;
}
.styled-select #categories {
  background: transparent;
  width: 268px;
  padding: 5px 5px 5px 30px;
  font-size: 16px;
  line-height: 1;
  border: 0;
  border-radius: 0;
  height: 34px;
  -webkit-appearance: none;
}
.styled-select {
  width: 240px;
  height: 34px;
  overflow: hidden;
  background: url(http://cdn.bavotasan.com/wp-content/uploads/2011/05/down_arrow_select.jpg)  no-repeat left #ddd;
  border: 1px solid #ccc;
}
@media screen and (max-width: 1230px) {
	.eachBlockWrapper div:nth-child(4)
	{
		display:inline-block;
	}
	.eachBlockWrapperCont
	{
		width:31%;
	}
	.searchLocation, .category
	{
		padding-left:0.5%;
		padding-right:0.5%;
	}
	.searchLocation
	{
		padding-left:1%;
	}
	.searchBtnWrapper
	{
		padding-right:5px;
	}
	.footerCont
	{
		width:93%;
	}
}
.proceed_to_payment_enable{height: 40px; width: 240px;border-radius:20px; background:#ed258f;cursor:pointer;color: #fff; text-align:center;padding-top:11px;font-size:14px; font-family:lator;margin-top:20px; margin-left:auto; margin-right:auto;}
.stripe-button-el{background:#ed258f;cursor:pointer;color: #fff; text-align:center;padding-top:11px;font-size:14px; font-family:lator;margin-top:20px; margin-left:auto; margin-right:auto;border: 1px solid #ed258f;padding-top: 0px;font-size: 18px;height: 45px; width: 300px; border-radius: 45px !important;background-image:none !important;visibility:hidden!important; display:none !important;}
.stripe-button-el span{background-image:none !important;background:#ed258f !important; text-shadow:none !important; box-shadow:none !important;font-size:14px !important;font-family: lator !important;}
</style>
</head>
<body>
<!-- loader -->
<div class="loading" style="display:none">
	<?php 
    	include('loader.php');
    ?>
</div>
<!-- end here -->

<!-- forgot password -->
<div id="forgot_password">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class" style="width:400px;"> <img src="assets/img/btn_close.png" id="logo-close" onclick="fp_close()" height="20" width="20" style="top:10px; right:10px;" />
      <div style="text-align:center; margin-top:35px; font-size:20px; font-weight:bold">Please provide email address</div>
      <div style="text-align:center; margin-top:20px; font-family: calibri;"><input type="text" style="" placeholder="Email" class="fp_text" /></div>
      <div style="margin-top:15px; width:250px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="forgot_password()">Ok</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="fp_close()">Cancel</span> </div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- signup prompt -->
<div id="signup-container">
  <div class="signup-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeSignup()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Join UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:25px; height:395px;">
      	<div style="height:55px;width:100%;">
        	<input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; float:left; outline:none" placeholder="First Name" class="txtFirstName" />
            <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; margin-left:20px; float:left; outline:none" placeholder="Last Name" class="txtLastName" />
        </div>
      	<input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="username" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Password (Atleast 4 characters)" class="password" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Confirm Password" class="confirm_password" />
        <div style="margin-bottom:10px; width:370px; text-align:left">
            <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15"/><span style="margin-top:-3px;">I agree to Ufundoo's term of use and privacy policy.</span>
        </div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUp()" />
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
      	<div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Already member ?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;"><input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Sign In"  onclick="openLogin()"/></div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- login prompt -->
<div id="login-container">
  <div class="login-container-wrapper">
  	<div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeLogin()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Login UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:15px; height:405px;">
      	<input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmail" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpassword" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:25px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:8px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signIn()"/>
        <div style="margin-bottom:10px; width:370px; height:15px;">
           <span style="">OR</span>
        </div>
        <a href="googleLogin1.php?status=user" style="text-decoration:none;color:white"><input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
        <span onclick="FBLogin('user');"><input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
      	<div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Not a member yet?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;"><input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join Now"  onclick="openSignup()" /></div>
      </div>
    </div>
  </div>
</div>
<!-- end here -->

<!-- signup prompt -->
<div id="promoter-container">
  <div class="promoter-container-wrapper">
  	<div class="admin-delete-class-promoter"><img src="assets/img/btn_close.png" id="logo-close" onclick="closePromoter()" height="20" width="20" />
    
    	<!-- login -->
    	<div style="float:left; width:50%; height:520px; border-right:2px solid #ccc; margin-top:25px; text-align:center;">
        	<div style="text-align:center;font-size:35px; font-weight:bold">Already a Promoter?</div>
            <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Login</div>
            <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmailPost" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpasswordPost" />
            <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:75px;" onclick="fp_open()">Forgot Password?</div>
            <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signInPost()"/>
            <!--<div style="margin-bottom:10px;height:15px;">
               <span style="">OR</span>
            </div>
            <a href="googleLogin1.php?status=promoter" style="text-decoration:none;color:white"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
            <span onclick="FBLogin('promoter');"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>-->
        </div>
        <!-- end here -->
        
        <!-- sign up -->
        <div style="float:left; width:50%; height:520px; margin-top:25px; text-align:center">
        	<div style="text-align:center;font-size:35px; font-weight:bold">Become a Promoter?</div>
            <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Sign Up</div>
            <div>
            	<input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Firstname" class="firstname" />
                <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none; margin-left:4%;" placeholder="Lastname" class="lastname" />
            </div>
            <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Email" class="username_post" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Password (Atleast 4 characters)" class="password_post" />
            <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:15px; outline:none" placeholder="Confirm Password" class="confirm_password_post" />
            <div style="margin-bottom:10px; width:75%; margin-left:auto; margin-right:auto">
                <img src="assets/img/unchecked.png" name="fainMail" onclick="check_single(this)" status="0" class="fainMail_post" style="cursor: pointer; float: left; margin-top:3px;" height="15" id="fainMail"/>Send me FainMail on everything events and entertainment.
            </div>
            <div style="margin-bottom:10px; width:70%; margin-left:64px; text-align:left">
                <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree_post" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15" id="agree"/>I agree to Ufundoo's term of use and privacy policy.
            </div>
            <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUpPost()" />
        </div>
        <!-- end here -->
        
    </div>
  </div>
</div>
<!-- end here -->

<!-- guest user hidden field -->
<?php
			$userType='manual';
			$userTypeVal='';
			if($_REQUEST['gusetUserEmail']!=''){
				$userType='guest';
				$userTypeVal=$_REQUEST['gusetUserEmail'];
			}
		?>
        <input type="hidden" id="userType" value="<?php echo $userType; ?>">
        <input type="hidden" id="userTypeEmail" value="<?php echo $userTypeVal; ?>">
<!-- end here -->
<div class="wrapper">
    	<?php 
			include('include/header.php'); 
		?>
        <div id="container_area" style="overflow:auto;">
            <div class="content" style="height: auto; width: 950px;margin-left: auto; margin-right: auto; min-height: 100px;margin-top: 30px;color:#000; margin-bottom:20px;">
            <!-- main content -->
                	<div style="height:60px; width:950px; border-bottom:1px solid #000;  margin-top:25px;font-family:lator;color:#000;">
                        <!-- event name -->
                        <div style="font-size:20px; text-transform:uppercase; line-height:20px; margin-bottom:10px; font-weight:600;" id="bt_event_name"><?php echo $result['eventName']; ?></div>
                        <!-- event date, time, location -->
                        <div style="font-size:16px;">
                            <span id="bt_event_date"><?php echo date_format($date_bt, 'jS M Y'); ?></span><span style="margin-left:25px;" id="bt_event_time"><?php echo date_format($time_bt, 'G:i a'); ?></span><span style="margin-left:20px; margin-right:20px;">|</span><span id="bt_event_location" style="text-transform:capitalize"><?php echo $result['locationName']; ?></span>
                        </div>
                    </div>
                    <table border="0" bordercolor="#0db3a5" cellpadding="10" cellspacing="0" style="border:2px solid #2e2e2e; font-size:16px; margin-top:20px; width:950px;">
                    	<?php if($result['ticketTypeId']=="1"){ ?>
                        <tr style="background:#2e2e2e; color:#fff; height:30px" align="center">
                            <td>Ticket Type</td>
                            <td>Total No Of Ticket</td>
                            <td>Unit Price</td>
                            <td>Subtotal</td>
                          
                            
                        </tr>
                        <?php
						$i=1;
						for($j=0;$j<sizeof($ticket);$j++)
						{
							$sql_ticket1 = mysqli_query($mysqli,"SELECT * FROM ticketStatus WHERE eventId='".$_REQUEST['eventId']."' and ticketId='".$ticket[$j][0]."' and availableQty!=0");
							$result1=mysqli_fetch_assoc($sql_ticket1);
						?>
                        <input type="hidden" id="ticketId<?php echo $i; ?>" value="<?php echo $ticket[$j][0]; ?>" />
                        <input type="hidden" id="qty<?php echo $i; ?>" value="<?php echo $ticket[$j][2]; ?>" />
                        <tr style="color:#2e2e2e; height:30px" align="center" class="check_rows">
                            <td><?php echo $ticket[$j][1]; ?></td>
                            <td style="padding-bottom: 5px;"><input type="number" style="width:70px; border-radius:4px; border:1px solid #000; height:32px;margin-left:10px; outline:none; padding-left:10px; font-size:15px; margin-top:6px;" placeholder="Qty" min="0" max="<?php echo $result1['availableQty']; ?>" id="qty_edit<?php echo $i; ?>" value="<?php echo $ticket[$j][2]; ?>" onChange="changeTotal(this,<?php echo $i; ?>)" onKeyUp="changeTotal(this,<?php echo $i; ?>)" readonly="readonly" /></td>
                            <td>$ <span id="price_per<?php echo $i; ?>"><?php echo $ticket[$j][4]; ?></span></td>
                            <td>$ <span id="total_amt<?php echo $i; ?>" class="check_rows1"><?php echo $ticket[$j][3]; ?></span></td>
                            
                        </tr>
                        <?php $i++;}} 
						else { 
							$sql_ticket2 = mysqli_query($mysqli,"SELECT * FROM ticketStatus WHERE eventId='".$_REQUEST['eventId']."' and ticketId='".$_REQUEST['ticketId']."' and availableQty!=0");
							$result2=mysqli_fetch_assoc($sql_ticket2);
						?> 
                        <tr style="background:#2e2e2e; color:#fff; height:30px" align="center">
                            <td>Ticket Type</td>
                            <td>Total No Of Ticket</td>
                        </tr> 
                        <input type="hidden" id="ticketId" value="<?php echo $_REQUEST['ticketId']; ?>" />
                        <input type="hidden" id="qty_free" value="<?php echo $_REQUEST['totalNum']; ?>" />
                        <tr style="color:#2e2e2e; height:30px" align="center" class="check_rows">
                            <td><?php echo $_REQUEST['ticketName']; ?></td>
                            <td style="padding-top: 5px;padding-bottom: 5px;"><input type="number" style="width:70px; border-radius:4px; border:1px solid #000; height:32px;margin-left:10px; outline:none; padding-left:10px; font-size:15px;" placeholder="Qty" min="0" id="qty_free_edit" value="<?php echo $_REQUEST['totalNum']; ?>" maxRange="<?php echo $result2['availableQty']; ?>" onChange="changeTotalFree()" onKeyUp="changeTotalFree()" max="<?php echo $result2['availableQty']; ?>" readonly="readonly"/></td>
                        </tr>
                                              
                        <?php } ?>
                    </table>
                    <?php if($result['ticketTypeId']=="2"){ ?>
                    	<div align="right" style="margin-right:5px; font-size:16px; margin-top:20px;">Total : $ 00.00</div>
                    	<div class="proceed_to_payment_enable" align="center" onClick="proceedTicket('<?php echo $_REQUEST['eventId']; ?>')">GET TICKETS</div>
                    <?php 
						} else {
							$incents=$_REQUEST['grandAmt']; 
							$conversion=100;
							$indollar = $incents * $conversion;
					?>
                    	<div align="right" style="margin-right:5px; font-size:16px; margin-top:20px;">Ticket Total : $ <span><?php echo $_REQUEST['subTotal']; ?></span></div>
                        <div align="right" style="margin-right:5px; font-size:16px; margin-top:20px;">Service Charges : $ <span><?php echo $_REQUEST['totalServiceCharge']; ?></span></div>
                    	<div align="right" style="margin-right:5px; font-size:16px; margin-top:20px;">Total : $ <span id="grand_amt"><?php echo $_REQUEST['grandAmt']; ?></span></div>
                    	<?php /*?><div class="proceed_to_payment_enable" align="center" onClick="proceedTicket('<?php echo $_REQUEST['eventId']; ?>','paid')">PROCEED TO PAYMENT</div><?php */?>
                        <form action="thankyou.php" method="POST" id="checkout-form" style="text-align:center">
                        <script src="https://checkout.stripe.com/checkout.js" 
                        class="stripe-button" 
                        data-key="pk_live_ieh9CJTj8MqItJgNlgPoGzOH"
                        data-image="https://ufundoo.com/assets/img/favicon.png" 
                        data-name="Ufundoo.com" 
                        data-description=""
                        data-billingAddress="true"
                        data-amount="<?php echo $indollar; ?>" />
                        </script>
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="merchant_order_id" value="" id="ticket_order_id">
                        <input type="hidden" name="orderNo" value="" id="orderNo">
                        <input type="hidden" name="role" value="" id="role">
                        <input type="hidden" name="gusetUserEmail" value="<?php echo $userTypeVal; ?>" id="gusetUserEmail">
                        <input type='hidden' name='total' value='<?php echo $_REQUEST['grandAmt']; ?>' />
                        <button type='button' onclick="checkoutTickets(<?php echo $_REQUEST['eventId']; ?>,'paid')" class="proceed_to_payment_enable" align="center" style="border: 1px solid #ed258f;padding-top: 0px;font-size: 18px;height: 45px; width: 300px; border-radius: 45px;">PROCEED TO PAYMENT</button>
                    </form>
                    <?php } ?>
            <!-- end here -->
            </div>
    		<!-- end here -->
    
            <!-- footer -->
            <div class="footer">
                <?php include('include/footer.php'); ?>
            </div>
            <!-- end here -->
		</div>
</div>
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/ufundoo.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<!-- for inline checkout -->
<!--<script src="https://www.2checkout.com/static/checkout/javascript/direct.min.js"></script>
--><script>
var windowHeight=$(window).height();
$(document).ready(function(e) {
	$('.stripe-button-el span').text('PROCEED TO PAYMENT');
	var listEvents=windowHeight-80;
    $('#container_area').css('height',listEvents+'px');
	if(windowHeight<700)
	{
		$('.content').css('min-height','300px');
	}
	else
	{
		$('.content').css('min-height','400px');
	}
	$("#container_area").niceScroll({cursorcolor: "rgba(237, 37, 143, 0.5)",cursorwidth:"6px",cursorborderradius:"10px" ,cursorborder: "0px", railpadding: {right: 2}, zindex: 0});
	
});
function checkoutTickets(eventId,status)
{
	var manual=$('#userType').val();
	var gusetUserEmail=$('#userTypeEmail').val();
	if(status=="free")
	{
		var ticketId=$('#ticketId').val();
		var totalNum=$('#qty_free_edit').val();
		var ticketDetail='';
	}
	else
	{
		ticketId='';
		totalNum='';
		var ticketArray=Array();
		var count=1;
		$('.check_rows').each(function(index, element) {
			ticketArray.push([$('#ticketId'+count).val(),$('#qty_edit'+count).val()]);
			count++;
		});
		var ticketDetail=JSON.stringify(ticketArray);
	}
	$.ajax({
		url: 'Ajax/proceedTicket.php',
		type:"POST",
		data:"eventId="+encodeURIComponent(eventId)
		+"&ticketId="+encodeURIComponent(ticketId)
		+"&totalNum="+encodeURIComponent(totalNum)
		+"&ticket_detail="+encodeURIComponent(ticketDetail)
		+"&gusetUserEmail="+encodeURIComponent(gusetUserEmail)
		+"&type="+encodeURIComponent(manual),
		dataType : "json",
		success: function(json) {
			console.log(json.msg);
			console.log(json.ticketDetailId);
			if(json.success==1)
			{
				$('#ticket_order_id').val(json.ticketDetailId);
				$('#orderNo').val(json.orderNo);
				$('#role').val(json.role);
				$( ".stripe-button-el" ).trigger( "click" );
				//alert("Ticket booked.");
				//$('#buy_ticket').fadeOut();		
			}
			else
			{
				console.log(json.msg);
				alert("Error");
			}
		}
	});
}
/* facebook */
window.fbAsyncInit = function() {
		FB.init({
		appId      : '705873652868650', // replace your app id here
		channelUrl : '', 
		status     : true, 
		cookie     : true, 
		xfbml      : true  
		});
	};
	(function(d){
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		ref.parentNode.insertBefore(js, ref);
	}(document));
	
	function FBLogin(type){
		FB.login(function(response){
			if(response.authResponse){
				window.location.href = "actions.php?status="+type;
			}
		}, {scope: 'email,user_likes'});
	}
	function changeTotal(_this,countId){
   var qty_old=$("#qty_edit"+countId).val();
   //$('#li_'+countId+'_quantity').val(qty_old);
   var qty_new=parseFloat(qty_old).toFixed(2);//parse float with two decimal places
   var price_per_old=$('#price_per'+countId).html();
   var price_per_new=parseFloat(price_per_old).toFixed(2);//parse float with two decimal places
   var total_old=(qty_new) * (price_per_new);
   var total_new=parseFloat(total_old).toFixed(2);//multiply two digits of having datatype float
   $('#total_amt'+countId).html(total_new);//parse float with two decimal places - set value
   //$('#li_'+countId+'_price').val(total_new);
   /* dynamic grand total count */
   countVar=0;
   $('.check_rows1').each(function(index, element) {
        countVar=parseFloat(countVar)+parseFloat($(this).text());
   });
   $('#grand_amt').html(countVar.toFixed(2));
   
   /* end */
   var maxValue=$("#qty_edit"+countId).attr('max');//get the quantity value from 
   if(parseInt(qty_old) > parseInt(maxValue))//enter value > total quantity available
   {
	   alert('Maximum no of tickets available is '+maxValue+'.');//set total quantity available
	   $("#qty_edit"+countId).val(maxValue);
   }
   
}
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	if($("#signup-container").css('display')=="block")
	{
		signUp();
	}
	if($("#login-container").css('display')=="block")
	{
		signIn();
	}
}});
</script>
</body>
</html>