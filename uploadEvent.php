<?php
	session_start();
	require_once("config/conn.php");
	if(!isset($_SESSION["promoterId"]))
	{
		header("Location:index.php");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ufundoo | Add Event</title>
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/cal.css" type="text/css" />
<link rel="stylesheet" href="assets/css/uploadEvent.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />

<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script>
<script type='text/javascript' src="assets/js/ui.js"></script>
<script src="assets/js/jquery-ui-timepicker-addon.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<script src="assets/js/ufundoo.js"></script>
<script src="fileupload/js/vendor/jquery.ui.widget.js"></script> 
<!-- The Load Image plugin is included for the preview images and image resizing functionality --> 
<script src="fileupload/js/load-image.all.min.js"></script> 
<!-- The Canvas to Blob plugin is included for image resizing functionality --> 
<script src="fileupload/js/canvas-to-blob.min.js"></script> 
<!-- The Iframe Transport is required for browsers without support for XHR file uploads --> 
<script src="fileupload/js/jquery.iframe-transport.js"></script> 
<!-- The basic File Upload plugin --> 
<script src="fileupload/js/jquery.fileupload.js"></script> 
<!-- The File Upload processing plugin --> 
<script src="fileupload/js/jquery.fileupload-process.js"></script> 
<!-- The File Upload image preview & resize plugin --> 
<script src="fileupload/js/jquery.fileupload-image.js"></script> 
<!-- The File Upload validation plugin --> 
<script src="fileupload/js/jquery.fileupload-validate.js"></script> 
<!-- jQuery easing plugin -->
<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
<!-- maxlength.js -->
<script src="assets/js/maxlength.js"></script> 


<style>
.ui-datepicker-trigger{margin-top:4px; height:27px; position:absolute; cursor:pointer}
.ui-datepicker-div{padding-left:7px;}
.ui-timepicker-div{padding-left:7px; height:140px;}
.ui-slider .ui-slider-handle{height:0.8em; width:0.8em;top:-.4em}
input,textarea{outline:none;}
.headerBtn {
    color: #2e302d;
}
.select-wrapper{
		float: left;
		display: inline-block;
		background:url(assets/img/arrow.png) no-repeat right 8px center;
		cursor: pointer;
		
	}
	.select-wrapper, .select-wrapper select{
		width: 125px;
		//height: 40px;
		height:30px;
		//line-height: 26px;
		line-height: 30px;
		font-size:15px;
	}
	
	.select-wrapper .holder{
		display: block;
		//margin: 0 35px 0 5px;
		white-space: nowrap;            
		overflow: hidden;
		cursor: pointer;
		position: relative;
		//padding-top:7px;
		z-index: -1;
		color:#444;
		width:125px;
	}
	.select-wrapper select{
		margin: 0;
		position: absolute;
		z-index: 2;            
		cursor: pointer;
		outline: none;
		opacity: 0;
		/* CSS hacks for older browsers */
		_noFocusLine: expression(this.hideFocus=true); 
		-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
		filter: alpha(opacity=0);
		-khtml-opacity: 0;
		-moz-opacity: 0;
		width:125px;
	}
	.custom-select
	{
		background-color:#fff;
	}
</style>
</head>

<body>
	<!-- loader -->
    <div class="loading" style="display:none">
        <?php 
            include('loader.php');
        ?>
    </div>
    <!-- end here -->
    
	<!-- header -->
	<div class="header">
        <?php include('include/header.php'); ?>
    </div>
    <!-- end here -->
	<div class="wrapper" align="center">
    
    <!-- container -->
	<?php include('include/postNewEventForm.php'); ?>        
    <!-- end here -->
    
    <!-- footer -->
  	<!--<div class="footer">
   		<?php /*?><?php include('include/footer.php'); ?><?php */?>
    </div>-->
    <!-- end here -->
</div>

<script>
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(document).ready(function(e) {
	$(function() {
				$( "#datepick" ).datepicker({
				  minDate: 0,
				  showOn: "button",
				  buttonImage: "https://ufundoo.com/assets/img/calander.png",
				  buttonImageOnly: true,
				  dateFormat: 'yy-mm-dd',
				  onSelect: function (date) {
						var dt2 = $('#datepick2');
						var maxDate = $(this).datepicker('getDate');
						dt2.datepicker('option', 'maxDate', maxDate);
					}
			});
  }); 
   $(function() {
				$( "#datepick1" ).datepicker({
				  minDate: 0,
				  showOn: "button",
				  buttonImage: "https://ufundoo.com/assets/img/calander.png",
				  buttonImageOnly: true,
				  dateFormat: 'yy-mm-dd',
				  onSelect: function (date) {
						var dt2 = $('#datepick2');
						var minDate = $(this).datepicker('getDate');
						dt2.datepicker('option', 'minDate', minDate);
					}
			});
  });
  $(function() {
				$( "#datepick2" ).datepicker({
				  showOn: "button",
				  buttonImage: "https://ufundoo.com/assets/img/calander.png",
				  buttonImageOnly: true,
				  dateFormat: 'yy-mm-dd'
			});
  });
  
  $(function() {
				$( "#datepicker_time1" ).timepicker({
				  showOn: "button",
				  buttonImage: "assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  }); 
  $(function() {
				$( "#datepicker_time2" ).timepicker({
				  showOn: "button",
				  buttonImage: "assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  });
  $(function() {
				$( "#datepicker_time3" ).timepicker({
				  showOn: "button",
				  buttonImage: "assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  }); 
  $(function() {
				$( "#datepicker_time4" ).timepicker({
				  showOn: "button",
				  buttonImage: "assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  });
  $(".next").click(function(){
	if($(this).attr('attrNum')=='first')
	{
		if($('#org_name').val()=='')
		{
			$('.error').html('Please Enter Organizaton Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#org_name').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('#org_email').val())) 
		{
			$('.error').html('Invalid Email Address').fadeIn().delay( 4000 ).fadeOut();
			$('#org_email').focus();
			return false;
		}
	}
	else if($(this).attr('attrNum')=='second')
	{
		if($('#event_name').val()=='')
		{
			$('.error').html('Please Enter Event Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_name').focus();
			return false;
		}
		if($('#event_type').val()=='Select')
		{
			$('.error').html('Please Select Event Type').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_type').focus();
			return false;
		}
		if($('#event_location').val()=='')
		{
			$('.error').html('Please Enter Event Location').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_location').focus();
			return false;
		}
		if($('#event_address').val()=='')
		{
			$('.error').html('Please Enter Event Address').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_address').focus();
			return false;
		}
		if($('#datepick').val()=='')
		{
			$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepick').focus();
			return false;
		}
		if($('#datepicker_time1').val()=='')
		{
			$('.error').html('Please Select Start Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time1').focus();
			return false;
		}
		if($('#datepicker_time2').val()=='')
		{
			$('.error').html('Please Select End Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time2').focus();
			return false;
		}
		var checkedValueMap=document.getElementById("mapCheck").checked;
		if(checkedValueMap)
		{
			if($('#address').val()=='')
			{
				$('.error').html('Please Enter Address To Generate Map').fadeIn().delay( 4000 ).fadeOut(); // empty username
				$('#address').focus();
				return false;
			}
		}	
	}
	else
	{
		var flagQty=true;
		$('.qty_ava').each(function(index, element) {
			if($(this).val()=='')
			{
				flagQty=false;
			}
		});
		if(!flagQty)
		{
			$('.error').html('Quantity must be minimum 1').fadeIn().delay( 4000 ).fadeOut(); // empty username
			return false;
		}
		var flagName=true;
		$('.ticket_name').each(function(index, element) {
			if($(this).val()=='')
			{
				flagName=false;
			}
		});
		if(!flagName)
		{
			$('.error').html('Please Enter Ticket Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
			return false;
		}
		if($('#ticket_price_wrapper_single').css('display')!="none")
		{
			var flagPrice=true;
			$('.ticket_price').each(function(index, element) {
				if($(this).val()=='')
				{
					flagPrice=false;
				}
			});
			if(!flagPrice)
			{
				$('.error').html('Please Enter Ticket Price').fadeIn().delay( 4000 ).fadeOut(); // empty username
				return false;
			}
		}
		if($('#datepick1').val()=='')
		{
			$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepick1').focus();
			return false;
		}
		if($('#datepick2').val()=='')
		{
			$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepick2').focus();
			return false;
		}
		if($('#datepicker_time3').val()=='')
		{
			$('.error').html('Please Select Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time3').focus();
			return false;
		}
		if($('#datepicker_time4').val()=='')
		{
			$('.error').html('Please Select Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time4').focus();
			return false;
		}
	}
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'transform': 'scale('+scale+')'});
			next_fs.css({'top': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'top': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
});
});
$(".custom-select").each(function(){
            $(this).wrap("<span class='select-wrapper'></span>");
            $(this).after("<span class='holder'></span>");
        });
        $(".custom-select").change(function(){
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        }).trigger('change');
		
function typeTicket(button)
{
	$(button).addClass('button_event_upload_color');
	$(button).removeClass('button_event_upload');
	$(button).parent('div').siblings('div').children('div').addClass('button_event_upload');
	$(button).parent('div').siblings('div').children('div').removeClass('button_event_upload_color');
}

var uploadButton='';
var appendImgPath='';
var appendPath='';
var countNoOfFiles=0;
/* for images */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = 'fileupload/server/php/';
	$('#fileupload-image').each(function () {
		$(this).on('click', function () {
			uploadButton = $(this).parent().parent().attr('id');
			//alert(uploadButton);
		});
    $(this).fileupload({
        url: url,
        dataType: 'json',
        maxFileSize: 100000000, // 5 MB
		maxNumberOfFiles: 5,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
		$(".popup-loader").fadeIn();
	}).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        //$('#progress .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
		$(".popup-loader").fadeOut();
		$('#event_image_wrapper').css('display','table-cell');
        $.each(data.result.files, function (index, file) {
		appendImgPath='';
		if(countNoOfFiles>=5)
		{
			alert("You can upload max 5 imgaes for a particular event.");
		}
		else
		{
			countNoOfFiles+=1;
			appendImgPath=('<div style="margin-right:0px; margin-right:10px; margin-bottom:10px;float:left"><img src="'+file.thumbnailUrl+'" height="60" style="border:1px solid #ccc;" /><img src="https://ufundoo.com/assets/img/status-remove.png" style="text-align:right; vertical-align:top; position:relative; right:10px; top:-10px; cursor:pointer;" onClick="deleteUploadedImage(this,\''+file.url+'\',\''+file.deleteUrl+'\')" class="removedImageIcon"></div>');
			appendPath+=file.url+'$';
		}
		});
		$('#event_image_wrapper').append(appendImgPath);
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
			console.log(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
});

function deleteUploadedImage(imagePos,fileUrl,deleteUrl)
{
	    countNoOfFiles-=1;
		$(imagePos).parent('div').remove();
		var str1 = appendPath;
		var str2 = fileUrl;
		appendPath = str1.replace(str2+'$','');	
		
	$.ajax({
    	dataType: 'json',
        type: "DELETE",
        url: deleteUrl,
    }).done(function (data) {
    	console.log(data);
	});
}

function deleteUploadedImageSingle(imagePos,deleteUrl,type)
{
	if(type=="video")
	{
		$(imagePos).siblings('video').attr('src','');
	}
	else
	{
		$(imagePos).siblings('img').attr('src','');
	}
	$(imagePos).parent('div').css('display','none');
	$.ajax({
    	dataType: 'json',
        type: "DELETE",
        url: deleteUrl,
    }).done(function (data) {
    	console.log(data);
	});
}

/* for seating chart image */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = 'fileupload/server/php/';
	$('#fileupload-seat-chart').each(function () {
		$(this).on('click', function () {
			uploadButton = $(this).parent().parent().attr('id');
			//alert(uploadButton);
		});
    $(this).fileupload({
        url: url,
        dataType: 'json',
        maxFileSize: 100000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
		$(".popup-loader").fadeIn();
	}).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        //$('#progress .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
		$(".popup-loader").fadeOut();
		$('#event_seat_chart_wrapper').css('display','table-cell');
		$('#event_seat_chart').attr('src',file.url);
		$('#deleteSeatChart').attr('onClick','deleteUploadedImageSingle(this,\''+file.deleteUrl+'\',"image")');
		});
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
			console.log(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
});

/* for cover image */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = 'fileupload/server/php/';
	$('#fileupload-image-cover').each(function () {
		$(this).on('click', function () {
			uploadButton = $(this).parent().parent().attr('id');
			//alert(uploadButton);
		});
    $(this).fileupload({
        url: url,
        dataType: 'json',
        maxFileSize: 100000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
		$(".popup-loader").fadeIn();
	}).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        //$('#progress .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
		$(".popup-loader").fadeOut();
		$('#event_image_wrapper_cover').css('display','table-cell');
		$('#event_image_cover').attr('src',file.url);
		$('#deleteCover').attr('onClick','deleteUploadedImageSingle(this,\''+file.deleteUrl+'\',"image")');
		});
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
			console.log(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
});

/* featured image */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = 'fileupload/server/php/';
	$('#fileupload-image-featured').each(function () {
		$(this).on('click', function () {
			uploadButton = $(this).parent().parent().attr('id');
			//alert(uploadButton);
		});
    $(this).fileupload({
        url: url,
        dataType: 'json',
        maxFileSize: 100000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
		$(".popup-loader").fadeIn();
	}).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        //$('#progress .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
		$(".popup-loader").fadeOut();
		$('#event_image_wrapper_featured').css('display','table-cell');
		$('#event_image_featured').attr('src',file.url);
		$('#deleteFeatured').attr('onClick','deleteUploadedImageSingle(this,\''+file.deleteUrl+'\',"image")');
		});
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
			console.log(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
});

function includeMap()
{
	$('#address').val('');
	var checkedValue=document.getElementById("mapCheck").checked;
	if(checkedValue)
	{
		$('#include_map').fadeIn();
		var locationVal=$('#event_location').val();
		$('#address').val(locationVal);
		codeAddress();
	}
	else
	{
		$('#include_map').fadeOut();
		$('#map_canvas_wrapper').fadeOut();
	}
}
function freeTicket(type)
{
	if($(type).val()==2)
	{
		$('#ticket_price_wrapper_single').fadeOut();
		$('#ticket_price_wrapper_single').css('display','none');
		$('#multi_ticket').css('display','none');
		$('#multiTicketWrapper').css('display','none');
	}
	else
	{
		$('#ticket_price_wrapper_single').fadeIn();
		$('#multi_ticket').show();
		$('#multiTicketWrapper').css('display','table-cell');
	}
}
var id=1;
function addMultiTicket()
{
	$('#ticket_price_wrapper_single').css('margin-bottom','10px');
	$('#multiTicketWrapper').css('display','table-cell');
	++id;
	$('#multiTicketWrapper').append('<div style="border-top:1px solid #ccc; width:940px; padding-top:5px;"><div align="left" style="margin-top:15px;"><div style="width:180px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px; padding-right:20px;text-align: right;">Ticket name<span style="color:#f00">*</span></div><div align="left" style="display:table-cell; vertical-align:middle;"><input type="text" name="ticket_name" id="ticket_name_'+id+'" class="ticket_name check_rows" style="width:215px;"/></div></div><div align="left" style="margin-top:15px;"><div style="width:180px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;padding-right:20px;text-align: right;">Quantity available<span style="color:#f00">*</span></div><div align="left" style="display:table-cell; vertical-align:middle;"><input type="text" name="qty_ava" id="qty_ava_'+id+'" style="width:215px;" class"qty_ava" onkeypress="return isNumberKey(event)"/></div></div><div align="left" style="margin-top:15px; margin-bottom:10px;" class="ticket_price_wrapper"><div style="width:180px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;padding-right:20px;text-align: right;">Price<span style="color:#f00">*</span></div><div align="left" style="display:table-cell; vertical-align:middle;"><input type="text" name="ticket_price" id="ticket_price_'+id+'" class="ticket_price" style="width:215px;" onkeypress="return isNumberKeyFloat(event)"/></div></div></div>');
	
}
function fillMapAdd()
{
	$('#address').val()=$('#event_location').val();
}

/* load map */
var geocoder;
var map;
function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-34.397, 150.644);
  var mapOptions = {
    zoom: 10,
    center: latlng
  }
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
}

function codeAddress() {
	if($('#address').val()=='')
	{
		$('.error').html('Please Enter Address To Generate Map').fadeIn().delay( 4000 ).fadeOut(); // empty username
		$('#address').focus();
		return false;
	}
	else
	{
	$('#map_canvas_wrapper').fadeIn();
	initialize();
	var address = document.getElementById('address').value;
  	geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
  }
}

</script>
</body>
</html>