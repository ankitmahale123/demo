<?php
	session_start();
	require_once("config/conn.php"); 
	$date=date('Y-m-d');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Ufundoo | Thank You</title>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="https://ufundoo.com" />
<meta name="twitter:title" content="Small Island Developing States Photo Submission" />
<meta name="twitter:description" content="View the album on Flickr." />
<meta name="twitter:image" content="https://ufundoo.com/img.png" />
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<link rel="stylesheet" href="assets/css/datepicker.css" type="text/css" />

<style>
html,body
{
	overflow:hidden;
}
.headerBtnActive
{
	border: 1px solid #2e302d;
	color:#2e302d;
}
.headerBtn
{
	color:#2e302d;
}
</style>
</head>

<body>
<!-- loader -->
<div class="loading" style="display:none">
	<?php 
    	include('loader.php');
    ?>
</div>
<!-- end here -->

	<div class="wrapper">
			<!-- header -->
            <?php include('include/header.php'); ?>
    		<!-- end header -->
            
            <div id="container_area" style="overflow:auto">
            	<div style="height:390px; width:500px;display:table; margin-left:auto; margin-right:auto; margin-top:30px;" align="center">
                	<img src="assets/img/oops.png" style="text-align:right; vertical-align:top" />
                    <div style="display:inline-block; height:210px;text-align:left;padding-left:20px; ">
                    	<div style="font-size:80px; color:#ed258f; margin-top:30px; width:100%;">Oops!</div>
                        <div style="font-size:18px; color:#000; width:100%; text-align:center">Something went wrong</div>
                    </div>
                    <div class="searchBtn" style="width: 210px; font-size: 19px; padding-top: 8px; height: 45px; margin-left:170px;">Try again</div>
                    <div style="font-size:18px; color:#000; text-align:center; margin-top:20px; margin-left:90px;">Still having the same problem?</div>
                    <div style="font-size:18px; color:#000; text-align:center; margin-top:10px; margin-left:90px;">Contact us at <span style="color:#ed258f; font-weight:bold;">customerservice@ufundoo.com</span></div>
                </div>
             <!-- footer -->
             <?php include('include/footer.php'); ?>   
             <!-- end footer -->
      </div>
</div>
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/ufundoo.js"></script>
<script>
var windowHeight=$(window).height();
$(document).ready(function(e) {
	$('#dp').datepicker({
		format: 'yyyy-mm-dd'
	});
	listEvents=windowHeight-80;
    $('#container_area').css('height',listEvents+'px');
	$("#container_area").niceScroll({cursorcolor: "rgba(237, 37, 143, 0.5)",cursorwidth:"6px",cursorborderradius:"10px" ,cursorborder: "0px", railpadding: {right: 2}, zindex: 0});
	
});
</script>
</body>
</html>