<?php
	session_start();
	require_once("config/conn.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UFundoo</title>
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/ufundoo.js"></script>
<style>
.headerBtn {
	color: #2e302d;
}
.description {
	font-size:18px;
	padding: 15px;
}
.center {text-align:center;color: #2e302d;
    font-size: 28px;
    font-family: leagueSpartan;
	color:#4e4e4e;
	}
.referral{display:none !important;}
.juicer-feed ul.j-filters li{width:135px !important;}
.j-filters{padding:0 !important;}
.juicer-feed .j-paginate:hover{background-color:#ed258f !important; color:#fff !important;}
.juicer-feed .j-paginate{    border: 2px solid #ED2591 !important;color: #ED2591 !important;}
</style>
<script>
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	if($("#signup-container").css('display')=="block")
	{
		signUp();
	}
	else if($("#login-container").css('display')=="block")
	{
		signIn();
	}
	else
	{
		feedback();
	}
}});
</script>
</head>

<body>
<!-- loader -->
<div class="loading" style="display:none">
  <?php 
    	include('loader.php');
    ?>
</div>
<!-- end here --> 

<!-- forgot password -->
<div id="forgot_password">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class" style="width:400px;"> <img src="assets/img/btn_close.png" id="logo-close" onclick="fp_close()" height="20" width="20" style="top:10px; right:10px;" />
      <div style="text-align:center; margin-top:35px; font-size:20px; font-weight:bold">Please provide email address</div>
      <div style="text-align:center; margin-top:20px; font-family: calibri;">
        <input type="text" style="" placeholder="Email" class="fp_text" />
      </div>
      <div style="margin-top:15px; width:250px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="forgot_password()">Ok</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="fp_close()">Cancel</span> </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- signup prompt -->
<div id="signup-container">
  <div class="signup-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeSignup()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Join UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:25px; height:395px;">
        <div style="height:55px;width:100%;">
          <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; float:left; outline:none" placeholder="First Name" class="txtFirstName" />
          <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; margin-left:20px; float:left; outline:none" placeholder="Last Name" class="txtLastName" />
        </div>
        <input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="username" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Password (Atleast 4 characters)" class="password" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Confirm Password" class="confirm_password" />
        <div style="margin-bottom:10px; width:370px; text-align:left"> <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15"/><span style="margin-top:-3px;">I agree to Ufundoo's term of use and privacy policy.</span> </div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUp()" />
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
        <div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Already member ?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;">
          <input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Sign In"  onclick="openLogin()"/>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- login prompt -->
<div id="login-container">
  <div class="login-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeLogin()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Login UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:15px; height:405px;">
        <input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmail" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpassword" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:25px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:8px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signIn()"/>
        <div style="margin-bottom:10px; width:370px; height:15px;"> <span style="">OR</span> </div>
        <a href="googleLogin1.php?status=user" style="text-decoration:none;color:white">
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" />
        </a> <span onclick="FBLogin('user');">
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" />
        </span> </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
        <div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Not a member yet?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;">
          <input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join Now"  onclick="openSignup()" />
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- signup prompt -->
<div id="promoter-container">
  <div class="promoter-container-wrapper">
    <div class="admin-delete-class-promoter"><img src="assets/img/btn_close.png" id="logo-close" onclick="closePromoter()" height="20" width="20" /> 
      
      <!-- login -->
      <div style="float:left; width:50%; height:520px; border-right:2px solid #ccc; margin-top:25px; text-align:center;">
        <div style="text-align:center;font-size:35px; font-weight:bold">Already a Promoter?</div>
        <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Login</div>
        <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmailPost" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpasswordPost" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:75px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signInPost()"/>
        <!--<div style="margin-bottom:10px;height:15px;">
               <span style="">OR</span>
            </div>
            <a href="googleLogin1.php?status=promoter" style="text-decoration:none;color:white"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
            <span onclick="FBLogin('promoter');"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>--> 
      </div>
      <!-- end here --> 
      
      <!-- sign up -->
      <div style="float:left; width:50%; height:520px; margin-top:25px; text-align:center">
        <div style="text-align:center;font-size:35px; font-weight:bold">Become a Promoter?</div>
        <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Sign Up</div>
        <div>
          <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Firstname" class="firstname" />
          <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none; margin-left:4%;" placeholder="Lastname" class="lastname" />
        </div>
        <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Email" class="username_post" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Password (Atleast 4 characters)" class="password_post" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:15px; outline:none" placeholder="Confirm Password" class="confirm_password_post" />
        <div style="margin-bottom:10px; width:75%; margin-left:auto; margin-right:auto"> <img src="assets/img/unchecked.png" name="fainMail" onclick="check_single(this)" status="0" class="fainMail_post" style="cursor: pointer; float: left; margin-top:3px;" height="15" id="fainMail"/>Send me FainMail on everything events and entertainment. </div>
        <div style="margin-bottom:10px; width:70%; margin-left:64px; text-align:left"> <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree_post" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15" id="agree"/>I agree to Ufundoo's term of use and privacy policy. </div>
        <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUpPost()" />
      </div>
      <!-- end here --> 
      
    </div>
  </div>
</div>
<!-- end here --> 

<!-- header -->
<div class="header">
  <?php include('include/header.php'); ?>
</div>
<!-- end here --> 

<!-- wrapper -->
<div class="wrapper"> 
  
  <!-- container -->
  <div class="content">
    <div style="padding: 0 0 25px;border-bottom:2px solid #ed258f;">
    <div><img src="assets/img/sevathon.png" class="img-responsive" style="margin: 30px auto 0;"/></div>
    <div class="center">
      		<div style="padding:25px 0;">Welcome to Sevathon<br/>
           world's most valuable plateform for Non Profit Organizations</div>
          <div style="font-size:18px;line-height:30px;">Raise funds<br/>
            Find volunteers<br/>
            Stand for a cause<br/></div>
      </div>
      <div class="searchBarContainer" style="width: 225px;background-color: transparent;padding: 8px 0;height: initial;">
        <div class="searchBtn" style="padding: 8px 0;width: 200px;line-height: normal;text-align: center;margin: 0 auto;" onclick="window.open('http://sevathon.org/index.html','_self')">Register</div>
      </div>
    </div>
    <!-- main content -->
    <div style="display:table; min-height:100px; height:auto; width:1024px; margin-top:30px; margin-left:auto; margin-right:auto; margin-bottom:30px;">
      <div style="display:table-row">
        <div style="display:table-cell; width:50%;">
          <div class="mostPopEvents" style="margin-bottom:20px;">RECENT STORIES</div>
          <div><iframe src ="https://www.scu.edu/is/secure/blog-news-and-events/" width="90%" height="600">
<p>Your browser does not support iframes.</p>
</iframe>
</div>
        </div>
        <div style="display:table-cell; width:50%;">
          <div class="mostPopEvents" style="margin-bottom:20px;"> FEEDS </div>
          <div>
          	<script src="//assets.juicer.io/embed.js" type="text/javascript"></script>
<link href="//assets.juicer.io/embed.css" media="all" rel="stylesheet" type="text/css" />
<ul class="juicer-feed" data-feed-id="sevathon"  data-per="5"></ul>
          </div>
        </div>
      </div>
    </div>
    <!-- end here --> 
  </div>
  <!-- end here --> 
  
  <!-- footer -->
  <div class="footer">
    <?php include('include/footer.php'); ?>
  </div>
  <!-- end here --> 
</div>
<!-- end here -->
</body>
</html>