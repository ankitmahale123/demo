<?php
session_start(); 
require_once("config/conn.php");
$date=date('Y-m-d H:i:s');
	$queryEmailCheck=mysqli_query($mysqli,'select user.id,user.email,profile.firstName from user inner join profile on profile.id=user.profileId where user.id="'.$_REQUEST['id'].'" and user.status="inactive"');
	$resultEmailCheck=mysqli_fetch_assoc($queryEmailCheck);
	$email=$resultEmailCheck['email'];
	if(mysqli_num_rows($queryEmailCheck)==1)
	{
		$query_profile_update=mysqli_query($mysqli,'update user set status="active",lastLogin="'.$date.'" where id="'.$_REQUEST['id'].'"');
		if($query_profile_update==1)
		{
						include("config/configEmail.php");
						$to_addresss1='"'.$email.'"';
						$body1 =  '<div style="font-family: calibri;font-size:15px;">Hi '.ucfirst($resultEmailCheck['firstName']).',<br/><br/>Welcome to our community!<br/><br/>Log in <a href="https://ufundoo.com/confirmUser.php?id='.$userId.'&role='.$_REQUEST['role'].'"> here</a> to ufundoo.<br/><br/>Thanks,<br/>Ufundoo Team</div>';
						$subject = "Welcome to ufundoo!";
						//echo $body1;
						//exit();
						require_once('phpmailer/class.phpmailer.php');
						$mail = new PHPMailer();
						$mail->IsHTML(true);
						$mail->IsSMTP(); // telling the class to use SMTP
						$mail->Host       = $SmtpServer; // SMTP server
						//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
						$mail->SMTPSecure = "ssl";										   
						$mail->SMTPAuth   = true;                  // enable SMTP authentication
						$mail->Host       = $SmtpServer; // sets the SMTP server
						$mail->Port       = $SmtpPort;                    // set the SMTP port for the GMAIL server
						$mail->Username   = $SmtpUser; // SMTP account username
						$mail->Password   = $SmtpPass;        // SMTP account password
						
						$mail->From=$SmtpUser;
						$mail->FromName=$SmtpUser;
						
						$mail->Subject    = $subject;
						
						$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
						$mail->MsgHTML($body);
						$mail->Body=$body1;
				
						
						//smtp_host,smtp_port,username,password,from_address,to_address
						
						$mail->AddAddress($to_addresss1, $to_addresss1);
						
						if(!$mail->Send()) {
						  //$msg = "Error in request to admin"; 
						} else {
						  //$msg = "Mail sent";
						}
		}
	}
	else
	{
		header('Location : https://ufundoo.com');
	}
?>
<!DOCTYPE html>
<html>
<head>
<title>Ufundoo | Confirm Email</title>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
</head>
<body>

<div style="margin-top:15px; height:70px;" align="center">
	<img src="assets/img/logo.png" height="67" style="cursor:pointer;" onClick="window.open('index.php','_self');"/>
</div>

<!-- container of form -->
<div style="width:100%;">
      <div style="text-align: center; margin-left:auto; margin-right:auto; margin-top:20px; height:120px;">
      	<div style="margin-bottom:15px; text-align:center; margin-left:auto; margin-right:auto;">Thank you for completing the process.</div>
      	<div class="join resetbBtn" onclick="window.open('https://ufundoo.com','_self')" style="display: inline-block;width: 150px;">Home page</div>
        <div class="join resetbBtn" onclick="window.open('https://ufundoo.com/index.php?reset=yes&resetRole=user','_self')" style="display: inline-block;width: 150px;margin-left: 50px;">Login</div>
      </div>
</div>
<!-- end here -->

<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/ufundoo.js"></script>
</body>
</html>