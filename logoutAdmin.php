<?php 
require_once("config/conn.php");
session_start();
$_SESSION['adminId']='';
unset($_SESSION['adminId']);
$_SESSION['adminEmail']='';
unset($_SESSION['adminEmail']);
session_destroy();
header("Location: Admin/index.php");
?>