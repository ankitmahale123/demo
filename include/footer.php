<div class="footer" style="position:relative">
            	<div class="footerCont">
                	<?php if(!isset($_SESSION["userId"])){ ?>
                    <div class="footerEach" id="promoter_footer_tab">
                    	<div align="left">
                            <div align="left" class="footerTitle">PROMOTER</div>
                            <div align="left" class="footerLine"></div>
                        </div>
                        <div class="footerEachData">
                        	<?php if(isset($_SESSION["promoterId"])){ ?>
                           	<div onclick="window.open('uploadEvent.php','_self')">Post events</div>
                            <?php } else { ?>
                            <div onclick="openPromoter()">Post events</div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="footerEach">
                    	<div align="left">
                            <div align="left" class="footerTitle">ABOUT US</div>
                            <div align="left" class="footerLine" style="margin-left:10px;"></div>
                        </div>
                        <div class="footerEachData">
                        	<div onclick="window.open('aboutus.php','_self')">About</div>
                           	<div onclick="window.open('callUs.php','_self')">Feedback</div>
                            <div onclick="window.open('career.php','_self')">Jobs</div>
                        </div>
                    </div>
                    <div style="width:18%" class="footerEach">
                    	<div align="left">
                            <div align="left" class="footerTitle">CONTACT US</div>
                            <div align="left" class="footerLine" style="margin-left:20px;"></div>
                        </div>
                        <div class="footerEachData">
                           	<div style="line-height: 25px;"><img src="assets/img/contactus_phone.png"/>+1-510-818-0468</div>
                            <div style="line-height: 30px;"><img src="assets/img/contactus_location.png"/>Fremont, CA</div>
                        </div>
                    </div>
                    <div style="width:18%"  class="footerEach">
                    	<div align="left">
                            <div align="left" class="footerTitle">LINKS</div>
                            <div align="left" class="footerLine" style="width:25px; margin-left:6px;"></div>
                            <div class="footerEachData">
                                <div onclick="window.open('index.php#popularEvents','_self')">Most Popular Events</div>
                                <div onclick="window.open('index.php#browseEvents','_self')">Browse Events</div>
                                <div onclick="window.open('Terms_Of_Service.php','_self')">Terms Of Service</div>
                                <div onclick="window.open('Policy.php','_self')">Fulfillment Policy</div>
                                <div onclick="window.open('faq.php','_self')">FAQ</div>
                            </div>
                        </div>
                        
                    </div>
                    <div style="width:34%;padding-top:25px;" class="footerEach">
                    	<div align="center" class="footerTitle" style="font-size:23px;">Get updates for all events</div>
                        <div style="font-size:10px; color:#fff">Subscribe here</div>
                        <div class="subscribeCont">
                        	<div class="inputSubscribeWrapper">
                            	<input type="text" class="subscribeInput" placeholder="Enter email ID here" />
                            </div>
                            <div class="subscribeBtn" onclick="subscribe()">SUBMIT</div>
                        </div>
                        <div class="footerEachData">
                        	<a href="https://www.facebook.com/dialog/feed?app_id=705873652868650&redirect_uri=https://ufundoo.com&link=https://ufundoo.com&picture=https://ufundoo.com/assets/img/logo.png&caption=A brand new entertainment&description=A brand new entertainment experience is coming soon." onclick="window.open(this.href); return false;">
                           		<img src="assets/img/mostp_fb.png" title="Facebook"/>
                            </a>
                            <a href="http://twitter.com/share?text=A brand new entertainment experience is coming soon%0a&url=https://ufundoo.com" onclick="window.open(this.href); return false;">
                            	<img src="assets/img/mostp_tweet.png" title="Twitter"/>
                            </a>
                            <a href="http://pinterest.com/pin/create/button/?url=https://ufundoo.com&media=https://ufundoo.com/assets/img/logo.png&description=Ufundoo - A brand new entertainment experience is coming soon" onclick="window.open(this.href); return false;">
                            	<img src="assets/img/mostp_pin.png" style="margin-right:0px;" title="Pin It"/>
                            </a>
                        </div>	
                    </div>
                </div>
            </div>