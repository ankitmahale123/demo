<?php
	session_start();
	
	if($_REQUEST['page']=="promoter")
	{
		require_once("../config/conn.php");
	}
	else
	{
		$_REQUEST['page']="normal";
	}
	/* event type */
	//echo $_SESSION['promoterEmail']; 
	$queryType=mysqli_query($mysqli,'select * from eventType');
	$queryEvent=mysqli_query($mysqli,"select promoter.id,promoter.email as promoterEmail,organization.id as orgId,organization.name as orgName,organization.description as orgDesc,organization.email as orgEmail,organization.contactNo as orgContact,organization.fax as orgFax,organization.website as orgWebsite from promoter inner join organization on organization.promoterId=promoter.id where promoter.email='".$_SESSION['promoterEmail']."'");
	$resultEvent=mysqli_fetch_assoc($queryEvent);
?>
<div class="content" align="center"> 
  <!-- main content -->
  <div style="width:100%;margin-top:35px;" align="center"> 
    
    <!-- title -->
    <div align="center" style="font-size:30px;font-family: Helvetica; color:#ccc; margin-bottom:20px;">Post Your Event</div>
    <div align="center" style="margin-top:20px; border-bottom:1px solid #ccc; height:1px; width:940px; margin-bottom:40px;"></div>
    <!-- end here --> 
    
    <!-- multistep form -->
    <form id="msform">
      <!-- progressbar -->
      <ul id="progressbar">
        <li class="active">Organization Details</li>
        <li>Event Details</li>
        <li>Create Tickets</li>
        <li>Preview</li>
      </ul>
      <!-- fieldsets -->
      <fieldset>
        <input type="hidden" value="<?php echo $resultEvent['orgId']; ?>" id="org_id" />
        <div align="left">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Organization name<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="org_name" id="org_name" style="width:715px;" value="<?php echo $resultEvent['orgName'] ?>"/>
          </div>
        </div>
        <div align="left" style="margin-top:15px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Description</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <textarea name="org_desc" id="org_desc" style="width:715px; height:100px; resize:none;"><?php echo $resultEvent['orgDesc'] ?></textarea>
          </div>
        </div>
        <div align="left" style="margin-top:15px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Organizer contact</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="org_email" id="org_email" style="width:340px;" placeholder="Enter Email" value="<?php echo $resultEvent['orgEmail'] ?>"/>
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="org_contact_no" id="org_contact_no" style="width:340px; margin-left:15px;" placeholder="Enter Contact Number" value="<?php echo $resultEvent['orgContact'] ?>"/>
          </div>
        </div>
        <div align="left" style="margin-top:15px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="org_fax" id="org_fax" style="width:340px;" placeholder="Enter Fax" value="<?php echo $resultEvent['orgFax'] ?>"/>
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="org_website" id="org_website" style="width:340px; margin-left:15px;" placeholder="Enter Website" value="<?php echo $resultEvent['orgWebsite'] ?>"/>
          </div>
        </div>
        <input type="button" name="next" class="NewNext" value="Next" style="margin-left:200px;background-color: #ed258f;border: 1px solid #ed258f; cursor:pointer;" attrNum="first"/>
        <!--<input type="button" value="Save as draft" class="draft"/>-->
        <input type="button" value="Cancel" class="cancel" onclick="javascript:location.reload()" style="background-color: #ed258f;border: 1px solid #ed258f; cursor:pointer;"/>
        <div style="margin-top:100px; display:none; color:#727272; font-size:14px;font-family: Helvetica;" align="center" class="error"></div>
      </fieldset>
      <fieldset>
        <div align="left" style="margin-top:65px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Name of your event<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="event_name" id="event_name" style="width:715px;" data-maxsize="65"/>
          </div>
        </div>
        <div align="left" style="margin-top:10px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle; color:#727272;font-family: Helvetica; font-size:16px;">Type of event<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;border:1px solid #ccc; border-radius:20px; padding-left:10px; height:35px;">
            <select name="timepass" class="custom-select" id="event_type"  style="margin-left:-10px;padding: 7px;">
              <option style="font-size:14px;">Select</option>
              <?php while($result=mysqli_fetch_assoc($queryType)){ ?>
              <option style="font-size:14px;"><?php echo $result['name']; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div align="left" style="margin-top:20px; height:45px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle; color:#727272;font-family: Helvetica; font-size:16px;">Date<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" style="width:200px; margin-right:10px;" id="datepick" readonly="readonly"/>
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle; padding-left:75px; color:#727272"><span style="padding-right:10px;">Time<span style="color:#f00">*</span></span>
            <input type="text" style="width:100px; margin-right:10px;" id="datepicker_time1" value="00:00" readonly="readonly"/>
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle;padding-left:55px; color:#727272"><span style="padding-right:10px;">To<span style="color:#f00">*</span></span>
            <input type="text" style="width:100px; margin-right:10px;" id="datepicker_time2" value="00:00" readonly="readonly"/>
          </div>
        </div>
        <div align="left">
          <div align="left" style="display:table-cell; vertical-align:middle; padding-left:570px; color:#727272; font-size:14px;">Start Time</div>
          <div align="left" style="display:table-cell; vertical-align:middle;padding-left:160px;color:#727272; font-size:14px;">End Time</div>
        </div>
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Location name<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="event_location" id="event_location" style="width:715px;"/>
          </div>
        </div>
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Address<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <textarea name="event_address" id="event_address" style="width:715px; height:92px; resize:none;"></textarea>
          </div>
        </div>
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Zip code</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="zip_code" id="zip_code" style="width:215px;"/>
            <span style="margin-left:20px;">
            <input type="checkbox" style="margin-right:20px;" onchange="includeMap()" id="mapCheck" name="mapCheck" />
            <span style="color:#727272">Include Map</span></span></div>
        </div>
        <div align="left" style="margin-top:20px; display:none;" id="include_map">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Generate Map<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell;vertical-align:middle;">
            <input type="text" style="width:600px;" id="address" placeholder="Location name (e.g : Los Angeles, CA, United States)"/>
          </div>
          <div align="left" style="display:table-cell; padding-left:17px;">
            <input type="button" style="width:100px; padding:0px; height:35px; color:#fff; font-size:16px; background-color:#ed258f; border:1px solid #ed258f;font-family: Helvetica; position:absolute; cursor:pointer;" value="Done" onclick="codeAddress()" />
          </div>
        </div>
        <div align="left" style="margin-top:20px; display:none;" id="map_canvas_wrapper">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div align="left" style="display:table-cell;vertical-align:middle; width:739px; height:300px;">
            <div id="map-canvas" style="height:300px; width:739px;"></div>
          </div>
        </div>
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Event description</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <textarea style="width:715px; height:100px; resize:none;" id="event_desc" name="event_desc"></textarea>
          </div>
        </div>
        <!--<div align="left" style="margin-top:20px;">
                    	<div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Event image</div>
                        <div align="left" style="vertical-align:middle; display:none;" id="event_image_wrapper"></div>
                        <div align="left" style="display:table-cell; vertical-align:middle;"><input type="button" style="width:200px; padding:0px; height:40px; color:#fff; font-size:16px; background-color:#ed258f; border:1px solid #ed258f;font-family: Helvetica; position:absolute; cursor:pointer;" value="Upload file" /><input type="file" style="position:relative; top:0; right:0; z-index:2; padding-top:0px; padding-left:0px;opacity:0;height:25px;width:190px;cursor:pointer;border:none" id="fileupload-image" multiple/></div>
                    </div>
                    <div align="left" style="margin-top:10px;">
                    	<div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
                        <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:12px;">Upload any file with .jpg, .jpeg, .png format. Image size not be more than 10MB. You can upload max 5 imgaes for a particular event. Please upload only square image.</div>
                    </div>-->
        
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Event image</div>
          <div align="left" style="vertical-align:middle; display:none;" id="event_image_wrapper_cover"><img src="" id="event_image_cover" height="60" style="border: 1px solid #ccc;"/><img src="https://ufundoo.com/assets/img/status-remove.png" style="text-align:right; vertical-align:top; position:relative; right:10px; top:-10px; cursor:pointer; padding-right:20px;" class="removedImageIcon" id="deleteCover"></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="button" style="width:200px; padding:0px; height:40px; color:#fff; font-size:16px; background-color:#ed258f; border:1px solid #ed258f;font-family: Helvetica; position:absolute; cursor:pointer;" value="Upload file" />
            <input type="file" style="position:relative; top:0; right:0; z-index:2; padding-top:0px; padding-left:0px;opacity:0;height:25px;width:190px;cursor:pointer;border:none" id="fileupload-image-cover"/>
          </div>
        </div>
        <div align="left" style="margin-top:10px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:12px;">Upload any file with .jpg, .jpeg, .png format. You can upload max 1 imgaes for a particular event. Image size must be atleast 700x200.</div>
        </div>
        <div align="left" style="margin-top:20px;">
          <div style="width:180px; text-align:right; padding-right:20px;display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px; padding-top:12px;">Event video</div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="event_video" id="event_video" style="width:715px;" placeholder="e.g : https://www.youtube.com/watch?v=eu7k0kSvnUo"/>
          </div>
        </div>
        <!--<div align="left" style="margin-top:20px;">
                    	<div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Event seating chart</div>
                        <div align="left" style="vertical-align:middle; display:none;" id="event_seat_chart_wrapper"><img src="" id="event_seat_chart" height="60" style="border: 1px solid #ccc;" /><img src="https://ufundoo.com/assets/img/status-remove.png" style="text-align:right; vertical-align:top; position:relative; right:10px; top:-10px; cursor:pointer; padding-right:20px;" class="removedImageIcon" id="deleteSeatChart"></div>
                        <div align="left" style="display:table-cell; vertical-align:middle;"><input type="button" style="width:200px; padding:0px; height:40px; color:#fff; font-size:16px; background-color:#ed258f; border:1px solid #ed258f;font-family: Helvetica; position:absolute; cursor:pointer;" value="Upload file" /><input type="file" style="position:relative; top:0; right:0; z-index:2; padding-top:0px; padding-left:0px;opacity:0;height:25px;width:190px;cursor:pointer;border:none" id="fileupload-seat-chart"/></div>
                    </div>
                    <div align="left" style="margin-top:10px;">
                    	<div style="width:180px; text-align:right; padding-right:20px;display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
                        <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:12px;">Upload any file with .jpg, .jpeg, .png format. Image size not be more than 100MB. You can upload max 1 image of seating chart for a particular event.</div>
                    </div>--> 
        
        <!-- <div align="left" style="margin-top:20px;">
                    	<div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Featured Image</div>
                        <div align="left" style="vertical-align:middle; display:none;" id="event_image_wrapper_featured"><img src="" id="event_image_featured" height="60" style="border: 1px solid #ccc;"/><img src="https://ufundoo.com/assets/img/status-remove.png" style="text-align:right; vertical-align:top; position:relative; right:10px; top:-10px; cursor:pointer; padding-right:20px;" class="removedImageIcon" id="deleteFeatured"></div>
                        <div align="left" style="display:table-cell; vertical-align:middle;"><input type="button" style="width:200px; padding:0px; height:40px; color:#fff; font-size:16px; background-color:#ed258f; border:1px solid #ed258f;font-family: Helvetica; position:absolute; cursor:pointer;" value="Upload file" /><input type="file" style="position:relative; top:0; right:0; z-index:2; padding-top:0px; padding-left:0px;opacity:0;height:25px;width:190px;cursor:pointer;border:none" id="fileupload-image-featured"/></div>
                    </div>
                    <div align="left" style="margin-top:10px;">
                    	<div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
                        <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:12px;">Upload any file with .jpg, .jpeg, .png format. You can upload max 1 imgaes for a particular event. Image size must be atleast 1200x445.</div>
                    </div>-->
        <input type="button" name="previous" class="previous" value="Previous" />
        <input type="button" name="next" class="NewNext" value="Next" attrNum="second" />
        <!--<input type="button" value="Save as draft" class="draft"/>-->
        <input type="button" value="Cancel" class="cancel" onclick="javascript:location.reload()" style="background-color: #ed258f;border: 1px solid #ed258f; cursor:pointer;"/>
        <div style="margin-top:100px; display:none; color:#727272; font-size:14px;font-family: Helvetica;" align="center" class="error"></div>
      </fieldset>
      <fieldset>
        <div align="left" style="margin-top:65px;">
          <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:15px; width:200px;">
            <input type="radio" style="margin-right:10px;" name="ticket_type" value="1" class="ticket_type" onchange="freeTicket(this)"/>
            Paid Ticket</div>
          <div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:15px;width:200px;">
            <input type="radio" style="margin-right:10px;" name="ticket_type" value="2" class="ticket_type" onchange="freeTicket(this)" checked="checked" />
            Free Ticket</div>
			<div align="left" style="display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:15px;width:200px;">
            <input type="radio" style="margin-right:10px;" name="ticket_type" value="3" class="ticket_type"  onchange="freeTick(this)" />
            Promotions only</div>
        </div>
		
		<div id="urlWrapper" align="left" style="margin-top:15px; display:none;"> </div>
	  <div align="left" style="margin-top:15px; display:none" id="url">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">URL<span style="color:#f00"></span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="url" id="urllink" class="ticket_price" style="width:215px;" />
          </div>
        </div>
		
		
        
        <div align="left" style="margin-top:15px; display:none" id="ticket_price_wrapper_single">
		  <div align="left" style="margin-top:15px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Ticket name<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="ticket_name" id="ticket_name_1" class="ticket_name check_rows" style="width:215px;"/>
          </div>
        </div>
        <div align="left" style="margin-top:15px;">
          <div style="width:180px; text-align:right; padding-right:20px;display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Quantity available<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="qty_ava" id="qty_ava_1" style="width:215px;" onkeypress="return isNumberKey(event)" class="qty_ava"/>
          </div>
        </div>
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Price<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="ticket_price" id="ticket_price_1" class="ticket_price" style="width:215px;" onkeypress="return isNumberKeyFloat(event)"/>
          </div>
        </div>
		
		
		
		 <div align="left" style="margin-top:15px; display:none" id="ticket_price_wrapper_second">
		  <div align="left" style="margin-top:15px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Ticket name<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="ticket_name" id="ticket_name_1" class="ticket_name check_rows" style="width:215px;"/>
          </div>
        </div>
        <div align="left" style="margin-top:15px;">
          <div style="width:180px; text-align:right; padding-right:20px;display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Quantity available<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" name="qty_ava" id="qty_ava_1" style="width:215px;" onkeypress="return isNumberKey(event)" class="qty_ava"/>
          </div>
        </div>
		
        </div>
		
		
		
		
		
		
		
		
		
		
		
        <div id="multiTicketWrapper" align="left" style="margin-top:15px; display:none;"> </div>
        <div align="left" style="margin-top:15px; height:30px; display:none;" id="multi_ticket">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
          <div style="width:180px; text-align:right; padding-right:20px;display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"><span style="color:#727272; font-size:14px;font-family: Helvetica; text-decoration:underline; cursor:pointer; " onclick="addMultiTicket()">Add more ticket</span></div>
           <div align="left" style="margin-top:15px; height:45px;">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle; color:#727272;font-family: Helvetica; font-size:16px;">Ticket sales timing<span style="color:#f00">*</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" style="width:100px; margin-right:10px;" id="datepick1" readonly="readonly"/>
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle; padding-left:50px; color:#727272">
            <input type="text" style="width:100px; margin-right:10px;" id="datepicker_time3" value="00:00" readonly="readonly"/>
            <span style="padding-right:20px; padding-left:55px;">to</span></div>
          <div align="left" style="display:table-cell; vertical-align:middle;">
            <input type="text" style="width:100px; margin-right:10px;" id="datepick2" readonly="readonly"/>
          </div>
          <div align="left" style="display:table-cell; vertical-align:middle;padding-left:50px; color:#727272">
            <input type="text" style="width:100px; margin-right:10px;" id="datepicker_time4" value="00:00" readonly="readonly"/>
          </div>
        </div>
		</div>
		
		
		
		
		

        <input type="button" name="previous" class="previous" value="Previous"/>
        <input type="button" name="next" class="NewNext" value="Next" attrNum="third"/>
        <!--<input type="button" value="Save as draft" class="draft"/>-->
        <input type="button" value="Cancel" class="cancel" onclick="javascript:location.reload()" style="background-color: #ed258f;border: 1px solid #ed258f; cursor:pointer;"/>
        <div style="margin-top:100px; display:none; color:#727272; font-size:14px;font-family: Helvetica;" align="center" class="error"></div>
      </fieldset>
	  
	  
		
		
        <div align="left" style="margin-top:15px; height:30px; display:none;" id="url_ticket">
          <div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;"></div>
        </div>
		
		
      <fieldset>
        <div align="left" style="margin-top:65px; margin-left:200px; color:#727272; font-size:15px;">Submit your event and it will be publish soon.</div>
        <input type="button" name="previous" class="previous" value="Previous" style="background-color: #ed258f;border: 1px solid #ed258f; cursor:pointer;"/>
        <input type="submit" name="submit" class="submit" value="Submit" onclick="submitEvent('<?php echo $_REQUEST['page'];?>')" />
        <!--<input type="button" value="Save as draft" class="draft"/>-->
        <input type="button" value="Cancel" class="cancel" onclick="javascript:location.reload()" style="background-color: #ed258f;border: 1px solid #ed258f; cursor:pointer;"/>
        <div style="margin-top:100px; display:none; color:#727272; font-size:14px;font-family: Helvetica;" align="center" class="error"></div>
      </fieldset>
      <?php if($_REQUEST['page']=="normal") { ?>
      <input type="hidden" value="<?php echo $_SESSION['userId']; ?>" id="userId" />
      <?php } else { ?>
      <input type="hidden" value="<?php echo $_SESSION['promoterId']; ?>" id="userId" />
      <?php } ?>
    </form>
  </div>
  <!-- end here --> 
</div>
<script>
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(document).ready(function(e) {
   $('.btn-admin').eq(0).trigger('click');
	if($(window).width()<1200)
	{
		$('.logout').css({'right':'-8px'});
	}
	$(function() {
				$( "#datepick" ).datepicker({
				  minDate: 0,
				  showOn: "button",
				  buttonImage: "https://ufundoo.com/assets/img/calander.png",
				  buttonImageOnly: true,
				  dateFormat: 'yy-mm-dd',
				  onSelect: function (date) {
						var dt2 = $('#datepick2');
						var maxDate = $(this).datepicker('getDate');
						dt2.datepicker('option', 'maxDate', maxDate);
					}
			});
  }); 
   $(function() {
				$( "#datepick1" ).datepicker({
				  minDate: 0,
				  showOn: "button",
				  buttonImage: "https://ufundoo.com/assets/img/calander.png",
				  buttonImageOnly: true,
				  dateFormat: 'yy-mm-dd',
				  onSelect: function (date) {
						var dt2 = $('#datepick2');
						var minDate = $(this).datepicker('getDate');
						dt2.datepicker('option', 'minDate', minDate);
					}
			});
  });
  $(function() {
				$( "#datepick2" ).datepicker({
				  showOn: "button",
				  buttonImage: "https://ufundoo.com/assets/img/calander.png",
				  buttonImageOnly: true,
				  dateFormat: 'yy-mm-dd'
			});
  });
  $(function() {
				$( "#datepicker_time1" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  }); 
  $(function() {
				$( "#datepicker_time2" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  });
  $(function() {
				$( "#datepicker_time3" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  }); 
  $(function() {
				$( "#datepicker_time4" ).timepicker({
				  showOn: "button",
				  buttonImage: "../assets/img/clock.png",
				  buttonImageOnly: true,
			});
	  
  });
  $(".NewNext").click(function(){
	  
	
	  	var rangQty=true;
	  	var rangPrice=true;
	if($(this).attr('attrNum')=='first')
	{
		if($('#org_name').val()=='')
		{
			$('.error').html('Please Enter Organizaton Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#org_name').focus();
			return false;
		}
		var userEmail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; // test for valid email id format
		
		if( !userEmail.test( $('#org_email').val())) 
		{
			$('.error').html('Invalid Email Address').fadeIn().delay( 4000 ).fadeOut();
			$('#org_email').focus();
			return false;
		}
	}
	else if($(this).attr('attrNum')=='second')
	{
		if($('#event_name').val()=='')
		{
			$('.error').html('Please Enter Event Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_name').focus();
			return false;
		}
		if($('#event_type').val()=='Select')
		{
			$('.error').html('Please Select Event Type').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_type').focus();
			return false;
		}
		if($('#event_location').val()=='')
		{
			$('.error').html('Please Enter Event Location').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_location').focus();
			return false;
		}
		if($('#event_address').val()=='')
		{
			$('.error').html('Please Enter Event Address').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#event_address').focus();
			return false;
		}
		if($('#datepick').val()=='')
		{
			$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepick').focus();
			return false;
		}
		if($('#datepicker_time1').val()=='')
		{
			$('.error').html('Please Select Start Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time1').focus();
			return false;
		}
		if($('#datepicker_time2').val()=='')
		{
			$('.error').html('Please Select End Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time2').focus();
			return false;
		}
		var checkedValueMap=document.getElementById("mapCheck").checked;
		if(checkedValueMap)
		{
			if($('#address').val()=='')
			{
				$('.error').html('Please Enter Address To Generate Map').fadeIn().delay( 4000 ).fadeOut(); // empty username
				$('#address').focus();
				return false;
			}
		}	
	}
	else
	{
		var flagQty=true;
		$('.qty_ava').each(function(index, element) {
			if($(this).val()=='')
			{
				flagQty=false;
			}
		});
		if(!flagQty)
		{
			$('.error').html('Quantity must be minimum 1').fadeIn().delay( 4000 ).fadeOut(); // empty username
			return false;
		}
		
		var flagName=true;
		$('.ticket_name').each(function(index, element) {
			if($(this).val()=='')
			{
				flagName=false;
			}
		});
		if(!flagName)
		{
			$('.error').html('Please Enter Ticket Name').fadeIn().delay( 4000 ).fadeOut(); // empty username
			return false;
		}
		if($('#ticket_price_wrapper_single').css('display')!="none")
		{
			var flagPrice=true;
			$('.ticket_price').each(function(index, element) {
				if($(this).val()=='')
				{
					flagPrice=false;
				}
			});
			if(!flagPrice)
			{
				$('.error').html('Please Enter Ticket Price').fadeIn().delay( 4000 ).fadeOut(); // empty username
				return false;
			}
		}
		
		$('.qty_ava').each(function(index, element) {
			if($(this).val() <= 100000 && $(this).val() >= 10)
				{
					rangQty=true;
				}
				else
				{
					rangQty=false;
				}
		});
		if(!rangQty)
		{
			$('.error').html('Quantity must be between 10 to 100000').fadeIn().delay( 4000 ).fadeOut(); // empty username
			return false;
		}
	
		if($('#ticket_price_wrapper_single').css('display')!="none")
		{
			$('.ticket_price').each(function(index, element) {
				if($(this).val() <= 5000 && $(this).val() >= 1)
				{
					rangPrice=true;
				}
				else
				{
					rangPrice=false;
				}
			});
			if(!rangPrice)
			{
				$('.error').html('Price must be between $1 to $5000').fadeIn().delay( 4000 ).fadeOut(); // empty username
				return false;
			}
		}
	
		if($('#datepick1').val()=='')
		{
			$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepick1').focus();
			return false;
		}
		if($('#datepick2').val()=='')
		{
			$('.error').html('Please Select Date').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepick2').focus();
			return false;
		}
		if($('#datepicker_time3').val()=='')
		{
			$('.error').html('Please Select Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time3').focus();
			return false;
		}
		if($('#datepicker_time4').val()=='')
		{
			$('.error').html('Please Select Time').fadeIn().delay( 4000 ).fadeOut(); // empty username
			$('#datepicker_time4').focus();
			return false;
		}
	}
	

	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'transform': 'scale('+scale+')'});
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});

});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
});
});
$(".custom-select").each(function(){
            //$(this).wrap("<span class='select-wrapper' style='width:115px;'></span>");
            //$(this).after("<span class='holder'></span>");
        });
        $(".custom-select").change(function(){
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        }).trigger('change');
		
function typeTicket(button)
{
	$(button).addClass('button_event_upload_color');
	$(button).removeClass('button_event_upload');
	$(button).parent('div').siblings('div').children('div').addClass('button_event_upload');
	$(button).parent('div').siblings('div').children('div').removeClass('button_event_upload_color');
}

var uploadButton='';
var appendImgPath='';
var appendPath='';
var countNoOfFiles=0;
/* for images */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '../fileupload/server/php/';
	$('#fileupload-image').each(function () {
		$(this).on('click', function () {
			uploadButton = $(this).parent().parent().attr('id');
			//alert(uploadButton);
		});
    $(this).fileupload({
        url: url,
        dataType: 'json',
        maxFileSize: 100000000, // 5 MB
		maxNumberOfFiles: 5,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
		$(".popup-loader").fadeIn();
	}).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        //$('#progress .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
		$(".popup-loader").fadeOut();
		$('#event_image_wrapper').css('display','table-cell');
        $.each(data.result.files, function (index, file) {
		appendImgPath='';
		if(countNoOfFiles>=5)
		{
			alert("You can upload max 5 imgaes for a particular event.");
		}
		else
		{
			countNoOfFiles+=1;
			appendImgPath=('<div style="margin-right:0px; margin-right:10px; margin-bottom:10px;float:left"><img src="'+file.thumbnailUrl+'" height="60" style="border:1px solid #ccc;" /><img src="https://ufundoo.com/assets/img/status-remove.png" style="text-align:right; vertical-align:top; position:relative; right:10px; top:-10px; cursor:pointer;" onClick="deleteUploadedImage(this,\''+file.url+'\',\''+file.deleteUrl+'\')" class="removedImageIcon"></div>');
			appendPath+=file.url+'$';
		}
		});
		$('#event_image_wrapper').append(appendImgPath);
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
			console.log(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
});

function deleteUploadedImage(imagePos,fileUrl,deleteUrl)
{
	    countNoOfFiles-=1;
		$(imagePos).parent('div').remove();
		var str1 = appendPath;
		var str2 = fileUrl;
		appendPath = str1.replace(str2+'$','');	
		
	$.ajax({
    	dataType: 'json',
        type: "DELETE",
        url: deleteUrl,
    }).done(function (data) {
    	console.log(data);
	});
}

function deleteUploadedImageSingle(imagePos,deleteUrl,type)
{
	if(type=="video")
	{
		$(imagePos).siblings('video').attr('src','');
	}
	else
	{
		$(imagePos).siblings('img').attr('src','');
	}
	$(imagePos).parent('div').css('display','none');
	$.ajax({
    	dataType: 'json',
        type: "DELETE",
        url: deleteUrl,
    }).done(function (data) {
    	console.log(data);
	});
}

/* for seating chart image */
/*
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '../fileupload/server/php/';
	$('#fileupload-seat-chart').each(function () {
		$(this).on('click', function () {
			uploadButton = $(this).parent().parent().attr('id');
			//alert(uploadButton);
		});
    $(this).fileupload({
        url: url,
        dataType: 'json',
        maxFileSize: 100000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
		$(".popup-loader").fadeIn();
	}).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        //$('#progress .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
		$(".popup-loader").fadeOut();
		$('#event_seat_chart_wrapper').css('display','table-cell');
		$('#event_seat_chart').attr('src',file.url);
		$('#deleteSeatChart').attr('onClick','deleteUploadedImageSingle(this,\''+file.deleteUrl+'\',"image")');
		});
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
			console.log(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
});
*/

/* for cover image */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '../fileupload/server/php/';
	$('#fileupload-image-cover').each(function () {
		$(this).on('click', function () {
			uploadButton = $(this).parent().parent().attr('id');
			//alert(uploadButton);
		});
    $(this).fileupload({
        url: url,
        dataType: 'json',
        maxFileSize: 100000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
		$(".popup-loader").fadeIn();
	}).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        //$('#progress .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
		$(".popup-loader").fadeOut();
		$('#event_image_wrapper_cover').css('display','table-cell');
		$('#event_image_cover').attr('src',file.url);
		$('#deleteCover').attr('onClick','deleteUploadedImageSingle(this,\''+file.deleteUrl+'\',"image")');
		});
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
			console.log(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
});

/* featured image */
/*
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = '../fileupload/server/php/';
	$('#fileupload-image-featured').each(function () {
		$(this).on('click', function () {
			uploadButton = $(this).parent().parent().attr('id');
			//alert(uploadButton);
		});
    $(this).fileupload({
        url: url,
        dataType: 'json',
        maxFileSize: 100000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
		$(".popup-loader").fadeIn();
	}).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        //$('#progress .progress-bar').css('width',progress + '%');
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
		$(".popup-loader").fadeOut();
		$('#event_image_wrapper_featured').css('display','table-cell');
		$('#event_image_featured').attr('src',file.url);
		$('#deleteFeatured').attr('onClick','deleteUploadedImageSingle(this,\''+file.deleteUrl+'\',"image")');
		});
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
			console.log(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	});
});
*/

function includeMap()
{
	$('#address').val('');
	var checkedValue=document.getElementById("mapCheck").checked;
	if(checkedValue)
	{
		$('#include_map').fadeIn();
		var locationVal=$('#event_location').val();
		$('#address').val(locationVal);
		codeAddress();
	}
	else
	{
		$('#include_map').fadeOut();
		$('#map_canvas_wrapper').fadeOut();
	}
}

function freeTicket(type)
{
	
	if($(type).val()==2)
	{
		$('#ticket_price_wrapper_second').fadeIn();
		$('#multi_ticket').show();
		$('#multiTicketWrapper').css('display','table-cell');
		
		
		
	}
		
	else
	{
		$('#ticket_price_wrapper_single').fadeIn();
		$('#multi_ticket').show();
		$('#multiTicketWrapper').css('display','table-cell');
	}
}
function freeTick(type)
{
	console.log($(type).val());
        $('#url').fadeIn();
		$('#url_ticket').show();
		$('#urlWrapper').css('display','none');

}	
	


var id=1;
function addMultiTicket()
{
	$('#ticket_price_wrapper_single').css('margin-bottom','10px');
	$('#multiTicketWrapper').css('display','table-cell');
	++id;
	$('#multiTicketWrapper').append('<div style="border-top:1px solid #ccc; width:940px; padding-top:5px;"><div align="left" style="margin-top:15px;"><div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Ticket name<span style="color:#f00">*</span></div><div align="left" style="display:table-cell; vertical-align:middle;"><input type="text" name="ticket_name" id="ticket_name_'+id+'" class="ticket_name check_rows" style="width:215px;"/></div></div><div align="left" style="margin-top:15px;"><div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Quantity available<span style="color:#f00">*</span></div><div align="left" style="display:table-cell; vertical-align:middle;"><input type="text" name="qty_ava" id="qty_ava_'+id+'" style="width:215px;" onkeypress="return isNumberKey(event)" class="qty_ava"/></div></div><div align="left" style="margin-top:15px; margin-bottom:10px;" class="ticket_price_wrapper"><div style="width:180px; text-align:right; padding-right:20px; display:table-cell; vertical-align:middle;color:#727272;font-family: Helvetica; font-size:16px;">Price<span style="color:#f00">*</span></div><div align="left" style="display:table-cell; vertical-align:middle;"><input type="text" name="ticket_price" id="ticket_price_'+id+'" class="ticket_price" style="width:215px;" onkeypress="return isNumberKeyFloat(event)"/></div></div></div>');
	
}

/* load map */
var geocoder;
var map;
function initialize() {
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-34.397, 150.644);
  var mapOptions = {
    zoom: 10,
    center: latlng
  }
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
}

function codeAddress() {
	if($('#address').val()=='')
	{
		$('.error').html('Please Enter Address To Generate Map').fadeIn().delay( 4000 ).fadeOut(); // empty username
		$('#address').focus();
		return false;
	}
	else
	{
	$('#map_canvas_wrapper').fadeIn();
	initialize();
	var address = document.getElementById('address').value;
  	geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location
      });
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
  }
}

</script>