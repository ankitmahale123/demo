<?php
	session_start();
	require_once("config/conn.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UFundoo</title>
<link rel="shortcut icon" href="assets/img/favicon.png" type="image/png"/>
<link rel="stylesheet" href="assets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="assets/css/ufundoo.css" type="text/css" />
<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="assets/js/jquery-ui.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/ufundoo.js"></script>
<style>
.headerBtn {
	color: #2e302d;
}
.description {
	font-size:16px;
	padding: 0 15px;
}
h3, .h3 {
    font-size: 20px;
}
</style>
<script>
$(document).keypress(function(e) {
  	if (e.keyCode == 13 && !e.shiftKey) { //submit on enter key
    e.preventDefault();
	if($("#signup-container").css('display')=="block")
	{
		signUp();
	}
	else if($("#login-container").css('display')=="block")
	{
		signIn();
	}
	else
	{
		feedback();
	}
}});
</script>
</head>

<body>
<!-- loader -->
<div class="loading" style="display:none">
  <?php 
    	include('loader.php');
    ?>
</div>
<!-- end here --> 

<!-- forgot password -->
<div id="forgot_password">
  <div class="admin-popup-delete-container">
    <div class="admin-delete-class" style="width:400px;"> <img src="assets/img/btn_close.png" id="logo-close" onclick="fp_close()" height="20" width="20" style="top:10px; right:10px;" />
      <div style="text-align:center; margin-top:35px; font-size:20px; font-weight:bold">Please provide email address</div>
      <div style="text-align:center; margin-top:20px; font-family: calibri;">
        <input type="text" style="" placeholder="Email" class="fp_text" />
      </div>
      <div style="margin-top:15px; width:250px; margin-left:auto; margin-right:auto;">
        <div id="yes-btn" class="admin-btn" style="float:left; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="forgot_password()">Ok</span> </div>
        <div id="no-btn" class="admin-btn" style="float:right; width:100px; height:40px; border:1px solid #ed258f; background:#ed258f; border-radius:20px; color:#fff;text-align: center;padding-top: 7px; font-size:17px;cursor:pointer"> <span onclick="fp_close()">Cancel</span> </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- signup prompt -->
<div id="signup-container">
  <div class="signup-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeSignup()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Join UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:25px; height:395px;">
        <div style="height:55px;width:100%;">
          <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; float:left; outline:none" placeholder="First Name" class="txtFirstName" />
          <input type="text" style="width:200px; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; margin-left:20px; float:left; outline:none" placeholder="Last Name" class="txtLastName" />
        </div>
        <input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="username" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Password (Atleast 4 characters)" class="password" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Confirm Password" class="confirm_password" />
        <div style="margin-bottom:10px; width:370px; text-align:left"> <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15"/><span style="margin-top:-3px;">I agree to Ufundoo's term of use and privacy policy.</span> </div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUp()" />
      </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
        <div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Already member ?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;">
          <input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Sign In"  onclick="openLogin()"/>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- login prompt -->
<div id="login-container">
  <div class="login-container-wrapper">
    <div class="admin-delete-class"> <img src="assets/img/btn_close.png" id="logo-close" onclick="closeLogin()" height="20" width="20" />
      <div style="text-align:center; margin-top:30px; font-size:35px; font-weight:bold">Login UFUNDOO</div>
      <div align="center" style="margin-left:50px; margin-right:50px; margin-top:15px; height:405px;">
        <input type="text" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmail" />
        <input type="password" style="width:100%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpassword" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:25px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:8px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signIn()"/>
        <div style="margin-bottom:10px; width:370px; height:15px;"> <span style="">OR</span> </div>
        <a href="googleLogin1.php?status=user" style="text-decoration:none;color:white">
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" />
        </a> <span onclick="FBLogin('user');">
        <input type="button" style="width:100%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" />
        </span> </div>
      <div style="height:2px; background:#bdbdbd; width:100%;"></div>
      <div style="width:555px; margin-left:20px;">
        <div style="float:left; width:50%;height:auto; text-align:center; font-size:20px; margin-top:18px;">Not a member yet?</div>
        <div style="float:left; width:240px;height:auto; margin-top:15px;">
          <input type="button" style="width:70%; height:45px; font-size:20px;background:#fff; color:#ed2590; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join Now"  onclick="openSignup()" />
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end here --> 

<!-- signup prompt -->
<div id="promoter-container">
  <div class="promoter-container-wrapper">
    <div class="admin-delete-class-promoter"><img src="assets/img/btn_close.png" id="logo-close" onclick="closePromoter()" height="20" width="20" /> 
      
      <!-- login -->
      <div style="float:left; width:50%; height:520px; border-right:2px solid #ccc; margin-top:25px; text-align:center;">
        <div style="text-align:center;font-size:35px; font-weight:bold">Already a Promoter?</div>
        <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Login</div>
        <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; outline:none" placeholder="Email" class="txtLoginmailPost" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; outline:none" placeholder="Password (Atleast 4 characters)" class="txtLoginpasswordPost" />
        <div align="right" style="margin-bottom:5px; cursor:pointer; margin-right:75px;" onclick="fp_open()">Forgot Password?</div>
        <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:5px; border:1px solid #ed2590; outline:none" value="Login"  onclick="signInPost()"/>
        <!--<div style="margin-bottom:10px;height:15px;">
               <span style="">OR</span>
            </div>
            <a href="googleLogin1.php?status=promoter" style="text-decoration:none;color:white"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Google" /></a>
            <span onclick="FBLogin('promoter');"><input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Login with Facebook" /></span>--> 
      </div>
      <!-- end here --> 
      
      <!-- sign up -->
      <div style="float:left; width:50%; height:520px; margin-top:25px; text-align:center">
        <div style="text-align:center;font-size:35px; font-weight:bold">Become a Promoter?</div>
        <div style="text-align:center; font-size:35px; font-weight:bold; margin-bottom:25px;">Sign Up</div>
        <div>
          <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Firstname" class="firstname" />
          <input type="text" style="width:38%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none; margin-left:4%;" placeholder="Lastname" class="lastname" />
        </div>
        <input type="text" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Email" class="username_post" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:10px; outline:none" placeholder="Password (Atleast 4 characters)" class="password_post" />
        <input type="password" style="width:80%; height:55px; font-size:20px; border:1px solid #bebebe; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:15px; outline:none" placeholder="Confirm Password" class="confirm_password_post" />
        <div style="margin-bottom:10px; width:75%; margin-left:auto; margin-right:auto"> <img src="assets/img/unchecked.png" name="fainMail" onclick="check_single(this)" status="0" class="fainMail_post" style="cursor: pointer; float: left; margin-top:3px;" height="15" id="fainMail"/>Send me FainMail on everything events and entertainment. </div>
        <div style="margin-bottom:10px; width:70%; margin-left:64px; text-align:left"> <img src="assets/img/unchecked.png" name="agree" onclick="check_single(this)" status="0" class="agree_post" style="cursor: pointer; float: left; margin-top:3px; margin-right:5px;" height="15" id="agree"/>I agree to Ufundoo's term of use and privacy policy. </div>
        <input type="button" style="width:80%; height:55px; font-size:20px;background:#ed2590; color:#fff; padding:10px; border-radius:35px; padding-left:20px; padding-right:20px; margin-bottom:20px; border:1px solid #ed2590; outline:none" value="Join" onclick="signUpPost()" />
      </div>
      <!-- end here --> 
      
    </div>
  </div>
</div>
<!-- end here --> 

<!-- header -->
<div class="header">
  <?php include('include/header.php'); ?>
</div>
<!-- end here --> 

<!-- wrapper -->
<div class="wrapper"> 
  
  <!-- container -->
  <div class="content"> 
    
    <!-- main content -->
    <div style="display:table; min-height:100px; height:auto; width:1024px; margin-top:30px; margin-left:auto; margin-right:auto; margin-bottom:30px;">
      <div style="display:table-row">
        <div style="display:table-cell; width:550px; height:auto; padding-left:25px;">
          <h3>Ticket Buying Process</h3>
          <div class="description">
            <p><b>How do you send the tickets to me?</b></br>
              At the completion of your transaction, Ufundoo will provide you with an E-Ticket receipt and you will need to show a printed copy of this E-Ticket receipt along with your Photo ID to collect your tickets at the venue. Please do carry a Photo ID along with you to the event to assure your admission and the admission of your guests.
              Unless otherwise noted, the E-Tickets are used as "Will Call" ticket purchases and you receive your actual physical tickets (and seat assignments) at the event box office itself.</p>
            <p><b>What forms of payment can I use to purchase tickets?</b></br>
              We accept Visa, MasterCard and Discover. For security purposes, we are unable to accept personal checks, money orders or cash. </p>
            <p><b>Since I am typing in personal information while buying the ticket I am worried about security. Is your process secure against abuse?</b></br>
              All transactions on Ufundoo take place in 128-bit encryption supported by Geotrust, Inc. So you are completely protected from abuse. Shopping on Ufundoo is just as safe as shopping on Amazon.com or any other trusted e-commerce site. </p>
            <p><b>What is the CVV Number?</b></br>
              Card Verification Value code. CVV is a new authentication procedure established by credit card companies to further efforts towards reducing fraud for internet transactions. It consists of requiring a card holder to enter the CVV number in at transaction time to verify that the card is on hand. The CVV code is a security feature for "card not present" transactions (e.g., Internet transactions), and now appears on most (but not all) major credit and debit cards. This new feature is a three or four digit code which provides a cryptographic check of the information embossed on the card. Therefore, the CVV code is not part of the card number itself.</p>
            <p>The CVV code helps ascertain that the customer placing the order actually possesses the credit/debit card and that the card account is legitimate. Each credit card company has its own name for the CVV code, but it functions the same for all major cards. (VISA refers to the code as CVV2, MasterCard calls it CVC2, and American Express calls it CID.)</p>
            <p>The back panel of most Visa/MasterCard cards contain the full 16-digit account number, followed by the CVV/CVC code. Some banks, though, only show the last four digits of the account number followed by the code. To aid in the prevention of fraudulent credit card use, we now require the 3 or 4 digit code on the back of your credit card. When you submit your credit card information your data is protected by Secure Socket Layer (SSL) technology certified by a digital certificate. </p>
            <p><b>Where is the CVV Number?</b></br>
              This number is printed on the back of your credit cards, around the signature area. It is the last 3 digits AFTER the credit card number in the signature area of the card. Click here for an example.</p>
          </div>
          <h3>Ticket Prices Explained</h3>
          <div class="description">
            <p>Ticket prices are based on the following: </p>
            <p>Face Value of a Ticket: Promoter, venue, or artist determines the face value of a ticket. EventCombo.com does not determine the face value of tickets. </p>
            <p>Service Fees: EventCombo.com's service fees cover the cost to fulfill your ticket request when you purchase the tickets online. This charge includes services like accepting your order, processing it and fulfilling it. It is applied to an entire order. Both the venue or promoter and EventCombo.com determine the charge on an event-by-event basis. In almost all cases, additional delivery prices may be charged based on the delivery method that you choose.</p>
          </div>
          <h3>Customer Service</h3>
          <div class="description">
            <p><b>I got to the ticket purchase order confirmation screen, but I did not get a confirmation e-mail. Did my sale go through?</b></br>
              Yes, if the confirmation page came up WITH AN ORDER NUMBER your sale was completed. If you did not get the confirmation e-mail, you may have inadvertently made a typo when entering your email address or your e-mail program may have filtered the confirmation e-mail into a "junk", "bulk", or "trash" folder. It all depends on the settings of your e-mail program. If the email address is right, you should receive a confirmation e-mail within a few seconds after placing the order. If you do not receive a confirmation e-mail and YOU HAVE AN ORDER NUMBER, you may <a target="_blank" href="https://www.ufundoo.com/callUs.php">Contact Us</a>. </p>
            <p><b>Who handles refunds?</b></br>
              The decision of whether or not to allow refunds is made completely and solely by the venue or promoter. EventCombo.com assumes no responsibility for making any such decision, and will have no responsibility to issue refunds. However, if a refund is issued, then it will be EventCombo.com who will process refunds for those tickets sold through our website. Please note that delivery prices and certain other service fees associated with a ticket order will not be refunded. Please see our Terms and Conditions for more details.</p>
            <p><b>I bought a ticket yesterday. I used my friend's credit card to buy the tickets and used his name instead of mine, but he will not be coming to the show. Can you please change it to my name?</b></br>
              We are sorry, but we cannot make this kind of change once a transaction has been processed. We urge you to provide correct and accurate information while buying tickets on EventCombo.com. We suggest that you contact the organizer of the event regarding this matter. Contact details are available on the event page for the specific event. We also do our best to help you when this happens.</p>
            <p><b>I bought a ticket yesterday. But I don't think I can make it to the show. How do I cancel my order? OR I bought a ticket for an event but I am not sure whether I will be able to make it to the event. Will I get a refund? Can I give this ticket to my family member or friend who is interested in attending the event?</b></br>
              All tickets are non-refundable and non-transferable. Unfortunately, EventCombo.com is not authorized to refund any customers without permission and approval from the event organizer. Please contact the event organizer regarding this matter. Contact details are available on the event page for the specific event. If you still wish to have us help you, please do <a target="_blank" href="https://www.ufundoo.com/callUs.php">contact us</a> directly after having already contacted the event organizer. </p>
            <p><b>How are cancellations, postponements and artist venue changes handled?</b></br>
              EventCombo.com sells tickets on behalf of the event organizer. Unfortunately, EventCombo.com is not authorized to refund any customers without permission and approval from the event organizer. Once we get the request/authorization from the organizer to process a refund, customers expected to attend the organizer's event will be given an immediate refund. Customers should expect the changes in their credit card statements within 4 to 5 business days from the refunded date. </p>
            <p> <b>I've been charged more than once! I could not place an order, but my Credit Card still shows a charge, how come?</b></br>
              If you did not receive an Order # or confirmation of any type for your purchase then your order did not go through. Most Credit Card companies will still place a hold on your account in the amount of the transaction you attempted to purchase even if that purchase was unsuccessful for security purposes. If you could not place a successful order for tickets but you still see a charge on your Credit Card as pending, please give it 5-7 business days to clear completely. You will know that this is not a permanent charge because it will appear as a PENDING transaction. If it still does not clear, please <a target="_blank" href="https://www.ufundoo.com/callUs.php">contact us</a>.
              Additional Information: When using a debit card, please be aware that your financial institution may place a hold on your account REGARDLESS OF YOUR ORDER GOING THROUGH OR NOT. EventCombo.com is not responsible for placing or removing debit holds. Please contact your financial institution if you would like more information. </p>
            <p><b>I read all of the FAQ's about purchasing tickets on-line, but I still need some help. Who do I contact?</b></br>
              EventCombo.com Customer Care is available to assist you Monday-Friday 10:00AM to 7:00PM EST. You can reach us by Clicking here.</p>
          </div>
          <h3>Seating for Concerts, how does it work?</h3>
          <div class="description">
            <p>You have purchased "Will Call" tickets, as is stated on your E-Ticket Confirmation/Receipt. </p>
            <p>If there is a mailing option available and you elected to receive the tickets via mail, then you will receive the physical tickets at your billing address. </p>
            <p><b>If there was NO mailing option and/or you did NOT elect the mailing option, then this means your tickets and SEATS WILL BE ASSIGNED BY THE EVENT ORGANIZER and made available to you on the day of the show for pick up once you present your E-Ticket & Photo ID at the event box office/will call booth.</b></p>
            <p>EventCombo.com sells "Will Call" tickets because they are the most affordable method of buying tickets for you, our customer. </p>
            <p>When you buy tickets from an outlet such as Ticketmaster, where you can choose your seating at the time of purchase, you pay a heavy premium cost for that. </p>
            <p>When you buy tickets from us, you are guaranteed your attendance to the event at the level that you purchased. </p>
            <p>Seating is assigned by the event organizer on a first come, first serve basis (from the time of your purchase). This is the fairest and cheapest way to buy tickets and we are very proud to be able to deliver this service to you at the very lowest cost. </p>
            <p><b>I read all of the FAQ's about purchasing tickets on-line, but I still need some help. Who do I contact?</b><br/>
              EventCombo.com Customer Care is available to assist you Monday-Friday 10:00AM to 7:00PM EST. You can reach us by <a target="_blank" href="https://www.ufundoo.com/callUs.php">Clicking here</a>.</p>
          </div>
        </div>
      </div>
    </div>
    <!-- end here --> 
  </div>
  <!-- end here --> 
  
  <!-- footer -->
  <div class="footer">
    <?php include('include/footer.php'); ?>
  </div>
  <!-- end here --> 
</div>
<!-- end here -->
</body>
</html>